if [ $#  -ge  1 ];
then
    echo "suppression ancien WAR"
    rm ../webapps/ASHCompta.war
    echo "deploiement du script V"$1".sql"
    sed -i "s/%AppName%/ASH/" /home/pimouss/Documents/1/Workspace/MEP/V"$1".sql
    mysql -u<user> -p<password> < /home/pimouss/Documents/1/Workspace/MEP/V"$1".sql
    sed -i "s/ASH/%AppName%/" /home/pimouss/Documents/1/Workspace/MEP/V"$1".sql
    sleep 30
    echo "deploiement de FFSS-Compta-"$1
    cp /home/pimouss/Documents/1/Workspace/MEP/FFSS-Compta-"$1".war ../webapps/ASHCompta.war
    sleep 30
    echo "deploiement de l'instance"
    sed -i "s/%Title%/ASH - Association de Secouristes Herblinois/" ../webapps/ASHCompta/ffss.html
    sed -i "s/%AppName%/ASH-Compta/" ../webapps/ASHCompta/WEB-INF/classes/ffss.properties
    touch ../webapps/ASHCompta/WEB-INF/web.xml
else
    echo "version manquante"
fi
