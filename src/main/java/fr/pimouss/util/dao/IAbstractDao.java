package fr.pimouss.util.dao;

import java.util.List;

import fr.pimouss.land.shared.dao.AbstractDo;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface IAbstractDao<D extends AbstractDo> {
	D save(D obj) throws DaoException;
	List<D> save(List<D> listeObj) throws DaoException;
	D getById(Long id) throws DaoException;
	List<D> getAll() throws DaoException;
	Long countAll() throws DaoException;
	void delete(D obj) throws DaoException;
	void deleteById(Long id) throws DaoException;
	boolean ping() throws DaoException;
	List<D> getAllSorted() throws DaoException;
}
