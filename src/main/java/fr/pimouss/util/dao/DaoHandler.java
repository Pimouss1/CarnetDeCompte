package fr.pimouss.util.dao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.List;

import fr.pimouss.land.shared.dao.AbstractDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DaoHandler<C> implements InvocationHandler{
	private static DecimalFormat df=new DecimalFormat("#,##0");
	private Class<C> classDao;
	private C dao;

	/*-------------------------------------------------------------------------*/
	public DaoHandler(Class<C> classDao) throws DaoException {
		this.classDao=classDao;
		try {
			dao=classDao.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		try{
			long t0=System.currentTimeMillis();
			Method m;
			if(args!=null){
				@SuppressWarnings("rawtypes")
				Class[] classArg=new Class[args.length];
				for(int i=0;i<args.length;i++){
					if("save".equals(method.getName()) || "delete".equals(method.getName())){
						if(args[i] instanceof AbstractDo){
							classArg[i]=AbstractDo.class;
						}else if(args[i] instanceof List){
							classArg[i]=List.class;
						}else{
							classArg[i]=args[i].getClass().getSuperclass();
						}
					}else{
						classArg[i]=args[i].getClass();
					}
				}
				m=classDao.getMethod(method.getName(),classArg);
			}else{
				m=classDao.getMethod(method.getName());
			}
			Object result=m.invoke(dao,args);
			long t=System.currentTimeMillis()-t0;
			if(t>100){
				Loggeur.timer("Dao     - "+classDao.getSimpleName()+"."+method.getName()+" => "+df.format(t)+"ms");
			}else if(Loggeur.isDebugEnabled()){
				Loggeur.debug("Dao     - "+classDao.getSimpleName()+"."+method.getName()+" => "+df.format(t)+"ms");
			}
			return result;
		}catch(InvocationTargetException e){
			Loggeur.error(e.getTargetException());
			throw e.getTargetException();
		}catch (Exception e) {
			Loggeur.error(e);
			throw e;
		}
	}

	/*-------------------------------------------------------------------------*/
}
