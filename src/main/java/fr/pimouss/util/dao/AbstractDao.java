package fr.pimouss.util.dao;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.CommunicationsException;

import fr.pimouss.land.shared.dao.AbstractDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.log.Loggeur;

/**
 *
 * @author Rodolphe BARBARIT
 * @version 2.2.1
 *
 * Nouveauté dans la version 2.2 : Ajout d'un cache
 * 
 * règles pour le bon fonctionnement de cette classe :
 * 1-les noms des attributs des classes doivent être les mêmes que le noms des champs dans la base de données
 * 2-un "rowid" de type long est obligatoire dans chaque table
 * 
 */
public abstract class AbstractDao<D extends AbstractDo> implements IAbstractDao<D> {
	private static final String SELECT="SELECT * FROM ";
	private static final String DELETE="DELETE FROM ";
	private static final String WHERE=" WHERE ";
	private static final String WHERE_ROWID=" WHERE rowid=";
	private static final String CLASS="class";
	private static final String MAPFIELD="mapField";
	private static final DateFormat FORMAT_DATE=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static Connection connection;
	private Class<D> classBO;
	private String tableName;
	private Map<Long, D> cache;

	/*-------------------------------------------------------------------------*/
	protected AbstractDao(Class<D> classBO, String tableName) {
		this.classBO=classBO;
		this.tableName=tableName;
		this.cache=new HashMap<>();
	}

	/*-------------------------------------------------------------------------*/
	private static synchronized Connection connect() throws DaoException{
		try {
			Class.forName(ConfEnv.getJdbcDriver());
			return DriverManager.getConnection(ConfEnv.getJdbcUrl(),ConfEnv.getJdbcUser(),ConfEnv.getJdbcPassword());
		} catch (ClassNotFoundException | IOException | SQLException e) {
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	protected static synchronized ResultSet executeQuery(String sqlQuery) throws DaoException{
		try{
			if(sqlQuery.toUpperCase().contains("UPDATE") || sqlQuery.toUpperCase().contains("INSERT") || sqlQuery.toUpperCase().contains("DELETE")){
				Loggeur.security("Instruction interdite dans la requête : "+sqlQuery);
				throw new DaoException("Instruction interdite dans un select", null);
			}
			if (connection == null || connection.isClosed()) {
				connection = connect();
			}
			Loggeur.sql(sqlQuery);
			return connection.createStatement().executeQuery(sqlQuery);
		}catch(CommunicationsException e1){
			try{
				// trace non bloquante du problème de connection
				Loggeur.error(e1.getMessage());
				connection=connect();
				return connection.createStatement().executeQuery(sqlQuery);
			}catch(SQLException e2){throw new DaoException(e2.getMessage(),e2);}
		}catch(SQLException e1){throw new DaoException(e1.getMessage(),e1);}
	}

	/*-------------------------------------------------------------------------*/
	protected static synchronized void executeUpdate(String sqlQuery) throws DaoException{
		try{
			if (connection == null || connection.isClosed()) {
				connection = connect();
			}
			Loggeur.sql(sqlQuery);
			connection.createStatement().executeUpdate(sqlQuery);
		}catch(CommunicationsException e1){
			try{
				// trace non bloquante du problème de connection
				Loggeur.error(e1.getMessage());
				connection=connect();
				connection.createStatement().executeUpdate(sqlQuery);
			}catch(SQLException e2){throw new DaoException(e2.getMessage(),e2);}
		}catch(SQLException e1){throw new DaoException(e1.getMessage(),e1);}
	}

	/*-------------------------------------------------------------------------*/
	private static synchronized void mapping(Object obj,ResultSet rs) throws DaoException{
		try {
			BeanInfo bi=Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] pd=bi.getPropertyDescriptors();
			for(int i=0;i<pd.length;i++){
				if(!CLASS.equals(pd[i].getName()) && !MAPFIELD.equals(pd[i].getName())){
					Object arg[]=new Object[1];
					arg[0]=rs.getObject(pd[i].getName());
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==String.class)
						arg[0]="";
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==Float.TYPE)
						arg[0]=0;
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==Double.TYPE)
						arg[0]=0;
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==Integer.TYPE)
						arg[0]=0;
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==Long.TYPE)
						arg[0]=0;
					if(arg[0]==null && pd[i].getReadMethod().getReturnType()==Boolean.TYPE)
						arg[0]=false;
					if(arg[0] instanceof BigInteger && pd[i].getReadMethod().getReturnType()==Long.TYPE)
						arg[0]=((BigInteger)arg[0]).longValue();
					try{
						pd[i].getWriteMethod().invoke(obj,arg);
					}catch(IllegalArgumentException iae){
						try{
							throw new DaoException(iae.getMessage()+" / "+pd[i].getName()+"="+arg[0]+" ("+arg[0].getClass()+")",iae);
						}catch(Exception e){
							throw new DaoException(e.getMessage(),e);
						}
					}
				}
			}
		} catch (IllegalAccessException | InvocationTargetException | SQLException | IntrospectionException e) {
			Loggeur.error(e.getMessage());
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	private static synchronized void update(String tableName, AbstractDo obj) throws DaoException{
		try{
			BeanInfo bi=Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] pd=bi.getPropertyDescriptors();
			StringBuilder sqlQuery=new StringBuilder("UPDATE "+tableName+" SET ");
			boolean firstCol=true;
			for(int i=0;i<pd.length;i++){
				if(!CLASS.equals(pd[i].getName()) && !"rowid".equals(pd[i].getName()) && !MAPFIELD.equals(pd[i].getName())){
					if(firstCol){
						firstCol=false;
					}else{
						sqlQuery.append(", ");
					}
					Object arg[]={};
					if(pd[i].getReadMethod().getReturnType()==String.class){
						String str=((String)pd[i].getReadMethod().invoke(obj,arg));
						if(str==null || "null".equals(str)){str="";}
						if(str!=null){str=str.replace("'","''");}
						sqlQuery.append(pd[i].getName()+"='"+str+"'");
					}else if(pd[i].getReadMethod().getReturnType()==Date.class){
						Date date=(Date)pd[i].getReadMethod().invoke(obj,arg);
						if(date==null){
							sqlQuery.append(pd[i].getName()+"="+null);
						}else{
							sqlQuery.append(pd[i].getName()+"='"+FORMAT_DATE.format(date)+"'");
						}
					}else{
						sqlQuery.append(pd[i].getName()+"="+pd[i].getReadMethod().invoke(obj,arg));
					}
				}
			}
			sqlQuery.append(WHERE_ROWID+obj.getRowid());
			executeUpdate(sqlQuery.toString());
		} catch (IllegalAccessException | InvocationTargetException | IntrospectionException e) {
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	private static synchronized void insert(String tableName, AbstractDo obj) throws DaoException{
		try{
			BeanInfo bi=Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] pd=bi.getPropertyDescriptors();
			StringBuilder sqlUpdate=new StringBuilder("INSERT INTO "+tableName+"(");
			for(int i=0;i<pd.length;i++){
				if(!pd[i].getName().equals(CLASS) && !pd[i].getName().equals("rowid") && !pd[i].getName().equals(MAPFIELD)){
					sqlUpdate.append(pd[i].getName()+",");
				}
			}
			sqlUpdate=sqlUpdate.deleteCharAt(sqlUpdate.length()-1).append(") VALUES (");
			for(int i=0;i<pd.length;i++){
				if(!pd[i].getName().equals(CLASS) && !pd[i].getName().equals("rowid") && !pd[i].getName().equals(MAPFIELD)){
					if(pd[i].getReadMethod().getReturnType()==String.class){
						String str=((String)pd[i].getReadMethod().invoke(obj,new Object[]{}));
						if(str==null || "null".equals(str)){str="";}
						if(str!=null){str=str.replace("'","''");}
						sqlUpdate.append("'"+str+"',");
					}else if(pd[i].getReadMethod().getReturnType()==Date.class){
						Date date=(Date)pd[i].getReadMethod().invoke(obj,new Object[]{});
						if(date==null){
							sqlUpdate.append(null+",");
						}else{
							sqlUpdate.append("'"+FORMAT_DATE.format(date)+"',");
						}
					}else{
						sqlUpdate.append(pd[i].getReadMethod().invoke(obj,new Object[]{})+",");
					}
				}
			}
			sqlUpdate=sqlUpdate.deleteCharAt(sqlUpdate.length()-1).append(")");
			executeUpdate(sqlUpdate.toString());
		} catch (IllegalAccessException | InvocationTargetException | IntrospectionException e) {
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized List<D> getList(String sqlQuery) throws DaoException{
		try{
			List<D> lr=new ArrayList<>();
			ResultSet rs=executeQuery(sqlQuery);
			while(rs.next()){
				D obj=classBO.newInstance();
				mapping(obj,rs);
				lr.add(obj);
				cache.put(obj.getRowid(),obj);
			}
			return lr;
		} catch (InstantiationException | IllegalAccessException | SQLException e) {
			throw new DaoException(e.getMessage(),e);
		} catch (DaoException e) {
			throw new DaoException(sqlQuery+" / "+e.getMessage(),(Exception)e.getCause());
		}
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized List<D> getListWithCriteria(String criteria) throws DaoException{
		return getList(SELECT+tableName+WHERE+criteria);
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized D getObject(String sqlQuery) throws DaoException{
		try{
			D obj=null;
			ResultSet rs=executeQuery(sqlQuery);
			if(rs.next()){
				obj=classBO.newInstance();
				mapping(obj,rs);
				cache.put(obj.getRowid(),obj);
			}
			return obj;
		} catch (InstantiationException | IllegalAccessException | SQLException e) {
			throw new DaoException(e.getMessage(),e);
		} catch (DaoException e) {
			throw new DaoException(sqlQuery+" / "+e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized D getObjectWithCriteria(String criteria) throws DaoException{
		return getObject(SELECT+tableName+WHERE+criteria);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized D save(D obj) throws DaoException{
		if(obj.getRowid()==0){
			insert(tableName,obj);
			return getObject(SELECT+tableName+" t1 WHERE t1.rowid=(SELECT MAX(t2.rowid) FROM "+tableName+" t2)");
		}else{
			update(tableName,obj);
			cache.put(obj.getRowid(),obj);
			return obj;
		}
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> save(List<D> listeObj) throws DaoException{
		List<D> result=new ArrayList<>();
		if(listeObj!=null){
			for(D elt:listeObj){
				result.add(save(elt));
			}
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized D getById(Long id) throws DaoException{
		if(cache.containsKey(id)){
			return cache.get(id);
		}
		return getObject(SELECT+tableName+WHERE_ROWID+id);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> getAllSorted() throws DaoException{
		List<D> result=getAll();
		Collections.sort(result);
		return result;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> getAll() throws DaoException{
		if(cache.size()==countAll()){
			return new ArrayList<>(cache.values());
		}
		return getList(SELECT+tableName);
	}

	/*-------------------------------------------------------------------------*/
	@SuppressWarnings("unchecked")
	protected synchronized <T> List<T> getListOneObject(String sqlQuery) throws DaoException{
		try{
			List<T> lr=new ArrayList<>();
			ResultSet rs=executeQuery(sqlQuery);
			while(rs.next()){
				Object obj=rs.getObject(1);
				if(obj instanceof BigInteger){
					obj=((BigInteger)obj).longValue();
				}
				lr.add((T)obj);
			}
			return lr;
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(),e);
		} catch (DaoException e) {
			throw new DaoException(sqlQuery+" / "+e.getMessage(),(Exception)e.getCause());
		}
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized List<Long> getListOneLong(String sqlQuery) throws DaoException{
		return getListOneObject(sqlQuery);
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized Object getOneObject(String sqlQuery) throws DaoException{
		ResultSet rs=executeQuery(sqlQuery);
		try{
			if(rs.next()){
				return rs.getObject(1);
			}
			return null;
		}catch(SQLException e){
			throw new DaoException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized Long countAll() throws DaoException{
		return (Long)getOneObject("SELECT COUNT(1) FROM "+tableName);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized void delete(D obj) throws DaoException{
		executeUpdate(DELETE+tableName+WHERE_ROWID+obj.getRowid());
		cache.remove(obj.getRowid());
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized void deleteById(Long id) throws DaoException{
		executeUpdate(DELETE+tableName+WHERE_ROWID+id);
		cache.remove(id);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized boolean ping() throws DaoException{
		ResultSet rs=executeQuery("SELECT 1 FROM "+tableName);
		try{
			if(rs.next()){
				if(rs.getInt(1)==1){
					return true;
				}
				return false;
			}
			return false;
		}catch(SQLException e){
			throw new DaoException("Impossible de pinger la table "+tableName,e);
		}
	}

	/*-------------------------------------------------------------------------*/
}
