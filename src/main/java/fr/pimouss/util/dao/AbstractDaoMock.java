package fr.pimouss.util.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.pimouss.land.shared.dao.AbstractDo;
import fr.pimouss.land.shared.exception.DaoException;

/**
 *
 * @author Rodolphe BARBARIT
 * @version 2.2.1
 *
 * Mock de AbstractDao
 * 
 */
public abstract class AbstractDaoMock<D extends AbstractDo> implements IAbstractDao<D> {
	private List<D> data;
	private Map<Long, D> cache;

	/*-------------------------------------------------------------------------*/
	protected AbstractDaoMock() {
		this.data=new ArrayList<>();
		this.cache=new HashMap<>();
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized D save(D obj) throws DaoException{
		if(obj.getRowid()==0) {
			long maxRowid=0;
			for(D elt:data) {
				if(elt.getRowid()>maxRowid) {
					maxRowid=elt.getRowid();
				}
			}
			obj.setRowid(maxRowid+1);
		}
		D eltToRemove=null;
		for(D elt:data) {
			if(elt.getRowid()==obj.getRowid()) {
				eltToRemove=elt;
			}
		}
		if(eltToRemove!=null) {
			data.remove(eltToRemove);
		}
		data.add(obj);
		return obj;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> save(List<D> listeObj) throws DaoException{
		List<D> result=new ArrayList<>();
		if(listeObj!=null){
			for(D elt:listeObj){
				result.add(save(elt));
			}
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized D getById(Long id) throws DaoException{
		if(cache.containsKey(id)){
			return cache.get(id);
		}
		for(D elt:data) {
			if(elt.getRowid()==id) {
				return elt;
			}
		}
		return null;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> getAllSorted() throws DaoException{
		List<D> result=getAll();
		Collections.sort(result);
		return result;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized List<D> getAll() throws DaoException{
		return data;
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized void delete(D obj) throws DaoException{
		data.remove(obj);
		cache.remove(obj.getRowid());
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized void deleteById(Long id) throws DaoException{
		D eltToRemove=null;
		for(D elt:data) {
			if(elt.getRowid()==id) {
				eltToRemove=elt;
			}
		}
		if(eltToRemove!=null) {
			data.remove(eltToRemove);
		}
		cache.remove(id);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public Long countAll() throws DaoException {
		return new Long(data.size());
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public synchronized boolean ping() throws DaoException{
		return true;
	}

	/*-------------------------------------------------------------------------*/
	protected interface Condition<D> {
		boolean condition(D elt);
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized List<D> getList(Condition<D> condition) throws DaoException{
		List<D> result=new ArrayList<>();
		for(D elt:getAll()) {
			if(condition.condition(elt)) {
				result.add(elt);
			}
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
	protected synchronized D getObject(Condition<D> condition) throws DaoException{
		for(D elt:getAll()) {
			if(condition.condition(elt)) {
				return elt;
			}
		}
		return null;
	}

	/*-------------------------------------------------------------------------*/
}
