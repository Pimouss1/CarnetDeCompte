package fr.pimouss.util.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilDate {
	public static final DateFormat FORMAT_DATE_SYS=new SimpleDateFormat("yyyyMMdd");
	public static final DateFormat FORMAT_DATE_SYS_COMP=new SimpleDateFormat("yyyyMMdd-HHmmss");
	public static final DateFormat FORMAT_DATE_FR=new SimpleDateFormat("dd/MM/yyyy");
	public static final DateFormat FORMAT_DATE_FR_DDMM=new SimpleDateFormat("dd/MM");
	public static final DateFormat FORMAT_DATE_FR_MMMYYYY=new SimpleDateFormat("MMM yyyy");
	public static final DateFormat FORMAT_DATE_FR_MMMMYYYY=new SimpleDateFormat("MMMM yyyy");
	public static final DateFormat FORMAT_DATE_FR_MMMM=new SimpleDateFormat("MMMM");
	public static final DateFormat FORMAT_DATE_FR_YYYY=new SimpleDateFormat("yyyy");
	public static final DateFormat FORMAT_DATE_FR_COMP=new SimpleDateFormat("dd/MM/yyyy HH:mm");
	public static final DateFormat FORMAT_DATE_FR_COMP2=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static final DateFormat FORMAT_DATE_FR_LONG=new SimpleDateFormat("dd MMM yyyy HH:mm");
	public static final DateFormat FORMAT_DATE_EN=new SimpleDateFormat("yyyy-MM-dd");
	public static final DateFormat FORMAT_DATE_EN_COMP=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final DateFormat FORMAT_DATE_GOOGLE=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static final long UNE_SECONDE=TimeUnit.MILLISECONDS.convert(1, TimeUnit.SECONDS);
	public static final long UNE_MINUTE=TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
	public static final long UNE_HEURE=TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS);
	public static final long UNE_JOURNEE=TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);

	public static final int[] NUM_DAY_OF_WEEK=new int[]{
		DateTimeConstants.MONDAY,
		DateTimeConstants.TUESDAY,
		DateTimeConstants.WEDNESDAY,
		DateTimeConstants.THURSDAY,
		DateTimeConstants.FRIDAY,
		DateTimeConstants.SATURDAY,
		DateTimeConstants.SUNDAY,
	};

	private static Map<Integer,String> mapInitialDayOfWeek;

	/*-------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private UtilDate() {}

	/*-------------------------------------------------------------------------*/
	public static String getInitialDayOfWeek(int numDayOfWeek){
		if(mapInitialDayOfWeek==null){
			mapInitialDayOfWeek=new HashMap<Integer, String>();
			mapInitialDayOfWeek.put(DateTimeConstants.MONDAY,"L");
			mapInitialDayOfWeek.put(DateTimeConstants.TUESDAY,"M");
			mapInitialDayOfWeek.put(DateTimeConstants.WEDNESDAY,"M");
			mapInitialDayOfWeek.put(DateTimeConstants.THURSDAY,"J");
			mapInitialDayOfWeek.put(DateTimeConstants.FRIDAY,"V");
			mapInitialDayOfWeek.put(DateTimeConstants.SATURDAY,"S");
			mapInitialDayOfWeek.put(DateTimeConstants.SUNDAY,"D");
		}
		return mapInitialDayOfWeek.get(numDayOfWeek);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isNumDayOfMonth(DateTime date, int numDay, int day){
		if(day==0){
			return true;
		}
		if(numDay==0){
			return isDay(date,day);
		}
		DateTime d=getFirstDateOfMonth(date);
		DateTime end=getLastDateOfMonth(date);
		int num=0;
		while(d.isBefore(end)){
			if(isDay(d,day)){
				num++;
				if(num==numDay && isDateEqual(date,d)){
					return true;
				}
			}
			d=d.plusDays(1);
		}
		return false;
	}
	
	/*-------------------------------------------------------------------------*/
	public static Date getLastSundayOfMonth(Date date){
		return getLastSundayOfMonth(new DateTime(date)).toDate();
	}
	public static DateTime getLastSundayOfMonth(DateTime date){
		date=getLastDateOfMonth(date);
		while(!isSunday(date)){
			date=date.minusDays(1);
		}
		return date;
	}

	/*-------------------------------------------------------------------------*/
	public static Date getNextSunday(Date date){
		return getNextSunday(new DateTime(date)).toDate();
	}
	public static DateTime getNextSunday(DateTime date){
		while(!isSunday(date)){
			date=date.plusDays(1);
		}
		return date;
	}

	/*-------------------------------------------------------------------------*/
	public static long getTimeOfDay(DateTime date){
		return getTimeOfDay(date.getMillis());
	}
	public static long getTimeOfDay(long date){
		return date-getTimeBeginDay(new DateTime(date));
	}
	public static long getTimeOfDay(Date date){
		return getTimeOfDay(date.getTime());
	}

	/*-------------------------------------------------------------------------*/
	public static long getTimeBeginDay(DateTime date){
		return setTimeMinuit(date).getMillis();
	}
	public static long getTimeBeginDay(long date){
		return setTimeMinuit(new DateTime(date)).getMillis();
	}
	public static long getTimeBeginDay(Date date){
		return setTimeMinuit(date).getTime();
	}

	/*-------------------------------------------------------------------------*/
	public static Date getLastDateOfMonth(Date date){
		return getLastDateOfMonth(new DateTime(date)).toDate();
	}
	public static DateTime getLastDateOfMonth(DateTime date){
		return date.plusMonths(1).dayOfMonth().setCopy(1).minusDays(1);
	}

	/*-------------------------------------------------------------------------*/
	public static Date getFirstDateOfMonth(Date date){
		return getFirstDateOfMonth(new DateTime(date)).toDate();
	}
	public static DateTime getFirstDateOfMonth(DateTime date){
		return date.dayOfMonth().setCopy(1);
	}

	/*-------------------------------------------------------------------------*/
	public static Long getFirstDateOfWeek(Long date){
		return getFirstDateOfWeek(new DateTime(date)).getMillis();
	}
	public static Date getFirstDateOfWeek(Date date){
		return getFirstDateOfWeek(new DateTime(date)).toDate();
	}
	public static DateTime getFirstDateOfWeek(DateTime date){
		return date.dayOfWeek().setCopy(DateTimeConstants.MONDAY).millisOfDay().setCopy(0);
	}

	/*-------------------------------------------------------------------------*/
	public static Date getLastDayOfWeek(Date date){
		return getLastDayOfWeek(new DateTime(date)).toDate();
	}
	public static DateTime getLastDayOfWeek(DateTime date){
		return date.dayOfWeek().setCopy(DateTimeConstants.SUNDAY);
	}

	/*-------------------------------------------------------------------------*/
	public static Long[] getAllDateOfWeek(long date){
		DateTime[] tmp=getAllDateOfWeek(new DateTime(date));
		Long[] result=new Long[8];
		for(int i=DateTimeConstants.MONDAY;
				i<=DateTimeConstants.SUNDAY;i++){
			result[i]=tmp[i].getMillis();
		}
		return result;
	}
	public static Date[] getAllDateOfWeek(Date date){
		DateTime[] tmp=getAllDateOfWeek(new DateTime(date));
		Date[] result=new Date[8];
		for(int i=DateTimeConstants.MONDAY;
				i<=DateTimeConstants.SUNDAY;i++){
			result[i]=tmp[i].toDate();
		}
		return result;
	}
	public static DateTime[] getAllDateOfWeek(DateTime date){
		DateTime[] result=new DateTime[8];
		result[DateTimeConstants.MONDAY]=getFirstDateOfWeek(date);
		result[DateTimeConstants.TUESDAY]=getFirstDateOfWeek(date).plusDays(1);
		result[DateTimeConstants.WEDNESDAY]=getFirstDateOfWeek(date).plusDays(2);
		result[DateTimeConstants.THURSDAY]=getFirstDateOfWeek(date).plusDays(3);
		result[DateTimeConstants.FRIDAY]=getFirstDateOfWeek(date).plusDays(4);
		result[DateTimeConstants.SATURDAY]=getFirstDateOfWeek(date).plusDays(5);
		result[DateTimeConstants.SUNDAY]=getFirstDateOfWeek(date).plusDays(6);
		return result;
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isDateEqual(Date date1, Date date2){
		return isDateEqual(new DateTime(date1), new DateTime(date2));
	}
	public static boolean isDateEqual(DateTime date1, Date date2){
		return isDateEqual(date1, new DateTime(date2));
	}
	public static boolean isDateEqual(DateTime date1, DateTime date2){
		return date1.getYear()==date2.getYear()
			   && date1.getMonthOfYear()==date2.getMonthOfYear()
			   && date1.getDayOfMonth()==date2.getDayOfMonth();
	}

	/*-------------------------------------------------------------------------*/
	public static long setTimeMinuit(long date){
		return setTimeMinuit(new DateTime(date)).getMillis();
	}
	public static Date setTimeMinuit(Date date){
		return setTimeMinuit(new DateTime(date)).toDate();
	}
	public static DateTime setTimeMinuit(DateTime date){
		return date.millisOfDay().setCopy(0);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isSaturday(Date date){
		return isSaturday(new DateTime(date));
	}
	public static boolean isSaturday(DateTime date){
		return(date.dayOfWeek().get()==DateTimeConstants.SATURDAY);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isSunday(Date date){
		return isSunday(new DateTime(date));
	}
	public static boolean isSunday(DateTime date){
		return(date.dayOfWeek().get()==DateTimeConstants.SUNDAY);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isToday(long date){
		return getTimeBeginDay(System.currentTimeMillis())<=date && date<getTimeBeginDay(new DateTime(System.currentTimeMillis()).plusDays(1));
	}
	public static boolean isToday(DateTime date){
		return isDateEqual(date, new DateTime());
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isDay(Date date, int day){
		return isDay(new DateTime(date),day);
	}
	public static boolean isDay(DateTime date, int day){
		return(date.dayOfWeek().get()==day);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isFerie(long date){
		return isFerie(new DateTime(date));
	}
	public static boolean isFerie(Date date){
		return isFerie(new DateTime(date));
	}
	public static boolean isFerie(DateTime date){
		int annee=date.getYear();
	    int a = annee%19;
	    int b = annee/100;
	    int c = annee%100;
	    int d = b/4;
	    int e = b%4;
	    int f = (b+8)/25;
	    int g = (b-f+1)/3;
	    int h = (19*a+b-d-g+15)%30;
	    int i = c/4;
	    int k = c%4;
	    int l = (32+2*e+2*i-h-k)%7;
	    int m = (a+11*h+22*l)/451;
	    int n = (h+l-7*m+114)/31;
	    int p = (h+l-7*m+114)%31;

	    DateTime jourFerie;

	    // jour de l'an
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.JANUARY).dayOfMonth().setCopy(1);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // dimanche de paques
	    jourFerie=date.monthOfYear().setCopy(n).dayOfMonth().setCopy(p+1);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // lundi de paques
	    jourFerie=jourFerie.plusDays(1);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // jeudi de l'ascension
	    jourFerie=jourFerie.plusDays(38);
	    if(isDateEqual(date, jourFerie)){return true;}
	    
	    // lundi de pentecote
	    jourFerie=jourFerie.plusDays(11);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 1er mai
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.MAY).dayOfMonth().setCopy(1);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 8 mai
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.MAY).dayOfMonth().setCopy(8);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 14 juillet
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.JULY).dayOfMonth().setCopy(14);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 15 aout assomption
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.AUGUST).dayOfMonth().setCopy(15);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 1er novembre toussaint
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.NOVEMBER).dayOfMonth().setCopy(1);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 11 novembre
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.NOVEMBER).dayOfMonth().setCopy(11);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // 25 decembre noel
	    jourFerie=date.monthOfYear().setCopy(DateTimeConstants.DECEMBER).dayOfMonth().setCopy(25);
	    if(isDateEqual(date, jourFerie)){return true;}

	    // on n'a trouvé aucun jour férié
	    return false;
	}

	/*-------------------------------------------------------------------------*/
	public static List<DateTime> getListeFerie(int annee){
	    int a = annee%19;
	    int b = annee/100;
	    int c = annee%100;
	    int d = b/4;
	    int e = b%4;
	    int f = (b+8)/25;
	    int g = (b-f+1)/3;
	    int h = (19*a+b-d-g+15)%30;
	    int i = c/4;
	    int k = c%4;
	    int l = (32+2*e+2*i-h-k)%7;
	    int m = (a+11*h+22*l)/451;
	    int n = (h+l-7*m+114)/31;
	    int p = (h+l-7*m+114)%31;

	    DateTime date=new DateTime().year().setCopy(annee);
	    List<DateTime> result=new ArrayList<DateTime>();

	    // jour de l'an
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.JANUARY).dayOfMonth().setCopy(1));

	    // dimanche de paques
	    DateTime paque=date.monthOfYear().setCopy(n).dayOfMonth().setCopy(p+1);
	    result.add(paque);

	    // lundi de paques
	    result.add(paque.plusDays(1));

	    // jeudi de l'ascension
	    result.add(paque.plusDays(39));
	    
	    // lundi de pentecote
	    result.add(paque.plusDays(50));

	    // 1er mai
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.MAY).dayOfMonth().setCopy(1));

	    // 8 mai
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.MAY).dayOfMonth().setCopy(8));

	    // 14 juillet
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.JULY).dayOfMonth().setCopy(14));

	    // 15 aout assomption
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.AUGUST).dayOfMonth().setCopy(15));

	    // 1er novembre toussaint
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.NOVEMBER).dayOfMonth().setCopy(1));

	    // 11 novembre
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.NOVEMBER).dayOfMonth().setCopy(11));

	    // 25 decembre noel
	    result.add(date.monthOfYear().setCopy(DateTimeConstants.DECEMBER).dayOfMonth().setCopy(25));

	    return result;
	}

	/*-------------------------------------------------------------------------*/
	public static int nbJoursOuvresByYear(DateTime dateDebutAnnee){
		DateTime dateFinAnnee=dateDebutAnnee.plusYears(1);
		int result=0;
		DateTime dateTmp=dateDebutAnnee;
		while(dateTmp.isBefore(dateFinAnnee)){
			if(!isSaturday(dateTmp) && !isSunday(dateTmp) && !isFerie(dateTmp)){result++;}
			dateTmp=dateTmp.plusDays(1);
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
	public static int nbJoursOuvresByMonth(DateTime dateDebutMois){
		DateTime dateFinMois=dateDebutMois.plusMonths(1);
		int result=0;
		DateTime dateTmp=dateDebutMois;
		while(dateTmp.isBefore(dateFinMois)){
			if(!isSaturday(dateTmp) && !isSunday(dateTmp) && !isFerie(dateTmp)){result++;}
			dateTmp=dateTmp.plusDays(1);
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
	public static int getWeekNumber(Date date){
		return (new DateTime(date)).getWeekOfWeekyear();
	}
	public static int getWeekNumber(long date){
		return (new DateTime(date)).getWeekOfWeekyear();
	}

	/*-------------------------------------------------------------------------*/
	public static DateTime jourOuvreSuivant(DateTime date){
		date=date.plusDays(1);
		while(isSaturday(date) || isSunday(date) || isFerie(date)){
			date=date.plusDays(1);
		}
		return date;
	}

	/*-------------------------------------------------------------------------*/
	public static int getHeure(long date){
		return (new DateTime(date)).getHourOfDay();
	}
	public static int getMinute(long date){
		return (new DateTime(date)).getMinuteOfHour();
	}

	/*-------------------------------------------------------------------------*/
}
