package fr.pimouss.util.date;

import org.joda.time.DateTime;

public class DateMain {

	public static void main(String[] args) {
		DateTime dt=new DateTime();
		for(int i=2010;i<2021;i++) {
			dt=dt.dayOfYear().setCopy(1).year().setCopy(i).millisOfDay().setCopy(0);
			System.out.println(UtilDate.FORMAT_DATE_FR_COMP2.format(dt.toDate())+"="+dt.getMillis());
		}
	}

}
