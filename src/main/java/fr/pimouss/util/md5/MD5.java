package fr.pimouss.util.md5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import fr.pimouss.land.shared.exception.UtilException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MD5 {

	/*-------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private MD5() {}

    /*---------------------------------------------------------------------------------------------*/
	public static String encode(String password) throws UtilException {
		byte[] uniqueKey = password.getBytes();
		byte[] hash = null;
		try{
			hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
		}catch (NoSuchAlgorithmException e) {
			throw new UtilException(e.getMessage(),e);
		}
		StringBuilder hashString = new StringBuilder();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			} else
				hashString.append(hex.substring(hex.length() - 2));
		}
		return hashString.toString();
	}

    /*---------------------------------------------------------------------------------------------*/
}
