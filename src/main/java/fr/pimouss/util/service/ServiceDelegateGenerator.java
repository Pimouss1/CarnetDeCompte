package fr.pimouss.util.service;

import java.io.File;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ServiceDelegateGenerator {
	private static String[] tabFolderName=new String[]{
		//"/home/rodolphe/WorkspaceOxygen/CarnetDeCompte/src/main/java/fr/pimouss/land/server/service/itf"
		"D:/WorkspaceOxygen/CarnetDeCompte/src/main/java/fr/pimouss/land/server/service/itf"
	};

	public static void main(String[] args) {
		for(int j=0;j<tabFolderName.length;j++){
			File folder=new File(tabFolderName[j]);
			File[] tabFile=folder.listFiles();
			for(int i=0;i<tabFile.length;i++){
				if(tabFile[i].getName().endsWith(".java")){
					String interfaceName=tabFile[i].getName().substring(0, tabFile[i].getName().indexOf("."));
					String className=interfaceName.substring(1);
					String attributName=className.substring(0,1).toLowerCase()+className.substring(1);
					// 1 
//					System.out.println("\tprivate static "+className+" "+attributName+"=new "+className+"();");
					// 2
					System.out.println("\tprivate static "+interfaceName+" "+attributName+";");
					System.out.println("\tpublic static "+interfaceName+" get"+className+"() {");
					System.out.println("\t\tif("+attributName+"==null){");
					System.out.println("\t\t\ttry{");
					System.out.println("\t\t\t\t"+attributName+"=("+interfaceName+")Proxy.newProxyInstance(");
					System.out.println("\t\t\t\t\t"+interfaceName+".class.getClassLoader(),");
					System.out.println("\t\t\t\t\tnew Class[]{"+interfaceName+".class},");
					System.out.println("\t\t\t\t\tnew ServiceHandler<"+className+">("+className+".class));");
					System.out.println("\t\t\t} catch (IllegalArgumentException | ServiceException e) {");
					System.out.println("\t\t\t\tLoggeur.error(e);");
					System.out.println("\t\t\t}");
					System.out.println("\t\t}");
					System.out.println("\t\treturn "+attributName+";");
					System.out.println("\t}");
					System.out.println();
				}
			}
		}
	}

}
