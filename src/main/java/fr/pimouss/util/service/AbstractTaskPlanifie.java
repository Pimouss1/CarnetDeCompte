package fr.pimouss.util.service;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.joda.time.DateTime;

import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public abstract class AbstractTaskPlanifie extends TimerTask {
	private Timer timer;
	private long dernierPassage=System.currentTimeMillis();

	/*-------------------------------------------------------------------------*/
	protected AbstractTaskPlanifie(int heurePlanifie, int minutePlanifie) {
		DateTime prochainTraitement=((new DateTime()).hourOfDay().setCopy(heurePlanifie).minuteOfHour().setCopy(minutePlanifie).secondOfMinute().setCopy(0));
		if(prochainTraitement.getMillis()<System.currentTimeMillis()){
			prochainTraitement=((new DateTime()).plusDays(1).hourOfDay().setCopy(heurePlanifie).minuteOfHour().setCopy(minutePlanifie).secondOfMinute().setCopy(0));
		}
		Loggeur.debug("prochain traitement de "+getClass().getSimpleName()+" le "+UtilDate.FORMAT_DATE_FR_COMP2.format(prochainTraitement.toDate()));
		timer=new Timer();
		timer.schedule(this,prochainTraitement.toDate(),UtilDate.UNE_JOURNEE);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void run() {
		dernierPassage=System.currentTimeMillis();
		try{
			Loggeur.debug("DEBUT traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			traitement();
			Loggeur.debug("  FIN traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
		}catch(Exception e){
			Loggeur.error("Echec du traitement de "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			Loggeur.error(e);
		}
	}

	/*-------------------------------------------------------------------------*/
	public abstract void traitement() throws Exception;

	/*-------------------------------------------------------------------------*/
	public void stop() {
		cancel();
		timer.cancel();
	}

	/*-------------------------------------------------------------------------*/
	public void force(){
		dernierPassage=System.currentTimeMillis();
		try{
			Loggeur.debug("DEBUT traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			traitement();
			Loggeur.debug("  FIN traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
		}catch(Exception e){
			Loggeur.error("Echec du traitement de "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			Loggeur.error(e);
		}
	}

	/*-------------------------------------------------------------------------*/
	public long getDernierPassage() {
		return dernierPassage;
	}

	/*-------------------------------------------------------------------------*/
}
