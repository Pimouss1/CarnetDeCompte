package fr.pimouss.util.service;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public abstract class AbstractTask extends TimerTask {
	private Timer timer;
	private long dernierPassage=System.currentTimeMillis();

	/*-------------------------------------------------------------------------*/
	protected AbstractTask(long frequence) {
		timer=new Timer();
		timer.schedule(this, frequence, frequence);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void run() {
		dernierPassage=System.currentTimeMillis();
		try{
			Loggeur.debug("DEBUT traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			traitement();
			Loggeur.debug("  FIN traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
		}catch(Exception e){
			Loggeur.error("Echec du traitement de "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			Loggeur.error(e);
		}
	}

	/*-------------------------------------------------------------------------*/
	public abstract void traitement() throws Exception;

	/*-------------------------------------------------------------------------*/
	public void stop() {
		cancel();
		timer.cancel();
	}

	/*-------------------------------------------------------------------------*/
	public void force(){
		dernierPassage=System.currentTimeMillis();
		try{
			Loggeur.debug("DEBUT traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			traitement();
			Loggeur.debug("  FIN traitement "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
		}catch(Exception e){
			Loggeur.error("Echec du traitement de "+this.getClass().getSimpleName()+" à "+UtilDate.FORMAT_DATE_EN_COMP.format(new Date()));
			Loggeur.error(e);
		}
	}

	/*-------------------------------------------------------------------------*/
	public long getDernierPassage() {
		return dernierPassage;
	}

	/*-------------------------------------------------------------------------*/
}
