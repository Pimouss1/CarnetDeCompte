package fr.pimouss.util.service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import fr.pimouss.land.shared.exception.ServiceException;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ServiceHandler<C> implements InvocationHandler{
	private static DecimalFormat df=new DecimalFormat("#,##0");
	private Class<C> classService;
	private C service;

	/*-------------------------------------------------------------------------*/
	public ServiceHandler(Class<C> classService) throws ServiceException {
		this.classService=classService;
		try {
			service=classService.newInstance();
		} catch (InstantiationException e) {
			Loggeur.error(e);
			throw new ServiceException(e.getMessage(),e);
		} catch (IllegalAccessException e) {
			Loggeur.error(e);
			throw new ServiceException(e.getMessage(),e);
		}
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		try{
			long t0=System.currentTimeMillis();
			Method m=null;
			if(args!=null){
				@SuppressWarnings("rawtypes")
				Class[] classArg=null;
				int nbMethode=0;
				for(Method m2:classService.getMethods()){
					if(m2.getName().equals(method.getName())
							&& args.length==m2.getParameterTypes().length){
						nbMethode++;
						m=m2;
						classArg=m2.getParameterTypes();
					}
				}
				if(nbMethode==0){
					throw new NoSuchMethodError("Méthode : "+method.getName()+" introuvable.");
				}
				if(nbMethode>1){
					for(int i=0;i<args.length;i++){
						if(args[i]==null){
							for(Method m2:classService.getMethods()){
								if(m2.getName().equals(method.getName())
										&& m2.getParameterTypes().length==args.length){
									classArg[i]=m2.getParameterTypes()[i];
								}
							}
						}else{
							classArg[i]=args[i].getClass();
						}
					}
					m=classService.getMethod(method.getName(),classArg);
				}
			}else{
				m=classService.getMethod(method.getName());
			}
			Object result=m.invoke(service,args);
			long t=System.currentTimeMillis()-t0;
			if(t>100){
				Loggeur.timer("Service - "+classService.getSimpleName()+"."+method.getName()+" => "+df.format(t)+"ms");
			}else if(Loggeur.isDebugEnabled()){
				Loggeur.debug("Service - "+classService.getSimpleName()+"."+method.getName()+" => "+df.format(t)+"ms");
			}
			return result;
		}catch(InvocationTargetException e){
			if(e.getTargetException() instanceof WarningException){
				Loggeur.warn(e.getTargetException().getMessage());
			}else{
				Loggeur.error(e.getTargetException());
			}
			throw e.getTargetException();
		}catch (Exception e) {
			Loggeur.error(e);
			throw e;
		}
	}

	/*-------------------------------------------------------------------------*/
}
