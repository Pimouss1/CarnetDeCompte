package fr.pimouss.util.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.channels.FileChannel;

import fr.pimouss.land.shared.exception.ErrorException;
import fr.pimouss.land.shared.exception.UtilException;
import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilFile {
	private static final int SIZE_BUFFER=1024;

	/*-------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private UtilFile() {}

	/*---------------------------------------------------------------------------------------------*/
	public static void copyFile(String srcFileName, String destFileName) throws IOException{
		copyFile(new File(srcFileName),new File(destFileName));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void copyFile(File srcFile, String destFileName) throws IOException{
		copyFile(srcFile,new File(destFileName));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void saveRep(String srcDir, String destDir) throws ErrorException, IOException{
		File srcDirFile=new File(srcDir);
		File destDirFile=new File(destDir);
		if(!srcDirFile.isDirectory()){
			throw new ErrorException(srcDir+" n'est pas un répertoire",null);
		}else if(!destDirFile.isDirectory()){
			throw new ErrorException(destDir+" n'est pas un répertoire",null);
		}else{
			destDirFile=new File(destDir+"-"+UtilDate.FORMAT_DATE_EN);
			if(!destDirFile.mkdir()){
				throw new ErrorException("impossible de créer le répertoire "+destDirFile.getAbsolutePath(),null);
			}
			saveContenuRep(srcDirFile,destDirFile);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private static void saveContenuRep(File srcDirFile, File destDirFile) throws IOException{
		String newDestDir=destDirFile.getAbsolutePath()+"/"+srcDirFile.getName();
		File newDestDirFile=new File(newDestDir);
		newDestDirFile.mkdir();

		File[] listeFichier=srcDirFile.listFiles();
		for(int i=0;i<listeFichier.length;i++){
			if(listeFichier[i].isDirectory()){
				saveContenuRep(listeFichier[i],newDestDirFile);
			}else{
				File newDestFile=new File(newDestDirFile.getAbsolutePath()+"/"+listeFichier[i].getName());
				copyFile(listeFichier[i],newDestFile);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void copyFile(File srcfile, File destFile) throws IOException{
		try{
			if(!srcfile.exists()){
				Loggeur.warn("le fichier "+srcfile.getAbsolutePath()+" n'existe pas");
			}else{
				FileInputStream is=new FileInputStream(srcfile);
				FileChannel sourceChannel=is.getChannel();
				FileOutputStream os=new FileOutputStream(destFile);
				FileChannel destChannel=os.getChannel();
				sourceChannel.transferTo(0,sourceChannel.size(),destChannel);
				sourceChannel.close();
				is.close();
				destChannel.close();
				os.close();
			}
		}catch(IOException e){
			Loggeur.warn("Copie du fichier : "+srcfile.getAbsolutePath()+" par le buffer.");
			Reader r=new FileReader(srcfile);
			Writer w=new FileWriter(destFile);
			char[] buffer=new char[SIZE_BUFFER];
			int nbRead;
			while((nbRead=r.read(buffer))!=-1){
				w.write(buffer,0,nbRead);
			}
			w.close();
			r.close();
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void vidangeFolder(String path) throws ErrorException{
		File dir=new File(path);
		File[] tabFile=dir.listFiles();
		for(int i=0;i<tabFile.length;i++){
			if(!tabFile[i].delete()){
				throw new ErrorException("impossible de supprimer le fichier "+tabFile[i].getAbsolutePath(), null);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean deleteFolder(String path) throws UtilException{
		File dir=new File(path);
		if(!dir.isDirectory()){
			throw new UtilException(path+" n'est pas un dossier", null);
		}
		File[] tabFile=dir.listFiles();
		for(int i=0;i<tabFile.length;i++){
			if(tabFile[i].isDirectory()){
				deleteFolder(tabFile[i].getAbsolutePath());
			}else{
				tabFile[i].delete();
			}
		}
		return dir.delete();
	}

	/*---------------------------------------------------------------------------------------------*/
	public static File[] sortByName(File[] tabFile){
		for(int i=0;i<tabFile.length-1;i++){
			for(int j=i+1;j<tabFile.length;j++){
				if(tabFile[i].getName().toLowerCase().compareTo(tabFile[j].getName().toLowerCase())>0){
					File tmp=tabFile[i];
					tabFile[i]=tabFile[j];
					tabFile[j]=tmp;
				}
			}
		}
		return tabFile;
	}

	/*---------------------------------------------------------------------------------------------*/
}
