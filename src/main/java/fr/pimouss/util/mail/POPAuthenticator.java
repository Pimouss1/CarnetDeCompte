package fr.pimouss.util.mail;

import java.io.IOException;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class POPAuthenticator extends Authenticator {

	protected PasswordAuthentication getPasswordAuthentication() {
        try {
			return new PasswordAuthentication(ConfEnv.getMailUserPOPAuthenticator(), ConfEnv.getMailPasswordPOPAuthenticator());
		} catch (IOException e) {
			Loggeur.error(e);
			return null;
		}
    }
}
