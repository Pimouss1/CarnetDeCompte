package fr.pimouss.util.mail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import fr.pimouss.util.cst.ConfEnv;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MailService {
	private String destinataire;
	private String body;
	private String subject;
	private String filenameAttach;
	private String[] filenameImage;

	/*-------------------------------------------------------------------------*/
	public MailService(String destinataire, String body, String subject, String filenameAttach, String[] filenameImage) throws IOException, AddressException, MessagingException {
		if(ConfEnv.getMailRedirect()){
			this.destinataire=ConfEnv.getMailRedirectAddress();
		}else{
			this.destinataire=destinataire;
		}
		this.body=body;
		this.subject=subject;
		this.filenameAttach=filenameAttach;
		this.filenameImage=filenameImage;
		run();
	}

	/*-------------------------------------------------------------------------*/
	public void run() throws FileNotFoundException, IOException, AddressException, MessagingException {
    	//-----création du mail-----
        Message message;
        Session session;
        Properties properties = new Properties();
        properties.put("mail.smtp.host",ConfEnv.getMailSmtpServer());
        properties.put("mail.smtp.port",ConfEnv.getMailSmtpPort());
        properties.put("mail.smtp.socketFactory.port",ConfEnv.getMailSmtpPort());
        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth","true");
        properties.put("mail.pop3.host",ConfEnv.getMailSmtpServer());
        Authenticator authenticator=new POPAuthenticator();
        session=Session.getInstance(properties, authenticator);
        session.setDebug(ConfEnv.getMailLogDebug());
        message=new MimeMessage(session);
        message.setFrom(new InternetAddress(ConfEnv.getMailFromAddress()));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinataire));
        message.setSubject(subject);

        //Multipart mmp=new MimeMultipart("related");
        Multipart multiPart=new MimeMultipart();
        //-----corps du mail-----
        BodyPart messageBodyPart=new MimeBodyPart();
        messageBodyPart.setDisposition(Part.INLINE);
        messageBodyPart.setContent(body,"text/html");
        multiPart.addBodyPart(messageBodyPart);

        //-----images-----
        if(filenameImage!=null){
        	for(int i=0;i<filenameImage.length;i++){
        		DataSource fds=new FileDataSource(filenameImage[i]);
        		BodyPart imageBodyPart=new MimeBodyPart();
        		imageBodyPart.setDataHandler(new DataHandler(fds));
        		imageBodyPart.setHeader("Content-ID","<image"+(i+1)+">");
        		multiPart.addBodyPart(imageBodyPart);
        	}
        }

		//-----pièce jointe-----
        if(filenameAttach!=null){
        	BodyPart attachBodyPart=new MimeBodyPart();
        	File f=new File(filenameAttach);
        	DataSource fds=new FileDataSource(f);
        	attachBodyPart.setFileName(f.getName());
        	//attachBodyPart.setDisposition(Part.ATTACHMENT);
        	//attachBodyPart.setDescription("Attached file: " + f.getName());
        	attachBodyPart.setDataHandler(new DataHandler(fds));
        	multiPart.addBodyPart(attachBodyPart);
        }

		//-----envoi du mail-----
		message.setContent(multiPart);
        Transport.send(message);
	}

	/*-------------------------------------------------------------------------*/
}
