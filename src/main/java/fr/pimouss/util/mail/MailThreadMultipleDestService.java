package fr.pimouss.util.mail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MailThreadMultipleDestService implements Runnable {
	private List<String> listeDestinataire;
	private RecipientType typeDest;
	private String body;
	private String subject;
	private String filenameAttach;
	private String[] filenameImage;

	/*-------------------------------------------------------------------------*/
	public MailThreadMultipleDestService(List<String> listeDestinataire, RecipientType typeDest,String body, String subject, String filenameAttach, String[] filenameImage) throws IOException {
		if(ConfEnv.getMailRedirect()){
			this.listeDestinataire=new ArrayList<String>();
			this.listeDestinataire.add(ConfEnv.getMailRedirectAddress());
		}else{
			this.listeDestinataire=listeDestinataire;
		}
		this.typeDest=typeDest;
		this.body=body;
		this.subject=subject;
		this.filenameAttach=filenameAttach;
		this.filenameImage=filenameImage;
    	(new Thread(this)).start();
	}

	/*-------------------------------------------------------------------------*/
	public void run() {
   		try{
    		//-----création du mail-----
    		Message message;
    		Session session;
    		Properties properties = new Properties();
    		properties.put("mail.smtp.host",ConfEnv.getMailSmtpServer());
            properties.put("mail.smtp.port",ConfEnv.getMailSmtpPort());
            properties.put("mail.smtp.socketFactory.port",ConfEnv.getMailSmtpPort());
            properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.auth","true");
            properties.put("mail.pop3.host",ConfEnv.getMailSmtpServer());
            Authenticator authenticator=new POPAuthenticator();
            session=Session.getInstance(properties, authenticator);
    		session.setDebug(ConfEnv.getMailLogDebug());
    		message=new MimeMessage(session);
    		message.setFrom(new InternetAddress(ConfEnv.getMailFromAddress()));
    		for(String elt:listeDestinataire){
    			message.addRecipient(typeDest, new InternetAddress(elt));
    		}
    		message.setSubject(subject);
    		
    		//Multipart mmp=new MimeMultipart("related");
    		Multipart multiPart=new MimeMultipart();
    		//-----corps du mail-----
    		BodyPart messageBodyPart=new MimeBodyPart();
    		messageBodyPart.setDisposition(Part.INLINE);
    		messageBodyPart.setContent(body,"text/html");
    		multiPart.addBodyPart(messageBodyPart);
    		
    		//-----images-----
    		if(filenameImage!=null){
    			for(int i=0;i<filenameImage.length;i++){
    				DataSource fds=new FileDataSource(filenameImage[i]);
    				BodyPart imageBodyPart=new MimeBodyPart();
    				imageBodyPart.setDataHandler(new DataHandler(fds));
    				imageBodyPart.setHeader("Content-ID","<image"+(i+1)+">");
    				multiPart.addBodyPart(imageBodyPart);
    			}
    		}
    		
    		//-----pièce jointe-----
    		if(filenameAttach!=null){
    			BodyPart attachBodyPart=new MimeBodyPart();
    			File f=new File(filenameAttach);
    			DataSource fds=new FileDataSource(f);
    			attachBodyPart.setFileName(f.getName());
    			//attachBodyPart.setDisposition(Part.ATTACHMENT);
    			//attachBodyPart.setDescription("Attached file: " + f.getName());
    			attachBodyPart.setDataHandler(new DataHandler(fds));
    			multiPart.addBodyPart(attachBodyPart);
    		}
    		
    		//-----envoi du mail-----
    		message.setContent(multiPart);
    		Transport.send(message);
   		}catch(Exception e){
   			Loggeur.error(e);
   		}
	}

	/*-------------------------------------------------------------------------*/
}
