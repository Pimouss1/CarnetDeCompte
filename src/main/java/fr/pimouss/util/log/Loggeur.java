package fr.pimouss.util.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.mail.MailThreadService;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class Loggeur {
	public static final int FATAL = 8;
	public static final int INFO = 7;
	public static final int ERROR = 6;
	public static final int SECURITY = 5;
	public static final int WARN = 4;
	public static final int TIMER = 3;
	public static final int DEBUG = 2;
	public static final int SQL = 1;

	private static Loggeur instance;

	private int level;
	/* 	Les conteneurs web et les serveurs d'applications sont de bons exemples d'applications qui utilisent des classloaders personnalisés.
	 	Généralement, chaque application déployée possède son propre classloader ce qui permet une meilleure isolation des applications exécutées dans la JVM.
		Ceci est vrai parce qu'une classe chargée dans la JVM est identifiée par son nom et son classloader.
		Ceci permet par exemple à un singleton utilisé par plusieurs applications d'être unique par application et non unique dans la JVM puisque chaque application possède son propre classloader.
	 	Ceci permet aussi un rechargement des classes d'une application déployée sans être obligé de relancer la JVM.
	 	rèf : http://www.jmdoudoux.fr/java/dej/chap-jvm.htm
	 	§51.3.2.1
	*/
	private PrintWriter logFile;

	/*---------------------------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private Loggeur(){
		try{
			level = ConfEnv.getAppLogLevel();
			if(ConfEnv.getAppRepLog()!=null){
				File dirLog=new File(ConfEnv.getAppRepLog());
				if(!dirLog.exists()){
					throw new FileNotFoundException(dirLog.getAbsolutePath()+" est introuvable");
				}
				if(!dirLog.isDirectory()){
					throw new IOException(dirLog.getAbsolutePath()+" n'est pas un répertoire");
				}
				String logFileName = ConfEnv.getAppRepLog() + "/"
						+ ConfApp.getApplicationName() + "-"
						+ UtilDate.FORMAT_DATE_SYS_COMP.format(new Date()) + ".log";
				logFile=new PrintWriter(new FileOutputStream(logFileName),true);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private static void sendAdmin(String message, String level){
		try{
			for(UtilisateurDo elt:DaoDelegate.getUtilisateurDao().getUtilisateurInGroupe(GroupeUtilisateurDo.ADMINISTRATEUR)){
				new MailThreadService(elt.getMail(),message,ConfApp.getApplicationName()+" - "+level,null,null);
			}
		}catch(DaoException de){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" "+level+" - "+de.getMessage());
			de.printStackTrace(System.out);
			de.printStackTrace(getInstance().logFile);
			if(de instanceof ApplicationException && ((ApplicationException)de).getException()!=null){
				((ApplicationException)de).getException().printStackTrace(System.out);
				((ApplicationException)de).getException().printStackTrace(getInstance().logFile);
			}
		}catch(IOException ioe){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" "+level+" - "+ioe.getMessage());
			ioe.printStackTrace(System.out);
			ioe.printStackTrace(getInstance().logFile);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private static void println(String str){
		System.out.println(str);
		getInstance().logFile.println(str);
	}

	/*---------------------------------------------------------------------------------------------*/
	public static Loggeur getInstance(){
		if (instance == null) {
			instance = new Loggeur();
		}
		return instance;
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean isDebugEnabled(){
		return Loggeur.DEBUG>=getInstance().level;
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void close(){
		System.out.close();
		getInstance().logFile.close();
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void error(String message) {
		if(Loggeur.ERROR>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" ERROR    - "+message);
			sendAdmin(message,"ERROR");
		}
	}
	public static void security(String message) {
		if(Loggeur.SECURITY>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" SECURITY - "+message);
			sendAdmin(message,"SECURITY");
		}
	}
	public static void warn(String message) {
		if(Loggeur.WARN>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" WARNING  - "+message);
		}
	}
	public static void timer(String message) {
		if(Loggeur.WARN>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" TIMER    - "+message);
		}
	}
	public static void info(String message) {
		if(Loggeur.INFO>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" INFO     - "+message);
		}
	}
	public static void debug(String message) {
		if(Loggeur.DEBUG>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" DEBUG    - "+message);
		}
	}
	public static void sql(String message) {
		if(Loggeur.SQL>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" SQL      - "+message);
		}
	}
	/*---------------------------------------------------------------------------------------------*/
	public static void fatal(Throwable throwable, boolean withSendMail) {
		if(Loggeur.FATAL>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" FATAL    - "+throwable.getMessage());
			throwable.printStackTrace(System.out);
			throwable.printStackTrace(getInstance().logFile);
			if(throwable instanceof ApplicationException && ((ApplicationException)throwable).getException()!=null){
				((ApplicationException)throwable).getException().printStackTrace(System.out);
				((ApplicationException)throwable).getException().printStackTrace(getInstance().logFile);
			}
			if(withSendMail){
				sendAdmin(throwable.getMessage(),"FATAL");
			}
		}
	}
	public static void fatal(Throwable throwable) {
		fatal(throwable, true);
	}

	public static void error(Throwable throwable, boolean withSendMail) {
		if(Loggeur.ERROR>=getInstance().level){
			println(UtilDate.FORMAT_DATE_SYS_COMP.format(new Date())+" ERROR    - "+throwable.getMessage());
			throwable.printStackTrace(System.out);
			throwable.printStackTrace(getInstance().logFile);
			if(throwable instanceof ApplicationException && ((ApplicationException)throwable).getException()!=null){
				((ApplicationException)throwable).getException().printStackTrace(System.out);
				((ApplicationException)throwable).getException().printStackTrace(getInstance().logFile);
			}
			if(withSendMail){
				sendAdmin(throwable.getMessage(),"ERROR");
			}
		}
	}
	public static void error(Throwable throwable) {
		error(throwable, true);
	}

	/*---------------------------------------------------------------------------------------------*/
}
