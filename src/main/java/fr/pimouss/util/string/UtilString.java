package fr.pimouss.util.string;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilString {

	/*-------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private UtilString() {}

	/*-------------------------------------------------------------------------*/
	public static String capitalize(String str){
		return str.substring(0,1).toUpperCase()+str.substring(1);
	}

	/*-------------------------------------------------------------------------*/
	public static boolean isNullOrBlank(String str){
		return (str==null || str.trim().length()==0 || "null".equals(str));
	}

	/*-------------------------------------------------------------------------*/
	public static String withoutSpecialCharacter(String str){
		String strTmp=str.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\+","")
				.replaceAll("&","and").replaceAll("é","e").replaceAll("é","e").replaceAll("à","a").replaceAll("ê","e").replaceAll("î","i")
				.replaceAll("ô","o").replaceAll("\\?","").replaceAll("´","").replaceAll("'","").replaceAll("Ì","").replaceAll("°","")
				.replaceAll("€","E");
		String result="";
		for(int i=0;i<strTmp.length();i++){
			if(strTmp.codePointAt(i)<127){
				result+=strTmp.charAt(i);
			}
		}
		return result;
	}

	/*-------------------------------------------------------------------------*/
}
