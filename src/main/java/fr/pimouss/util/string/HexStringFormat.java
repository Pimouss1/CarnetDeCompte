package fr.pimouss.util.string;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.pimouss.land.shared.exception.UtilException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class HexStringFormat {
	private static DecimalFormat df=new DecimalFormat("00000000");
	private String pattern;

	/*-------------------------------------------------------------------------*/
	public HexStringFormat(String pattern) throws UtilException {
		// validation du pattern (ne doit contenir que des 0)
		Pattern validator=Pattern.compile("[^0]");
		Matcher matcher=validator.matcher(pattern);
		if(matcher.find()){
			throw new UtilException("pattern "+pattern+" non valide",null);
		}
		this.pattern=pattern;
	}

	/*-------------------------------------------------------------------------*/
	private String format(String str){
		String result=pattern;
		int pos=pattern.length()-str.length();
		if(pos<0){pos=0;}
		result=result.substring(0, pos);
		result+=str;
		return result;
	}

	/*-------------------------------------------------------------------------*/
	public String format(long decimalNumber){
		return format(Long.toHexString(decimalNumber)).toUpperCase();
	}

	/*-------------------------------------------------------------------------*/
	public String format(int decimalNumber){
		return format(Integer.toHexString(decimalNumber)).toUpperCase();
	}

	/*-------------------------------------------------------------------------*/
	public static String createFilename(long id, String ext) throws UtilException{
		HexStringFormat hsf=new HexStringFormat("00000000");
		long c=4000000000l;
		String result=df.format(id);
		result+=hsf.format(Math.round(c*Math.random()));
		result+=ext;
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
}
