package fr.pimouss.util.http;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServlet;

import fr.pimouss.land.shared.exception.ErrorException;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.util.cst.ConfEnv;

public class HttpProxy extends HttpServlet {
	private static final long serialVersionUID = -2818165986771414531L;

	/*---------------------------------------------------------------------------------------------*/
	protected Object envoiRequete(String serviceName, String methodName, Object[] params) throws ErrorException, WarningException {
		try{
			Requete requete=new Requete();
			requete.setServiceName(serviceName);
			requete.setMethodeName(methodName);
			requete.setParams(params);
			URL url=new URL(ConfEnv.getPimoussLandServerAdress()+"proxyService");
			HttpURLConnection connection=(HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			ObjectOutputStream oos=new ObjectOutputStream(connection.getOutputStream());
			oos.writeObject(requete);
			oos.flush();
			oos.close();
			ObjectInputStream ois=new ObjectInputStream(connection.getInputStream());
			Object result=ois.readObject();
			ois.close();
			if(result instanceof InvocationTargetException){
				if(((InvocationTargetException)result).getTargetException() instanceof WarningException){
					WarningException target=(WarningException)((InvocationTargetException)result).getTargetException();
					throw target;
				}else{
					ErrorException target=(ErrorException)((InvocationTargetException)result).getTargetException();
					throw target;
				}
			}
			if(result instanceof Exception){
				throw new ErrorException(((Exception)result).getMessage(),(Exception)result);
			}
			return result;
		}catch(IOException ioe){
			throw new ErrorException(ioe.getMessage(), ioe);
		}catch(ClassNotFoundException cnfe){
			throw new ErrorException(cnfe.getMessage(), cnfe);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
