package fr.pimouss.util.http;

import java.io.Serializable;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class Requete implements Serializable {
	private static final long serialVersionUID = 4009812025198832296L;

	private String serviceName;
	private String methodeName;
	private Object[] params;

	public String getMethodeName() {
		return methodeName;
	}
	public void setMethodeName(String methodeName) {
		this.methodeName = methodeName;
	}
	public Object[] getParams() {
		return params;
	}
	public void setParams(Object[] params) {
		this.params = params;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
