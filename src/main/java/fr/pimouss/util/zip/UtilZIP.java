package fr.pimouss.util.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilZIP {
	private static final int BUFFER = 2048;

	/*-------------------------------------------------------------------------*/
	// Constructeur privé pour définir une classe utilitaire, avec seulement des méthodes static
	private UtilZIP() {}

	/*-------------------------------------------------------------------------*/
	public static String zipName(String titre){
		return titre.replaceAll("[\\?]","").replaceAll("\"","").replaceAll("[\\*]","").replaceAll(":","")
				.replaceAll("<","").replaceAll(">","")/*.replaceAll("|","")*/.replaceAll("/","").replaceAll("[\\\\]","");
	}

	/*-------------------------------------------------------------------------*/
	public static void zipFile(String sourceFileName, String zipFileName) throws IOException {
		ZipOutputStream zos=new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileName)));
		byte data[]=new byte[BUFFER];
		FileInputStream fis=new FileInputStream(sourceFileName);
		BufferedInputStream bis=new BufferedInputStream(fis,BUFFER);
		ZipEntry entry=new ZipEntry(sourceFileName);
		zos.putNextEntry(entry);
		int count;
		while((count=bis.read(data,0,BUFFER))!=-1){
			zos.write(data, 0, count);
		}
		bis.close();
		zos.close();
	}

	/*-------------------------------------------------------------------------*/
	public static void zipFolder(String sourceFolderName, String zipFileName) throws IOException {
		ZipOutputStream zos=new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileName)));
		byte data[]=new byte[BUFFER];
		File sourceFolder=new File(sourceFolderName);
		File tabFiles[]=sourceFolder.listFiles();
		for(int i=0;i<tabFiles.length;i++){
			if(!tabFiles[i].isDirectory()){
				FileInputStream fis=new FileInputStream(tabFiles[i]);
				BufferedInputStream bis=new BufferedInputStream(fis,BUFFER);
				ZipEntry entry=new ZipEntry(tabFiles[i].getName());
				zos.putNextEntry(entry);
				int count;
				while((count=bis.read(data,0,BUFFER))!=-1){
					zos.write(data, 0, count);
				}
				bis.close();
			}
		}
		zos.close();
	}

	/*-------------------------------------------------------------------------*/
	public static final void unzip(File file, String destinationPath, boolean force, Checksum checksum) throws IOException {
		ZipFile zipfile = new ZipFile(file);
		byte[] buf = new byte[BUFFER];
		FileInputStream fis = new FileInputStream(zipfile.getName());
		CheckedInputStream cis = (checksum == null) ? null : new CheckedInputStream(fis, checksum);
		BufferedInputStream bis = new BufferedInputStream((cis != null) ? cis : fis);
		ZipInputStream zis = new ZipInputStream(bis);
		ZipEntry entry = null;
		try {
			while ((entry = zis.getNextEntry()) != null) {
				File f = new File(destinationPath + File.separatorChar + entry.getName());
				if (entry.isDirectory()) {
					new File(entry.getName()).getParentFile().mkdir();
				}
				if (f.exists()) {
					if (force) {
						if(!f.delete()){
							throw new IOException("Impossible de supprimer le fichier "+f.getAbsolutePath());
						}
					} else {
						continue;
					}
				}
				if(!f.createNewFile()){
					throw new IOException("Impossible de créer le fichier "+f.getAbsolutePath());
				}
				int nbRead;
				FileOutputStream fos=null;
				BufferedOutputStream bos=null;
				try{
					fos = new FileOutputStream(f);
					bos = new BufferedOutputStream(fos, BUFFER);
					while ((nbRead = zis.read(buf)) > 0) {
						bos.write(buf, 0, nbRead);
					}
				} finally {
					if (bos != null) {
						bos.flush();
						bos.close();
					}
					if (fos != null) {
						fos.close();
					}
				}
			}
		} finally {
			zis.close();
			bis.close();
			if (cis != null) {
				cis.close();
			}
			fis.close();
			zipfile.close();
		}
	}

	/*-------------------------------------------------------------------------*/
}
