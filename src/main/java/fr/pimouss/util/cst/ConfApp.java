package fr.pimouss.util.cst;

import java.io.IOException;
import java.util.Properties;

import com.google.gwt.resources.client.CssResource.ClassName;

import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ConfApp {
	private static final String CONF_NAME = "pimoussland.";
	private static Properties propApp;

	/*---------------------------------------------------------------------------------------------*/
	private ConfApp() {}

	/*---------------------------------------------------------------------------------------------*/
	private static Properties getPropApp() throws IOException{
		if(propApp==null){
			propApp=new Properties();
			propApp.load(ClassName.class.getClassLoader().getResourceAsStream(CONF_NAME+"properties"));
			Loggeur.debug("Chargement de "+CONF_NAME+"properties effectué avec succès");
		}
		return propApp;
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getVersion() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"version");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getApplicationName() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"applicationName");
	}

	/*---------------------------------------------------------------------------------------------*/
}
