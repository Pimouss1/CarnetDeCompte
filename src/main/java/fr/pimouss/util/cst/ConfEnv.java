package fr.pimouss.util.cst;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.google.gwt.resources.client.CssResource.ClassName;

import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ConfEnv {
	private static final String CONF_NAME = "carnetcompte.";
	private static final String CATALINA_HOME = "catalina.home";
	private static Properties propApp;

	/*---------------------------------------------------------------------------------------------*/
	private ConfEnv() {}

	/*---------------------------------------------------------------------------------------------*/
	public static void setPropAppInClassPath(String propertiesFileInClassPath) throws IOException{
		propApp=new Properties();
		propApp.load(ClassName.class.getClassLoader().getResourceAsStream(propertiesFileInClassPath));
	}

	/*---------------------------------------------------------------------------------------------*/
	private static Properties getPropApp() throws IOException{
		if(propApp==null){
			int meth=0;
			String repConf=null;
			if(System.getProperty(CATALINA_HOME)!=null && (new File(System.getProperty(CATALINA_HOME)+"/conf/")).isDirectory()){
				repConf=System.getProperty(CATALINA_HOME)+"/conf/";
				meth=1;
			}else if((new File("C:/Program Files/Apache Software Foundation/Tomcat 8.5/conf/")).isDirectory()){
				repConf="C:/Program Files/Apache Software Foundation/Tomcat 8.5/conf/";
				meth=2;
			}else if((new File("/usr/share/tomcat8/conf/")).isDirectory()){
				repConf="/usr/share/tomcat8/conf/";
				meth=3;
			}else if((new File("C:/PACKDEV/apache-tomcat-8.0.22/conf/")).isDirectory()){
				repConf="C:/PACKDEV/apache-tomcat-8.0.22/conf/";
				meth=4;
			}

			if(repConf!=null){
				File f=new File(repConf+CONF_NAME+ConfApp.getApplicationName()+".properties");
				if(f.exists()){
					propApp=new Properties();
					propApp.load(new FileInputStream(f));
				}else{
					throw new FileNotFoundException(f.getAbsolutePath()+" introuvable.");
				}
			}else{
				throw new FileNotFoundException("repConf="+repConf);
			}
			Loggeur.debug("Chargement de "+repConf+CONF_NAME+ConfApp.getApplicationName()+".properties effectué avec succès par méthode "+meth);
		}
		return propApp;
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcUrl() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.url");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcUser() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.user");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcPassword() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.password");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcDriver() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.driver");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcSaveDataBaseFolder() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.saveDataBaseFolder");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getJdbcDumpDataBaseProc() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"jdbc.dumpDataBaseProc");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean getMailLogDebug() throws IOException{
		return "true".equals((String)getPropApp().get(CONF_NAME+"mail.logDebug"));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getMailSmtpServer() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"mail.smtpServer");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static int getMailSmtpPort() throws IOException{
		return Integer.parseInt((String)getPropApp().get(CONF_NAME+"mail.smtpPort"));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getMailFromAddress() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"mail.fromAddress");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getMailUserPOPAuthenticator() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"mail.userPOPAuthenticator");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getMailPasswordPOPAuthenticator() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"mail.passwordPOPAuthenticator");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean getMailRedirect() throws IOException{
		return "true".equals((String)getPropApp().get(CONF_NAME+"mail.redirect"));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getMailRedirectAddress() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"mail.redirectAddress");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getAppRepLog() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"app.repLog");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static int getAppLogLevel() throws IOException{
		try{
			return Integer.parseInt((String)getPropApp().get(CONF_NAME+"app.logLevel"));
		}catch (NumberFormatException e) {
			return 1;
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getAppSuperPassword() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"app.superPassword");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean getAppLogClient() throws IOException{
		return "true".equals((String)getPropApp().get(CONF_NAME+"app.logClient"));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static boolean getAppDaoMock() throws IOException{
		return "true".equals((String)getPropApp().get(CONF_NAME+"app.daoMock"));
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getDocPath() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"doc.path");
	}

	/*---------------------------------------------------------------------------------------------*/
	public static String getPimoussLandServerAdress() throws IOException{
		return (String)getPropApp().get(CONF_NAME+"pimoussland.serverAdress");
	}

	/*---------------------------------------------------------------------------------------------*/
}
