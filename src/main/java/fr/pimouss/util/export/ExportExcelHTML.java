package fr.pimouss.util.export;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ExportExcelHTML {
	private ByteArrayOutputStream os;
	private BufferedWriter bw;

	/*---------------------------------------------------------------------------------------------*/
	public ExportExcelHTML() throws IOException {
		os=new ByteArrayOutputStream();
		bw=new BufferedWriter(new OutputStreamWriter(os));
		bw.write("<table><tr>");
	}

	/*---------------------------------------------------------------------------------------------*/
	public void ecrire(String texte, String style, int rowspan, int colspan) throws IOException{
		bw.write("<td colspan=\""+colspan+"\" rowspan=\""+rowspan+"\" style=\""+style+"\">"+texte+"</td>");
	}

	/*---------------------------------------------------------------------------------------------*/
	public void ecrire(String texte, String style) throws IOException{
		bw.write("<td style=\""+style+"\">"+texte+"</td>");
	}

	/*---------------------------------------------------------------------------------------------*/
	public void ecrire(String texte) throws IOException{
		bw.write("<td>"+texte+"</td>");
	}

	/*---------------------------------------------------------------------------------------------*/
	public void ligneSuivante() throws IOException{
		bw.write("</tr><tr>");
	}

	/*---------------------------------------------------------------------------------------------*/
	private void eof() throws IOException{
		bw.write("</tr></table>");
	}

	/*---------------------------------------------------------------------------------------------*/
	public ByteArrayOutputStream close() throws IOException{
		eof();
		bw.close();
		os.flush();
//		os.close();
		return os;
	}

	/*---------------------------------------------------------------------------------------------*/
}
