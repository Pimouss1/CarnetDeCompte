package fr.pimouss.land.server.dao.itf;

import fr.pimouss.land.shared.dao.VersionDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface IVersionDao extends IAbstractDao<VersionDo> {
	VersionDo getLastVersion() throws DaoException;
}
