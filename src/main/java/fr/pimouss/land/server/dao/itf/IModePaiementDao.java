package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IModePaiementDao extends IAbstractDao<ModePaiementDo> {
	List<ModePaiementDo> getByType(String typeMouvement) throws DaoException;
}
