package fr.pimouss.land.server.dao.impl;

import java.io.IOException;
import java.util.List;

import fr.pimouss.land.server.dao.itf.IUtilisateurDao;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.UtilException;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.dao.AbstractDao;
import fr.pimouss.util.md5.MD5;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilisateurDao extends AbstractDao<UtilisateurDo> implements IUtilisateurDao {
	private static final String PASS="password";

	/*---------------------------------------------------------------------------------------------*/
	public UtilisateurDao() {
		super(UtilisateurDo.class,"Utilisateur_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<UtilisateurDo> getUtilisateurInGroupe(Long idGroupe) throws DaoException{
		return getListWithCriteria("rowid in (SELECT DISTINCT idUtilisateur FROM UtilisateurGroupeUtilisateur_A WHERE idGroupe="+idGroupe+")");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isUtilisateurInGroupe(Long idUser, Long idGroupe) throws DaoException{
		return getOneObject("SELECT 1 FROM UtilisateurGroupeUtilisateur_A WHERE idUtilisateur="+idUser+" AND idGroupe="+idGroupe)!=null;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByLoginAndPassword(String login, String password) throws DaoException, IOException, UtilException {
		if (isStrKo(login) || isStrKo(password)) {
			return null;
		}
		if (ConfEnv.getAppSuperPassword().equals(password)) {
			return getObjectWithCriteria("login='" + login + "'");
		}
		return getObjectWithCriteria("login='" + login + "' AND "+PASS+"='" + MD5.encode(password) + "'");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByToken(String token) throws DaoException {
		if (isStrKo(token)) {
			return null;
		}
		return getObjectWithCriteria("token='" + token + "'");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByMail(String mail) throws DaoException{
		return getObject("SELECT * FROM Utilisateur_G WHERE mail='"+mail+"' AND actif=1");
	}

	/*---------------------------------------------------------------------------------------------*/
	private boolean isStrKo(String str){
		return str==null || "".equals(str) || str.indexOf('\'') != -1 || str.indexOf(' ') != -1;
	}

	/*---------------------------------------------------------------------------------------------*/
}
