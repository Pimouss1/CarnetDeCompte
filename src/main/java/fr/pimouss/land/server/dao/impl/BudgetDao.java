package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IBudgetDao;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetDao extends AbstractDao<BudgetDo> implements IBudgetDao {

	/*---------------------------------------------------------------------------------------------*/
	public BudgetDao() {
		super(BudgetDo.class,"Budget_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BudgetDo getBudgetByIdTypeVentilAndIdPeriode(Long idTypeVentil, Long idPeriode) throws DaoException{
		return getObjectWithCriteria("idTypeVentilation="+idTypeVentil+" AND idPeriode="+idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BudgetDo> getBudgetByTypeMvtByIdPeriode(String typeMvt, Long idPeriode) throws DaoException{
		return getListWithCriteria("idTypeVentilation IN (SELECT rowid FROM TypeVentilation_R WHERE typeMouvement='"+typeMvt+"') AND idPeriode="+idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
}
