package fr.pimouss.land.server.dao.mock;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IVentilationDao;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationDaoMock extends AbstractDaoMock<VentilationDo> implements IVentilationDao {

	/*---------------------------------------------------------------------------------------------*/
	public VentilationDaoMock() {
		super();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<VentilationDo> getByIdMvt(final Long idMvt) throws DaoException {
		return getList(new Condition<VentilationDo>() {
			@Override
			public boolean condition(VentilationDo elt) {
				return elt.getIdMouvement()==idMvt;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<VentilationDo> getByIdTypeVentilation(final Long idTypeVentil) throws DaoException {
		return getList(new Condition<VentilationDo>() {
			@Override
			public boolean condition(VentilationDo elt) {
				return elt.getIdTypeVentilation()==idTypeVentil;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteByIdMvt(final Long idMvt) throws DaoException {
		List<VentilationDo> listeToRemove=getList(new Condition<VentilationDo>() {
			@Override
			public boolean condition(VentilationDo elt) {
				return elt.getIdMouvement()==idMvt;
			}
		});
		for(VentilationDo elt:listeToRemove) {
			delete(elt);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
