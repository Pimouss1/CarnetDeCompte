package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IJustificatifDao;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class JustificatifDao extends AbstractDao<JustificatifDo> implements IJustificatifDao {

	/*---------------------------------------------------------------------------------------------*/
	public JustificatifDao() {
		super(JustificatifDo.class,"Justificatif_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<JustificatifDo> getByIdMvt(Long idMvt) throws DaoException {
		return getListWithCriteria("idMouvement="+idMvt);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteByIdMvt(Long idMvt) throws DaoException {
		executeUpdate("delete from Justificatif_G where idMouvement="+idMvt);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<JustificatifDo> getJustifWithSpecialCharacteres() throws DaoException {
		return getListWithCriteria("filename like '%?%'");
	}

	/*---------------------------------------------------------------------------------------------*/
}
