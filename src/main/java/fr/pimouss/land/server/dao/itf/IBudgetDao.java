package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IBudgetDao extends IAbstractDao<BudgetDo> {
	BudgetDo getBudgetByIdTypeVentilAndIdPeriode(Long idTypeVentil, Long idPeriode) throws DaoException;
//	List<BudgetDo> getBudgetByIdPeriode(Long idPeriode) throws DaoException;
	List<BudgetDo> getBudgetByTypeMvtByIdPeriode(String typeMvt, Long idPeriode) throws DaoException;
}
