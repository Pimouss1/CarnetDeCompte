package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IVentilationDao extends IAbstractDao<VentilationDo> {
	List<VentilationDo> getByIdMvt(Long idMvt) throws DaoException;
	void deleteByIdMvt(Long idMvt) throws DaoException;
	List<VentilationDo> getByIdTypeVentilation(Long idTypeVentil) throws DaoException;
}
