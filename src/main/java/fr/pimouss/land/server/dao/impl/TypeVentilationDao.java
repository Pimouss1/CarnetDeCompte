package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.ITypeVentilationDao;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class TypeVentilationDao extends AbstractDao<TypeVentilationDo> implements ITypeVentilationDao {

	/*---------------------------------------------------------------------------------------------*/
	public TypeVentilationDao() {
		super(TypeVentilationDo.class,"TypeVentilation_R");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getByType(String typeMouvement) throws DaoException{
		return getListWithCriteria("typeMouvement='"+typeMouvement+"' AND actif=1 ORDER BY typeVentilation");
	}

	/*---------------------------------------------------------------------------------------------*/
}
