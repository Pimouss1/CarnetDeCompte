package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IGroupeUtilisateurDao;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GroupeUtilisateurDao extends AbstractDao<GroupeUtilisateurDo> implements IGroupeUtilisateurDao {

	/*---------------------------------------------------------------------------------------------*/
	public GroupeUtilisateurDao() {
		super(GroupeUtilisateurDo.class,"GroupeUtilisateur_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<GroupeUtilisateurDo> getGroupeByUser(Long idUser) throws DaoException{
		return getListWithCriteria(" rowid in (select idGroupe from UtilisateurGroupeUtilisateur_A where idUtilisateur="+idUser);
	}

	/*---------------------------------------------------------------------------------------------*/
}
