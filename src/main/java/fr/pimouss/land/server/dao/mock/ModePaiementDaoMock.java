package fr.pimouss.land.server.dao.mock;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IModePaiementDao;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ModePaiementDaoMock extends AbstractDaoMock<ModePaiementDo> implements IModePaiementDao {

	/*---------------------------------------------------------------------------------------------*/
	public ModePaiementDaoMock() throws DaoException {
		super();
		save(new ModePaiementDo(1, MouvementDo.DEPENSE, "Chèque"));
		save(new ModePaiementDo(2, MouvementDo.DEPENSE, "Carte bancaire"));
		save(new ModePaiementDo(3, MouvementDo.DEPENSE, "Virement"));
		save(new ModePaiementDo(4, MouvementDo.DEPENSE, "Prélèvement"));
		save(new ModePaiementDo(5, MouvementDo.RECETTE, "Remise de chèque"));
		save(new ModePaiementDo(6, MouvementDo.RECETTE, "Dépôt d'espèces"));
		save(new ModePaiementDo(7, MouvementDo.RECETTE, "Virement"));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getByType(final String typeMouvement) throws DaoException{
		return getList(new Condition<ModePaiementDo>() {
			@Override
			public boolean condition(ModePaiementDo elt) {
				return elt.getTypeMouvement().equals(typeMouvement);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
