package fr.pimouss.land.server.dao.itf;

import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface ISoldeDao extends IAbstractDao<SoldeDo> {
	SoldeDo getLastSolde(Long idCompte) throws DaoException;
	SoldeDo getFirstSolde(Long idCompte) throws DaoException;
	void razSolde() throws DaoException;
}
