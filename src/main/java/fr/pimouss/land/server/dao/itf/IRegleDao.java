package fr.pimouss.land.server.dao.itf;

import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.util.dao.IAbstractDao;

public interface IRegleDao extends IAbstractDao<RegleDo> {
}
