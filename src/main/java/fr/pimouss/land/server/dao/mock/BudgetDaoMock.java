package fr.pimouss.land.server.dao.mock;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IBudgetDao;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetDaoMock extends AbstractDaoMock<BudgetDo> implements IBudgetDao {

	/*---------------------------------------------------------------------------------------------*/
	public BudgetDaoMock() throws DaoException {
		super();
//		save(new BudgetDo(1,1,1,new BigDecimal(1000)));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BudgetDo getBudgetByIdTypeVentilAndIdPeriode(final Long idTypeVentil, final Long idPeriode) throws DaoException{
		return getObject(new Condition<BudgetDo>() {
			@Override
			public boolean condition(BudgetDo elt) {
				return elt.getIdTypeVentilation()==idTypeVentil && elt.getIdPeriode()==idPeriode;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BudgetDo> getBudgetByTypeMvtByIdPeriode(final String typeMvt, final Long idPeriode) throws DaoException {
		return getList(new Condition<BudgetDo>() {
			@Override
			public boolean condition(BudgetDo elt) {
				return elt.getIdPeriode()==idPeriode;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
