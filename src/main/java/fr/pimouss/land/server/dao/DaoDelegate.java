package fr.pimouss.land.server.dao;

import java.io.IOException;
import java.lang.reflect.Proxy;

import fr.pimouss.land.server.dao.impl.BudgetDao;
import fr.pimouss.land.server.dao.impl.CompteDao;
import fr.pimouss.land.server.dao.impl.GroupeUtilisateurDao;
import fr.pimouss.land.server.dao.impl.JustificatifDao;
import fr.pimouss.land.server.dao.impl.ModePaiementDao;
import fr.pimouss.land.server.dao.impl.MouvementDao;
import fr.pimouss.land.server.dao.impl.PeriodeDao;
import fr.pimouss.land.server.dao.impl.RegleDao;
import fr.pimouss.land.server.dao.impl.SoldeDao;
import fr.pimouss.land.server.dao.impl.TypeVentilationDao;
import fr.pimouss.land.server.dao.impl.UtilisateurDao;
import fr.pimouss.land.server.dao.impl.VentilationDao;
import fr.pimouss.land.server.dao.impl.VersionDao;
import fr.pimouss.land.server.dao.itf.IBudgetDao;
import fr.pimouss.land.server.dao.itf.ICompteDao;
import fr.pimouss.land.server.dao.itf.IGroupeUtilisateurDao;
import fr.pimouss.land.server.dao.itf.IJustificatifDao;
import fr.pimouss.land.server.dao.itf.IModePaiementDao;
import fr.pimouss.land.server.dao.itf.IMouvementDao;
import fr.pimouss.land.server.dao.itf.IPeriodeDao;
import fr.pimouss.land.server.dao.itf.IRegleDao;
import fr.pimouss.land.server.dao.itf.ISoldeDao;
import fr.pimouss.land.server.dao.itf.ITypeVentilationDao;
import fr.pimouss.land.server.dao.itf.IUtilisateurDao;
import fr.pimouss.land.server.dao.itf.IVentilationDao;
import fr.pimouss.land.server.dao.itf.IVersionDao;
import fr.pimouss.land.server.dao.mock.BudgetDaoMock;
import fr.pimouss.land.server.dao.mock.CompteDaoMock;
import fr.pimouss.land.server.dao.mock.GroupeUtilisateurDaoMock;
import fr.pimouss.land.server.dao.mock.JustificatifDaoMock;
import fr.pimouss.land.server.dao.mock.ModePaiementDaoMock;
import fr.pimouss.land.server.dao.mock.MouvementDaoMock;
import fr.pimouss.land.server.dao.mock.PeriodeDaoMock;
import fr.pimouss.land.server.dao.mock.RegleDaoMock;
import fr.pimouss.land.server.dao.mock.SoldeDaoMock;
import fr.pimouss.land.server.dao.mock.TypeVentilationDaoMock;
import fr.pimouss.land.server.dao.mock.UtilisateurDaoMock;
import fr.pimouss.land.server.dao.mock.VentilationDaoMock;
import fr.pimouss.land.server.dao.mock.VersionDaoMock;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.dao.DaoHandler;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DaoDelegate {
	private static IBudgetDao budgetDao;
	public static IBudgetDao getBudgetDao() {
		if(budgetDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					budgetDao=(IBudgetDao)Proxy.newProxyInstance(
						IBudgetDao.class.getClassLoader(),
						new Class[]{IBudgetDao.class},
						new DaoHandler<BudgetDaoMock>(BudgetDaoMock.class));
				} else {
					budgetDao=(IBudgetDao)Proxy.newProxyInstance(
						IBudgetDao.class.getClassLoader(),
						new Class[]{IBudgetDao.class},
						new DaoHandler<BudgetDao>(BudgetDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return budgetDao;
	}

	private static ICompteDao compteDao;
	public static ICompteDao getCompteDao() {
		if(compteDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					compteDao=(ICompteDao)Proxy.newProxyInstance(
						ICompteDao.class.getClassLoader(),
						new Class[]{ICompteDao.class},
						new DaoHandler<CompteDaoMock>(CompteDaoMock.class));
				} else {
					compteDao=(ICompteDao)Proxy.newProxyInstance(
						ICompteDao.class.getClassLoader(),
						new Class[]{ICompteDao.class},
						new DaoHandler<CompteDao>(CompteDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return compteDao;
	}

	private static IGroupeUtilisateurDao groupeUtilisateurDao;
	public static IGroupeUtilisateurDao getGroupeUtilisateurDao() {
		if(groupeUtilisateurDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					groupeUtilisateurDao=(IGroupeUtilisateurDao)Proxy.newProxyInstance(
						IGroupeUtilisateurDao.class.getClassLoader(),
						new Class[]{IGroupeUtilisateurDao.class},
						new DaoHandler<GroupeUtilisateurDaoMock>(GroupeUtilisateurDaoMock.class));
				} else {
					groupeUtilisateurDao=(IGroupeUtilisateurDao)Proxy.newProxyInstance(
						IGroupeUtilisateurDao.class.getClassLoader(),
						new Class[]{IGroupeUtilisateurDao.class},
						new DaoHandler<GroupeUtilisateurDao>(GroupeUtilisateurDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return groupeUtilisateurDao;
	}

	private static IJustificatifDao justificatifDao;
	public static IJustificatifDao getJustificatifDao() {
		if(justificatifDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					justificatifDao=(IJustificatifDao)Proxy.newProxyInstance(
						IJustificatifDao.class.getClassLoader(),
						new Class[]{IJustificatifDao.class},
						new DaoHandler<JustificatifDaoMock>(JustificatifDaoMock.class));
				} else {
					justificatifDao=(IJustificatifDao)Proxy.newProxyInstance(
						IJustificatifDao.class.getClassLoader(),
						new Class[]{IJustificatifDao.class},
						new DaoHandler<JustificatifDao>(JustificatifDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return justificatifDao;
	}

	private static IModePaiementDao modePaiementDao;
	public static IModePaiementDao getModePaiementDao() {
		if(modePaiementDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					modePaiementDao=(IModePaiementDao)Proxy.newProxyInstance(
						IModePaiementDao.class.getClassLoader(),
						new Class[]{IModePaiementDao.class},
						new DaoHandler<ModePaiementDaoMock>(ModePaiementDaoMock.class));
				} else {
					modePaiementDao=(IModePaiementDao)Proxy.newProxyInstance(
						IModePaiementDao.class.getClassLoader(),
						new Class[]{IModePaiementDao.class},
						new DaoHandler<ModePaiementDao>(ModePaiementDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return modePaiementDao;
	}

	private static IMouvementDao mouvementDao;
	public static IMouvementDao getMouvementDao() {
		if(mouvementDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					mouvementDao=(IMouvementDao)Proxy.newProxyInstance(
						IMouvementDao.class.getClassLoader(),
						new Class[]{IMouvementDao.class},
						new DaoHandler<MouvementDaoMock>(MouvementDaoMock.class));
				} else {
					mouvementDao=(IMouvementDao)Proxy.newProxyInstance(
						IMouvementDao.class.getClassLoader(),
						new Class[]{IMouvementDao.class},
						new DaoHandler<MouvementDao>(MouvementDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return mouvementDao;
	}

	private static IPeriodeDao periodeDao;
	public static IPeriodeDao getPeriodeDao() {
		if(periodeDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					periodeDao=(IPeriodeDao)Proxy.newProxyInstance(
						IPeriodeDao.class.getClassLoader(),
						new Class[]{IPeriodeDao.class},
						new DaoHandler<PeriodeDaoMock>(PeriodeDaoMock.class));
				} else {
					periodeDao=(IPeriodeDao)Proxy.newProxyInstance(
						IPeriodeDao.class.getClassLoader(),
						new Class[]{IPeriodeDao.class},
						new DaoHandler<PeriodeDao>(PeriodeDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return periodeDao;
	}

	private static IRegleDao regleDao;
	public static IRegleDao getRegleDao() {
		if(regleDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					regleDao=(IRegleDao)Proxy.newProxyInstance(
						IRegleDao.class.getClassLoader(),
						new Class[]{IRegleDao.class},
						new DaoHandler<RegleDaoMock>(RegleDaoMock.class));
				} else {
					regleDao=(IRegleDao)Proxy.newProxyInstance(
						IRegleDao.class.getClassLoader(),
						new Class[]{IRegleDao.class},
						new DaoHandler<RegleDao>(RegleDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return regleDao;
	}

	private static ISoldeDao soldeDao;
	public static ISoldeDao getSoldeDao() {
		if(soldeDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					soldeDao=(ISoldeDao)Proxy.newProxyInstance(
						ISoldeDao.class.getClassLoader(),
						new Class[]{ISoldeDao.class},
						new DaoHandler<SoldeDaoMock>(SoldeDaoMock.class));
				} else {
					soldeDao=(ISoldeDao)Proxy.newProxyInstance(
						ISoldeDao.class.getClassLoader(),
						new Class[]{ISoldeDao.class},
						new DaoHandler<SoldeDao>(SoldeDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return soldeDao;
	}

	private static ITypeVentilationDao typeVentilationDao;
	public static ITypeVentilationDao getTypeVentilationDao() {
		if(typeVentilationDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					typeVentilationDao=(ITypeVentilationDao)Proxy.newProxyInstance(
						ITypeVentilationDao.class.getClassLoader(),
						new Class[]{ITypeVentilationDao.class},
						new DaoHandler<TypeVentilationDaoMock>(TypeVentilationDaoMock.class));
				} else {
					typeVentilationDao=(ITypeVentilationDao)Proxy.newProxyInstance(
						ITypeVentilationDao.class.getClassLoader(),
						new Class[]{ITypeVentilationDao.class},
						new DaoHandler<TypeVentilationDao>(TypeVentilationDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return typeVentilationDao;
	}

	private static IUtilisateurDao utilisateurDao;
	public static IUtilisateurDao getUtilisateurDao() {
		if(utilisateurDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					utilisateurDao=(IUtilisateurDao)Proxy.newProxyInstance(
						IUtilisateurDao.class.getClassLoader(),
						new Class[]{IUtilisateurDao.class},
						new DaoHandler<UtilisateurDaoMock>(UtilisateurDaoMock.class));
				} else {
					utilisateurDao=(IUtilisateurDao)Proxy.newProxyInstance(
						IUtilisateurDao.class.getClassLoader(),
						new Class[]{IUtilisateurDao.class},
						new DaoHandler<UtilisateurDao>(UtilisateurDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return utilisateurDao;
	}

	private static IVentilationDao ventilationDao;
	public static IVentilationDao getVentilationDao() {
		if(ventilationDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					ventilationDao=(IVentilationDao)Proxy.newProxyInstance(
						IVentilationDao.class.getClassLoader(),
						new Class[]{IVentilationDao.class},
						new DaoHandler<VentilationDaoMock>(VentilationDaoMock.class));
				} else {
					ventilationDao=(IVentilationDao)Proxy.newProxyInstance(
						IVentilationDao.class.getClassLoader(),
						new Class[]{IVentilationDao.class},
						new DaoHandler<VentilationDao>(VentilationDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return ventilationDao;
	}

	private static IVersionDao versionDao;
	public static IVersionDao getVersionDao() {
		if(versionDao==null) {
			try {
				if(ConfEnv.getAppDaoMock()) {
					versionDao=(IVersionDao)Proxy.newProxyInstance(
						IVersionDao.class.getClassLoader(),
						new Class[]{IVersionDao.class},
						new DaoHandler<VersionDaoMock>(VersionDaoMock.class));
				} else {
					versionDao=(IVersionDao)Proxy.newProxyInstance(
						IVersionDao.class.getClassLoader(),
						new Class[]{IVersionDao.class},
						new DaoHandler<VersionDao>(VersionDao.class));
				}
			} catch (IllegalArgumentException | DaoException | IOException e) {
				Loggeur.error(e);
			}
		}
		return versionDao;
	}

}
