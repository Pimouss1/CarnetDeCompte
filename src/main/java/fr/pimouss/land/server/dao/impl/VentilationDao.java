package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IVentilationDao;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationDao extends AbstractDao<VentilationDo> implements IVentilationDao {

	/*---------------------------------------------------------------------------------------------*/
	public VentilationDao() {
		super(VentilationDo.class,"Ventilation_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<VentilationDo> getByIdMvt(Long idMvt) throws DaoException {
		return getListWithCriteria("idMouvement="+idMvt);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<VentilationDo> getByIdTypeVentilation(Long idTypeVentil) throws DaoException {
		return getListWithCriteria("idTypeVentilation="+idTypeVentil);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteByIdMvt(Long idMvt) throws DaoException {
		executeUpdate("delete from Ventilation_G where idMouvement="+idMvt);
	}

	/*---------------------------------------------------------------------------------------------*/
}
