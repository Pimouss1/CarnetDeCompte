package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IJustificatifDao extends IAbstractDao<JustificatifDo> {
	List<JustificatifDo> getByIdMvt(Long idMvt) throws DaoException;
	void deleteByIdMvt(Long idMvt) throws DaoException;
	List<JustificatifDo> getJustifWithSpecialCharacteres() throws DaoException;
}
