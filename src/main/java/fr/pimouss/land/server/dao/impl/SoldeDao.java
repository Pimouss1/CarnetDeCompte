package fr.pimouss.land.server.dao.impl;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.dao.itf.ISoldeDao;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeDao extends AbstractDao<SoldeDo> implements ISoldeDao {

	/*---------------------------------------------------------------------------------------------*/
	public SoldeDao() {
		super(SoldeDo.class,"Solde_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public SoldeDo getLastSolde(Long idCompte) throws DaoException {
		String sqlQuery="SELECT * FROM Solde_G WHERE date=(SELECT MAX(date) FROM Solde_G WHERE idCompte="+idCompte+") AND idCompte="+idCompte;
		return getObject(sqlQuery);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public SoldeDo getFirstSolde(Long idCompte) throws DaoException {
		String sqlQuery="SELECT * FROM Solde_G WHERE date=(SELECT MIN(date) FROM Solde_G WHERE idCompte="+idCompte+") AND idCompte="+idCompte;
		return getObject(sqlQuery);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void razSolde() throws DaoException{
		for(CompteDo compte:DaoDelegate.getCompteDao().getAll()){
			SoldeDo firstSolde=getFirstSolde(compte.getRowid());
			executeUpdate("DELETE FROM Solde_G WHERE date<>"+firstSolde.getDate()+" AND idCompte="+firstSolde.getIdCompte());
		}
		executeUpdate("DELETE FROM Solde_G WHERE idCompte=0");
		executeUpdate("UPDATE Mouvement_G SET solde=0");
	}

	/*---------------------------------------------------------------------------------------------*/
}
