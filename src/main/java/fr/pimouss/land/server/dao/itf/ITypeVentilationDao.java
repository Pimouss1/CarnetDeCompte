package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface ITypeVentilationDao extends IAbstractDao<TypeVentilationDo> {
	List<TypeVentilationDo> getByType(String typeMouvement) throws DaoException;
}
