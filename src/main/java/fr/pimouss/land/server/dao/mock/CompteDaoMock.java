package fr.pimouss.land.server.dao.mock;

import fr.pimouss.land.server.dao.itf.ICompteDao;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class CompteDaoMock extends AbstractDaoMock<CompteDo> implements ICompteDao {

	/*---------------------------------------------------------------------------------------------*/
	public CompteDaoMock() throws DaoException {
		super();
		save(new CompteDo(1,"Compte courant"));
		save(new CompteDo(2,"Compte épargne"));
	}

	/*---------------------------------------------------------------------------------------------*/
}
