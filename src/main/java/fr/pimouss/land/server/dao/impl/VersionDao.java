package fr.pimouss.land.server.dao.impl;

import fr.pimouss.land.server.dao.itf.IVersionDao;
import fr.pimouss.land.shared.dao.VersionDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VersionDao extends AbstractDao<VersionDo> implements IVersionDao {

	/*---------------------------------------------------------------------------------------------*/
	public VersionDao() {
		super(VersionDo.class,"Version_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public VersionDo getLastVersion() throws DaoException {
		String sqlQuery="SELECT * FROM Version_G WHERE date=(SELECT MAX(date) FROM Version_G)";
		return getObject(sqlQuery);
	}

	/*---------------------------------------------------------------------------------------------*/
}
