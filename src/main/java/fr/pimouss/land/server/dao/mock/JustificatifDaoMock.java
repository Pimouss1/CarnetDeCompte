package fr.pimouss.land.server.dao.mock;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IJustificatifDao;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class JustificatifDaoMock extends AbstractDaoMock<JustificatifDo> implements IJustificatifDao {

	/*---------------------------------------------------------------------------------------------*/
	public JustificatifDaoMock() {
		super();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<JustificatifDo> getByIdMvt(final Long idMvt) throws DaoException {
		return getList(new Condition<JustificatifDo>() {
			@Override
			public boolean condition(JustificatifDo elt) {
				return elt.getIdMouvement()==idMvt;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteByIdMvt(final Long idMvt) throws DaoException {
		for(JustificatifDo elt:getAll()) {
			if(elt.getIdMouvement()==idMvt) {
				delete(elt);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<JustificatifDo> getJustifWithSpecialCharacteres() throws DaoException {
		return null;
	}

	/*---------------------------------------------------------------------------------------------*/
}
