package fr.pimouss.land.server.dao.itf;

import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.util.dao.IAbstractDao;

public interface ICompteDao extends IAbstractDao<CompteDo> {
}
