package fr.pimouss.land.server.dao.mock;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import fr.pimouss.land.server.dao.itf.ISoldeDao;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;
import fr.pimouss.util.date.UtilDate;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeDaoMock extends AbstractDaoMock<SoldeDo> implements ISoldeDao {
	private static DateFormat df=new SimpleDateFormat("dd/MM/yyyy");

	/*---------------------------------------------------------------------------------------------*/
	public SoldeDaoMock() throws DaoException, ParseException {
		super();
		save(new SoldeDo(1, 1, UtilDate.setTimeMinuit(df.parse("01/01/2017").getTime()), new BigDecimal(0)));
		save(new SoldeDo(2, 2, UtilDate.setTimeMinuit(df.parse("01/01/2017").getTime()), new BigDecimal(1000)));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public SoldeDo getLastSolde(final Long idCompte) throws DaoException {
		SoldeDo result=null;
		long maxDate=0;
		for(SoldeDo elt:getAll()) {
			if(elt.getIdCompte()==idCompte && elt.getDate()>maxDate) {
				result=elt;
				maxDate=elt.getDate();
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public SoldeDo getFirstSolde(final Long idCompte) throws DaoException {
		SoldeDo result=null;
		long minDate=System.currentTimeMillis();
		for(SoldeDo elt:getAll()) {
			if(elt.getIdCompte()==idCompte && elt.getDate()<minDate) {
				result=elt;
				minDate=elt.getDate();
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void razSolde() throws DaoException{
		// Compliqué sans SQL...
//		for(SoldeDo elt:getAll()) {
//			
//			if(elt.getIdCompte()==0) {
//				delete(elt);
//			}
//		}
//		for(MouvementDo elt:DaoDelegate.getMouvementDao().getAll()) {
//			elt.setSolde(false);
//			DaoDelegate.getMouvementDao().save(elt);
//		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
