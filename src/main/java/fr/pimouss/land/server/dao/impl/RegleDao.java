package fr.pimouss.land.server.dao.impl;

import fr.pimouss.land.server.dao.itf.IRegleDao;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleDao extends AbstractDao<RegleDo> implements IRegleDao {

	/*---------------------------------------------------------------------------------------------*/
	public RegleDao() {
		super(RegleDo.class,"Regle_G");
	}

	/*---------------------------------------------------------------------------------------------*/
}
