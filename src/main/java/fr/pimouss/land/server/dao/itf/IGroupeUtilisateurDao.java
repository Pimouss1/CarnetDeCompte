package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface IGroupeUtilisateurDao extends IAbstractDao<GroupeUtilisateurDo> {
	List<GroupeUtilisateurDo> getGroupeByUser(Long idUser) throws DaoException;
}
