package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IModePaiementDao;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ModePaiementDao extends AbstractDao<ModePaiementDo> implements IModePaiementDao {

	/*---------------------------------------------------------------------------------------------*/
	public ModePaiementDao() {
		super(ModePaiementDo.class,"ModePaiement_R");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getByType(String typeMouvement) throws DaoException{
		return getListWithCriteria("typeMouvement='"+typeMouvement+"'");
	}

	/*---------------------------------------------------------------------------------------------*/
}
