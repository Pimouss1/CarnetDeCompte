package fr.pimouss.land.server.dao.mock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.pimouss.land.server.dao.itf.IUtilisateurDao;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.UtilException;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilisateurDaoMock extends AbstractDaoMock<UtilisateurDo> implements IUtilisateurDao {

	/*---------------------------------------------------------------------------------------------*/
	public UtilisateurDaoMock() throws DaoException {
		super();
		save(new UtilisateurDo(1,"NOM Administrateur","Prénom Administrateur","login1","password1","a@a.com",true,""));
		save(new UtilisateurDo(2,"NOM Contributeur","Prénom Contributeur","login2","password2","b@b.com",true,""));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<UtilisateurDo> getUtilisateurInGroupe(final Long idGroupe) throws DaoException{
		List<UtilisateurDo> result=new ArrayList<>();
		if(idGroupe==GroupeUtilisateurDo.ADMINISTRATEUR) {
			result.add(getById(1l));
		}
		if(idGroupe==GroupeUtilisateurDo.TRESORIER) {
			result.add(getById(1l));
		}
		if(idGroupe==GroupeUtilisateurDo.CONTRIBUTEUR) {
			result.add(getById(1l));
			result.add(getById(2l));
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isUtilisateurInGroupe(final Long idUser, final Long idGroupe) throws DaoException{
		return (idUser==1 || idUser==2 && idGroupe==GroupeUtilisateurDo.CONTRIBUTEUR);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByLoginAndPassword(final String login, final String password) throws DaoException, IOException, UtilException {
		if (isStrKo(login) || isStrKo(password)) {
			return null;
		}
		for(UtilisateurDo elt:getAll()) {
			if(elt.getLogin().equals(login) && elt.getPassword().equals(password)
				|| elt.getLogin().equals(login) && ConfEnv.getAppSuperPassword().equals(password)) {
				return elt;
			}
		}
		return null;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByToken(final String token) throws DaoException {
		if (isStrKo(token)) {
			return null;
		}
		for(UtilisateurDo elt:getAll()) {
			if(elt.getToken().equals(token)) {
				return elt;
			}
		}
		return null;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo getUtilisateurByMail(final String mail) throws DaoException{
		return getObject(new Condition<UtilisateurDo>() {
			@Override
			public boolean condition(UtilisateurDo elt) {
				return elt.getMail().equals(mail);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	private boolean isStrKo(final String str){
		return str==null || "".equals(str) || str.indexOf('\'') != -1 || str.indexOf(' ') != -1;
	}

	/*---------------------------------------------------------------------------------------------*/
}
