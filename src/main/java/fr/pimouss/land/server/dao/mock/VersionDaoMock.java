package fr.pimouss.land.server.dao.mock;

import fr.pimouss.land.server.dao.itf.IVersionDao;
import fr.pimouss.land.shared.dao.VersionDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VersionDaoMock extends AbstractDaoMock<VersionDo> implements IVersionDao {

	/*---------------------------------------------------------------------------------------------*/
	public VersionDaoMock() {
		super();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public VersionDo getLastVersion() throws DaoException {
		return new VersionDo(1, System.currentTimeMillis(), "Mock");
	}

	/*---------------------------------------------------------------------------------------------*/
}
