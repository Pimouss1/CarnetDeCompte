package fr.pimouss.land.server.dao.mock;

import java.util.List;

import fr.pimouss.land.server.dao.itf.ITypeVentilationDao;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class TypeVentilationDaoMock extends AbstractDaoMock<TypeVentilationDo> implements ITypeVentilationDao {

	/*---------------------------------------------------------------------------------------------*/
	public TypeVentilationDaoMock() throws DaoException {
		super();
		save(new TypeVentilationDo(1, MouvementDo.DEPENSE, "CatD1 - TypeVentil1", true));
		save(new TypeVentilationDo(2, MouvementDo.RECETTE, "CatR1 - TypeVentil1", true));
		save(new TypeVentilationDo(3, MouvementDo.DEPENSE, "CatD2 - TypeVentil2", true));
		save(new TypeVentilationDo(4, MouvementDo.RECETTE, "CatR2 - TypeVentil2", true));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getByType(final String typeMouvement) throws DaoException{
		return getList(new Condition<TypeVentilationDo>() {
			@Override
			public boolean condition(TypeVentilationDo elt) {
				return elt.getTypeMouvement().equals(typeMouvement) && elt.isActif();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
