package fr.pimouss.land.server.dao.impl;

import fr.pimouss.land.server.dao.itf.ICompteDao;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class CompteDao extends AbstractDao<CompteDo> implements ICompteDao {

	/*---------------------------------------------------------------------------------------------*/
	public CompteDao() {
		super(CompteDo.class,"Compte_R");
	}

	/*---------------------------------------------------------------------------------------------*/
}
