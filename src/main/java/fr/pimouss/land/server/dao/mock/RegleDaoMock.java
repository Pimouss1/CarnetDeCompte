package fr.pimouss.land.server.dao.mock;

import fr.pimouss.land.server.dao.itf.IRegleDao;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleDaoMock extends AbstractDaoMock<RegleDo> implements IRegleDao {

	/*---------------------------------------------------------------------------------------------*/
	public RegleDaoMock() {
		super();
	}

	/*---------------------------------------------------------------------------------------------*/
}
