package fr.pimouss.land.server.dao.impl;

import java.util.List;

import fr.pimouss.land.server.dao.itf.IMouvementDao;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementDao extends AbstractDao<MouvementDo> implements IMouvementDao {
	private static final long UNE_JOURNEE=24l*60l*60l*1000l;

	/*---------------------------------------------------------------------------------------------*/
	public MouvementDao() {
		super(MouvementDo.class,"Mouvement_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getByTypeByDate(String type, Long beginDate, Long endDate, Long idCompte) throws DaoException {
		return getListWithCriteria("typeMouvement='"+type+"' AND dateFacturation>="+beginDate+" AND dateFacturation<"+(endDate+UNE_JOURNEE)+" AND idCompte="+idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getBilanByTypeByDate(String type, Long beginDate, Long endDate, Long idCompte) throws DaoException {
		return getListWithCriteria("typeMouvement='"+type+"' AND dateFacturation>="+beginDate+" AND dateFacturation<"+(endDate+UNE_JOURNEE)+" AND idCompte="+idCompte+" AND horsBilan=0");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public Integer getMaxNumPiece(Integer annee) throws DaoException{
		return (Integer)getOneObject("select max(numPiece) from Mouvement_G where numPiece like '"+annee+"%'");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isNumChequeExist(Long idMvt, Integer numCheque) throws DaoException{
		return getOneObject("select 1 from Mouvement_G where numCheque="+numCheque+" AND rowid!="+idMvt)!=null;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotSoldeBeforeToday(Long idCompte) throws DaoException{
		return getListWithCriteria("solde=0 AND dateFacturation<"+System.currentTimeMillis()+" AND idCompte="+idCompte+" AND previsionnel=0");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotSolde(Long idCompte) throws DaoException{
		return getListWithCriteria("solde=0 AND idCompte="+idCompte+" AND previsionnel=0");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getSolde(Long idCompte) throws DaoException{
		return getListWithCriteria("solde=1 AND dateFacturation<"+System.currentTimeMillis()+" AND idCompte="+idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotRapproch(Long idCompte) throws DaoException{
		return getListWithCriteria("rapprochementBancaire=0 AND dateFacturation<"+System.currentTimeMillis()+" AND idCompte="+idCompte+" AND previsionnel=0 ORDER BY dateFacturation");
	}

	/*---------------------------------------------------------------------------------------------*/
}
