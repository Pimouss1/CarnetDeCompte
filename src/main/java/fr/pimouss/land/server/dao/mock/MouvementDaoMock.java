package fr.pimouss.land.server.dao.mock;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import fr.pimouss.land.server.dao.itf.IMouvementDao;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;
import fr.pimouss.util.date.UtilDate;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementDaoMock extends AbstractDaoMock<MouvementDo> implements IMouvementDao {
	private static DateFormat df=new SimpleDateFormat("dd/MM/yyyy");

	/*---------------------------------------------------------------------------------------------*/
	public MouvementDaoMock() throws DaoException, ParseException {
		super();
		save(new MouvementDo(1l, 1l, MouvementDo.DEPENSE, 0, UtilDate.setTimeMinuit(df.parse("01/01/2018").getTime()), "libelle D1", "objet D1",
				1l, 0, new BigDecimal(123.45), 20180001, false, false, true, false, false));
		save(new MouvementDo(2l, 1l, MouvementDo.RECETTE, 0, UtilDate.setTimeMinuit(df.parse("02/01/2018").getTime()), "libelle R1", "objet R1",
				2l, 0, new BigDecimal(789.45), 20180002, false, false, true, false, false));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getByTypeByDate(final String type, final Long beginDate, final Long endDate, final Long idCompte) throws DaoException {
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return elt.getTypeMouvement().equals(type) && elt.getDateFacturation()>=beginDate && elt.getDateFacturation()<endDate && elt.getIdCompte()==idCompte;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getBilanByTypeByDate(final String type, final Long beginDate, final Long endDate, final Long idCompte) throws DaoException {
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return elt.getTypeMouvement().equals(type) && elt.getDateFacturation()>=beginDate && elt.getDateFacturation()<endDate && elt.getIdCompte()==idCompte && !elt.isHorsBilan();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public Integer getMaxNumPiece(final Integer annee) throws DaoException{
		Integer result=0;
		for(MouvementDo elt:getAll()) {
			if(elt.getNumPiece()>result && elt.getNumPiece()/10000==annee.intValue()) {
				result=elt.getNumPiece();
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isNumChequeExist(final Long idMvt, final Integer numCheque) throws DaoException{
		boolean result=false;
		for(MouvementDo elt:getAll()) {
			if(elt.getNumCheque()==numCheque && elt.getRowid()!=idMvt) {
				result=true;
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotSoldeBeforeToday(final Long idCompte) throws DaoException{
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return !elt.isSolde() && elt.getDateFacturation()<System.currentTimeMillis() && elt.getIdCompte()==idCompte && !elt.isPrevisionnel();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotSolde(final Long idCompte) throws DaoException{
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return !elt.isSolde() && elt.getIdCompte()==idCompte && !elt.isPrevisionnel();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getSolde(final Long idCompte) throws DaoException{
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return elt.isSolde() && elt.getDateFacturation()<System.currentTimeMillis() && elt.getIdCompte()==idCompte;
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getNotRapproch(final Long idCompte) throws DaoException{
		return getList(new Condition<MouvementDo>() {
			@Override
			public boolean condition(MouvementDo elt) {
				return !elt.isRapprochementBancaire() && elt.getDateFacturation()<System.currentTimeMillis() && elt.getIdCompte()==idCompte && !elt.isPrevisionnel();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
