package fr.pimouss.land.server.dao.mock;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import fr.pimouss.land.server.dao.itf.IPeriodeDao;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;
import fr.pimouss.util.date.UtilDate;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PeriodeDaoMock extends AbstractDaoMock<PeriodeDo> implements IPeriodeDao {
	private static DateFormat df=new SimpleDateFormat("dd/MM/yyyy");

	/*---------------------------------------------------------------------------------------------*/
	public PeriodeDaoMock() throws DaoException, ParseException {
		super();
		save(new PeriodeDo(1, UtilDate.setTimeMinuit(df.parse("01/01/2017").getTime()), UtilDate.setTimeMinuit(df.parse("31/12/2017").getTime()), "2017", true));
		save(new PeriodeDo(2, UtilDate.setTimeMinuit(df.parse("01/01/2018").getTime()), UtilDate.setTimeMinuit(df.parse("31/12/2018").getTime()), "2018", false));
		save(new PeriodeDo(3, UtilDate.setTimeMinuit(df.parse("01/01/2019").getTime()), UtilDate.setTimeMinuit(df.parse("31/12/2019").getTime()), "2019", false));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo getByDate(final Long date) throws DaoException{
		return getObject(new Condition<PeriodeDo>() {
			@Override
			public boolean condition(PeriodeDo elt) {
				return elt.getDateDebut()<=date && date<elt.getDateFin();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
