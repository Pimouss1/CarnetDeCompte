package fr.pimouss.land.server.dao.impl;

import fr.pimouss.land.server.dao.itf.IPeriodeDao;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PeriodeDao extends AbstractDao<PeriodeDo> implements IPeriodeDao {
	private static final long UNE_JOURNEE=24l*60l*60l*1000l;

	/*---------------------------------------------------------------------------------------------*/
	public PeriodeDao() {
		super(PeriodeDo.class,"Periode_G");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo getByDate(Long date) throws DaoException{
		return getObjectWithCriteria("dateDebut<="+date+" AND "+(date-UNE_JOURNEE)+"<dateFin");
	}

	/*---------------------------------------------------------------------------------------------*/
}
