package fr.pimouss.land.server.dao.itf;

import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IPeriodeDao extends IAbstractDao<PeriodeDo> {
	PeriodeDo getByDate(Long date) throws DaoException;
}
