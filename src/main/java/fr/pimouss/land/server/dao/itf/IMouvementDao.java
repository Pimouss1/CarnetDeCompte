package fr.pimouss.land.server.dao.itf;

import java.util.List;

import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.IAbstractDao;

public interface IMouvementDao extends IAbstractDao<MouvementDo> {
	List<MouvementDo> getByTypeByDate(String type, Long beginDate, Long endDate, Long idCompte) throws DaoException;
	List<MouvementDo> getBilanByTypeByDate(String type, Long beginDate, Long endDate, Long idCompte) throws DaoException;
	Integer getMaxNumPiece(Integer annee) throws DaoException;
	List<MouvementDo> getNotSoldeBeforeToday(Long idCompte) throws DaoException;
	List<MouvementDo> getNotSolde(Long idCompte) throws DaoException;
	List<MouvementDo> getNotRapproch(Long idCompte) throws DaoException;
	List<MouvementDo> getSolde(Long idCompte) throws DaoException;
	boolean isNumChequeExist(Long idMvt, Integer numCheque) throws DaoException;
}
