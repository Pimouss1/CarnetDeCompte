package fr.pimouss.land.server.dao.itf;

import java.io.IOException;
import java.util.List;

import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.UtilException;
import fr.pimouss.util.dao.IAbstractDao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface IUtilisateurDao extends IAbstractDao<UtilisateurDo> {
	List<UtilisateurDo> getUtilisateurInGroupe(Long idGroupe) throws DaoException;
	UtilisateurDo getUtilisateurByLoginAndPassword(String login, String password) throws DaoException, UtilException, IOException;
	UtilisateurDo getUtilisateurByMail(String mail) throws DaoException;
	UtilisateurDo getUtilisateurByToken(String token) throws DaoException;
	boolean isUtilisateurInGroupe(Long idUser, Long idGroupe) throws DaoException;
}
