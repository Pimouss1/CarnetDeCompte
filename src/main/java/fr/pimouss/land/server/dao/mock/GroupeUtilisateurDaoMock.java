package fr.pimouss.land.server.dao.mock;

import java.util.ArrayList;
import java.util.List;

import fr.pimouss.land.server.dao.itf.IGroupeUtilisateurDao;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.dao.AbstractDaoMock;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GroupeUtilisateurDaoMock extends AbstractDaoMock<GroupeUtilisateurDo> implements IGroupeUtilisateurDao {

	/*---------------------------------------------------------------------------------------------*/
	public GroupeUtilisateurDaoMock() throws DaoException {
		super();
		save(new GroupeUtilisateurDo(GroupeUtilisateurDo.ADMINISTRATEUR, "Administrateur"));
		save(new GroupeUtilisateurDo(GroupeUtilisateurDo.TRESORIER, "Trésorier"));
		save(new GroupeUtilisateurDo(GroupeUtilisateurDo.CONTRIBUTEUR, "Contributeur"));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<GroupeUtilisateurDo> getGroupeByUser(final Long idUser) throws DaoException{
		List<GroupeUtilisateurDo> result=new ArrayList<>();
		if(idUser==1) {
			result.add(getById(GroupeUtilisateurDo.ADMINISTRATEUR));
			result.add(getById(GroupeUtilisateurDo.TRESORIER));
			result.add(getById(GroupeUtilisateurDo.CONTRIBUTEUR));
		}else {
			result.add(getById(GroupeUtilisateurDo.CONTRIBUTEUR));
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
}
