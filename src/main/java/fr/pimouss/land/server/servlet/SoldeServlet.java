package fr.pimouss.land.server.servlet;

import java.math.BigDecimal;
import java.util.List;

import fr.pimouss.land.client.controller.SoldeController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = -8083579843285077983L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal getSoldeTheorique(long idUser, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getSoldeService().calculSoldeTheorique(false,idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal getSoldeAnticipe(long idUser, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getSoldeService().calculSoldeAnticipe(idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal getSoldeBancaire(long idUser, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getSoldeService().calculSoldeBancaire(idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementDo> getMvtNotRapproch(long idUser, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return DaoDelegate.getMouvementDao().getNotRapproch(idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal rapprochMvt(long idUser, long idMvt, long datePaiement, boolean rapproch, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		trace("rapprochMvt", null);
		return ServiceDelegate.getSoldeService().rapprochMvt(idMvt, datePaiement, rapproch, idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<CompteDo> getAllCompte() throws DaoException {
		return DaoDelegate.getCompteDao().getAll();
	}

	/*---------------------------------------------------------------------------------------------*/
}
