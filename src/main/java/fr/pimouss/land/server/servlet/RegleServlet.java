package fr.pimouss.land.server.servlet;

import java.util.List;

import fr.pimouss.land.client.controller.RegleController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = 1699567113109787464L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<RegleDo> getAllRegle() throws DaoException {
		return DaoDelegate.getRegleDao().getAll();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public RegleDo saveRegle(long idUser, RegleDo regle) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		trace("saveRegle", idUser);
		return DaoDelegate.getRegleDao().save(regle);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteRegle(long idUser, long idRegle) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		trace("deleteRegle", idUser);
		DaoDelegate.getRegleDao().deleteById(idRegle);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getAllModePaiementDepense() throws DaoException {
		return DaoDelegate.getModePaiementDao().getByType(MouvementDo.DEPENSE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getAllTypeVentilationDepense() throws DaoException {
		return DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.DEPENSE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getAllModePaiementRecette() throws DaoException {
		return DaoDelegate.getModePaiementDao().getByType(MouvementDo.RECETTE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getAllTypeVentilationRecette() throws DaoException {
		return DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.RECETTE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<CompteDo> getAllCompte() throws DaoException {
		return DaoDelegate.getCompteDao().getAll();
	}

	/*---------------------------------------------------------------------------------------------*/
}
