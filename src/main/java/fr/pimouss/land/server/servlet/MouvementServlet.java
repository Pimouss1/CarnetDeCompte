package fr.pimouss.land.server.servlet;

import java.io.IOException;
import java.util.List;

import fr.pimouss.land.client.controller.MouvementController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.MouvementBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = -957691633558377463L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getAllModePaiementDepense() throws DaoException {
		return DaoDelegate.getModePaiementDao().getByType(MouvementDo.DEPENSE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getAllTypeVentilationDepense() throws DaoException {
		return DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.DEPENSE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<ModePaiementDo> getAllModePaiementRecette() throws DaoException {
		return DaoDelegate.getModePaiementDao().getByType(MouvementDo.RECETTE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getAllTypeVentilationRecette() throws DaoException {
		return DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.RECETTE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public MouvementBo saveMvt(long idUser, MouvementBo mvt) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		trace("saveMvt", null);
		return ServiceDelegate.getMouvementService().saveMvt(mvt);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteMvt(long idUser, long idMouvement, boolean force) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		trace("deleteMvt", null);
		ServiceDelegate.getMouvementService().deleteMvt(idMouvement,force);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteJustificatif(long idUser, long idJustificatif) throws ApplicationException, IOException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		trace("deleteJustificatif", null);
		ServiceDelegate.getMouvementService().deleteJustificatif(idJustificatif);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public MouvementBo loadJustificatif(long idUser, MouvementBo mvt) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getMouvementService().loadJustificatif(mvt);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementBo> getDepenseByPeriode(long idUser, long idPeriode, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getMouvementService().getDepenseByPeriode(idPeriode,idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementBo> getRecetteByPeriode(long idUser, long idPeriode, long idCompte) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getMouvementService().getRecetteByPeriode(idPeriode,idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo getPeriodeByDate(long date) throws DaoException {
		return DaoDelegate.getPeriodeDao().getByDate(date);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<PeriodeDo> getAllPeriode() throws DaoException {
		return DaoDelegate.getPeriodeDao().getAllSorted();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<CompteDo> getAllCompte() throws DaoException {
		return DaoDelegate.getCompteDao().getAll();
	}

	/*---------------------------------------------------------------------------------------------*/
}
