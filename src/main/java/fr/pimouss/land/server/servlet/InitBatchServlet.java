package fr.pimouss.land.server.servlet;

import java.util.TimeZone;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import fr.pimouss.land.server.service.impl.CalculSoldeTask;
import fr.pimouss.land.server.service.impl.CalculVentilTask;
import fr.pimouss.land.server.service.impl.PingDataBaseTask;
import fr.pimouss.land.server.service.impl.RegleTask;
import fr.pimouss.land.server.service.impl.SaveDataBaseTask;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class InitBatchServlet extends HttpServlet {
	private static final long serialVersionUID = 8438287812761441258L;
	private static SaveDataBaseTask saveDataBaseTask;
	private static PingDataBaseTask pingDataBaseTask;
	private static CalculSoldeTask calculSoldeTask;
	private static CalculVentilTask calculVentilTask;
	private static RegleTask regleTask;

	/*---------------------------------------------------------------------------------------------*/
	public void init(ServletConfig arg0) throws ServletException {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
		try{
			//05:19
			pingDataBaseTask=new PingDataBaseTask(5,19);
			//05:20
			saveDataBaseTask=new SaveDataBaseTask(5,20);
			//05:30
			calculSoldeTask=new CalculSoldeTask(5,30);
			//05:35
			calculVentilTask=new CalculVentilTask(5,35);
			//05:40
			regleTask=new RegleTask(5,40);

			super.init(arg0);
		}catch(Exception e){
   			Loggeur.error(e);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public void destroy() {
		saveDataBaseTask.stop();
		pingDataBaseTask.stop();
		calculSoldeTask.stop();
		calculVentilTask.stop();
		regleTask.stop();

		Loggeur.close();
		super.destroy();
	}

	/*---------------------------------------------------------------------------------------------*/
	// Utile au forçage
	public static SaveDataBaseTask getSaveDataBaseTask() {
		return saveDataBaseTask;
	}
	public static PingDataBaseTask getPingDataBaseTask() {
		return pingDataBaseTask;
	}
	public static CalculSoldeTask getCalculSoldeTask() {
		return calculSoldeTask;
	}
	public static CalculVentilTask getCalculVentilTask() {
		return calculVentilTask;
	}

	/*---------------------------------------------------------------------------------------------*/
}
