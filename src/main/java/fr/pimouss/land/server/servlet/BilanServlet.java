package fr.pimouss.land.server.servlet;

import java.util.List;

import fr.pimouss.land.client.controller.BilanController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.BilanBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BilanServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = -8083579843285077983L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanDepenseByPeriode(long idPeriode) throws DaoException {
		return ServiceDelegate.getBilanService().getBilanDepenseByPeriode(idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanRecetteByPeriode(long idPeriode) throws DaoException {
		return ServiceDelegate.getBilanService().getBilanRecetteByPeriode(idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanGroupeDepenseByPeriode(long idPeriode) throws ApplicationException {
		return ServiceDelegate.getBilanService().getBilanGroupeByTypeMvtByPeriode(MouvementDo.DEPENSE, idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanGroupeRecetteByPeriode(long idPeriode) throws ApplicationException {
		return ServiceDelegate.getBilanService().getBilanGroupeByTypeMvtByPeriode(MouvementDo.RECETTE, idPeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<PeriodeDo> getAllPeriode() throws DaoException {
		return DaoDelegate.getPeriodeDao().getAllSorted();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo getPeriodeByDate(long date) throws DaoException {
		return DaoDelegate.getPeriodeDao().getByDate(date);
	}

	/*---------------------------------------------------------------------------------------------*/
}
