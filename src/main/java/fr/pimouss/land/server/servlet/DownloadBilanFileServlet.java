package fr.pimouss.land.server.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.protocol.HTTP;

import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DownloadBilanFileServlet extends HttpServlet {
	private static final long serialVersionUID = 2890389581201963635L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding(HTTP.UTF_8);

		try{
			long idPeriode=Long.parseLong(req.getParameter("idPeriode"));
			ByteArrayOutputStream in=ServiceDelegate.getBilanService().exportExcelHTML(idPeriode);
			ServletOutputStream out=resp.getOutputStream();
			resp.setContentType("application/x-download");
			resp.setHeader("Content-Disposition","attachment; filename=\"bilan.xls\"");

			byte[] byt=in.toByteArray();
			out.write(byt,0,byt.length);

			in.close();
			out.flush();
			out.close();
		}catch(Exception e){
			Loggeur.error(e);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
