package fr.pimouss.land.server.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.WarningException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class TracableServlet extends RemoteServiceServlet {
	private static final long serialVersionUID = 5366674451275493720L;
	private static final String ID_USER="idUser";

	/*---------------------------------------------------------------------------------------------*/
	protected void verifAutorisation(Long idGroupeRequis) throws ApplicationException {
		Long idUserSession=(Long)getThreadLocalRequest().getSession().getAttribute(ID_USER);
		verifAutorisation(idUserSession, idGroupeRequis);
	}
	protected void verifAutorisation(Long idUser, Long idGroupeRequis) throws ApplicationException {
		if(idUser==null || idUser==0 || !DaoDelegate.getUtilisateurDao().isUtilisateurInGroupe(idUser, idGroupeRequis)){
			throw new WarningException("Vous n'êtes pas autorisé à accéder à cette fonctionnalité", null);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	protected void trace(String methodName, Long idUser) {
		Long idUserSession=(Long)getThreadLocalRequest().getSession().getAttribute(ID_USER);
		if((idUserSession==null || idUserSession==0) && !(idUser==null || idUser==0)){
			getThreadLocalRequest().getSession().setAttribute(ID_USER, idUser);
			idUserSession=(Long)getThreadLocalRequest().getSession().getAttribute(ID_USER);
		}
		if(idUserSession!=null){
			ServiceDelegate.getSurveillanceService().trace(idUserSession,getThreadLocalRequest().getRemoteAddr(),this.getClass().getSimpleName(),methodName);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
