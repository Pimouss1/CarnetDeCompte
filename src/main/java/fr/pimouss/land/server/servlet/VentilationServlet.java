package fr.pimouss.land.server.servlet;

import java.util.List;

import fr.pimouss.land.client.controller.VentilationController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = -5778739933670488568L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<TypeVentilationDo> getAllTypeVentilation() throws DaoException {
		return DaoDelegate.getTypeVentilationDao().getAllSorted();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public TypeVentilationDo saveVentil(long idUser, TypeVentilationDo ventil) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		trace("saveVentil", null);
		return DaoDelegate.getTypeVentilationDao().save(ventil);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteVentil(long idUser, long idVentil) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		trace("deleteVentil", null);
		ServiceDelegate.getVentilationService().deleteTypeVentilation(idVentil);
	}

	/*---------------------------------------------------------------------------------------------*/
}
