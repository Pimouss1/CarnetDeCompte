package fr.pimouss.land.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.protocol.HTTP;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DownloadJustificatifFileServlet extends HttpServlet {
	private static final long serialVersionUID = 5603179635255725773L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding(HTTP.UTF_8);

		try{
			long idJustificatif=Long.parseLong(req.getParameter("idJustificatif"));
			JustificatifDo justificatifDo=DaoDelegate.getJustificatifDao().getById(idJustificatif);
			String filename=ConfEnv.getDocPath()+"/"+justificatifDo.getFilename();
			File f=new File(filename);

			InputStream in=new DataInputStream(new FileInputStream(f));
			ServletOutputStream out=resp.getOutputStream();
			resp.setContentType("application/x-download");
			resp.setContentLength((int)f.length());
			resp.setHeader("Content-Disposition","attachment; filename=\""+f.getName()+"\"");

			byte[] byt=new byte[1024];
			int length;
			while((length=in.read(byt))!=-1){
				out.write(byt,0,length);
			}

			in.close();
			out.flush();
			out.close();
		}catch(Exception e){
			Loggeur.error(e);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
