package fr.pimouss.land.server.servlet;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.http.protocol.HTTP;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.http.HttpUploadProxy;
import fr.pimouss.util.log.Loggeur;
import fr.pimouss.util.string.UtilString;
import gwtupload.server.exceptions.UploadActionException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UploadJustificatifFileServlet extends HttpUploadProxy {
	private static final long serialVersionUID = -2416275926360305618L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public String executeAction(HttpServletRequest req, List<FileItem> sessionFiles) throws UploadActionException {
		try{
			req.setCharacterEncoding(HTTP.UTF_8);
		}catch (UnsupportedEncodingException e) {
			throw new UploadActionException(e);
		}

		long idMouvement=Long.parseLong(req.getParameter("idMouvement"));

		if(!UtilString.isNullOrBlank(req.getContentType())){
			for(FileItem item:sessionFiles){
				if(!item.isFormField()){
					File destFile=null;
					try{
						MouvementDo mvt=DaoDelegate.getMouvementDao().getById(idMouvement);
						String filename=UtilString.withoutSpecialCharacter(item.getName());
						destFile=new File(URLDecoder.decode(ConfEnv.getDocPath()+"/"+String.valueOf(mvt.getNumPiece())+"_"+UtilString.withoutSpecialCharacter(filename),HTTP.UTF_8));
						item.write(destFile);
						if(destFile!=null){
							JustificatifDo justificatifDo=new JustificatifDo();
							justificatifDo.setFilename(destFile.getName());
							justificatifDo.setIdMouvement(mvt.getRowid());
							DaoDelegate.getJustificatifDao().save(justificatifDo);
							envoiRequete("DocumentService","archiveDocument",new Object[]{destFile.getAbsolutePath()});
						}
					}catch(Exception e){
						Loggeur.error(e);
					}
				}				
			}
		}else{
			Loggeur.warn("req.getContentType()="+req.getContentType());
		}

		return null;
	}

	/*---------------------------------------------------------------------------------------------*/
}
