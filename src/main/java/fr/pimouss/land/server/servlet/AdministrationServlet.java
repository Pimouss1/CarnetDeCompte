package fr.pimouss.land.server.servlet;

import java.io.IOException;
import java.util.List;

import fr.pimouss.land.client.controller.AdministrationController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.ErrorException;
import fr.pimouss.land.shared.service.SurveillanceBo;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.cst.ConfEnv;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class AdministrationServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = -957691633558377463L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLogClient() throws IOException {
		return ConfEnv.getAppLogClient();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo connect(String login, String password) throws ApplicationException, IOException {
		final UtilisateurDo result=ServiceDelegate.getConnectionService().connect(login, password);
		getThreadLocalRequest().getSession().setAttribute("idUser", result.getRowid());
		trace("connect", result.getRowid());

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo connectByToken(String token) throws ApplicationException, IOException {
		final UtilisateurDo result=ServiceDelegate.getConnectionService().connectByToken(token);
		getThreadLocalRequest().getSession().setAttribute("idUser", result.getRowid());
		trace("connectByToken", result.getRowid());

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void motDePassePerdu(String mail) throws ApplicationException, IOException {
		trace("motDePassePerdu", null);
		ServiceDelegate.getConnectionService().motDePassePerdu(mail);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void changePassword(Long idUser, String password, String newPassword) throws ApplicationException {
		trace("changePassword", idUser);
		ServiceDelegate.getConnectionService().changePassword(idUser, password, newPassword);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public String[] getNumVersion() throws ApplicationException {
		try{
			String[] result=new String[2];
			result[0]=ConfApp.getVersion();
			result[1]=DaoDelegate.getVersionDao().getLastVersion().getNumVersion();
			return result;
		}catch(IOException e){
			throw new ErrorException(e.getMessage(), e);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isUserTresorier(long idUser) throws DaoException {
		return DaoDelegate.getUtilisateurDao().isUtilisateurInGroupe(idUser, GroupeUtilisateurDo.TRESORIER);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isUserAdministrateur(long idUser) throws DaoException {
		return DaoDelegate.getUtilisateurDao().isUtilisateurInGroupe(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isUserContributeur(long idUser) throws DaoException {
		return DaoDelegate.getUtilisateurDao().isUtilisateurInGroupe(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void forceSaveDatabase(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		trace("forceSaveDataBase", null);
		InitBatchServlet.getSaveDataBaseTask().force();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void forceRAZSolde(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		trace("forceRAZSolde", null);
		DaoDelegate.getSoldeDao().razSolde();
		InitBatchServlet.getCalculSoldeTask().force();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<SurveillanceBo> getLastAction(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		return ServiceDelegate.getSurveillanceService().getLastSurveillance();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void forceVerifSolde(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		trace("forceVerifSolde", null);
		InitBatchServlet.getCalculSoldeTask().force();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void forceVerifVentil(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		trace("forceVerifVentil", null);
		InitBatchServlet.getCalculVentilTask().force();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<PeriodeDo> getAllPeriode() throws DaoException {
		return DaoDelegate.getPeriodeDao().getAllSorted();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo createPeriode(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		return ServiceDelegate.getPeriodeService().createPeriode();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<PeriodeDo> saveListePeriode(long idUser, List<PeriodeDo> listePeriode) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		return DaoDelegate.getPeriodeDao().save(listePeriode);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void verifJustif(long idUser) throws ApplicationException, IOException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		ServiceDelegate.getMouvementService().verifJustif();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void forceVerifNumPiece(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.ADMINISTRATEUR);
		ServiceDelegate.getMouvementService().verifNumPiece();
	}

	/*---------------------------------------------------------------------------------------------*/
}
