package fr.pimouss.land.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.protocol.HTTP;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.file.UtilFile;
import fr.pimouss.util.log.Loggeur;
import fr.pimouss.util.zip.UtilZIP;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DownloadAllJustificatifFileServlet extends HttpServlet {
	private static final long serialVersionUID = 5603179635255725773L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding(HTTP.UTF_8);

		try{
			long idPeriode=Long.parseLong(req.getParameter("idPeriode"));
			PeriodeDo periode=DaoDelegate.getPeriodeDao().getById(idPeriode);

			//Construction du ZIP
			String zipFilename=ConfEnv.getDocPath()+"/zip/"+periode.getNom()+".zip";
			File dir=new File(ConfEnv.getDocPath());
			if(!dir.exists() || !dir.isDirectory()){
				throw new ServletException(ConfEnv.getDocPath()+"/tmp n'existe pas ou n'est pas un répertoire");
			}
			File f=new File(zipFilename);
			UtilFile.vidangeFolder(ConfEnv.getDocPath()+"/tmp");
			UtilFile.vidangeFolder(ConfEnv.getDocPath()+"/zip");
			File[] tabFile=dir.listFiles();
			String annee=String.valueOf(ServiceDelegate.getMouvementService().getAnneeByDate(periode.getDateDebut()));
			for(int i=0;i<tabFile.length;i++){
				if(tabFile[i].isFile() && tabFile[i].getName().startsWith(annee)){
					UtilFile.copyFile(tabFile[i],ConfEnv.getDocPath()+"/tmp/"+tabFile[i].getName());
				}
			}
			UtilZIP.zipFolder(ConfEnv.getDocPath()+"/tmp", zipFilename);

			InputStream in=new DataInputStream(new FileInputStream(f));
			ServletOutputStream out=resp.getOutputStream();
			resp.setContentType("application/x-download");
			resp.setContentLength((int)f.length());
			resp.setHeader("Content-Disposition","attachment; filename=\""+periode.getNom()+".zip\"");

			byte[] byt=new byte[1024];
			int length;
			while((length=in.read(byt))!=-1){
				out.write(byt,0,length);
			}

			in.close();
			out.flush();
			out.close();
		}catch(Exception e){
			Loggeur.error(e);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
