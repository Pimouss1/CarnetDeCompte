package fr.pimouss.land.server.servlet;

import java.util.List;

import fr.pimouss.land.client.controller.BudgetController.IFace;
import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.dao.GroupeUtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.service.TabBudgetBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetServlet extends TracableServlet implements IFace{
	private static final long serialVersionUID = 7414641746881995156L;

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public TabBudgetBo[] getBudgetDepense(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getBudgetService().getBudgetDepense();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public TabBudgetBo[] getBudgetRecette(long idUser) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.CONTRIBUTEUR);
		return ServiceDelegate.getBudgetService().getBudgetRecette();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void saveBudget(long idUser, List<BudgetDo> listeBudget) throws ApplicationException {
		verifAutorisation(idUser, GroupeUtilisateurDo.TRESORIER);
		trace("saveBudget", null);
		DaoDelegate.getBudgetDao().save(listeBudget);
	}

	/*---------------------------------------------------------------------------------------------*/
}
