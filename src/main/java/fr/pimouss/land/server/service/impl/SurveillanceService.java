package fr.pimouss.land.server.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.ISurveillanceService;
import fr.pimouss.land.shared.dao.SurveillanceDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.SurveillanceBo;
import fr.pimouss.util.log.Loggeur;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SurveillanceService implements ISurveillanceService {
	private static Map<Long, SurveillanceDo> mapLastConnected=new HashMap<>();

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void trace(final Long idUtilisateur, final String ip, final String classe, final String methode){
		(new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					if(!mapLastConnected.containsKey(idUtilisateur)){
						mapLastConnected.put(idUtilisateur, new SurveillanceDo());
					}
					mapLastConnected.get(idUtilisateur).setClasse(classe);
					mapLastConnected.get(idUtilisateur).setDate(System.currentTimeMillis());
					mapLastConnected.get(idUtilisateur).setIdUtilisateur(idUtilisateur);
					mapLastConnected.get(idUtilisateur).setIp(ip);
					mapLastConnected.get(idUtilisateur).setMethode(methode);
				}catch(Exception e){
					Loggeur.error(e);
				}
			}
		})).start();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<SurveillanceBo> getLastSurveillance() throws DaoException{
		List<SurveillanceBo> result=new ArrayList<>();
		for(SurveillanceDo elt:mapLastConnected.values()){
			SurveillanceBo surveillance=new SurveillanceBo(elt);
			surveillance.setUtilisateurDo(DaoDelegate.getUtilisateurDao().getById(elt.getIdUtilisateur()));
			result.add(surveillance);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
}
