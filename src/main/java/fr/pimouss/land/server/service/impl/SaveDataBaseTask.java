package fr.pimouss.land.server.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.mail.MailService;
import fr.pimouss.util.service.AbstractTaskPlanifie;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SaveDataBaseTask extends AbstractTaskPlanifie {

	/*-------------------------------------------------------------------------*/
	public SaveDataBaseTask(int heurePlanifie, int minutePlanifie) {
		super(heurePlanifie,minutePlanifie);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void traitement() throws IOException, DaoException, AddressException, MessagingException {
		// 1 - nom du fichier SQL
		int pos1=(ConfEnv.getJdbcUrl()).lastIndexOf("/");
		int pos2=(ConfEnv.getJdbcUrl()).indexOf("?");
		String dataBaseName;
		if(pos2!=-1){
			dataBaseName=(ConfEnv.getJdbcUrl()).substring(pos1+1,pos2);
		}else{
			dataBaseName=(ConfEnv.getJdbcUrl()).substring(pos1+1);
		}
		String fileNameDestination=ConfEnv.getJdbcSaveDataBaseFolder() +
				"/" + dataBaseName +
				"-" + UtilDate.FORMAT_DATE_SYS_COMP.format(new Date()) +
				"-" + DaoDelegate.getVersionDao().getLastVersion().getNumVersion() + ".sql";

		// 2 - appel de la commande de backup
		Process p=Runtime.getRuntime().exec(
				ConfEnv.getJdbcDumpDataBaseProc()+" -u " +
				ConfEnv.getJdbcUser()+" -p" +
				ConfEnv.getJdbcPassword()+" "+dataBaseName);
		BufferedReader br=new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileNameDestination)));
		String line;
		while((line=br.readLine())!=null){
			bw.write(line+"\n");
		}
		bw.close();
		br.close();

		// 3 - envoi par mail
		new MailService("pimouss.land@gmail.com","","dataBase "+ConfApp.getApplicationName(),fileNameDestination,null);
	}

	/*-------------------------------------------------------------------------*/
}
