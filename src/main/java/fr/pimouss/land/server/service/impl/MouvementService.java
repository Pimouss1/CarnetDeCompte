package fr.pimouss.land.server.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IMouvementService;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.ErrorException;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.land.shared.service.MouvementBo;
import fr.pimouss.util.cst.ConfEnv;
import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.log.Loggeur;
import fr.pimouss.util.string.UtilString;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementService implements IMouvementService {
	public static final DecimalFormat DECIMAL_FORMAT=new DecimalFormat("#,##0.00");
	public static final DecimalFormat INTEGER_FORMAT=new DecimalFormat("#,##0");
	private static Map<Long, List<VentilationDo>> cacheVentilationByIdMvt=new HashMap<>();
	private static Map<Long, List<JustificatifDo>> cacheJustificatifByIdMvt=new HashMap<>();

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementBo> getDepenseByPeriode(long idPeriode, long idCompte) throws DaoException {
		return getByTypeMvtByPeriode(MouvementDo.DEPENSE, idPeriode, idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<MouvementBo> getRecetteByPeriode(long idPeriode, long idCompte) throws DaoException {
		return getByTypeMvtByPeriode(MouvementDo.RECETTE, idPeriode, idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> getByTypeMvtByPeriode(String typeMvt, long idPeriode, long idCompte) throws DaoException {
		PeriodeDo periode=DaoDelegate.getPeriodeDao().getById(idPeriode);
		return loadListeMvt(DaoDelegate.getMouvementDao().getByTypeByDate(typeMvt,periode.getDateDebut(),periode.getDateFin(),idCompte));
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public MouvementBo loadJustificatif(MouvementBo mvt) throws DaoException{
		mvt.setListeJustificatif(DaoDelegate.getJustificatifDao().getByIdMvt(mvt.getMouvementDo().getRowid()));
		return mvt;
	}

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> loadListeMvt(List<MouvementDo> listeMouvementDo) throws DaoException {
		List<MouvementBo> result=new ArrayList<>();
		for(MouvementDo mvtDo:listeMouvementDo){
			MouvementBo mvtBo=new MouvementBo(mvtDo);
			mvtBo.setModePaiementDo(DaoDelegate.getModePaiementDao().getById(mvtDo.getIdModePaiement()));
			if(!cacheJustificatifByIdMvt.containsKey(mvtDo.getRowid())) {
				cacheJustificatifByIdMvt.put(mvtDo.getRowid(), DaoDelegate.getJustificatifDao().getByIdMvt(mvtDo.getRowid()));
			}
			mvtBo.setListeJustificatif(cacheJustificatifByIdMvt.get(mvtDo.getRowid()));
			if(!cacheVentilationByIdMvt.containsKey(mvtDo.getRowid())) {
				cacheVentilationByIdMvt.put(mvtDo.getRowid(), DaoDelegate.getVentilationDao().getByIdMvt(mvtDo.getRowid()));
			}
			mvtBo.setListeVentilation(cacheVentilationByIdMvt.get(mvtDo.getRowid()));
			result.add(mvtBo);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public MouvementBo saveMvt(MouvementBo mvt) throws ApplicationException{
		// Vérification du numéro de chèque déjà existant
		if(mvt.getMouvementDo().getIdModePaiement()==ModePaiementDo.CHEQUE && mvt.getMouvementDo().getNumCheque()!=0){
			if(DaoDelegate.getMouvementDao().isNumChequeExist(mvt.getMouvementDo().getRowid(), mvt.getMouvementDo().getNumCheque())){
				throw new WarningException("Impossible d'enregistrer ce mouvement.\n Ce numéro de chèque a déjà été enregistré.",null);
			}
		}

		// Numéro de pièce
		if(mvt.getMouvementDo().getNumPiece()==0){
			setNumPiece(mvt.getMouvementDo());
		}else{
			int anneeNumPiece=mvt.getMouvementDo().getNumPiece()/10000;
			if(anneeNumPiece!=getAnneeByDate(mvt.getMouvementDo().getDateFacturation())){
				setNumPiece(mvt.getMouvementDo());
			}
		}

		DaoDelegate.getVentilationDao().deleteByIdMvt(mvt.getMouvementDo().getRowid());

		// Si mvt déjà soldé on le supprime du solde
		if(mvt.getMouvementDo().getRowid()!=0){
			MouvementDo mvtAvantSauvegarde=DaoDelegate.getMouvementDao().getById(mvt.getMouvementDo().getRowid());
			if(mvtAvantSauvegarde.isSolde()){
				mvt.getMouvementDo().setSolde(false);
				SoldeDo lastSolde=DaoDelegate.getSoldeDao().getLastSolde(mvt.getMouvementDo().getIdCompte());
				SoldeDo newSolde=new SoldeDo();
				newSolde.setDate(System.currentTimeMillis());
				newSolde.setIdCompte(mvt.getMouvementDo().getIdCompte());
				if(MouvementDo.DEPENSE.equals(mvtAvantSauvegarde.getTypeMouvement())){
					newSolde.setMontant(lastSolde.getMontant().add(mvtAvantSauvegarde.getMontant()));
				}else{
					newSolde.setMontant(lastSolde.getMontant().subtract(mvtAvantSauvegarde.getMontant()));
				}
				DaoDelegate.getSoldeDao().save(newSolde);
			}
		}

		// Application de règles
		mvt.setMouvementDo(DaoDelegate.getMouvementDao().save(mvt.getMouvementDo()));
		cacheVentilationByIdMvt.remove(mvt.getMouvementDo().getRowid());
		cacheJustificatifByIdMvt.remove(mvt.getMouvementDo().getRowid());
		if(!mvt.getMouvementDo().isRegleAppliquee()){
			for(RegleDo regle:DaoDelegate.getRegleDao().getAll()){
				if(regle.getIdTypeVentilationSrc()!=0){
					for(VentilationDo ventil:mvt.getListeVentilation()){
						if(ventil.getIdTypeVentilation()==regle.getIdTypeVentilationSrc()){
							createMvtByRegle(mvt.getMouvementDo().getDateFacturation(), regle);
						}
					}
				}
			}
			mvt.getMouvementDo().setRegleAppliquee(true);
			mvt.setMouvementDo(DaoDelegate.getMouvementDao().save(mvt.getMouvementDo()));
		}
		
		for(VentilationDo ventilationDo:mvt.getListeVentilation()){
			ventilationDo.setIdMouvement(mvt.getMouvementDo().getRowid());
			DaoDelegate.getVentilationDao().save(ventilationDo);
		}
		for(JustificatifDo justificatifDo:mvt.getListeJustificatif()){
			justificatifDo.setIdMouvement(mvt.getMouvementDo().getRowid());
			DaoDelegate.getJustificatifDao().save(justificatifDo);
		}

		return mvt;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void setNumPiece(MouvementDo mvt) throws ApplicationException{
		Integer maxNumPiece=DaoDelegate.getMouvementDao().getMaxNumPiece(getAnneeByDate(mvt.getDateFacturation()));
		if(maxNumPiece==null || maxNumPiece==0){
			maxNumPiece=getAnneeByDate(mvt.getDateFacturation())*10000;
		}
		mvt.setNumPiece(maxNumPiece+1);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void createMvtByRegle(long date, RegleDo regle) throws ApplicationException{
		MouvementDo newMvt=new MouvementDo();
		newMvt.setDateFacturation(date);
		newMvt.setIdCompte(regle.getIdCompte());
		newMvt.setIdModePaiement(regle.getIdModePaiement());
		newMvt.setLibelle(regle.getLibelle());
		newMvt.setMontant(regle.getMontant());
		newMvt.setHorsBilan(regle.isHorsBilan());
		Integer maxNumPiece=DaoDelegate.getMouvementDao().getMaxNumPiece(getAnneeByDate(newMvt.getDateFacturation()));
		if(maxNumPiece==null){
			maxNumPiece=getAnneeByDate(newMvt.getDateFacturation())*10000;
		}
		newMvt.setNumPiece(maxNumPiece+1);
		newMvt.setObjet(regle.getObjet());
		newMvt.setRapprochementBancaire(false);
		newMvt.setRegleAppliquee(false);
		newMvt.setSolde(false);
		newMvt.setTypeMouvement(regle.getTypeMouvementTarget());
		newMvt=DaoDelegate.getMouvementDao().save(newMvt);
		VentilationDo newVentil=new VentilationDo();
		newVentil.setIdMouvement(newMvt.getRowid());
		newVentil.setIdTypeVentilation(regle.getIdTypeVentilationTarget());
		newVentil.setMontant(regle.getMontant());
		DaoDelegate.getVentilationDao().save(newVentil);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteMvt(long idMouvement, boolean force) throws ApplicationException{
		MouvementDo mvt=DaoDelegate.getMouvementDao().getById(idMouvement);
		if(!force && mvt.isRapprochementBancaire()){
			throw new WarningException("Impossible de supprimer un mouvement dont le rapprochement a été fait.", null);
		}

		// Si mvt déjà soldé il faut le supprimer du solde
		if(mvt.isSolde()){
			SoldeDo lastSolde=DaoDelegate.getSoldeDao().getLastSolde(mvt.getIdCompte());
			SoldeDo newSolde=new SoldeDo();
			newSolde.setDate(System.currentTimeMillis());
			newSolde.setIdCompte(mvt.getIdCompte());
			if(MouvementDo.DEPENSE.equals(mvt.getTypeMouvement())){
				newSolde.setMontant(lastSolde.getMontant().add(mvt.getMontant()));
			}else{
				newSolde.setMontant(lastSolde.getMontant().subtract(mvt.getMontant()));
			}
			DaoDelegate.getSoldeDao().save(newSolde);
		}

		DaoDelegate.getJustificatifDao().deleteByIdMvt(idMouvement);
		DaoDelegate.getVentilationDao().deleteByIdMvt(idMouvement);
		DaoDelegate.getMouvementDao().deleteById(idMouvement);
		cacheVentilationByIdMvt.remove(idMouvement);
		cacheJustificatifByIdMvt.remove(idMouvement);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteJustificatif(long idJustificatif) throws DaoException, IOException{
		JustificatifDo justificatifDo=DaoDelegate.getJustificatifDao().getById(idJustificatif);
		File f=new File(ConfEnv.getDocPath()+"/"+justificatifDo.getFilename());
		if(!f.delete()){
			Loggeur.warn("Impossible de supprimer le fichier "+f.getAbsolutePath());
		}
		DaoDelegate.getJustificatifDao().deleteById(idJustificatif);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public int getAnneeByDate(long date) throws ApplicationException{
		PeriodeDo periode=DaoDelegate.getPeriodeDao().getByDate(date);
		if(periode==null){
			throw new ErrorException("la date "+UtilDate.FORMAT_DATE_FR.format(new Date(date))+" n'appartient à aucune période comptable.", null);
		}
		DateTime dt=new DateTime(periode.getDateDebut());
		return dt.year().get();
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void verifJustif() throws DaoException, IOException{
		//1-Database
		for(JustificatifDo elt:DaoDelegate.getJustificatifDao().getJustifWithSpecialCharacteres()){
			elt.setFilename(UtilString.withoutSpecialCharacter(elt.getFilename()));
			DaoDelegate.getJustificatifDao().save(elt);
		}

		//2-Filesystem
		File dir=new File(ConfEnv.getDocPath());
		File[] tabFile=dir.listFiles();
		for(int i=0;i<tabFile.length;i++){
			if(!tabFile[i].getName().equals(UtilString.withoutSpecialCharacter(tabFile[i].getName()))){
				File renamedFile=new File(ConfEnv.getDocPath()+"/"+UtilString.withoutSpecialCharacter(tabFile[i].getName()));
				tabFile[i].renameTo(renamedFile);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void verifNumPiece() throws ApplicationException {
		for(MouvementDo mvt:DaoDelegate.getMouvementDao().getAll()){
			try{
				if(mvt.getNumPiece()==0){
					setNumPiece(mvt);
					DaoDelegate.getMouvementDao().save(mvt);
				}else{
					int anneeNumPiece=mvt.getNumPiece()/10000;
					if(anneeNumPiece!=getAnneeByDate(mvt.getDateFacturation())){
						int numPiecePrec=mvt.getNumPiece();
						setNumPiece(mvt);
						Loggeur.info("Le mouvement "+mvt.getLibelle()+" / "+mvt.getObjet()+" du "+
								UtilDate.FORMAT_DATE_FR.format(new Date(mvt.getDateFacturation()))+" a été renuméroté de "+numPiecePrec+" à "+mvt.getNumPiece());
						DaoDelegate.getMouvementDao().save(mvt);
					}
				}
			}catch (ErrorException e) {
				Loggeur.error(e.getMessage());
				Loggeur.error("Le mouvement "+mvt.getLibelle()+" / "+mvt.getObjet()+" du "+
								UtilDate.FORMAT_DATE_FR.format(new Date(mvt.getDateFacturation()))+" n'est pas dans une période valide.");
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
