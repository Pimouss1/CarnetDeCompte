package fr.pimouss.land.server.service.impl;

import java.util.Date;

import org.joda.time.DateTime;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IPeriodeService;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.date.UtilDate;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PeriodeService implements IPeriodeService {

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public PeriodeDo createPeriode() throws DaoException{
		PeriodeDo newPeriode=new PeriodeDo();

		long maxDateFin=0;
		for(PeriodeDo periode:DaoDelegate.getPeriodeDao().getAll()){
			if(periode.getDateFin()>maxDateFin){
				maxDateFin=periode.getDateFin();
			}
		}
		DateTime dt=new DateTime(maxDateFin);
		dt=dt.plusDays(1);
		newPeriode.setDateDebut(dt.getMillis());

		dt=new DateTime(maxDateFin);
		dt=dt.plusYears(1);
		newPeriode.setDateFin(dt.getMillis());

		if(UtilDate.FORMAT_DATE_FR_YYYY.format(new Date(newPeriode.getDateDebut())).equals(UtilDate.FORMAT_DATE_FR_YYYY.format(new Date(newPeriode.getDateFin())))){
			newPeriode.setNom(
					UtilDate.FORMAT_DATE_FR_YYYY.format(new Date(newPeriode.getDateDebut())));
		}else{
			newPeriode.setNom(
					UtilDate.FORMAT_DATE_FR_YYYY.format(new Date(newPeriode.getDateDebut()))+
					"-"+
					UtilDate.FORMAT_DATE_FR_YYYY.format(new Date(newPeriode.getDateFin())));
		}

		newPeriode.setBilanFige(false);

		return newPeriode;
	}

	/*---------------------------------------------------------------------------------------------*/
}
