package fr.pimouss.land.server.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IBilanService;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.land.shared.service.BilanBo;
import fr.pimouss.land.shared.service.MouvementBo;
import fr.pimouss.util.export.ExportExcelHTML;
import fr.pimouss.util.string.UtilString;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BilanService implements IBilanService {
	private static final DecimalFormat DECIMAL_FORMAT=new DecimalFormat("0.00");

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> getDepenseBilanByPeriode(long idPeriode, long idCompte) throws DaoException {
		return getBilanByTypeMvtByPeriode(MouvementDo.DEPENSE, idPeriode, idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> getRecetteBilanByPeriode(long idPeriode, long idCompte) throws DaoException {
		return getBilanByTypeMvtByPeriode(MouvementDo.RECETTE, idPeriode, idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> getBilanByTypeMvtByPeriode(String typeMvt, long idPeriode, long idCompte) throws DaoException {
		PeriodeDo periode=DaoDelegate.getPeriodeDao().getById(idPeriode);
		return loadListeMvt(DaoDelegate.getMouvementDao().getBilanByTypeByDate(typeMvt,periode.getDateDebut(),periode.getDateFin(),idCompte));
	}

	/*---------------------------------------------------------------------------------------------*/
	private List<MouvementBo> loadListeMvt(List<MouvementDo> listeMouvementDo) throws DaoException {
		List<MouvementBo> result=new ArrayList<>();
		for(MouvementDo mvtDo:listeMouvementDo){
			MouvementBo mvtBo=new MouvementBo(mvtDo);
			mvtBo.setModePaiementDo(DaoDelegate.getModePaiementDao().getById(mvtDo.getIdModePaiement()));
			mvtBo.setListeJustificatif(DaoDelegate.getJustificatifDao().getByIdMvt(mvtDo.getRowid()));
			mvtBo.setListeVentilation(DaoDelegate.getVentilationDao().getByIdMvt(mvtDo.getRowid()));
			result.add(mvtBo);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanDepenseByPeriode(long idPeriode) throws DaoException{
		Map<Long, BigDecimal> mapMontantByTypeVentil=new HashMap<>();
		Map<Long, BigDecimal> mapMontantEncaisseByTypeVentil=new HashMap<>();
		for(TypeVentilationDo typeVentilDo:DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.DEPENSE)){
			mapMontantByTypeVentil.put(typeVentilDo.getRowid(), new BigDecimal(0));
			mapMontantEncaisseByTypeVentil.put(typeVentilDo.getRowid(), new BigDecimal(0));
		}
		List<MouvementBo> listeMvtBo=new ArrayList<>();
		for(CompteDo compte:DaoDelegate.getCompteDao().getAll()){
			listeMvtBo.addAll(getDepenseBilanByPeriode(idPeriode,compte.getRowid()));
		}
		for(MouvementBo mvtBo:listeMvtBo){
			for(VentilationDo ventilDo:mvtBo.getListeVentilation()){
				mapMontantByTypeVentil.put(ventilDo.getIdTypeVentilation(),
						mapMontantByTypeVentil.get(ventilDo.getIdTypeVentilation()).add(ventilDo.getMontant()));
				if(mvtBo.getMouvementDo().isRapprochementBancaire()){
					mapMontantEncaisseByTypeVentil.put(ventilDo.getIdTypeVentilation(),
							mapMontantEncaisseByTypeVentil.get(ventilDo.getIdTypeVentilation()).add(ventilDo.getMontant()));
				}
			}
		}
		List<BilanBo> result=new ArrayList<>();
		for(Long key:mapMontantByTypeVentil.keySet()){
			BilanBo bilanBo=new BilanBo();
			bilanBo.setIdTypeVentilation(key);
			bilanBo.setTypeVentilation(DaoDelegate.getTypeVentilationDao().getById(key).getTypeVentilation());
			bilanBo.setMontant(mapMontantByTypeVentil.get(key).doubleValue());
			bilanBo.setMontantEncaisse(mapMontantEncaisseByTypeVentil.get(key).doubleValue());
			BudgetDo budgetDo=DaoDelegate.getBudgetDao().getBudgetByIdTypeVentilAndIdPeriode(key,idPeriode);
			if(budgetDo!=null){
				bilanBo.setBudget(budgetDo.getMontant().doubleValue());
			}else{
				bilanBo.setBudget(0);
			}
			result.add(bilanBo);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanRecetteByPeriode(long idPeriode) throws DaoException{
		Map<Long, BigDecimal> mapMontantByTypeVentil=new HashMap<>();
		Map<Long, BigDecimal> mapMontantEncaisseByTypeVentil=new HashMap<>();
		for(TypeVentilationDo typeVentilDo:DaoDelegate.getTypeVentilationDao().getByType(MouvementDo.RECETTE)){
			mapMontantByTypeVentil.put(typeVentilDo.getRowid(), new BigDecimal(0));
			mapMontantEncaisseByTypeVentil.put(typeVentilDo.getRowid(), new BigDecimal(0));
		}
		List<MouvementBo> listeMvtBo=new ArrayList<>();
		for(CompteDo compte:DaoDelegate.getCompteDao().getAll()){
			listeMvtBo.addAll(getRecetteBilanByPeriode(idPeriode,compte.getRowid()));
		}
		for(MouvementBo mvtBo:listeMvtBo){
			for(VentilationDo ventilDo:mvtBo.getListeVentilation()){
				mapMontantByTypeVentil.put(ventilDo.getIdTypeVentilation(),
						mapMontantByTypeVentil.get(ventilDo.getIdTypeVentilation()).add(ventilDo.getMontant()));
				if(mvtBo.getMouvementDo().isRapprochementBancaire()){
					mapMontantEncaisseByTypeVentil.put(ventilDo.getIdTypeVentilation(),
							mapMontantEncaisseByTypeVentil.get(ventilDo.getIdTypeVentilation()).add(ventilDo.getMontant()));
				}
			}
		}
		List<BilanBo> result=new ArrayList<>();
		for(Long key:mapMontantByTypeVentil.keySet()){
			BilanBo bilanBo=new BilanBo();
			bilanBo.setIdTypeVentilation(key);
			bilanBo.setTypeVentilation(DaoDelegate.getTypeVentilationDao().getById(key).getTypeVentilation());
			bilanBo.setMontant(mapMontantByTypeVentil.get(key).doubleValue());
			bilanBo.setMontantEncaisse(mapMontantEncaisseByTypeVentil.get(key).doubleValue());
			BudgetDo budgetDo=DaoDelegate.getBudgetDao().getBudgetByIdTypeVentilAndIdPeriode(key,idPeriode);
			if(budgetDo!=null){
				bilanBo.setBudget(budgetDo.getMontant().doubleValue());
			}else{
				bilanBo.setBudget(0);
			}
			result.add(bilanBo);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public List<BilanBo> getBilanGroupeByTypeMvtByPeriode(String typeMvt, long idPeriode) throws ApplicationException {
		Map<String, BigDecimal> mapMontantByCatVentil=new HashMap<>();
		Map<String, BigDecimal> mapMontantEncaisseByCatVentil=new HashMap<>();
		Map<String, BigDecimal> mapBudgetByCatVentil=new HashMap<>();
		for(TypeVentilationDo typeVentilDo:DaoDelegate.getTypeVentilationDao().getByType(typeMvt)){
			if(typeVentilDo.getTypeVentilation().indexOf(" - ")==-1){
				throw new WarningException("le type de ventilation : "+typeVentilDo.getTypeVentilation()+" ne possède pas de catégorie.", null);
			}
			String cat=typeVentilDo.getTypeVentilation().substring(0,typeVentilDo.getTypeVentilation().indexOf(" - "));
			if(!mapMontantByCatVentil.containsKey(cat)){
				mapMontantByCatVentil.put(cat, new BigDecimal(0));
				mapMontantEncaisseByCatVentil.put(cat, new BigDecimal(0));
				mapBudgetByCatVentil.put(cat, new BigDecimal(0));
			}
		}
		List<MouvementBo> listeMvtBo=new ArrayList<>();
		for(CompteDo compte:DaoDelegate.getCompteDao().getAll()){
			listeMvtBo.addAll(getBilanByTypeMvtByPeriode(typeMvt,idPeriode,compte.getRowid()));
		}
		for(MouvementBo mvtBo:listeMvtBo){
			for(VentilationDo ventilDo:mvtBo.getListeVentilation()){
				TypeVentilationDo typeVentilDo=DaoDelegate.getTypeVentilationDao().getById(ventilDo.getIdTypeVentilation());
				String cat=typeVentilDo.getTypeVentilation().substring(0,typeVentilDo.getTypeVentilation().indexOf(" - "));
				mapMontantByCatVentil.put(cat,mapMontantByCatVentil.get(cat).add(ventilDo.getMontant()));
				if(mvtBo.getMouvementDo().isRapprochementBancaire()){
					mapMontantEncaisseByCatVentil.put(cat,mapMontantEncaisseByCatVentil.get(cat).add(ventilDo.getMontant()));
				}
			}
		}

		for(BudgetDo budgetDo:DaoDelegate.getBudgetDao().getBudgetByTypeMvtByIdPeriode(typeMvt,idPeriode)){
			TypeVentilationDo typeVentilDo=DaoDelegate.getTypeVentilationDao().getById(budgetDo.getIdTypeVentilation());
			String cat=typeVentilDo.getTypeVentilation().substring(0,typeVentilDo.getTypeVentilation().indexOf(" - "));
			mapBudgetByCatVentil.put(cat,mapBudgetByCatVentil.get(cat).add(budgetDo.getMontant()));
		}

		List<BilanBo> result=new ArrayList<>();
		for(String key:mapMontantByCatVentil.keySet()){
			BilanBo bilanBo=new BilanBo();
			bilanBo.setTypeVentilation(key);
			bilanBo.setMontant(mapMontantByCatVentil.get(key).doubleValue());
			bilanBo.setMontantEncaisse(mapMontantEncaisseByCatVentil.get(key).doubleValue());
			bilanBo.setBudget(mapBudgetByCatVentil.get(key).doubleValue());
			result.add(bilanBo);
		}
		Collections.sort(result);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public ByteArrayOutputStream exportExcelHTML(long idPeriode) throws IOException, DaoException {
		ExportExcelHTML export=new ExportExcelHTML();

		export.ecrire("Depenses");
		export.ecrire("Budget");
		export.ecrire("Realise");
		export.ecrire("Decaisse");
		for(BilanBo bilan:getBilanDepenseByPeriode(idPeriode)){
			export.ligneSuivante();
			export.ecrire(UtilString.withoutSpecialCharacter(bilan.getTypeVentilation()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getBudget()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getMontant()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getMontantEncaisse()));
		}
		export.ligneSuivante();
		export.ligneSuivante();

		export.ecrire("Recettes");
		export.ecrire("Budget");
		export.ecrire("Realise");
		export.ecrire("Encaisse");
		for(BilanBo bilan:getBilanRecetteByPeriode(idPeriode)){
			export.ligneSuivante();
			export.ecrire(UtilString.withoutSpecialCharacter(bilan.getTypeVentilation()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getBudget()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getMontant()));
			export.ecrire(DECIMAL_FORMAT.format(bilan.getMontantEncaisse()));
		}

		return export.close();
	}

	/*---------------------------------------------------------------------------------------------*/
}
