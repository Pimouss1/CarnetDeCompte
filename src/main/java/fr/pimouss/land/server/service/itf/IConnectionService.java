package fr.pimouss.land.server.service.itf;

import java.io.IOException;

import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface IConnectionService {
	UtilisateurDo connect(String login, String password) throws ApplicationException, IOException;
	UtilisateurDo connectByToken(String token) throws ApplicationException;
	void motDePassePerdu(String mail) throws ApplicationException, IOException;
	void changePassword(Long idUser, String password, String newPassword) throws ApplicationException;
}
