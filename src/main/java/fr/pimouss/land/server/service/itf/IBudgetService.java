package fr.pimouss.land.server.service.itf;

import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.TabBudgetBo;

public interface IBudgetService {
	TabBudgetBo[] getBudgetDepense() throws DaoException;
	TabBudgetBo[] getBudgetRecette() throws DaoException;
}
