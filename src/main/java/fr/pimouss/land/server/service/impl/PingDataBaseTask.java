package fr.pimouss.land.server.service.impl;

import java.lang.reflect.Method;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.util.dao.IAbstractDao;
import fr.pimouss.util.service.AbstractTaskPlanifie;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PingDataBaseTask extends AbstractTaskPlanifie {

	/*-------------------------------------------------------------------------*/
	public PingDataBaseTask(int heurePlanifie, int minutePlanifie) {
		super(heurePlanifie,minutePlanifie);
	}

	/*-------------------------------------------------------------------------*/
	@SuppressWarnings("rawtypes")
	@Override
	public void traitement() throws Exception {
	  Method[] meth = DaoDelegate.class.getMethods();
		for (int i = 0; i < meth.length; i++) {
			if (meth[i].getName().startsWith("get") && !meth[i].getName().equals("getClass")) {
				((IAbstractDao)meth[i].invoke(null, new Object[]{})).ping();
			}
		}
	}

	/*-------------------------------------------------------------------------*/
}
