package fr.pimouss.land.server.service.impl;

import java.io.IOException;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.service.AbstractTaskPlanifie;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class CalculVentilTask extends AbstractTaskPlanifie {

	/*-------------------------------------------------------------------------*/
	public CalculVentilTask(int heurePlanifie, int minutePlanifie) {
		super(heurePlanifie,minutePlanifie);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void traitement() throws DaoException, IOException {
		for(MouvementDo mvt:DaoDelegate.getMouvementDao().getAll()){
			ServiceDelegate.getSoldeService().verifVentilByMvt(mvt);
		}
	}

	/*-------------------------------------------------------------------------*/
}
