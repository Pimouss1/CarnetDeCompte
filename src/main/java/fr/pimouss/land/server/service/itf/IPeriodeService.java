package fr.pimouss.land.server.service.itf;

import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.DaoException;

public interface IPeriodeService {
	PeriodeDo createPeriode() throws DaoException;
}
