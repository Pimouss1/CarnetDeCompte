package fr.pimouss.land.server.service.itf;

import java.io.IOException;
import java.util.List;

import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.MouvementBo;

public interface IMouvementService {
	List<MouvementBo> getDepenseByPeriode(long idPeriode, long idCompte) throws DaoException;
	List<MouvementBo> getRecetteByPeriode(long idPeriode, long idCompte) throws DaoException;
	MouvementBo saveMvt(MouvementBo mvt) throws ApplicationException;
	void deleteJustificatif(long idJustificatif) throws DaoException, IOException;
	MouvementBo loadJustificatif(MouvementBo mvt) throws DaoException;
	void deleteMvt(long idMouvement, boolean force) throws ApplicationException;
	int getAnneeByDate(long date) throws ApplicationException;
	void verifJustif() throws DaoException, IOException;
	void createMvtByRegle(long date, RegleDo regle) throws ApplicationException;
	void verifNumPiece() throws ApplicationException;
}
