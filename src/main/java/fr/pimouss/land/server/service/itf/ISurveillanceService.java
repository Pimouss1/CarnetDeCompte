package fr.pimouss.land.server.service.itf;

import java.util.List;

import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.SurveillanceBo;

public interface ISurveillanceService {
	void trace(Long idUtilisateur, String ip, String classe, String methode);
	List<SurveillanceBo> getLastSurveillance() throws DaoException;
}
