package fr.pimouss.land.server.service;

import java.lang.reflect.Proxy;

import fr.pimouss.land.server.service.impl.BilanService;
import fr.pimouss.land.server.service.impl.BudgetService;
import fr.pimouss.land.server.service.impl.ConnectionService;
import fr.pimouss.land.server.service.impl.MouvementService;
import fr.pimouss.land.server.service.impl.PeriodeService;
import fr.pimouss.land.server.service.impl.SoldeService;
import fr.pimouss.land.server.service.impl.SurveillanceService;
import fr.pimouss.land.server.service.impl.VentilationService;
import fr.pimouss.land.server.service.itf.IBilanService;
import fr.pimouss.land.server.service.itf.IBudgetService;
import fr.pimouss.land.server.service.itf.IConnectionService;
import fr.pimouss.land.server.service.itf.IMouvementService;
import fr.pimouss.land.server.service.itf.IPeriodeService;
import fr.pimouss.land.server.service.itf.ISoldeService;
import fr.pimouss.land.server.service.itf.ISurveillanceService;
import fr.pimouss.land.server.service.itf.IVentilationService;
import fr.pimouss.land.shared.exception.ServiceException;
import fr.pimouss.util.log.Loggeur;
import fr.pimouss.util.service.ServiceHandler;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ServiceDelegate {
	private static IBilanService bilanService;
	public static IBilanService getBilanService() {
		if(bilanService==null){
			try{
				bilanService=(IBilanService)Proxy.newProxyInstance(
					IBilanService.class.getClassLoader(),
					new Class[]{IBilanService.class},
					new ServiceHandler<BilanService>(BilanService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return bilanService;
	}

	private static IBudgetService budgetService;
	public static IBudgetService getBudgetService() {
		if(budgetService==null){
			try{
				budgetService=(IBudgetService)Proxy.newProxyInstance(
					IBudgetService.class.getClassLoader(),
					new Class[]{IBudgetService.class},
					new ServiceHandler<BudgetService>(BudgetService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return budgetService;
	}

	private static IConnectionService connectionService;
	public static IConnectionService getConnectionService() {
		if(connectionService==null){
			try{
				connectionService=(IConnectionService)Proxy.newProxyInstance(
					IConnectionService.class.getClassLoader(),
					new Class[]{IConnectionService.class},
					new ServiceHandler<ConnectionService>(ConnectionService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return connectionService;
	}

	private static IMouvementService mouvementService;
	public static IMouvementService getMouvementService() {
		if(mouvementService==null){
			try{
				mouvementService=(IMouvementService)Proxy.newProxyInstance(
					IMouvementService.class.getClassLoader(),
					new Class[]{IMouvementService.class},
					new ServiceHandler<MouvementService>(MouvementService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return mouvementService;
	}

	private static IPeriodeService periodeService;
	public static IPeriodeService getPeriodeService() {
		if(periodeService==null){
			try{
				periodeService=(IPeriodeService)Proxy.newProxyInstance(
					IPeriodeService.class.getClassLoader(),
					new Class[]{IPeriodeService.class},
					new ServiceHandler<PeriodeService>(PeriodeService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return periodeService;
	}

	private static ISoldeService soldeService;
	public static ISoldeService getSoldeService() {
		if(soldeService==null){
			try{
				soldeService=(ISoldeService)Proxy.newProxyInstance(
					ISoldeService.class.getClassLoader(),
					new Class[]{ISoldeService.class},
					new ServiceHandler<SoldeService>(SoldeService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return soldeService;
	}

	private static ISurveillanceService surveillanceService;
	public static ISurveillanceService getSurveillanceService() {
		if(surveillanceService==null){
			try{
				surveillanceService=(ISurveillanceService)Proxy.newProxyInstance(
					ISurveillanceService.class.getClassLoader(),
					new Class[]{ISurveillanceService.class},
					new ServiceHandler<SurveillanceService>(SurveillanceService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return surveillanceService;
	}

	private static IVentilationService ventilationService;
	public static IVentilationService getVentilationService() {
		if(ventilationService==null){
			try{
				ventilationService=(IVentilationService)Proxy.newProxyInstance(
					IVentilationService.class.getClassLoader(),
					new Class[]{IVentilationService.class},
					new ServiceHandler<VentilationService>(VentilationService.class));
			} catch (IllegalArgumentException | ServiceException e) {
				Loggeur.error(e);
			}
		}
		return ventilationService;
	}

}
