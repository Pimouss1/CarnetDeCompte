package fr.pimouss.land.server.service.impl;

import java.math.BigDecimal;
import java.util.List;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IBudgetService;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.BudgetBo;
import fr.pimouss.land.shared.service.TabBudgetBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetService implements IBudgetService {

	/*---------------------------------------------------------------------------------------------*/
	private TabBudgetBo[] getBudgetByTypeMouvement(String typeMouvement) throws DaoException{
		List<PeriodeDo> listePeriode=DaoDelegate.getPeriodeDao().getAllSorted();
		List<TypeVentilationDo> listeTypeVentil=DaoDelegate.getTypeVentilationDao().getByType(typeMouvement);

		TabBudgetBo[] result=new TabBudgetBo[listePeriode.size()];
		List<BudgetDo> allBudget=DaoDelegate.getBudgetDao().getAll();
		int c=0;
		for(PeriodeDo periode:listePeriode){
			result[c]=new TabBudgetBo();
			result[c].setTabBudgetBo(new BudgetBo[listeTypeVentil.size()]);
			int r=0;
			BigDecimal totalPeriode=new BigDecimal(0);
			for(TypeVentilationDo typeVentil:listeTypeVentil){
				boolean budgetTrouve=false;
				for(BudgetDo budgetDo:allBudget){
					if(budgetDo.getIdTypeVentilation()==typeVentil.getRowid() && periode.getRowid()==budgetDo.getIdPeriode()){
						budgetTrouve=true;
						BudgetBo budgetBo=new BudgetBo(budgetDo);
						budgetBo.setTypeVentilationDo(typeVentil);
						budgetBo.setPeriodeDo(periode);
						totalPeriode=totalPeriode.add(budgetDo.getMontant());
						result[c].getTabBudgetBo()[r]=budgetBo;
					}
				}
				if(!budgetTrouve){
					BudgetDo budgetDo=new BudgetDo();
					budgetDo.setIdPeriode(periode.getRowid());
					budgetDo.setIdTypeVentilation(typeVentil.getRowid());
					BudgetBo budgetBo=new BudgetBo(budgetDo);
					budgetBo.setPeriodeDo(periode);
					budgetBo.setTypeVentilationDo(typeVentil);
					result[c].getTabBudgetBo()[r]=budgetBo;
				}
				r++;
			}
			result[c].getTabBudgetBo()[0].setTotalPeriode(totalPeriode);
			c++;
		}

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public TabBudgetBo[] getBudgetDepense() throws DaoException{
		return getBudgetByTypeMouvement(MouvementDo.DEPENSE);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public TabBudgetBo[] getBudgetRecette() throws DaoException{
		return getBudgetByTypeMouvement(MouvementDo.RECETTE);
	}

	/*---------------------------------------------------------------------------------------------*/
}
