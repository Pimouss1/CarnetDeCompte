package fr.pimouss.land.server.service.impl;

import java.io.IOException;
import java.util.Random;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IConnectionService;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.mail.MailThreadService;
import fr.pimouss.util.md5.MD5;
import fr.pimouss.util.string.UtilString;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ConnectionService implements IConnectionService {

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo connect(String login, String password) throws ApplicationException, IOException {
		UtilisateurDo result;
		result=DaoDelegate.getUtilisateurDao().getUtilisateurByLoginAndPassword(login, password);
		if(result==null){
			throw new WarningException("Nom d'utilisateur ou mot de passe incorrect.", null);
		}
		if(!result.isActif()){
			throw new WarningException("Votre compte a été désactivé. Merci de contacter votre administrateur.", null);
		}
		if(UtilString.isNullOrBlank(result.getToken())){
			result.setToken(Integer.toHexString((int)(System.currentTimeMillis()%(Integer.MAX_VALUE))).toUpperCase());
			DaoDelegate.getUtilisateurDao().save(result);
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public UtilisateurDo connectByToken(String token) throws ApplicationException {
		UtilisateurDo result;
		result=DaoDelegate.getUtilisateurDao().getUtilisateurByToken(token);
		if(result==null){
			throw new WarningException("Nom d'utilisateur ou mot de passe incorrect.", null);
		}
		if(!result.isActif()){
			throw new WarningException("Votre compte a été désactivé. Merci de contacter votre administrateur.", null);
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void motDePassePerdu(String mail) throws ApplicationException, IOException{
		UtilisateurDo utilisateurDo=DaoDelegate.getUtilisateurDao().getUtilisateurByMail(mail);
		if(utilisateurDo==null){
			throw new WarningException("Aucun utilisateur trouvé pour cette adresse mail",null);
		}
		String password=createPassword();
		utilisateurDo.setPassword(MD5.encode(password));
		utilisateurDo=DaoDelegate.getUtilisateurDao().save(utilisateurDo);
		String body="Bonjour, votre mot de passe a changé, vous pouvez dès à présent vous connecter avec vos nouveaux codes de connection :<br/>";
		body+="nom d'utilisateur : "+utilisateurDo.getLogin()+"<br/>";
		body+="mot de passe : "+password+"<br/><br/>";
		body+="Cordialement<br/>";
		body+="L'équipe trésorerie<br/>";
		new MailThreadService(utilisateurDo.getMail(),body,ConfApp.getApplicationName(),null,null);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void changePassword(Long idUser, String password, String newPassword) throws ApplicationException{
		UtilisateurDo utilisateurDo=DaoDelegate.getUtilisateurDao().getById(idUser);
		if(UtilString.isNullOrBlank(password)){
			throw new WarningException("Vous devez saisir votre mot de passe",null);
		}
		if(!MD5.encode(password).equals(utilisateurDo.getPassword())){
			throw new WarningException("Votre mot de passe est incorrect",null);
		}
		if(UtilString.isNullOrBlank(newPassword)){
			throw new WarningException("Vous devez saisir votre nouveau mot de passe",null);
		}
		utilisateurDo.setPassword(MD5.encode(newPassword));
		DaoDelegate.getUtilisateurDao().save(utilisateurDo);
	}

	/*---------------------------------------------------------------------------------------------*/
	private String createPassword(){
		String[] tabPass={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
		String result=tabPass[new Random().nextInt(26)];
		result+=tabPass[new Random().nextInt(26)];
		result+=tabPass[new Random().nextInt(26)];
		result+=tabPass[new Random().nextInt(26)];
		result+=tabPass[new Random().nextInt(26)];
		result+=tabPass[new Random().nextInt(26)];
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
}
