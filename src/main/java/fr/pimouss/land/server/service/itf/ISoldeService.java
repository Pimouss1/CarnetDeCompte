package fr.pimouss.land.server.service.itf;

import java.io.IOException;
import java.math.BigDecimal;

import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.exception.DaoException;

public interface ISoldeService {
	boolean verifVentilByMvt(MouvementDo mvt) throws IOException, DaoException;
	BigDecimal calculSoldeTheorique(boolean solderMvt, long idCompte) throws DaoException;
	BigDecimal calculSoldeAnticipe(long idCompte) throws DaoException;
	BigDecimal calculSoldeBancaire(long idCompte) throws DaoException;
	SoldeDo createNewSolde(long idCompte) throws DaoException;
	BigDecimal rapprochMvt(long idMvt, long datePaiement, boolean rapproch, long idCompte) throws DaoException;
}
