package fr.pimouss.land.server.service.itf;

import fr.pimouss.land.shared.exception.ApplicationException;

public interface IVentilationService {
	void deleteTypeVentilation(long idTypeVentil) throws ApplicationException;
}
