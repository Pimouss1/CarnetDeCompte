package fr.pimouss.land.server.service.impl;

import java.io.IOException;
import java.math.BigDecimal;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.mail.MailThreadService;
import fr.pimouss.util.service.AbstractTaskPlanifie;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class CalculSoldeTask extends AbstractTaskPlanifie {

	/*-------------------------------------------------------------------------*/
	public CalculSoldeTask(int heurePlanifie, int minutePlanifie) {
		super(heurePlanifie,minutePlanifie);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void traitement() throws DaoException, IOException {
		for(CompteDo compte:DaoDelegate.getCompteDao().getAll()){
			// Calcul d'un nouveau solde
			SoldeDo newSolde=ServiceDelegate.getSoldeService().createNewSolde(compte.getRowid());
			if(newSolde.getMontant().compareTo(new BigDecimal(1000))<0){
				new MailThreadService("pimouss79@gmail.com", "Alerte !<br/>Solde faible.", ConfApp.getApplicationName(), null, null);
			}
			
			// Vérification du solde
			SoldeDo lastSolde=DaoDelegate.getSoldeDao().getLastSolde(compte.getRowid());
			SoldeDo firstSolde=DaoDelegate.getSoldeDao().getFirstSolde(compte.getRowid());
			BigDecimal solde=new BigDecimal(0);
			solde=solde.add(firstSolde.getMontant());
			for(MouvementDo mvt:DaoDelegate.getMouvementDao().getSolde(compte.getRowid())){
				if(MouvementDo.DEPENSE.equals(mvt.getTypeMouvement())){
					solde=solde.subtract(mvt.getMontant());
				}else{
					solde=solde.add(mvt.getMontant());
				}
			}
			if(lastSolde.getMontant().compareTo(solde)!=0){
				new MailThreadService("pimouss79@gmail.com", "Alerte !<br/>Solde incohérent.<br/>"
						+ "Dernier solde="+MouvementService.DECIMAL_FORMAT.format(lastSolde.getMontant())+"<br/>"
						+ "Solde calculé="+MouvementService.DECIMAL_FORMAT.format(solde)+"<br/>"
						+ "Pour le compte "+compte.getCompte(),
						ConfApp.getApplicationName(), null, null);
			}
		}
	}

	/*-------------------------------------------------------------------------*/
}
