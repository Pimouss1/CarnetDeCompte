package fr.pimouss.land.server.service.impl;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.IVentilationService;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.WarningException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationService implements IVentilationService {

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void deleteTypeVentilation(long idTypeVentil) throws ApplicationException {
		if(!DaoDelegate.getVentilationDao().getByIdTypeVentilation(idTypeVentil).isEmpty()){
			TypeVentilationDo typeVentilation=DaoDelegate.getTypeVentilationDao().getById(idTypeVentil);
			typeVentilation.setActif(false);
			DaoDelegate.getTypeVentilationDao().save(typeVentilation);
			throw new WarningException("Le type de ventilation a été désactivé et non supprimé.", null);
		}
		DaoDelegate.getTypeVentilationDao().deleteById(idTypeVentil);
	}

	/*---------------------------------------------------------------------------------------------*/
}
