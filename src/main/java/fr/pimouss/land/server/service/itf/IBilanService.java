package fr.pimouss.land.server.service.itf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.BilanBo;

public interface IBilanService {
	List<BilanBo> getBilanDepenseByPeriode(long idPeriode) throws DaoException;
	List<BilanBo> getBilanRecetteByPeriode(long idPeriode) throws DaoException;
	List<BilanBo> getBilanGroupeByTypeMvtByPeriode(String typeMvt, long idPeriode) throws ApplicationException;
	ByteArrayOutputStream exportExcelHTML(long idPeriode) throws IOException, DaoException;
}
