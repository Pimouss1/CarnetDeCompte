package fr.pimouss.land.server.service.impl;

import org.joda.time.DateTime;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.ServiceDelegate;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.util.service.AbstractTaskPlanifie;
import fr.pimouss.util.string.UtilString;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleTask extends AbstractTaskPlanifie {

	/*-------------------------------------------------------------------------*/
	public RegleTask(int heurePlanifie, int minutePlanifie) {
		super(heurePlanifie,minutePlanifie);
	}

	/*-------------------------------------------------------------------------*/
	@Override
	public void traitement() throws Exception {
		DateTime today=new DateTime();
		for(RegleDo elt:DaoDelegate.getRegleDao().getAll()) {
			if(!UtilString.isNullOrBlank(elt.getFrequence())) {
				if(RegleDo.FREQUENCE_MENSUELLE.equals(elt.getFrequence())) {
					DateTime dtAnticipe=today.plusDays(elt.getAnticipe());
					if(dtAnticipe.getDayOfMonth()==elt.getJourDuMois()) {
						ServiceDelegate.getMouvementService().createMvtByRegle(dtAnticipe.getMillis(), elt);
					}
				}
				if(RegleDo.FREQUENCE_ANNUELLE.equals(elt.getFrequence())) {
					DateTime dtAnticipe=today.plusDays(elt.getAnticipe());
					if(dtAnticipe.getDayOfMonth()==elt.getJourDuMois()
							&& dtAnticipe.getMonthOfYear()==elt.getMois()) {
						ServiceDelegate.getMouvementService().createMvtByRegle(dtAnticipe.getMillis(), elt);
					}
				}
			}
		}
	}

	/*-------------------------------------------------------------------------*/
}
