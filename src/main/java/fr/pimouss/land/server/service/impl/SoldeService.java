package fr.pimouss.land.server.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import fr.pimouss.land.server.dao.DaoDelegate;
import fr.pimouss.land.server.service.itf.ISoldeService;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.SoldeDo;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.date.UtilDate;
import fr.pimouss.util.log.Loggeur;
import fr.pimouss.util.mail.MailThreadService;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeService implements ISoldeService {
	private static final String HTML_SAUT_LIGNE="<br/>";

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public synchronized SoldeDo createNewSolde(long idCompte) throws DaoException{
		SoldeDo newSolde=new SoldeDo();
		newSolde.setDate(System.currentTimeMillis());
		newSolde.setMontant(calculSoldeTheorique(true,idCompte));
		newSolde.setIdCompte(idCompte);
		return DaoDelegate.getSoldeDao().save(newSolde);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal calculSoldeTheorique(boolean solderMvt, long idCompte) throws DaoException{
		SoldeDo lastSolde=DaoDelegate.getSoldeDao().getLastSolde(idCompte);
		BigDecimal result=new BigDecimal(0);
		if(lastSolde!=null){
			result=result.add(lastSolde.getMontant());
		}
		for(MouvementDo mvt:DaoDelegate.getMouvementDao().getNotSoldeBeforeToday(idCompte)){
			if(MouvementDo.DEPENSE.equals(mvt.getTypeMouvement())){
				result=result.subtract(mvt.getMontant());
			}else{
				result=result.add(mvt.getMontant());
			}
			if(solderMvt){
				mvt.setSolde(solderMvt);
				DaoDelegate.getMouvementDao().save(mvt);
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal calculSoldeAnticipe(long idCompte) throws DaoException{
		SoldeDo lastSolde=DaoDelegate.getSoldeDao().getLastSolde(idCompte);
		BigDecimal result=new BigDecimal(0);
		if(lastSolde!=null){
			result=result.add(lastSolde.getMontant());
		}
		for(MouvementDo mvt:DaoDelegate.getMouvementDao().getNotSolde(idCompte)){
			if(MouvementDo.DEPENSE.equals(mvt.getTypeMouvement())){
				result=result.subtract(mvt.getMontant());
			}else{
				result=result.add(mvt.getMontant());
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal rapprochMvt(long idMvt, long datePaiement, boolean rapproch, long idCompte) throws DaoException{
		MouvementDo mvt=DaoDelegate.getMouvementDao().getById(idMvt);
		mvt.setRapprochementBancaire(rapproch);
		mvt.setDatePaiement(datePaiement);
		mvt.setPrevisionnel(false);
		DaoDelegate.getMouvementDao().save(mvt);
		return calculSoldeBancaire(idCompte);
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public BigDecimal calculSoldeBancaire(long idCompte) throws DaoException{
		BigDecimal result=calculSoldeTheorique(false,idCompte);
		for(MouvementDo mvt:DaoDelegate.getMouvementDao().getNotRapproch(idCompte)){
			if(MouvementDo.DEPENSE.equals(mvt.getTypeMouvement())){
				result=result.add(mvt.getMontant());
			}else{
				result=result.subtract(mvt.getMontant());
			}
		}
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean verifVentilByMvt(MouvementDo mvt) throws IOException, DaoException{
		BigDecimal sommeVentil=new BigDecimal(0);
		for(VentilationDo ventil:DaoDelegate.getVentilationDao().getByIdMvt(mvt.getRowid())){
			sommeVentil=sommeVentil.add(ventil.getMontant());
		}
		if(!sommeVentil.equals(mvt.getMontant())){
			Loggeur.warn("Ventilation incohérente pour le mouvement du "
					+UtilDate.FORMAT_DATE_FR.format(new Date(mvt.getDateFacturation()))+" / "
					+ "Montant="+MouvementService.DECIMAL_FORMAT.format(mvt.getMontant())+" / "
					+ "Somme des ventilations="+MouvementService.DECIMAL_FORMAT.format(sommeVentil));
			new MailThreadService("pimouss79@gmail.com", "Alerte !"+HTML_SAUT_LIGNE+"Ventilation incohérente.<br/>"
					+ "Mouvement du "+UtilDate.FORMAT_DATE_FR.format(new Date(mvt.getDateFacturation()))+HTML_SAUT_LIGNE
					+ "Objet : "+mvt.getObjet()+HTML_SAUT_LIGNE
					+ "Libellé : "+mvt.getLibelle()+HTML_SAUT_LIGNE
					+ "Montant="+MouvementService.DECIMAL_FORMAT.format(mvt.getMontant())+HTML_SAUT_LIGNE
					+ "Somme des ventilations="+MouvementService.DECIMAL_FORMAT.format(sommeVentil),
				ConfApp.getApplicationName(), null, null);
			return false;
		}
		return true;
	}

	/*---------------------------------------------------------------------------------------------*/
}
