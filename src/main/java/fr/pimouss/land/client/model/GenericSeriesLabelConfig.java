package fr.pimouss.land.client.model;

import com.sencha.gxt.chart.client.chart.series.Series.LabelPosition;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite.TextAnchor;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite.TextBaseline;
import com.sencha.gxt.chart.client.chart.series.SeriesLabelConfig;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.StringLabelProvider;

public class GenericSeriesLabelConfig extends SeriesLabelConfig<GenericChartModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericSeriesLabelConfig() {
		TextSprite sprite=new TextSprite();
		sprite.setTextBaseline(TextBaseline.MIDDLE);
		sprite.setTextAnchor(TextAnchor.START);
		sprite.setFontSize(12);
		setSpriteConfig(sprite);
		setLabelPosition(LabelPosition.START);
		setValueProvider(new ValueProvider<GenericChartModel, String>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, String value) {/* nothing to do */}
			@Override
			public String getValue(GenericChartModel object) {
				return object.getShortLabel();
			}
		}, new StringLabelProvider<>());
	}

	/*---------------------------------------------------------------------------------------------*/
}
