package fr.pimouss.land.client.model;

public class GenericChartModel {
	private String key;
	private String shortLabel;
	private String longLabel;
	private double value1;
	private double value2;

	/*---------------------------------------------------------------------------------------------*/
	public GenericChartModel(String key, String shortLabel, String longLabel, double value1) {
		this.key=key;
		this.shortLabel=shortLabel;
		this.longLabel=longLabel;
		this.value1=value1;
	}
	public GenericChartModel(String key, String shortLabel, String longLabel, double value1, double value2) {
		this.key=key;
		this.shortLabel=shortLabel;
		this.longLabel=longLabel;
		this.value1=value1;
		this.value2=value2;
	}

	/*---------------------------------------------------------------------------------------------*/
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getShortLabel() {
		return shortLabel;
	}
	public void setShortLabel(String shortLabel) {
		this.shortLabel = shortLabel;
	}
	public String getLongLabel() {
		return longLabel;
	}
	public void setLongLabel(String longLabel) {
		this.longLabel = longLabel;
	}
	public double getValue1() {
		return value1;
	}
	public double getValue2() {
		return value2;
	}
	public void setValue1(double value1) {
		this.value1 = value1;
	}
	public void setValue2(double value2) {
		this.value2 = value2;
	}

	/*---------------------------------------------------------------------------------------------*/
}
