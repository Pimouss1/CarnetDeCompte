package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;

import fr.pimouss.land.shared.dao.TypeVentilationDo;

public class VentilationListStore extends ListStore<TypeVentilationDo>{

	public VentilationListStore() {
		super(new ModelKeyProvider<TypeVentilationDo>() {
			@Override
			public String getKey(TypeVentilationDo item) {
				return "v"+item.getRowid();
			}
		});
	}
}
