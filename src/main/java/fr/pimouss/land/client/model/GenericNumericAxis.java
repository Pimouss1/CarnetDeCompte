package fr.pimouss.land.client.model;

import com.sencha.gxt.chart.client.chart.Chart.Position;
import com.sencha.gxt.chart.client.chart.axis.NumericAxis;
import com.sencha.gxt.core.client.ValueProvider;

public class GenericNumericAxis extends NumericAxis<GenericChartModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericNumericAxis() {
		setPosition(Position.LEFT);
		setDisplayGrid(true);
		setWidth(30);
		addField(new ValueProvider<GenericChartModel, Number>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, Number value) {/* nothing to do*/}
			@Override
			public Number getValue(GenericChartModel object) {
				return object.getValue1();
			}
		});
		addField(new ValueProvider<GenericChartModel, Number>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, Number value) {/* nothing to do*/}
			@Override
			public Number getValue(GenericChartModel object) {
				return object.getValue2();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
