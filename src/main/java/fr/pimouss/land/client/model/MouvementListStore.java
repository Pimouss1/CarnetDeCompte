package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;

import fr.pimouss.land.shared.service.MouvementBo;

public class MouvementListStore extends ListStore<MouvementBo>{

	public MouvementListStore() {
		super(new ModelKeyProvider<MouvementBo>() {
			@Override
			public String getKey(MouvementBo item) {
				return "m"+item.getMouvementDo().getRowid();
			}
		});
	}
}
