package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GenericComboStore extends ListStore<GenericComboModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericComboStore() {
		super(new ModelKeyProvider<GenericComboModel>() {
			@Override
			public String getKey(GenericComboModel item) {
				return item.getKey();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
