package fr.pimouss.land.client.model;

import com.sencha.gxt.chart.client.chart.Chart.Position;
import com.sencha.gxt.chart.client.chart.event.SeriesSelectionEvent;
import com.sencha.gxt.chart.client.chart.event.SeriesSelectionEvent.SeriesSelectionHandler;
import com.sencha.gxt.chart.client.chart.series.BarSeries;
import com.sencha.gxt.core.client.ValueProvider;

import fr.pimouss.land.client.view.util.InfoBox;

public class GenericBarSeries extends BarSeries<GenericChartModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericBarSeries() {
		setYAxisPosition(Position.LEFT);
		setColumn(true);
		addYField(new ValueProvider<GenericChartModel, Number>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, Number value) {/* nothing to do*/}
			@Override
			public Number getValue(GenericChartModel object) {
				return object.getValue1();
			}
		});
		addYField(new ValueProvider<GenericChartModel, Number>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, Number value) {/* nothing to do*/}
			@Override
			public Number getValue(GenericChartModel object) {
				return object.getValue2();
			}
		});
		addSeriesSelectionHandler(new SeriesSelectionHandler<GenericChartModel>() {
			@Override
			public void onSeriesSelection(SeriesSelectionEvent<GenericChartModel> event) {
				InfoBox.show(event.getItem().getLongLabel().replace("\n","<br/>"));
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
