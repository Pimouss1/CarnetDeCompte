package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.TreeStore;

public class GenericTreeStore extends TreeStore<GenericTreeModel>{

	public GenericTreeStore() {
		super(new GenericModelKeyProvider());
	}
}
