package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ModelKeyProvider;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GenericModelKeyProvider implements ModelKeyProvider<GenericTreeModel> {
	@Override
	public String getKey(GenericTreeModel item) {
		return item.getKey();
	}
}
