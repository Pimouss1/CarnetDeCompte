package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;

import fr.pimouss.land.shared.dao.RegleDo;

public class RegleListStore extends ListStore<RegleDo>{

	public RegleListStore() {
		super(new ModelKeyProvider<RegleDo>() {
			@Override
			public String getKey(RegleDo item) {
				return "r"+item.getRowid();
			}
		});
	}
}
