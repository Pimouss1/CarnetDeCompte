package fr.pimouss.land.client.model;

import com.google.gwt.event.logical.shared.SelectionEvent;

public class GenericSelectionEvent extends SelectionEvent<GenericComboModel> {

	/*---------------------------------------------------------------------------------------------*/
	protected GenericSelectionEvent(GenericComboModel selectedItem) {
		super(selectedItem);
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void fire(GenericComboBox source, String key){
		for(GenericComboModel model:source.getStore().getAll()){
			if(model.getKey().equals(key)){
				fire(source, model);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
