package fr.pimouss.land.client.model;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GenericComboModel {
	private String key;
	private String displayCombo;

	/*---------------------------------------------------------------------------------------------*/
	public GenericComboModel(String key, String displayCombo) {
		this.key=key;
		this.displayCombo=displayCombo;
	}

	/*---------------------------------------------------------------------------------------------*/
	public String getDisplayCombo() {
		return displayCombo;
	}
	public void setDisplayCombo(String displayCombo) {
		this.displayCombo = displayCombo;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	/*---------------------------------------------------------------------------------------------*/
}
