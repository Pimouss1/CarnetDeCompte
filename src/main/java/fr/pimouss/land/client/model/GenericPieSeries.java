package fr.pimouss.land.client.model;

import com.sencha.gxt.chart.client.chart.event.SeriesSelectionEvent;
import com.sencha.gxt.chart.client.chart.event.SeriesSelectionEvent.SeriesSelectionHandler;
import com.sencha.gxt.chart.client.chart.series.AreaHighlighter;
import com.sencha.gxt.chart.client.chart.series.PieSeries;
import com.sencha.gxt.core.client.ValueProvider;

import fr.pimouss.land.client.view.util.InfoBox;

public class GenericPieSeries extends PieSeries<GenericChartModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericPieSeries() {
		setDonut(10);
		setHighlighting(true);
		setHighlighter(new AreaHighlighter());
		setAngleField(new ValueProvider<GenericChartModel, Number>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, Number value) {/* nothing to do*/}
			@Override
			public Number getValue(GenericChartModel object) {
				return object.getValue1();
			}
		});
		addSeriesSelectionHandler(new SeriesSelectionHandler<GenericChartModel>() {
			@Override
			public void onSeriesSelection(SeriesSelectionEvent<GenericChartModel> event) {
				InfoBox.show(event.getItem().getLongLabel().replace("\n","<br/>"));
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
