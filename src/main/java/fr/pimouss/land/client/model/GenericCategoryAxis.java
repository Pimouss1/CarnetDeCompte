package fr.pimouss.land.client.model;

import com.sencha.gxt.chart.client.chart.Chart.Position;
import com.sencha.gxt.chart.client.chart.axis.CategoryAxis;
import com.sencha.gxt.core.client.ValueProvider;

public class GenericCategoryAxis extends CategoryAxis<GenericChartModel, String> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericCategoryAxis() {
		setPosition(Position.BOTTOM);
		setField(new ValueProvider<GenericChartModel, String>() {
			@Override
			public String getPath() {return null;}
			@Override
			public void setValue(GenericChartModel object, String value) {/* nothing to do*/}
			@Override
			public String getValue(GenericChartModel object) {
				return object.getShortLabel();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
