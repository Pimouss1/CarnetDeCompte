package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.widget.core.client.form.ComboBox;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GenericComboBox extends ComboBox<GenericComboModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericComboBox(GenericComboStore store) {
		super(store, new LabelProvider<GenericComboModel>() {
			@Override
			public String getLabel(GenericComboModel item) {
				return item.getDisplayCombo();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	public void setValue(String key) {
		for(GenericComboModel model:getStore().getAll()){
			if(model.getKey().equals(key)){
				setValue(model);
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
