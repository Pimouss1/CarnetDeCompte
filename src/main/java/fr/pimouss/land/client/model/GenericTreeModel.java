package fr.pimouss.land.client.model;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GenericTreeModel {
	private String key;
	private String display;
	private int niveau;
	private Object source;

	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public int getNiveau() {
		return niveau;
	}
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	public Object getSource() {
		return source;
	}
	public void setSource(Object source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return display;
	}
}
