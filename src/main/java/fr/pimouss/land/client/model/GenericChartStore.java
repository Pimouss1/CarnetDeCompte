package fr.pimouss.land.client.model;

import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;

public class GenericChartStore extends ListStore<GenericChartModel> {

	/*---------------------------------------------------------------------------------------------*/
	public GenericChartStore() {
		super(new ModelKeyProvider<GenericChartModel>() {
			@Override
			public String getKey(GenericChartModel item) {
				return item.getKey();
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
