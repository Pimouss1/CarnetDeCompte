package fr.pimouss.land.client;

import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.TabPanel;

import fr.pimouss.land.client.controller.AdministrationController.IFace;
import fr.pimouss.land.client.controller.AdministrationController.IFaceAsync;
import fr.pimouss.land.client.view.util.ConnectDialog;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.widget.AdministrationWidget;
import fr.pimouss.land.client.view.widget.BilanWidget;
import fr.pimouss.land.client.view.widget.BudgetWidget;
import fr.pimouss.land.client.view.widget.MouvementWidget;
import fr.pimouss.land.client.view.widget.RegleWidget;
import fr.pimouss.land.client.view.widget.SoldeWidget;
import fr.pimouss.land.client.view.widget.VentilationWidget;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PimoussLand implements EntryPoint {
	public static final String TOKEN="TOKEN";
	private static IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"administrationService";
	private static TabPanel tabPanel=new TabPanel();
	private static MouvementWidget depenseWidget=new MouvementWidget(MouvementDo.DEPENSE);
	private static MouvementWidget recetteWidget=new MouvementWidget(MouvementDo.RECETTE);
	private static SoldeWidget soldeWidget=new SoldeWidget();
	private static BudgetWidget budgetWidget=new BudgetWidget();
	private static BilanWidget bilanWidget=new BilanWidget();
	private static VentilationWidget ventilationWidget=new VentilationWidget();
	private static RegleWidget regleWidget=new RegleWidget();
	private static AdministrationWidget administrationWidget=new AdministrationWidget();

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void onModuleLoad() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);

		tabPanel.add(depenseWidget,"Dépenses");
		tabPanel.add(recetteWidget,"Recettes");
		tabPanel.add(soldeWidget,"Solde");
		tabPanel.add(budgetWidget,"Budget");
		tabPanel.add(bilanWidget,"Bilan");
		tabPanel.add(ventilationWidget,"Ventilations");
		tabPanel.add(regleWidget,"Règles");
		tabPanel.add(administrationWidget,"Administration");
		tabPanel.addSelectionHandler(new SelectionHandler<Widget>() {
			@Override
			public void onSelection(SelectionEvent<Widget> event) {
				ITabPanelItem item=(ITabPanelItem)event.getSelectedItem();
				item.load("");
			}
		});
		RootPanel.get().add(tabPanel);

		service.isLogClient(new DefaultAsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				ContextClient.setLogClient(result);
			}
		});

		// gestion de la connection automatique par cookies
		if(Cookies.getCookie(TOKEN)!=null){
			service.connectByToken(Cookies.getCookie(TOKEN), new AsyncCallback<UtilisateurDo>() {
				@Override
				public void onSuccess(UtilisateurDo arg0) {
					if(arg0!=null){
						Cookies.setCookie(TOKEN, arg0.getToken(),new Date(System.currentTimeMillis()+1000l*3600l*24l*365l));
						ContextClient.setIdUserEnCours(arg0.getRowid());
						load("");
					}else{
						new ConnectDialog();
					}
				}
				@Override
				public void onFailure(Throwable arg0) {
					new ConnectDialog();
				}
			});
		}else{
			new ConnectDialog();
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void load(String ancre){
		service.isUserTresorier(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				ContextClient.setUserIsTresorier(result);
			}
		});
		service.isUserAdministrateur(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				ContextClient.setUserIsAdministrateur(result);
			}
		});
		service.isUserContributeur(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				ContextClient.setUserIsContributeur(result);
				if(result){
					depenseWidget.load("");
				}
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
