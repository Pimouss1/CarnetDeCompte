package fr.pimouss.land.client.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.BigDecimalField;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;

import fr.pimouss.land.client.controller.BudgetController.IFace;
import fr.pimouss.land.client.controller.BudgetController.IFaceAsync;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.InfoBox;
import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.service.TabBudgetBo;

public class BudgetWidget extends Composite implements ITabPanelItem {
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"budgetService";
	private FlowPanel rootPanel=new FlowPanel();

	private TabBudgetBo[] budgetDepense;
	private BigDecimalField[][] budgetDepenseMontantField;
	private FlexTable budgetDepensePanel=new FlexTable();
	private TabBudgetBo[] budgetRecette;
	private BigDecimalField[][] budgetRecetteMontantField;
	private FlexTable budgetRecettePanel=new FlexTable();

	/*---------------------------------------------------------------------------------------------*/
	public BudgetWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		VerticalPanel vp=new VerticalPanel();
		rootPanel.add(vp);

		HorizontalPanel hp=new HorizontalPanel();
		vp.add(hp);
		hp.add(initBudgetDepenseGrid());
		hp.add(initBudgetRecetteGrid());

		vp.add(initButtonPanel());
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initButtonPanel(){
		HorizontalPanel result=new HorizontalPanel();

		// enregistrer
		final TextButton saveButton=new TextButton("Enregistrer");
		result.add(saveButton);
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				save();
			}
		});

		// annuler
		final TextButton cancelButton=new TextButton("Annuler");
		result.add(cancelButton);
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				load("");
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initBudgetDepenseGrid(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Budget dépenses");
		final ScrollPanel sc=new ScrollPanel();
		result.add(sc);
		sc.setWidth((Window.getClientWidth()/2-25)+"px");
		sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-57)+"px");

		sc.add(budgetDepensePanel);

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				sc.setWidth((Window.getClientWidth()/2-25)+"px");
				sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-57)+"px");
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initBudgetRecetteGrid(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Budget recettes");
		final ScrollPanel sc=new ScrollPanel();
		result.add(sc);
		sc.setWidth((Window.getClientWidth()/2-25)+"px");
		sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-57)+"px");

		sc.add(budgetRecettePanel);

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				sc.setWidth((Window.getClientWidth()/2-25)+"px");
				sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-57)+"px");
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void loadPanel(BigDecimalField[][] budgetMontantField, TabBudgetBo[] tabBudget, FlexTable budgetPanel){
		for(int r=0;r<tabBudget[0].getTabBudgetBo().length;r++){
			TextField typeVentilField=new TextField();
			typeVentilField.setWidth(400);
			typeVentilField.setEnabled(false);
			typeVentilField.setValue(tabBudget[0].getTabBudgetBo()[r].getTypeVentilationDo().getTypeVentilation());
			budgetPanel.setWidget(r+1, 0, typeVentilField);
		}

		for(int c=0;c<tabBudget.length;c++){
			TextField anneeField=new TextField();
			anneeField.setWidth(80);
			anneeField.setEnabled(false);
			anneeField.setValue(tabBudget[c].getTabBudgetBo()[0].getPeriodeDo().getNom());
			budgetPanel.setWidget(0, c+1, anneeField);
			for(int r=0;r<tabBudget[c].getTabBudgetBo().length;r++){
				BigDecimalField montantField=new BigDecimalField();
				montantField.setFormat(ContextClient.DECIMAL_FORMAT);
				montantField.setWidth(80);
				montantField.setValue(tabBudget[c].getTabBudgetBo()[r].getBudgetDo().getMontant());
				budgetPanel.setWidget(r+1, c+1, montantField);
				budgetMontantField[c][r]=montantField;
			}
			BigDecimalField totalField=new BigDecimalField();
			totalField.setFormat(ContextClient.DECIMAL_FORMAT);
			totalField.setWidth(80);
			totalField.setEnabled(false);
			totalField.setValue(tabBudget[c].getTabBudgetBo()[0].getTotalPeriode());
			budgetPanel.setWidget(tabBudget[c].getTabBudgetBo().length+1, c+1, totalField);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		service.getBudgetDepense(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<TabBudgetBo[]>() {
			public void onSuccess(TabBudgetBo[] result) {
				budgetDepense=result;
				budgetDepenseMontantField=new BigDecimalField[budgetDepense.length][budgetDepense[0].getTabBudgetBo().length];
				loadPanel(budgetDepenseMontantField,budgetDepense,budgetDepensePanel);
			}
		});
		service.getBudgetRecette(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<TabBudgetBo[]>() {
			public void onSuccess(TabBudgetBo[] result) {
				budgetRecette=result;
				budgetRecetteMontantField=new BigDecimalField[budgetRecette.length][budgetRecette[0].getTabBudgetBo().length];
				loadPanel(budgetRecetteMontantField,budgetRecette,budgetRecettePanel);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	private void save(){
		final List<BudgetDo> listeBudget=new ArrayList<>();
		for(int c=0;c<budgetDepense.length;c++){
			for(int r=0;r<budgetDepense[c].getTabBudgetBo().length;r++){
				if(budgetDepenseMontantField[c][r]!=null && budgetDepenseMontantField[c][r].getValue()!=null){
					budgetDepense[c].getTabBudgetBo()[r].getBudgetDo().setMontant(budgetDepenseMontantField[c][r].getValue());
					listeBudget.add(budgetDepense[c].getTabBudgetBo()[r].getBudgetDo());
				}
			}
		}
		for(int c=0;c<budgetRecette.length;c++){
			for(int r=0;r<budgetRecette[c].getTabBudgetBo().length;r++){
				if(budgetRecetteMontantField[c][r]!=null && budgetRecetteMontantField[c][r].getValue()!=null){
					budgetRecette[c].getTabBudgetBo()[r].getBudgetDo().setMontant(budgetRecetteMontantField[c][r].getValue());
					listeBudget.add(budgetRecette[c].getTabBudgetBo()[r].getBudgetDo());
				}
			}
		}

		service.saveBudget(ContextClient.getIdUserEnCours(), listeBudget, new DefaultAsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				InfoBox.show("Sauvegarde effectuée avec succès.");
				load("");
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
