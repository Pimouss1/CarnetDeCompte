package fr.pimouss.land.client.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.RowClickEvent;
import com.sencha.gxt.widget.core.client.event.RowClickEvent.RowClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;

import fr.pimouss.land.client.controller.VentilationController.IFace;
import fr.pimouss.land.client.controller.VentilationController.IFaceAsync;
import fr.pimouss.land.client.model.GenericComboBox;
import fr.pimouss.land.client.model.GenericComboModel;
import fr.pimouss.land.client.model.GenericComboStore;
import fr.pimouss.land.client.model.VentilationListStore;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.WaitBox;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;

public class VentilationWidget extends Composite implements ITabPanelItem {
	private static final String STYLE_LABEL="fieldLabel";
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"ventilationService";
	private FlowPanel rootPanel=new FlowPanel();

	private Grid<TypeVentilationDo> ventilationGrid;
	private VentilationListStore ventilationGridStore=new VentilationListStore();
	private TypeVentilationDo ventilationEnCours;
	private GenericComboBox typeMouvementComboBox;
	private GenericComboStore typeMouvementComboStore=new GenericComboStore();
	private TextField typeVentilationField=new TextField();

	/*---------------------------------------------------------------------------------------------*/
	public VentilationWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		HorizontalPanel hp=new HorizontalPanel();
		rootPanel.add(hp);

		hp.add(initVentilationGrid());

		VerticalPanel vp=new VerticalPanel();
		hp.add(vp);
		vp.add(initVentilationFieldSet());
		vp.add(initButtonPanel());
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initVentilationGrid(){
		List<ColumnConfig<TypeVentilationDo, ?>> config=new ArrayList<>();

		ValueProvider<TypeVentilationDo, String> typeMouvementProvider=new ValueProvider<TypeVentilationDo, String>() {
			@Override
			public void setValue(TypeVentilationDo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(TypeVentilationDo object) {
				return object.getTypeMouvement();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<TypeVentilationDo, String> cf2=new ColumnConfig<>(typeMouvementProvider,120,"Type mouvement");
		cf2.setCell(new AbstractCell<String>() {
			@Override
			public void render(com.google.gwt.cell.client.Cell.Context context, String value, SafeHtmlBuilder sb) {
				if(value!=null){
					if(MouvementDo.DEPENSE.equals(value)){
						sb.appendHtmlConstant("Dépense");
					}else{
						sb.appendHtmlConstant("Recette");
					}
				}
			}
		});
		config.add(cf2);
		
		ValueProvider<TypeVentilationDo, String> typeVentilationProvider=new ValueProvider<TypeVentilationDo, String>() {
			@Override
			public void setValue(TypeVentilationDo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(TypeVentilationDo object) {
				if(!object.isActif()){
					return "w_"+object.getTypeVentilation();
				}
				return "i_"+object.getTypeVentilation();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<TypeVentilationDo, String> cf3=new ColumnConfig<>(typeVentilationProvider,400,"Nom");
		cf3.setCell(new AbstractCell<String>() {
			@Override
			public void render(Context context, String value, SafeHtmlBuilder sb) {
				String color="000000";
				if(value.startsWith("w_")) {
					color="FF9F5C";
				}
				sb.appendHtmlConstant("<span style=\"color:#"+color+"\">"+value.substring(2)+"</span>");
			}
		});
		config.add(cf3);

		ventilationGrid=new Grid<>(ventilationGridStore, new ColumnModel<>(config));
		ventilationGrid.addRowClickHandler(new RowClickHandler() {
			@Override
			public void onRowClick(RowClickEvent event) {
				ventilationEnCours=ventilationGrid.getStore().get(event.getRowIndex());
				load(false);
			}
		});
		ventilationGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT);
		ventilationGrid.setWidth(ContextClient.GRID_WIDTH);

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				ventilationGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT);
				ventilationGrid.setWidth(ContextClient.GRID_WIDTH);
			}
		});

		return ventilationGrid;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initVentilationFieldSet(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Informations");
		FlexTable table=new FlexTable();
		result.add(table);

		Label typeVentilLabel=new Label("Nom : ");
		typeVentilLabel.addStyleName(STYLE_LABEL);
		table.setWidget(0, 0, typeVentilLabel);
		typeVentilationField.setWidth(350);
		typeVentilationField.setEnabled(false);
		table.setWidget(0, 1, typeVentilationField);

		typeMouvementComboStore.add(new GenericComboModel(MouvementDo.DEPENSE, "Dépense"));
		typeMouvementComboStore.add(new GenericComboModel(MouvementDo.RECETTE, "Recette"));
		typeMouvementComboBox=new GenericComboBox(typeMouvementComboStore);
		Label typeMouvementLabel=new Label("Type de mouvement : ");
		typeMouvementLabel.addStyleName(STYLE_LABEL);
		table.setWidget(1, 0, typeMouvementLabel);
		table.setWidget(1, 1, typeMouvementComboBox);
		typeMouvementComboBox.setTriggerAction(TriggerAction.ALL);
		typeMouvementComboBox.setEditable(false);
		typeMouvementComboBox.setEnabled(false);
		typeMouvementComboBox.setValue(MouvementDo.DEPENSE);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		load(true);
	}
	private void load(boolean reloadListe) {
		if(reloadListe){
			service.getAllTypeVentilation(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
				@Override
				public void onSuccess(List<TypeVentilationDo> result) {
					ventilationGridStore.clear();
					TypeVentilationDo newTypeVentil=new TypeVentilationDo();
					newTypeVentil.setTypeVentilation("Nouveau");
					ventilationGridStore.add(newTypeVentil);
					ventilationGridStore.addAll(result);
				}
			});
		}

		if(ventilationEnCours!=null){
			typeMouvementComboBox.setValue(ventilationEnCours.getTypeMouvement());
			typeMouvementComboBox.setEnabled(true);
			typeVentilationField.setValue(ventilationEnCours.getTypeVentilation());
			typeVentilationField.setEnabled(true);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void save(){
		if(ventilationEnCours!=null){
			WaitBox.showBox();
			ventilationEnCours.setTypeMouvement(typeMouvementComboBox.getValue().getKey());
			ventilationEnCours.setTypeVentilation(typeVentilationField.getValue());
			ventilationEnCours.setActif(true);
			service.saveVentil(ContextClient.getIdUserEnCours(), ventilationEnCours, new DefaultAsyncCallback<TypeVentilationDo>() {
				@Override
				public void onSuccess(TypeVentilationDo arg0) {
					ventilationEnCours=arg0;
					load("");
					WaitBox.hideBox();
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initButtonPanel(){
		HorizontalPanel result=new HorizontalPanel();

		// enregistrer
		final TextButton saveButton=new TextButton("Enregistrer");
		result.add(saveButton);
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				save();
			}
		});

		// annuler
		final TextButton cancelButton=new TextButton("Annuler");
		result.add(cancelButton);
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				load("");
			}
		});

		// supprimer
		result.add(new SeparatorToolItem());
		final TextButton deleteButton=new TextButton("Supprimer");
		result.add(deleteButton);
		deleteButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if(ventilationEnCours!=null){
					if(Window.confirm("Etes-vous sûr de vouloir supprimer cette ventilation ?")){
						WaitBox.showBox();
						service.deleteVentil(ContextClient.getIdUserEnCours(), ventilationEnCours.getRowid(), new DefaultAsyncCallback<Void>() {
							@Override
							public void onSuccess(Void result) {
								ventilationEnCours=null;
								load("");
							}
						});
					}
				}
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
