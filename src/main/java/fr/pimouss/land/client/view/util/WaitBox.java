package fr.pimouss.land.client.view.util;

import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class WaitBox extends AutoProgressMessageBox {
	private static final String MESSAGE_BOX_TITLE="Attendez svp";
	private static final String MESSAGE_BOX_MSG="";
	private static final String MESSAGE_BOX_PROGRESS_TEXT="Chargement en cours...";
	private static WaitBox instance=new WaitBox();

	/*---------------------------------------------------------------------------------------------*/
	private WaitBox() {
		super(MESSAGE_BOX_TITLE, MESSAGE_BOX_MSG);
		setProgressText(MESSAGE_BOX_PROGRESS_TEXT);
		auto();
		setModal(true);
	}
	/*---------------------------------------------------------------------------------------------*/
	public static void showBox() {
		instance.setVisible(true);
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void hideBox() {
		instance.setVisible(false);
	}

	/*---------------------------------------------------------------------------------------------*/
}
