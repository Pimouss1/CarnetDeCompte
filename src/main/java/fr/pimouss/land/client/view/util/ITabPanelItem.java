package fr.pimouss.land.client.view.util;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface ITabPanelItem {
	void load(final String ancre);
	boolean isLoaded();
}
