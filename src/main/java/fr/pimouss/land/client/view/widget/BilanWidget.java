package fr.pimouss.land.client.view.widget;

import java.math.BigDecimal;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.chart.client.chart.Chart;
import com.sencha.gxt.chart.client.draw.Gradient;
import com.sencha.gxt.chart.client.draw.RGB;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

import fr.pimouss.land.client.controller.BilanController.IFace;
import fr.pimouss.land.client.controller.BilanController.IFaceAsync;
import fr.pimouss.land.client.model.GenericBarSeries;
import fr.pimouss.land.client.model.GenericCategoryAxis;
import fr.pimouss.land.client.model.GenericChartModel;
import fr.pimouss.land.client.model.GenericChartStore;
import fr.pimouss.land.client.model.GenericComboBox;
import fr.pimouss.land.client.model.GenericComboModel;
import fr.pimouss.land.client.model.GenericComboStore;
import fr.pimouss.land.client.model.GenericNumericAxis;
import fr.pimouss.land.client.model.GenericPieSeries;
import fr.pimouss.land.client.model.GenericSeriesLabelConfig;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.service.BilanBo;

public class BilanWidget extends Composite implements ITabPanelItem {
	private static final String TOTAL_DEPENSE_LABEL = "Total dépense = ";
	private static final String TOTAL_RECETTE_LABEL = "Total recette = ";
	private static final String TOTAL_DEPENSE_ENCAISSE_LABEL = "Total dépense décaissé = ";
	private static final String TOTAL_RECETTE_ENCAISSE_LABEL = "Total recette encaissé = ";
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"bilanService";
	private String downloadBilanFileModuleRelativeURL=GWT.getModuleBaseURL()+"downloadBilanFileService";
	private String downloadJustificatifFileModuleRelativeURL=GWT.getModuleBaseURL()+"downloadAllJustificatifFileService";
	private FlowPanel rootPanel=new FlowPanel();
	private GenericChartStore depenseStore = new GenericChartStore();
	private GenericChartStore recetteStore = new GenericChartStore();
	private GenericChartStore deltaDepenseStore = new GenericChartStore();
	private GenericChartStore deltaRecetteStore = new GenericChartStore();
	private Chart<GenericChartModel> depenseChart = new Chart<>();
	private Chart<GenericChartModel> recetteChart = new Chart<>();
	private Chart<GenericChartModel> deltaDepenseChart = new Chart<>();
	private Chart<GenericChartModel> deltaRecetteChart = new Chart<>();
	private BigDecimal totalDepense=new BigDecimal(0);
	private Label totalDepenseLabel=new Label(TOTAL_DEPENSE_LABEL);
	private BigDecimal totalRecette=new BigDecimal(0);
	private Label totalRecetteLabel=new Label(TOTAL_RECETTE_LABEL);
	private Label bilanLabel=new Label("Bilan = ");
	private BigDecimal totalDepenseEncaisse=new BigDecimal(0);
	private Label totalDepenseEncaisseLabel=new Label(TOTAL_DEPENSE_ENCAISSE_LABEL);
	private BigDecimal totalRecetteEncaisse=new BigDecimal(0);
	private Label totalRecetteEncaisseLabel=new Label(TOTAL_RECETTE_ENCAISSE_LABEL);
	private Label bilanEncaisseLabel=new Label("Bilan encaissé = ");
	private GenericComboBox periodeComboBox;
	private GenericComboStore anneeComboStore=new GenericComboStore();
	private GenericComboBox vueComboBox;
	private GenericComboStore vueComboStore=new GenericComboStore();

	/*---------------------------------------------------------------------------------------------*/
	public BilanWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		VerticalPanel vp=new VerticalPanel();
		rootPanel.add(vp);

		vp.add(initEntetePanel());

		HorizontalPanel hp1=new HorizontalPanel();
		vp.add(hp1);
		hp1.add(initDepenseChart());
		hp1.add(initRecetteChart());

		HorizontalPanel hp2=new HorizontalPanel();
		vp.add(hp2);
		hp2.add(initDeltaDepenseChart());
		hp2.add(initDeltaRecetteChart());
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initEntetePanel(){
		HorizontalPanel result=new HorizontalPanel();

		// selection année
		periodeComboBox=new GenericComboBox(anneeComboStore);
		result.add(periodeComboBox);
		service.getAllPeriode(new DefaultAsyncCallback<List<PeriodeDo>>() {
			@Override
			public void onSuccess(List<PeriodeDo> result) {
				for(PeriodeDo elt:result){
					anneeComboStore.add(new GenericComboModel("a"+elt.getRowid(),elt.getNom()));
				}
				service.getPeriodeByDate(System.currentTimeMillis(), new DefaultAsyncCallback<PeriodeDo>() {
					@Override
					public void onSuccess(PeriodeDo result) {
						ContextClient.setKeyPeriodeEnCours("a"+result.getRowid());
						periodeComboBox.setValue("a"+result.getRowid());
					}
				});
			}
		});
		periodeComboBox.setWidth(100);
		periodeComboBox.setTriggerAction(TriggerAction.ALL);
		periodeComboBox.setEditable(false);
		periodeComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				ContextClient.setKeyPeriodeEnCours(event.getSelectedItem().getKey());
				load("");
			}
		});

		// selection type graphique
		vueComboBox=new GenericComboBox(vueComboStore);
		result.add(vueComboBox);
		vueComboStore.add(new GenericComboModel("v1","Vue globale"));
		vueComboStore.add(new GenericComboModel("v2","Vue groupée"));
		vueComboBox.setTriggerAction(TriggerAction.ALL);
		vueComboBox.setEditable(false);
		vueComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				vueComboBox.setValue(event.getSelectedItem().getKey());
				load("");
			}
		});
		vueComboBox.setValue("v1");

		// exporter bilan
		final TextButton exportBilanButton=new TextButton("Exporter tableau de bilan");
		result.add(exportBilanButton);
		exportBilanButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Window.open(downloadBilanFileModuleRelativeURL+"?idPeriode="+periodeComboBox.getValue().getKey().substring(1),"","");
			}
		});

		// exporter justificatifs
		final TextButton exportJustifButton=new TextButton("Exporter justificatifs");
		result.add(exportJustifButton);
		exportJustifButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Window.open(downloadJustificatifFileModuleRelativeURL+"?idPeriode="+periodeComboBox.getValue().getKey().substring(1),"","");
			}
		});

		// sommes
		FlexTable table=new FlexTable();
		result.add(table);
		table.setWidget(0, 0, totalDepenseLabel);
		table.setWidget(0, 1, totalRecetteLabel);
		table.setWidget(0, 2, bilanLabel);
		table.setWidget(1, 0, totalDepenseEncaisseLabel);
		table.setWidget(1, 1, totalRecetteEncaisseLabel);
		table.setWidget(1, 2, bilanEncaisseLabel);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initDepenseChart(){
		Gradient g1 = new Gradient(45);
		g1.addStop(0, new RGB(120, 120, 170));
		g1.addStop(100, new RGB(200, 200, 250));
		Gradient g2 = new Gradient(45);
		g2.addStop(0, new RGB(170, 120, 120));
		g2.addStop(100, new RGB(250, 200, 200));
		Gradient g3 = new Gradient(45);
		g3.addStop(0, new RGB(120, 170, 120));
		g3.addStop(100, new RGB(200, 250, 200));

		GenericPieSeries series = new GenericPieSeries();
		series.addColor(g1);
		series.addColor(g2);
		series.addColor(g3);
		series.setLabelConfig(new GenericSeriesLabelConfig());

		depenseChart.setStore(depenseStore);
		depenseChart.setWidth(Window.getClientWidth()/2-10);
		depenseChart.setHeight(Window.getClientHeight()/2-ContextClient.MARGE_HEIGHT-7);
		depenseChart.setShadowChart(true);
		depenseChart.setAnimated(true);
		depenseChart.addGradient(g1);
		depenseChart.addGradient(g2);
		depenseChart.addGradient(g3);
		depenseChart.addSeries(series);

		return depenseChart;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initRecetteChart(){
		Gradient g1 = new Gradient(45);
		g1.addStop(0, new RGB(120, 120, 170));
		g1.addStop(100, new RGB(200, 200, 250));
		Gradient g2 = new Gradient(45);
		g2.addStop(0, new RGB(170, 120, 120));
		g2.addStop(100, new RGB(250, 200, 200));
		Gradient g3 = new Gradient(45);
		g3.addStop(0, new RGB(120, 170, 120));
		g3.addStop(100, new RGB(200, 250, 200));

		GenericPieSeries series = new GenericPieSeries();
		series.addColor(g1);
		series.addColor(g2);
		series.addColor(g3);
		series.setLabelConfig(new GenericSeriesLabelConfig());

		recetteChart.setStore(recetteStore);
		recetteChart.setWidth(Window.getClientWidth()/2-10);
		recetteChart.setHeight(Window.getClientHeight()/2-ContextClient.MARGE_HEIGHT-7);
		recetteChart.setShadowChart(true);
		recetteChart.setAnimated(true);
		recetteChart.addGradient(g1);
		recetteChart.addGradient(g2);
		recetteChart.addGradient(g3);
		recetteChart.addSeries(series);

		return recetteChart;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initDeltaDepenseChart(){
		GenericNumericAxis axeY=new GenericNumericAxis();

		GenericCategoryAxis axeX=new GenericCategoryAxis();

		GenericBarSeries serieBudget=new GenericBarSeries();
		serieBudget.addColor(new RGB(120,120,170));
		serieBudget.addColor(new RGB(170,120,120));

		deltaDepenseChart.setStore(deltaDepenseStore);
		deltaDepenseChart.setWidth(Window.getClientWidth()/2-10);
		deltaDepenseChart.setHeight(Window.getClientHeight()/2-ContextClient.MARGE_HEIGHT-7);
		deltaDepenseChart.setShadowChart(true);
		deltaDepenseChart.addAxis(axeY);
		deltaDepenseChart.addAxis(axeX);
		deltaDepenseChart.addSeries(serieBudget);

		return deltaDepenseChart;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initDeltaRecetteChart(){
		GenericNumericAxis axeY=new GenericNumericAxis();

		GenericCategoryAxis axeX=new GenericCategoryAxis();

		GenericBarSeries serieBudget=new GenericBarSeries();
		serieBudget.addColor(new RGB(120,120,170));
		serieBudget.addColor(new RGB(170,120,120));

		deltaRecetteChart.setStore(deltaRecetteStore);
		deltaRecetteChart.setWidth(Window.getClientWidth()/2-10);
		deltaRecetteChart.setHeight(Window.getClientHeight()/2-ContextClient.MARGE_HEIGHT-7);
		deltaRecetteChart.setShadowChart(true);
		deltaRecetteChart.addAxis(axeY);
		deltaRecetteChart.addAxis(axeX);
		deltaRecetteChart.addSeries(serieBudget);

		return deltaRecetteChart;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		periodeComboBox.setValue(ContextClient.getKeyPeriodeEnCours());

		if(periodeComboBox.getValue()!=null && !"".equals(periodeComboBox.getValue().getKey())){
			loadBilan();
		}else{
			service.getPeriodeByDate(System.currentTimeMillis(), new DefaultAsyncCallback<PeriodeDo>() {
				@Override
				public void onSuccess(PeriodeDo result) {
					periodeComboBox.setValue("a"+result.getRowid());
					loadBilan();
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void loadBilan(){
		if("v1".equals(vueComboBox.getValue().getKey())){
			service.getBilanDepenseByPeriode(Long.parseLong(periodeComboBox.getValue().getKey().substring(1)), new DefaultAsyncCallback<List<BilanBo>>() {
				@Override
				public void onSuccess(List<BilanBo> result) {
					depenseStore.clear();
					deltaDepenseStore.clear();
					totalDepense=new BigDecimal(0);
					totalDepenseEncaisse=new BigDecimal(0);
					for(BilanBo elt:result){
						if(elt.getMontant()!=0){
							depenseStore.add(new GenericChartModel(
									MouvementDo.DEPENSE+elt.getIdTypeVentilation(),
									ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation().replace(" - ","\n")+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getMontant()));
							deltaDepenseStore.add(new GenericChartModel(
									MouvementDo.DEPENSE+elt.getIdTypeVentilation(),
									ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation().replace(" - ","\n")+"\n"+"Budget="+ContextClient.DECIMAL_FORMAT.format(elt.getBudget())+"€\n"+"Réalisé="+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getBudget(),
									elt.getMontant()));
							totalDepense=totalDepense.add(BigDecimal.valueOf(elt.getMontant()));
							totalDepenseEncaisse=totalDepenseEncaisse.add(BigDecimal.valueOf(elt.getMontantEncaisse()));
						}
					}
					depenseChart.redrawChart();
					deltaDepenseChart.redrawChart();
					totalDepenseLabel.setText(TOTAL_DEPENSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalDepense)+" €");
					totalDepenseEncaisseLabel.setText(TOTAL_DEPENSE_ENCAISSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalDepenseEncaisse)+" €");
					calculBilan();
				}
			});
			service.getBilanRecetteByPeriode(Long.parseLong(periodeComboBox.getValue().getKey().substring(1)), new DefaultAsyncCallback<List<BilanBo>>() {
				@Override
				public void onSuccess(List<BilanBo> result) {
					recetteStore.clear();
					deltaRecetteStore.clear();
					totalRecette=new BigDecimal(0);
					totalRecetteEncaisse=new BigDecimal(0);
					for(BilanBo elt:result){
						if(elt.getMontant()!=0){
							recetteStore.add(new GenericChartModel(
									MouvementDo.RECETTE+elt.getIdTypeVentilation(),
									ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation().replace(" - ","\n")+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getMontant()));
							deltaRecetteStore.add(new GenericChartModel(
									MouvementDo.RECETTE+elt.getIdTypeVentilation(),
									ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation().replace(" - ","\n")+"\n"+"Budget="+ContextClient.DECIMAL_FORMAT.format(elt.getBudget())+"€\n"+"Réalisé="+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getBudget(),
									elt.getMontant()));
							totalRecette=totalRecette.add(BigDecimal.valueOf(elt.getMontant()));
							totalRecetteEncaisse=totalRecetteEncaisse.add(BigDecimal.valueOf(elt.getMontantEncaisse()));
						}
					}
					recetteChart.redrawChart();
					deltaRecetteChart.redrawChart();
					totalRecetteLabel.setText(TOTAL_RECETTE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalRecette)+" €");
					totalRecetteEncaisseLabel.setText(TOTAL_RECETTE_ENCAISSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalRecetteEncaisse)+" €");
					calculBilan();
				}
			});
		}else if("v2".equals(vueComboBox.getValue().getKey())){
			service.getBilanGroupeDepenseByPeriode(Long.parseLong(periodeComboBox.getValue().getKey().substring(1)), new DefaultAsyncCallback<List<BilanBo>>() {
				@Override
				public void onSuccess(List<BilanBo> result) {
					depenseStore.clear();
					deltaDepenseStore.clear();
					totalDepense=new BigDecimal(0);
					totalDepenseEncaisse=new BigDecimal(0);
					for(BilanBo elt:result){
						if(elt.getMontant()!=0){
							depenseStore.add(new GenericChartModel(
									MouvementDo.DEPENSE+elt.getTypeVentilation(),
									elt.getTypeVentilation()+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation()+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getMontant()));
							deltaDepenseStore.add(new GenericChartModel(
									MouvementDo.DEPENSE+elt.getTypeVentilation(),
									elt.getTypeVentilation(),
									elt.getTypeVentilation()+"\n"+"Budget="+ContextClient.DECIMAL_FORMAT.format(elt.getBudget())+"€\n"+"Réalisé="+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getBudget(),
									elt.getMontant()));
							totalDepense=totalDepense.add(BigDecimal.valueOf(elt.getMontant()));
							totalDepenseEncaisse=totalDepenseEncaisse.add(BigDecimal.valueOf(elt.getMontantEncaisse()));
						}
					}
					depenseChart.redrawChart();
					deltaDepenseChart.redrawChart();
					totalDepenseLabel.setText(TOTAL_DEPENSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalDepense)+" €");
					totalDepenseEncaisseLabel.setText(TOTAL_DEPENSE_ENCAISSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalDepenseEncaisse)+" €");
					calculBilan();
				}
			});
			service.getBilanGroupeRecetteByPeriode(Long.parseLong(periodeComboBox.getValue().getKey().substring(1)), new DefaultAsyncCallback<List<BilanBo>>() {
				@Override
				public void onSuccess(List<BilanBo> result) {
					recetteStore.clear();
					deltaRecetteStore.clear();
					totalRecette=new BigDecimal(0);
					totalRecetteEncaisse=new BigDecimal(0);
					for(BilanBo elt:result){
						if(elt.getMontant()!=0){
							recetteStore.add(new GenericChartModel(
									MouvementDo.RECETTE+elt.getTypeVentilation(),
									elt.getTypeVentilation()+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getTypeVentilation()+"\n"+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getMontant()));
							deltaRecetteStore.add(new GenericChartModel(
									MouvementDo.RECETTE+elt.getTypeVentilation(),
									elt.getTypeVentilation(),
									elt.getTypeVentilation()+"\n"+"Budget="+ContextClient.DECIMAL_FORMAT.format(elt.getBudget())+"€\n"+"Réalisé="+ContextClient.DECIMAL_FORMAT.format(elt.getMontant())+"€",
									elt.getBudget(),
									elt.getMontant()));
							totalRecette=totalRecette.add(BigDecimal.valueOf(elt.getMontant()));
							totalRecetteEncaisse=totalRecetteEncaisse.add(BigDecimal.valueOf(elt.getMontantEncaisse()));
						}
					}
					recetteChart.redrawChart();
					deltaRecetteChart.redrawChart();
					totalRecetteLabel.setText(TOTAL_RECETTE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalRecette)+" €");
					totalRecetteEncaisseLabel.setText(TOTAL_RECETTE_ENCAISSE_LABEL+ContextClient.DECIMAL_FORMAT.format(totalRecetteEncaisse)+" €");
					calculBilan();
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void calculBilan(){
		BigDecimal bilan=totalRecette.subtract(totalDepense);
		bilanLabel.setText("Bilan = "+ContextClient.DECIMAL_FORMAT.format(bilan)+" €");
		bilanLabel.getElement().getStyle().setColor(SoldeWidget.NOIR);
		if(bilan.doubleValue()<0){
			bilanLabel.getElement().getStyle().setColor(SoldeWidget.ROUGE);
		}
		BigDecimal bilanEncaisse=totalRecetteEncaisse.subtract(totalDepenseEncaisse);
		bilanEncaisseLabel.setText("Bilan encaissé = "+ContextClient.DECIMAL_FORMAT.format(bilanEncaisse)+" €");
		bilanEncaisseLabel.getElement().getStyle().setColor(SoldeWidget.NOIR);
		if(bilanEncaisse.doubleValue()<0){
			bilanEncaisseLabel.getElement().getStyle().setColor(SoldeWidget.ROUGE);
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
