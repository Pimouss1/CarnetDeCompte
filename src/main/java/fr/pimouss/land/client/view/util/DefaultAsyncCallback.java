package fr.pimouss.land.client.view.util;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public abstract class DefaultAsyncCallback<T> implements AsyncCallback<T>{

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public final void onFailure(Throwable caught) {
		WaitBox.hideBox();
		ExceptionBox.show(caught);
	}

	/*---------------------------------------------------------------------------------------------*/
}
