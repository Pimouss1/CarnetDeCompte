package fr.pimouss.land.client.view.util;

import com.google.gwt.i18n.client.NumberFormat;

public class ContextClient {
	public static final NumberFormat DECIMAL_FORMAT=NumberFormat.getFormat("#,##0.00");
	public static final int GRID_WIDTH=700;
	public static final int MARGE_HEIGHT=30;
	private static long idUserEnCours;
	private static String keyCompteEnCours;
	private static String keyPeriodeEnCours;
	private static Boolean userIsAdministrateur=null;
	private static Boolean userIsTresorier=null;
	private static Boolean userIsContributeur=null;
	private static boolean logClient;

	/*---------------------------------------------------------------------------------------------*/
	public static boolean isLogClient() {
		return logClient;
	}
	public static void setLogClient(boolean logClient) {
		ContextClient.logClient = logClient;
	}
	public static long getIdUserEnCours() {
		return idUserEnCours;
	}
	public static void setIdUserEnCours(long idUserEnCours) {
		ContextClient.idUserEnCours = idUserEnCours;
	}
	public static Boolean getUserIsTresorier() {
		return userIsTresorier;
	}
	public static void setUserIsTresorier(Boolean userIsTresorier) {
		ContextClient.userIsTresorier = userIsTresorier;
	}
	public static Boolean getUserIsAdministrateur() {
		return userIsAdministrateur;
	}
	public static void setUserIsAdministrateur(Boolean userIsAdministrateur) {
		ContextClient.userIsAdministrateur = userIsAdministrateur;
	}
	public static Boolean getUserIsContributeur() {
		return userIsContributeur;
	}
	public static void setUserIsContributeur(Boolean userIsContributeur) {
		ContextClient.userIsContributeur = userIsContributeur;
	}
	public static String getKeyCompteEnCours() {
		return keyCompteEnCours;
	}
	public static void setKeyCompteEnCours(String keyCompteEnCours) {
		ContextClient.keyCompteEnCours = keyCompteEnCours;
	}
	public static String getKeyPeriodeEnCours() {
		return keyPeriodeEnCours;
	}
	public static void setKeyPeriodeEnCours(String keyPeriodeEnCours) {
		ContextClient.keyPeriodeEnCours = keyPeriodeEnCours;
	}

	/*---------------------------------------------------------------------------------------------*/
}
