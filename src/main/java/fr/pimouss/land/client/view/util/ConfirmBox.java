package fr.pimouss.land.client.view.util;

import com.google.gwt.core.client.GWT;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

import fr.pimouss.land.client.view.image.Images;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ConfirmBox extends MessageBox {
	private static Images images = GWT.create(Images.class);

	/*---------------------------------------------------------------------------------------------*/
	public ConfirmBox(String ask, final BooleanCallback callback) {
		super("Question");
		setPredefinedButtons(PredefinedButton.YES,PredefinedButton.NO);
		setIcon(images.ask());
		setHideOnButtonClick(true);
		setModal(true);
		setMessage(ask);

		// bouttons YES
		getButton(PredefinedButton.YES).addSelectHandler(new SelectEvent.SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				callback.execute(true);
			}
		});

		// boutton NO
		getButton(PredefinedButton.NO).addSelectHandler(new SelectEvent.SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				callback.execute(false);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface BooleanCallback {
		void execute(boolean answer);
	}

	/*---------------------------------------------------------------------------------------------*/
}
