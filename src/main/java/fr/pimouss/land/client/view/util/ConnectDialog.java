package fr.pimouss.land.client.view.util;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

import fr.pimouss.land.client.PimoussLand;
import fr.pimouss.land.client.controller.AdministrationController.IFace;
import fr.pimouss.land.client.controller.AdministrationController.IFaceAsync;
import fr.pimouss.land.client.view.image.Images;
import fr.pimouss.land.shared.dao.UtilisateurDo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ConnectDialog extends Dialog {
	private static final String STYLE_FIELDLABEL="fieldLabel";
	private static Images images = GWT.create(Images.class);
	private IFaceAsync service=GWT.create(IFace.class);
	private String connectionModuleRelativeURL=GWT.getModuleBaseURL()+"administrationService";
	private TextField loginField=new TextField();
	private TextField mailField=new TextField();
	private PasswordField passwordField=new PasswordField();
	private CheckBox resterConnecteCheckBox=new CheckBox();

	/*---------------------------------------------------------------------------------------------*/
	public ConnectDialog() {
		((ServiceDefTarget)service).setServiceEntryPoint(connectionModuleRelativeURL);

		setPredefinedButtons(PredefinedButton.OK);

		VerticalPanel vp=new VerticalPanel();
		add(vp);
		vp.add(initConnectionPanel());
		vp.add(initMotDePassePerduPanel());

		getButton(PredefinedButton.OK).addSelectHandler(new SelectEvent.SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				submit();
			}
		});

		setFocusWidget(loginField);
		setModal(true);
		setResizable(false);
		show();
	}

	/*---------------------------------------------------------------------------------------------*/
	private FramedPanel initConnectionPanel(){
		//panel de connection
		FramedPanel result=new FramedPanel();
		result.setBodyBorder(false);
		result.setHeaderVisible(false);

		FlexTable table=new FlexTable();
		result.add(table);

		Label loginLabel=new Label("Nom d'utilisateur : ");
		loginLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(0,0,loginLabel);
		table.setWidget(0,1,loginField);

		Label passwordLabel=new Label("Mot de passe : ");
		passwordLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(1,0,passwordLabel);
		table.setWidget(1,1,passwordField);
		passwordField.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
					submit();
				}
			}
		});

		Label resteConnecteLabel=new Label("Rester connecté : ");
		resteConnecteLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(2,0,resteConnecteLabel);
		resterConnecteCheckBox.addStyleName(STYLE_FIELDLABEL);
		resterConnecteCheckBox.setBoxLabel("");
		table.setWidget(2,1,resterConnecteCheckBox);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private ContentPanel initMotDePassePerduPanel(){
		//panel de connection
		ContentPanel result=new ContentPanel();
		result.setHeadingText("Mot de passe perdu");
		result.setCollapsible(true);
		result.collapse();

		FlexTable table=new FlexTable();
		result.add(table);

		Label mailLabel=new Label("Mail : ");
		mailLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(0,0,mailLabel);
		table.setWidget(0,1,mailField);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void submit(){
		if(loginField.getValue()!=null && !"".equals(loginField.getValue())
				&& passwordField.getValue()!=null && !"".equals(passwordField.getValue())){
			service.connect(loginField.getValue(), passwordField.getValue(), new DefaultAsyncCallback<UtilisateurDo>() {
				@Override
				public void onSuccess(UtilisateurDo arg0) {
					if(arg0!=null){
						ContextClient.setIdUserEnCours(Long.valueOf(arg0.getRowid()));
						if(resterConnecteCheckBox.getValue()){
							Cookies.setCookie(PimoussLand.TOKEN, arg0.getToken(),new Date(System.currentTimeMillis()+1000l*3600l*24l*365l));
						}
						PimoussLand.load("");
						hide();
					}else{
						WarningBox.show("Identification incorrect.");
						passwordField.setValue("");
						setFocusWidget(passwordField);
					}
				}
			});
		}else if(mailField.getValue()!=null && !"".equals(mailField.getValue())){
			service.motDePassePerdu(mailField.getValue(),new DefaultAsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					MessageBox messageBox=new MessageBox("Message");
					messageBox.setPredefinedButtons(PredefinedButton.CLOSE);
					messageBox.setIcon(images.info());
					messageBox.setModal(true);
					messageBox.setMessage("Un nouveau mot de passe vient de vous être envoyé par mail");
					messageBox.setVisible(true);
					messageBox.getButton(PredefinedButton.CLOSE).addSelectHandler(new SelectHandler() {
						@Override
						public void onSelect(SelectEvent event) {
							Window.Location.reload();
						}
					});
				}
			});
		}else{
			WarningBox.show("Vous devez renseigner les champs.");
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
