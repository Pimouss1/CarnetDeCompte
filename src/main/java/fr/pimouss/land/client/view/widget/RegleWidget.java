package fr.pimouss.land.client.view.widget;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.BlurEvent;
import com.sencha.gxt.widget.core.client.event.RowClickEvent;
import com.sencha.gxt.widget.core.client.event.RowClickEvent.RowClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.BigDecimalField;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.IntegerField;
import com.sencha.gxt.widget.core.client.form.TextArea;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;

import fr.pimouss.land.client.controller.RegleController.IFace;
import fr.pimouss.land.client.controller.RegleController.IFaceAsync;
import fr.pimouss.land.client.model.GenericComboBox;
import fr.pimouss.land.client.model.GenericComboModel;
import fr.pimouss.land.client.model.GenericComboStore;
import fr.pimouss.land.client.model.GenericSelectionEvent;
import fr.pimouss.land.client.model.RegleListStore;
import fr.pimouss.land.client.view.image.Images;
import fr.pimouss.land.client.view.util.ConfirmBox;
import fr.pimouss.land.client.view.util.ConfirmBox.BooleanCallback;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.UtilForm;
import fr.pimouss.land.client.view.util.WaitBox;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;

public class RegleWidget extends Composite implements ITabPanelItem {
	private static final String PREFIX_KEY_DECLENCHEMENT="d";
	private static final String PREFIX_KEY_TYPE_MVT="t";
	private static final String PREFIX_KEY_JOUR="j";
	private static final String PREFIX_KEY_MOIS="m";
	private static final String PREFIX_KEY_MODE_PAIEMENT="m";
	private static final String PREFIX_KEY_VENTILATION_SOURCE="s";
	private static final String PREFIX_KEY_VENTILATION_TARGET="t";
	private static final String PREFIX_KEY_COMPTE="c";
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"regleService";
	private FlowPanel rootPanel=new FlowPanel();
	private final Images images = GWT.create(Images.class);
	private Grid<RegleDo> regleGrid;
	private RegleListStore regleGridStore=new RegleListStore();
	private GenericComboStore declenchementComboStore=new GenericComboStore();
	private GenericComboStore typeMvtComboStore=new GenericComboStore();
	private GenericComboStore compteComboStore=new GenericComboStore();
	private GenericComboStore modePaiementComboStore=new GenericComboStore();
	private GenericComboStore ventilationSrcComboStore=new GenericComboStore();
	private GenericComboStore ventilationTargetComboStore=new GenericComboStore();
	private GenericComboStore jourComboStore=new GenericComboStore();
	private GenericComboStore moisComboStore=new GenericComboStore();
	private GenericComboBox declenchementField=new GenericComboBox(declenchementComboStore);
	private GenericComboBox typeMvtSrcField=new GenericComboBox(typeMvtComboStore);
	private GenericComboBox ventilationSrcField=new GenericComboBox(ventilationSrcComboStore);
	private GenericComboBox jourField=new GenericComboBox(jourComboStore);
	private GenericComboBox moisField=new GenericComboBox(moisComboStore);
	private GenericComboBox compteField=new GenericComboBox(compteComboStore);
	private GenericComboBox typeMvtTargetField=new GenericComboBox(typeMvtComboStore);
	private GenericComboBox ventilationTargetField=new GenericComboBox(ventilationTargetComboStore);
	private GenericComboBox modePaiementField=new GenericComboBox(modePaiementComboStore);
	private TextArea libelleField=new TextArea();
	private TextArea objetField=new TextArea();
	private BigDecimalField montantField=new BigDecimalField();
	private IntegerField anticipeField=new IntegerField();
	private CheckBox horsBilanField=new CheckBox();

	private RegleDo regleEnCours;
	private boolean preload=false;

	/*---------------------------------------------------------------------------------------------*/
	public RegleWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		HorizontalPanel hp=new HorizontalPanel();
		rootPanel.add(hp);

		hp.add(initRegleGrid());

		VerticalPanel vp=new VerticalPanel();
		hp.add(vp);
		vp.add(initDeclencheurFieldSet());
		vp.add(initRegleFieldSet());
		vp.add(initButtonPanel());
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initRegleGrid() {
		List<ColumnConfig<RegleDo, ?>> config=new ArrayList<>();

		ValueProvider<RegleDo, String> declenchementProvider=new ValueProvider<RegleDo, String>() {
			@Override
			public void setValue(RegleDo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(RegleDo object) {
				if(object.getRowid()==0){
					return "";
				}
				if(object.getFrequence()==null || "".equals(object.getFrequence())){
					return "Par ventilation";
				}
				if(RegleDo.FREQUENCE_MENSUELLE.equals(object.getFrequence())){
					return "Automatique - Mensuel";
				}
				if(RegleDo.FREQUENCE_ANNUELLE.equals(object.getFrequence())){
					return "Automatique - Annuel";
				}
				return null;
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<RegleDo, String> cf4=new ColumnConfig<>(declenchementProvider,140,"Déclenchement");
		config.add(cf4);

		ValueProvider<RegleDo, ImageResource> imageProvider=new ValueProvider<RegleDo, ImageResource>() {
			@Override
			public void setValue(RegleDo object, ImageResource value) {/*nothing to do*/}
			@Override
			public ImageResource getValue(RegleDo object) {
				if(MouvementDo.DEPENSE.equals(object.getTypeMouvementTarget())){
					return images.moins();
				}
				if(MouvementDo.RECETTE.equals(object.getTypeMouvementTarget())){
					return images.plus();
				}
				return null;
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<RegleDo, ImageResource> cf1=new ColumnConfig<>(imageProvider,25,"");
		cf1.setCell(new ImageResourceCell());
		config.add(cf1);

		ValueProvider<RegleDo, String> libelleProvider=new ValueProvider<RegleDo, String>() {
			@Override
			public void setValue(RegleDo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(RegleDo object) {
				return object.getLibelle();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<RegleDo, String> cf5=new ColumnConfig<>(libelleProvider,ContextClient.GRID_WIDTH-440,"Libellé");
		config.add(cf5);

		ValueProvider<RegleDo, String> compteProvider=new ValueProvider<RegleDo, String>() {
			@Override
			public void setValue(RegleDo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(RegleDo object) {
				for(GenericComboModel elt:compteComboStore.getAll()){
					if(Long.parseLong(elt.getKey().substring(1))==object.getIdCompte()){
						return elt.getDisplayCombo();
					}
				}
				return null;
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<RegleDo, String> cf7=new ColumnConfig<>(compteProvider,160,"Compte");
		config.add(cf7);

		ValueProvider<RegleDo, BigDecimal> montantProvider=new ValueProvider<RegleDo, BigDecimal>() {
			@Override
			public void setValue(RegleDo object, BigDecimal value) {/*nothing to do*/}
			@Override
			public BigDecimal getValue(RegleDo object) {
				return object.getMontant();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<RegleDo, BigDecimal> cf6=new ColumnConfig<>(montantProvider,80,"Montant");
		cf6.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		cf6.setCell(new AbstractCell<BigDecimal>() {
			@Override
			public void render(com.google.gwt.cell.client.Cell.Context context, BigDecimal value, SafeHtmlBuilder sb) {
				if(value!=null){
					sb.appendHtmlConstant(ContextClient.DECIMAL_FORMAT.format(value)+" €");
				}
			}
		});
		config.add(cf6);

		regleGrid=new Grid<>(regleGridStore, new ColumnModel<>(config));
		regleGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT-22);
		regleGrid.setWidth(ContextClient.GRID_WIDTH);
		regleGrid.setVisible(true);
		regleGrid.addRowClickHandler(new RowClickHandler() {
			@Override
			public void onRowClick(RowClickEvent event) {
				regleEnCours=regleGrid.getStore().get(event.getRowIndex());
				loadRegleEnCours();
			}
		});

		return regleGrid;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void declenchementOnSelection(FlexTable table, boolean typeMvt, boolean jour, boolean mois){
		typeMvtSrcField.clear();
		typeMvtSrcField.setEnabled(typeMvt);
		typeMvtSrcField.setAllowBlank(!typeMvt);
		table.setWidget(1, 0, UtilForm.getFieldLabel(typeMvtSrcField, "Type mouvement source"));
		ventilationSrcField.clear();
		ventilationSrcField.setEnabled(typeMvt);
		ventilationSrcField.setAllowBlank(!typeMvt);
		table.setWidget(1, 2, UtilForm.getFieldLabel(ventilationSrcField, "Ventilation source"));
		jourField.clear();
		jourField.setEnabled(jour);
		jourField.setAllowBlank(!jour);
		table.setWidget(2, 0, UtilForm.getFieldLabel(jourField, "Jour"));
		moisField.clear();
		moisField.setEnabled(mois);
		moisField.setAllowBlank(!mois);
		table.setWidget(2, 2, UtilForm.getFieldLabel(moisField, "Mois"));
		anticipeField.clear();
		anticipeField.setEnabled(jour);
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initDeclencheurFieldSet(){
		final FieldSet result=new FieldSet();
		result.setHeadingText("Déclencheur");
		final FlexTable table=new FlexTable();
		result.add(table);
		int row=0;

		declenchementField.setTriggerAction(TriggerAction.ALL);
		declenchementField.setEditable(false);
		typeMvtSrcField.setTriggerAction(TriggerAction.ALL);
		typeMvtSrcField.setEditable(false);
		typeMvtSrcField.setWidth(100);
		ventilationSrcField.setTriggerAction(TriggerAction.ALL);
		ventilationSrcField.setEditable(false);
		ventilationSrcField.setWidth(400);
		jourField.setTriggerAction(TriggerAction.ALL);
		jourField.setEditable(false);
		moisField.setTriggerAction(TriggerAction.ALL);
		moisField.setEditable(false);
		compteField.setTriggerAction(TriggerAction.ALL);
		compteField.setEditable(false);
		compteField.setWidth(200);
		typeMvtTargetField.setTriggerAction(TriggerAction.ALL);
		typeMvtTargetField.setEditable(false);
		typeMvtTargetField.setWidth(100);
		libelleField.setWidth(350);
		libelleField.setHeight(50);
		objetField.setWidth(350);
		objetField.setHeight(50);
		ventilationTargetField.setTriggerAction(TriggerAction.ALL);
		ventilationTargetField.setEditable(false);
		ventilationTargetField.setWidth(400);
		modePaiementField.setTriggerAction(TriggerAction.ALL);
		modePaiementField.setEditable(false);
		enableFields(false);

		declenchementField.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				declenchementField.setValue(event.getSelectedItem().getKey());
				if((PREFIX_KEY_DECLENCHEMENT+"1").equals(event.getSelectedItem().getKey())){
					declenchementOnSelection(table, true, false, false);
				}
				if((PREFIX_KEY_DECLENCHEMENT+"2").equals(event.getSelectedItem().getKey())){
					declenchementOnSelection(table, false, true, false);
				}
				if((PREFIX_KEY_DECLENCHEMENT+"3").equals(event.getSelectedItem().getKey())){
					declenchementOnSelection(table, false, true, true);
				}
			}
		});

		typeMvtSrcField.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				typeMvtSrcField.setValue(event.getSelectedItem().getKey());
				if((PREFIX_KEY_TYPE_MVT+"1").equals(event.getSelectedItem().getKey())){
					service.getAllTypeVentilationDepense(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
						@Override
						public void onSuccess(List<TypeVentilationDo> arg0) {
							ventilationSrcComboStore.clear();
							for(TypeVentilationDo elt:arg0){
								ventilationSrcComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION_SOURCE+elt.getRowid(), elt.getTypeVentilation()));
							}
							if(regleEnCours.getRowid()!=0){
								ventilationSrcField.setValue(PREFIX_KEY_VENTILATION_SOURCE+regleEnCours.getIdTypeVentilationSrc());
							}
						}
					});
				}
				if((PREFIX_KEY_TYPE_MVT+"2").equals(event.getSelectedItem().getKey())){
					service.getAllTypeVentilationRecette(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
						@Override
						public void onSuccess(List<TypeVentilationDo> arg0) {
							ventilationSrcComboStore.clear();
							for(TypeVentilationDo elt:arg0){
								ventilationSrcComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION_SOURCE+elt.getRowid(), elt.getTypeVentilation()));
							}
							if(regleEnCours.getRowid()!=0){
								ventilationSrcField.setValue(PREFIX_KEY_VENTILATION_SOURCE+regleEnCours.getIdTypeVentilationSrc());
							}
						}
					});
				}
			}
		});

		typeMvtTargetField.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				typeMvtTargetField.setValue(event.getSelectedItem().getKey());
				ventilationTargetField.clear();
				modePaiementField.clear();
				if((PREFIX_KEY_TYPE_MVT+"1").equals(event.getSelectedItem().getKey())){
					service.getAllTypeVentilationDepense(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
						@Override
						public void onSuccess(List<TypeVentilationDo> arg0) {
							ventilationTargetComboStore.clear();
							for(TypeVentilationDo elt:arg0){
								ventilationTargetComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION_TARGET+elt.getRowid(), elt.getTypeVentilation()));
							}
							if(regleEnCours.getRowid()!=0){
								ventilationTargetField.setValue(PREFIX_KEY_VENTILATION_TARGET+regleEnCours.getIdTypeVentilationTarget());
							}
						}
					});
					service.getAllModePaiementDepense(new DefaultAsyncCallback<List<ModePaiementDo>>() {
						@Override
						public void onSuccess(List<ModePaiementDo> arg0) {
							modePaiementComboStore.clear();
							for(ModePaiementDo elt:arg0){
								modePaiementComboStore.add(new GenericComboModel(PREFIX_KEY_MODE_PAIEMENT+elt.getRowid(), elt.getModePaiement()));
							}
							if(regleEnCours.getRowid()!=0){
								modePaiementField.setValue(PREFIX_KEY_MODE_PAIEMENT+regleEnCours.getIdModePaiement());
							}
						}
					});
				}
				if((PREFIX_KEY_TYPE_MVT+"2").equals(event.getSelectedItem().getKey())){
					service.getAllTypeVentilationRecette(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
						@Override
						public void onSuccess(List<TypeVentilationDo> arg0) {
							ventilationTargetComboStore.clear();
							for(TypeVentilationDo elt:arg0){
								ventilationTargetComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION_TARGET+elt.getRowid(), elt.getTypeVentilation()));
							}
							if(regleEnCours.getRowid()!=0){
								ventilationTargetField.setValue(PREFIX_KEY_VENTILATION_TARGET+regleEnCours.getIdTypeVentilationTarget());
							}
						}
					});
					service.getAllModePaiementRecette(new DefaultAsyncCallback<List<ModePaiementDo>>() {
						@Override
						public void onSuccess(List<ModePaiementDo> arg0) {
							modePaiementComboStore.clear();
							for(ModePaiementDo elt:arg0){
								modePaiementComboStore.add(new GenericComboModel(PREFIX_KEY_MODE_PAIEMENT+elt.getRowid(), elt.getModePaiement()));
							}
							if(regleEnCours.getRowid()!=0){
								modePaiementField.setValue(PREFIX_KEY_MODE_PAIEMENT+regleEnCours.getIdModePaiement());
							}
						}
					});
				}
			}
		});

		declenchementField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(declenchementField, "Déclenchement"));
		table.setWidget(row, 1, declenchementField);
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(typeMvtSrcField, "Type mouvement source"));
		table.setWidget(row, 1, typeMvtSrcField);
		table.setWidget(row, 2, UtilForm.getFieldLabel(ventilationSrcField, "Ventilation source"));
		table.setWidget(row, 3, ventilationSrcField);
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(jourField, "Jour"));
		table.setWidget(row, 1, jourField);
		table.setWidget(row, 2, UtilForm.getFieldLabel(moisField, "Mois"));
		table.setWidget(row, 3, moisField);
		row++;

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initRegleFieldSet(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Règle");
		FlexTable table=new FlexTable();
		result.add(table);
		int row=0;

		typeMvtTargetField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(typeMvtTargetField,"Type mouvement cible"));
		table.setWidget(row, 1, typeMvtTargetField);
		ventilationTargetField.setAllowBlank(false);
		table.setWidget(row, 2, UtilForm.getFieldLabel(ventilationTargetField, "Ventilation cible"));
		table.setWidget(row, 3, ventilationTargetField);
		row++;

		compteField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(compteField, "Compte"));
		table.setWidget(row, 1, compteField);
		compteField.setTriggerAction(TriggerAction.ALL);
		compteField.setEditable(false);
		row++;

		libelleField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(libelleField, "Libellé"));
		libelleField.setWidth(380);
		libelleField.setHeight(50);
		table.setWidget(row, 1, libelleField);
		table.getFlexCellFormatter().setColSpan(row,1,3);
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(objetField, "Objet"));
		objetField.setWidth(380);
		objetField.setHeight(50);
		table.setWidget(row, 1, objetField);
		table.getFlexCellFormatter().setColSpan(row,1,3);
		row++;

		modePaiementField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(modePaiementField, "Mode de paiement"));
		table.setWidget(row, 1, modePaiementField);
		modePaiementField.setTriggerAction(TriggerAction.ALL);
		modePaiementField.setEditable(false);
		table.setWidget(row, 2, UtilForm.getFieldLabel(horsBilanField,"Hors bilan"));
		table.setWidget(row, 3, horsBilanField);
		horsBilanField.setBoxLabel("");
		row++;

		montantField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(montantField, "Montant"));
		table.setWidget(row, 1, montantField);
		montantField.setFormat(ContextClient.DECIMAL_FORMAT);
		table.setWidget(row, 2, UtilForm.getFieldLabel(anticipeField, "Anticiper"));
		table.setWidget(row, 3, anticipeField);
		row++;

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void save(){
		if(regleEnCours!=null && checkField()){
			WaitBox.showBox();
			if(anticipeField.getValue()!=null){
				regleEnCours.setAnticipe(anticipeField.getValue());
			}else{
				regleEnCours.setAnticipe(0);
			}
			if((PREFIX_KEY_DECLENCHEMENT+"1").equals(declenchementField.getValue().getKey())){
				regleEnCours.setFrequence("");
			}
			if((PREFIX_KEY_DECLENCHEMENT+"2").equals(declenchementField.getValue().getKey())){
				regleEnCours.setFrequence(RegleDo.FREQUENCE_MENSUELLE);
			}
			if((PREFIX_KEY_DECLENCHEMENT+"3").equals(declenchementField.getValue().getKey())){
				regleEnCours.setFrequence(RegleDo.FREQUENCE_ANNUELLE);
			}
			regleEnCours.setIdCompte(Long.parseLong(compteField.getValue().getKey().substring(1)));
			regleEnCours.setIdModePaiement(Long.parseLong(modePaiementField.getValue().getKey().substring(1)));
			if(ventilationSrcField.getValue()!=null){
				regleEnCours.setIdTypeVentilationSrc(Long.parseLong(ventilationSrcField.getValue().getKey().substring(1)));
			}else{
				regleEnCours.setIdTypeVentilationSrc(0);
			}
			regleEnCours.setIdTypeVentilationTarget(Long.parseLong(ventilationTargetField.getValue().getKey().substring(1)));
			if(jourField.getValue()!=null){
				regleEnCours.setJourDuMois(Integer.parseInt(jourField.getValue().getKey().substring(1)));
			}
			if(moisField.getValue()!=null){
				regleEnCours.setMois(Integer.parseInt(moisField.getValue().getKey().substring(1)));
			}
			regleEnCours.setLibelle(libelleField.getValue());
			regleEnCours.setObjet(objetField.getValue());
			if(montantField.getValue()!=null){
				regleEnCours.setMontant(montantField.getValue());
			}else{
				regleEnCours.setMontant(new BigDecimal(0));
			}
			if(horsBilanField.getValue()!=null){
				regleEnCours.setHorsBilan(horsBilanField.getValue());
			}else{
				regleEnCours.setHorsBilan(false);
			}
			if((PREFIX_KEY_TYPE_MVT+"1").equals(typeMvtTargetField.getValue().getKey())){
				regleEnCours.setTypeMouvementTarget(MouvementDo.DEPENSE);
			}
			if((PREFIX_KEY_TYPE_MVT+"2").equals(typeMvtTargetField.getValue().getKey())){
				regleEnCours.setTypeMouvementTarget(MouvementDo.RECETTE);
			}
			if(typeMvtSrcField.getValue()!=null && (PREFIX_KEY_TYPE_MVT+"1").equals(typeMvtSrcField.getValue().getKey())){
				regleEnCours.setTypeMouvementSrc(MouvementDo.DEPENSE);
			}
			if(typeMvtSrcField.getValue()!=null && (PREFIX_KEY_TYPE_MVT+"2").equals(typeMvtSrcField.getValue().getKey())){
				regleEnCours.setTypeMouvementSrc(MouvementDo.RECETTE);
			}
			service.saveRegle(ContextClient.getIdUserEnCours(), regleEnCours, new DefaultAsyncCallback<RegleDo>() {
				@Override
				public void onSuccess(RegleDo arg0) {
					regleEnCours=arg0;
					load("");
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void resetFields(){
		anticipeField.setValue(regleEnCours.getAnticipe());
		if("".equals(regleEnCours.getFrequence())){
			GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"1");
		}
		if(RegleDo.FREQUENCE_MENSUELLE.equals(regleEnCours.getFrequence())){
			GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"2");
		}
		if(RegleDo.FREQUENCE_ANNUELLE.equals(regleEnCours.getFrequence())){
			GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"3");
		}
		if(MouvementDo.DEPENSE.equals(regleEnCours.getTypeMouvementSrc())){
			GenericSelectionEvent.fire(typeMvtSrcField, PREFIX_KEY_TYPE_MVT+"1");
		}
		if(MouvementDo.RECETTE.equals(regleEnCours.getTypeMouvementSrc())){
			GenericSelectionEvent.fire(typeMvtSrcField, PREFIX_KEY_TYPE_MVT+"2");
		}
		if(MouvementDo.DEPENSE.equals(regleEnCours.getTypeMouvementTarget())){
			GenericSelectionEvent.fire(typeMvtTargetField, PREFIX_KEY_TYPE_MVT+"1");
		}
		if(MouvementDo.RECETTE.equals(regleEnCours.getTypeMouvementTarget())){
			GenericSelectionEvent.fire(typeMvtTargetField, PREFIX_KEY_TYPE_MVT+"2");
		}
		compteField.setValue(PREFIX_KEY_COMPTE+regleEnCours.getIdCompte());
		jourField.setValue(PREFIX_KEY_JOUR+regleEnCours.getJourDuMois());
		moisField.setValue(PREFIX_KEY_MOIS+regleEnCours.getMois());
		libelleField.setValue(regleEnCours.getLibelle());
		montantField.setValue(regleEnCours.getMontant());
		objetField.setValue(regleEnCours.getObjet());
		horsBilanField.setValue(regleEnCours.isHorsBilan());
	}

	/*---------------------------------------------------------------------------------------------*/
	private void preload(){
		if(declenchementComboStore.size()==0){
			declenchementComboStore.clear();
			declenchementComboStore.add(new GenericComboModel(PREFIX_KEY_DECLENCHEMENT+"1","Par ventilation"));
			declenchementComboStore.add(new GenericComboModel(PREFIX_KEY_DECLENCHEMENT+"2","Automatique - Mensuel"));
			declenchementComboStore.add(new GenericComboModel(PREFIX_KEY_DECLENCHEMENT+"3","Automatique - Annuel"));
		}

		if(typeMvtComboStore.size()==0){
			typeMvtComboStore.clear();
			typeMvtComboStore.add(new GenericComboModel(PREFIX_KEY_TYPE_MVT+"1","Dépense"));
			typeMvtComboStore.add(new GenericComboModel(PREFIX_KEY_TYPE_MVT+"2","Recette"));
		}

		if(jourComboStore.size()==0){
			jourComboStore.clear();
			for(int i=1;i<=28;i++){
				jourComboStore.add(new GenericComboModel(PREFIX_KEY_JOUR+i,String.valueOf(i)));
			}
		}

		if(moisComboStore.size()==0){
			moisComboStore.clear();
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"1","Janvier"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"2","Février"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"3","Mars"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"4","Avril"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"5","Mai"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"6","Juin"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"7","Juillet"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"8","Août"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"9","Septembre"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"10","Octobre"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"11","Novembre"));
			moisComboStore.add(new GenericComboModel(PREFIX_KEY_MOIS+"12","Décembre"));
		}

		if(compteComboStore.size()==0){
			service.getAllCompte(new DefaultAsyncCallback<List<CompteDo>>() {
				@Override
				public void onSuccess(List<CompteDo> result) {
					for(CompteDo elt:result){
						compteComboStore.add(new GenericComboModel(PREFIX_KEY_COMPTE+elt.getRowid(),elt.getCompte()));
					}
					preload=true;
					load("");
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		WaitBox.showBox();
		if(!preload){
			preload();
		}else{
			regleGridStore.clear();
			service.getAllRegle(new DefaultAsyncCallback<List<RegleDo>>() {
				@Override
				public void onSuccess(List<RegleDo> arg0) {
					RegleDo newRegle=new RegleDo();
					newRegle.setLibelle("<Nouveau>");
					regleGridStore.add(newRegle);
					regleGridStore.addAll(arg0);
					loadRegleEnCours();
					WaitBox.hideBox();
				}
			});
		}

	}

	/*---------------------------------------------------------------------------------------------*/
	private void loadRegleEnCours(){
		clearFields();
		if(regleEnCours==null){
			enableFields(false);
		}else{
			enableFields(true);
			if(regleEnCours.getRowid()==0){
				GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"1");
			}else{
				if("".equals(regleEnCours.getFrequence())){
					GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"1");
				}
				if(RegleDo.FREQUENCE_MENSUELLE.equals(regleEnCours.getFrequence())){
					GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"2");
				}
				if(RegleDo.FREQUENCE_ANNUELLE.equals(regleEnCours.getFrequence())){
					GenericSelectionEvent.fire(declenchementField, PREFIX_KEY_DECLENCHEMENT+"3");
				}
				if(MouvementDo.DEPENSE.equals(regleEnCours.getTypeMouvementSrc())){
					GenericSelectionEvent.fire(typeMvtSrcField, PREFIX_KEY_TYPE_MVT+"1");
				}
				if(MouvementDo.RECETTE.equals(regleEnCours.getTypeMouvementSrc())){
					GenericSelectionEvent.fire(typeMvtSrcField, PREFIX_KEY_TYPE_MVT+"2");
				}
				if(MouvementDo.DEPENSE.equals(regleEnCours.getTypeMouvementTarget())){
					GenericSelectionEvent.fire(typeMvtTargetField, PREFIX_KEY_TYPE_MVT+"1");
				}
				if(MouvementDo.RECETTE.equals(regleEnCours.getTypeMouvementTarget())){
					GenericSelectionEvent.fire(typeMvtTargetField, PREFIX_KEY_TYPE_MVT+"2");
				}
				compteField.setValue(PREFIX_KEY_COMPTE+regleEnCours.getIdCompte());
				jourField.setValue(PREFIX_KEY_JOUR+regleEnCours.getJourDuMois());
				moisField.setValue(PREFIX_KEY_MOIS+regleEnCours.getMois());
				libelleField.setValue(regleEnCours.getLibelle());
				montantField.setValue(regleEnCours.getMontant());
				objetField.setValue(regleEnCours.getObjet());
				horsBilanField.setValue(regleEnCours.isHorsBilan());
				anticipeField.setValue(regleEnCours.getAnticipe());
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void clearFields(){
		declenchementField.clear();
		typeMvtSrcField.clear();
		ventilationSrcField.clear();
		jourField.clear();
		moisField.clear();
		ventilationTargetField.clear();
		compteField.clear();
		libelleField.clear();
		objetField.clear();
		modePaiementField.clear();
		horsBilanField.clear();
		montantField.clear();
		anticipeField.clear();
	}

	/*---------------------------------------------------------------------------------------------*/
	private void enableFields(boolean enabled){
		declenchementField.setEnabled(enabled);
		typeMvtSrcField.setEnabled(enabled);
		ventilationSrcField.setEnabled(enabled);
		jourField.setEnabled(enabled);
		moisField.setEnabled(enabled);
		typeMvtTargetField.setEnabled(enabled);
		ventilationTargetField.setEnabled(enabled);
		compteField.setEnabled(enabled);
		libelleField.setEnabled(enabled);
		objetField.setEnabled(enabled);
		modePaiementField.setEnabled(enabled);
		horsBilanField.setEnabled(enabled);
		montantField.setEnabled(enabled);
		anticipeField.setEnabled(enabled);
	}

	/*---------------------------------------------------------------------------------------------*/
	private boolean checkField(){
		declenchementField.focus();
		declenchementField.fireEvent(new BlurEvent());
		typeMvtSrcField.focus();
		typeMvtSrcField.fireEvent(new BlurEvent());
		ventilationSrcField.focus();
		ventilationSrcField.fireEvent(new BlurEvent());
		jourField.focus();
		jourField.fireEvent(new BlurEvent());
		moisField.focus();
		moisField.fireEvent(new BlurEvent());
		typeMvtTargetField.focus();
		typeMvtTargetField.fireEvent(new BlurEvent());
		ventilationTargetField.focus();
		ventilationTargetField.fireEvent(new BlurEvent());
		compteField.focus();
		compteField.fireEvent(new BlurEvent());
		libelleField.focus();
		libelleField.fireEvent(new BlurEvent());
		objetField.focus();
		objetField.fireEvent(new BlurEvent());
		modePaiementField.focus();
		modePaiementField.fireEvent(new BlurEvent());
		horsBilanField.focus();
		horsBilanField.fireEvent(new BlurEvent());
		montantField.focus();
		montantField.fireEvent(new BlurEvent());
		anticipeField.focus();
		anticipeField.fireEvent(new BlurEvent());

		return !(!declenchementField.isAllowBlank() && declenchementField.getValue()==null
				|| !typeMvtSrcField.isAllowBlank() && typeMvtSrcField.getValue()==null
				|| !ventilationSrcField.isAllowBlank() && ventilationSrcField.getValue()==null
				|| !jourField.isAllowBlank() && jourField.getValue()==null
				|| !moisField.isAllowBlank() && moisField.getValue()==null
				|| !typeMvtTargetField.isAllowBlank() && typeMvtTargetField.getValue()==null
				|| !ventilationTargetField.isAllowBlank() && ventilationTargetField.getValue()==null
				|| !compteField.isAllowBlank() && compteField.getValue()==null
				|| !libelleField.isAllowBlank() && (libelleField.getValue()==null || "".equals(libelleField.getValue()))
				|| !objetField.isAllowBlank() && (objetField.getValue()==null || "".equals(objetField.getValue()))
				|| !modePaiementField.isAllowBlank() && modePaiementField.getValue()==null
				|| !montantField.isAllowBlank() && (montantField.getValue()==null || montantField.getValue().doubleValue()==0)
				|| !anticipeField.isAllowBlank() && anticipeField.getValue()==null);
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initButtonPanel(){
		HorizontalPanel result=new HorizontalPanel();

		// enregistrer
		final TextButton saveButton=new TextButton("Enregistrer");
		result.add(saveButton);
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				save();
			}
		});

		// annuler
		final TextButton cancelButton=new TextButton("Annuler");
		result.add(cancelButton);
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				resetFields();
			}
		});

		// supprimer
		result.add(new SeparatorToolItem());
		final TextButton deleteButton=new TextButton("Supprimer");
		result.add(deleteButton);
		deleteButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				new ConfirmBox("Etes-vous sûr de vouloir supprimer cette règle ?", new BooleanCallback() {
					@Override
					public void execute(boolean answer) {
						if(answer){
							WaitBox.showBox();
							service.deleteRegle(ContextClient.getIdUserEnCours(), regleEnCours.getRowid(), new DefaultAsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {
									regleEnCours=null;
									load("");
								}
							});
						}
					}
				}).show();
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
