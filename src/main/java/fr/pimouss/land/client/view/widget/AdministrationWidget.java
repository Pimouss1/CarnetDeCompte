package fr.pimouss.land.client.view.widget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

import fr.pimouss.land.client.PimoussLand;
import fr.pimouss.land.client.controller.AdministrationController.IFace;
import fr.pimouss.land.client.controller.AdministrationController.IFaceAsync;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.InfoBox;
import fr.pimouss.land.client.view.util.WaitBox;
import fr.pimouss.land.client.view.util.WarningBox;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.service.SurveillanceBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class AdministrationWidget extends Composite implements ITabPanelItem {
	private static final String STYLE_FIELDLABEL="fieldLabel";
	private static final DateTimeFormat DATETIME_FORMAT=DateTimeFormat.getFormat("dd/MM/yyyy HH:mm:ss");
	private boolean loaded=false;
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"administrationService";
	private FlowPanel rootPanel=new FlowPanel();
	private TextButton forceSaveDatabaseButton=new TextButton("Forcer la sauvegarde de la base de données");
	private TextButton forceRAZSoldeButton=new TextButton("Forcer RAZ solde");
	private TextButton forceVerifSoldeButton=new TextButton("Forcer vérification solde");
	private TextButton forceVerifVentilButton=new TextButton("Forcer vérification ventilations");
	private TextButton forceVerifNumPieceButton=new TextButton("Forcer vérification numéros de pièces");
	private TextButton verifJustifButton=new TextButton("Vérifier justificatifs");
	private FieldSet actionFieldSet=new FieldSet();
	private VerticalPanel actionPanel=new VerticalPanel();
	private FieldSet periodeFieldSet=new FieldSet();
	private VerticalPanel periodePanel=new VerticalPanel();
	private List<PeriodeDo> listePeriode=new ArrayList<>();
	private List<DateField> listeDateDebutField=new ArrayList<>();
	private List<DateField> listeDateFinField=new ArrayList<>();
	private List<TextField> listeNomField=new ArrayList<>();
	private List<CheckBox> listeBilanFigeField=new ArrayList<>();
	private TextButton addButton=new TextButton("Ajouter période");
	private TextButton cancelButton=new TextButton("Annuler");
	private TextButton saveButton=new TextButton("Enregistrer");

	/*---------------------------------------------------------------------------------------------*/
	public AdministrationWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		VerticalPanel vp=new VerticalPanel();
		rootPanel.add(vp);

		HorizontalPanel hp=new HorizontalPanel();
		vp.add(hp);
		hp.add(initChangePasswordPanel());
		hp.add(initDeconnectionPanel());
		hp.add(initVersionPanel());
		hp.add(initAdministrationPanel());

		vp.add(initPeriodePanel());
		vp.add(initActionPanel());
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(final String ancre){
		forceSaveDatabaseButton.setEnabled(ContextClient.getUserIsAdministrateur());
		forceRAZSoldeButton.setEnabled(ContextClient.getUserIsAdministrateur());
		forceVerifSoldeButton.setEnabled(ContextClient.getUserIsAdministrateur());
		forceVerifVentilButton.setEnabled(ContextClient.getUserIsAdministrateur());
		forceVerifNumPieceButton.setEnabled(ContextClient.getUserIsAdministrateur());
		addButton.setEnabled(ContextClient.getUserIsTresorier());
		saveButton.setEnabled(ContextClient.getUserIsTresorier());
		cancelButton.setEnabled(ContextClient.getUserIsTresorier());

		if(ContextClient.getUserIsAdministrateur()){
			service.getLastAction(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<List<SurveillanceBo>>() {
				@Override
				public void onSuccess(List<SurveillanceBo> result) {
					actionFieldSet.setVisible(true);
					actionPanel.clear();
					for(SurveillanceBo elt:result){
						actionPanel.add(new Label(elt.getUtilisateurDo().getPrenom()+" "+elt.getUtilisateurDo().getNom()+" - "+
							DATETIME_FORMAT.format(new Date(elt.getSurveillanceDo().getDate()))+" - "+
							elt.getSurveillanceDo().getIp()+" - "+
							elt.getSurveillanceDo().getClasse()+"."+elt.getSurveillanceDo().getMethode()));
					}
				}
			});
		}

		service.getAllPeriode(new DefaultAsyncCallback<List<PeriodeDo>>() {
			@Override
			public void onSuccess(List<PeriodeDo> result) {
				listePeriode.clear();
				periodePanel.clear();
				listeBilanFigeField.clear();
				listeDateDebutField.clear();
				listeDateFinField.clear();
				listeNomField.clear();
				for(PeriodeDo periode:result){
					periodePanel.add(createPeriodeLine(periode));
				}
			}
		});

		loaded=true;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initVersionPanel(){
		FieldSet result=new FieldSet();
		VerticalPanel vp1=new VerticalPanel();
		result.add(vp1);
		result.setHeadingText("Version");

		final Label versionAppLabel=new Label("Version App = ");
		vp1.add(versionAppLabel);
		final Label versionDBLabel=new Label("Version DB = ");
		vp1.add(versionDBLabel);
		service.getNumVersion(new DefaultAsyncCallback<String[]>() {
			@Override
			public void onSuccess(String[] result) {
				versionAppLabel.setText("Version App = "+result[0]);
				versionDBLabel.setText("Version DB = "+result[1]);
				if(!result[0].equals(result[1])){
					versionAppLabel.addStyleName("serverResponseLabelError");
					versionDBLabel.addStyleName("serverResponseLabelError");
				}
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initChangePasswordPanel(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Changez votre mot de passe");

		FlexTable table=new FlexTable();
		result.add(table);

		Label ancienMDPLabel=new Label("Ancien mot de passe : ");
		ancienMDPLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(0,0,ancienMDPLabel);
		final PasswordField ancienMDPField=new PasswordField();
		ancienMDPField.setAllowBlank(false);
		table.setWidget(0,1,ancienMDPField);

		Label nouveauMDPLabel=new Label("Nouveau mot de passe : ");
		nouveauMDPLabel.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(1,0,nouveauMDPLabel);
		final PasswordField nouveauMDPField=new PasswordField();
		nouveauMDPField.setAllowBlank(false);
		table.setWidget(1,1,nouveauMDPField);

		Label nouveauMDP2Label=new Label("Retapez votre mot de passe : ");
		nouveauMDP2Label.addStyleName(STYLE_FIELDLABEL);
		table.setWidget(2,0,nouveauMDP2Label);
		final PasswordField nouveauMDP2Field=new PasswordField();
		nouveauMDP2Field.setAllowBlank(false);
		table.setWidget(2,1,nouveauMDP2Field);

		TextButton changeButton=new TextButton(" OK ");
		table.setWidget(3,1,changeButton);
		changeButton.addSelectHandler(new SelectEvent.SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if(nouveauMDPField.getValue()==null || !nouveauMDPField.getValue().equals(nouveauMDP2Field.getValue())){
					WarningBox.show("Vous n'avez pas tapé le même mot de passe.");
				}else{
					service.changePassword(ContextClient.getIdUserEnCours(),
							ancienMDPField.getValue(),
							nouveauMDPField.getValue(),
							new DefaultAsyncCallback<Void>() {
						@Override
						public void onSuccess(Void result) {
							ancienMDPField.clear();
							nouveauMDPField.clear();
							nouveauMDP2Field.clear();
							InfoBox.show("Votre mot de passe a été changé avec succès.");
						}
					});
				}
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initActionPanel(){
		actionFieldSet.setHeadingText("Actions");
		actionFieldSet.setVisible(false);
		actionFieldSet.add(actionPanel);
		return actionFieldSet;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initPeriodePanel(){
		periodeFieldSet.setHeadingText("Périodes");
		VerticalPanel vp=new VerticalPanel();
		periodeFieldSet.add(vp);
		vp.add(periodePanel);

		HorizontalPanel hp=new HorizontalPanel();
		vp.add(hp);

		// ajouter
		hp.add(addButton);
		addButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				service.createPeriode(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<PeriodeDo>() {
					@Override
					public void onSuccess(PeriodeDo result) {
						periodePanel.add(createPeriodeLine(result));
					}
				});
			}
		});

		// enregistrer
		hp.add(saveButton);
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				save();
			}
		});

		// annuler
		hp.add(cancelButton);
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				load("");
			}
		});

		return periodeFieldSet;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget createPeriodeLine(final PeriodeDo periode){
		HorizontalPanel result=new HorizontalPanel();

		DateField dateDebutField=new DateField();
		dateDebutField.setEnabled(ContextClient.getUserIsTresorier());
		dateDebutField.setValue(new Date(periode.getDateDebut()));
		result.add(dateDebutField);
		listeDateDebutField.add(dateDebutField);

		DateField dateFinField=new DateField();
		dateFinField.setEnabled(ContextClient.getUserIsTresorier());
		dateFinField.setValue(new Date(periode.getDateFin()));
		result.add(dateFinField);
		listeDateFinField.add(dateFinField);

		TextField nomField=new TextField();
		nomField.setEnabled(ContextClient.getUserIsTresorier());
		nomField.setValue(periode.getNom());
		result.add(nomField);
		listeNomField.add(nomField);

		CheckBox bilanFigeField=new CheckBox();
		bilanFigeField.setEnabled(ContextClient.getUserIsTresorier());
		bilanFigeField.setValue(periode.isBilanFige());
		result.add(bilanFigeField);
		listeBilanFigeField.add(bilanFigeField);

		listePeriode.add(periode);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initDeconnectionPanel(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Déconnexion");

		VerticalPanel vp=new VerticalPanel();
		result.add(vp);
		Label label=new Label("La déconnexion entraînera la suppression des cookies.");
		vp.add(label);
		label=new Label("Vous ne serez plus automatiquement connecté à l'application.");
		vp.add(label);
		label=new Label("Vous devrez retaper votre login et votre mot de passe.");
		vp.add(label);

		TextButton deconnectionButton=new TextButton("Déconnexion");
		vp.add(deconnectionButton);
		deconnectionButton.addSelectHandler(new SelectEvent.SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Cookies.removeCookie(PimoussLand.TOKEN);
				Window.Location.reload();
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initAdministrationPanel(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Administration");
		VerticalPanel vp=new VerticalPanel();
		result.add(vp);

		vp.add(forceSaveDatabaseButton);
		forceSaveDatabaseButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.forceSaveDatabase(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		vp.add(forceVerifSoldeButton);
		forceVerifSoldeButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.forceVerifSolde(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		vp.add(forceRAZSoldeButton);
		forceRAZSoldeButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.forceRAZSolde(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		vp.add(forceVerifVentilButton);
		forceVerifVentilButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.forceVerifVentil(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		vp.add(forceVerifNumPieceButton);
		forceVerifNumPieceButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.forceVerifNumPiece(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		vp.add(verifJustifButton);
		verifJustifButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				WaitBox.showBox();
				service.verifJustif(ContextClient.getIdUserEnCours(), new DefaultAsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						load("");
						WaitBox.hideBox();
					}
				});
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private void save(){
		for(int i=0;i<listePeriode.size();i++){
			listePeriode.get(i).setBilanFige(listeBilanFigeField.get(i).getValue());
			listePeriode.get(i).setDateDebut(listeDateDebutField.get(i).getValue().getTime());
			listePeriode.get(i).setDateFin(listeDateFinField.get(i).getValue().getTime());
			listePeriode.get(i).setNom(listeNomField.get(i).getValue());
		}
		service.saveListePeriode(ContextClient.getIdUserEnCours(), listePeriode, new DefaultAsyncCallback<List<PeriodeDo>>() {
			@Override
			public void onSuccess(List<PeriodeDo> result) {
				load("");
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return loaded;
	}

	/*---------------------------------------------------------------------------------------------*/
}
