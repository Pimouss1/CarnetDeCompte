package fr.pimouss.land.client.view.util;

import com.google.gwt.core.client.GWT;
import com.sencha.gxt.widget.core.client.box.MessageBox;

import fr.pimouss.land.client.view.image.Images;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class InfoBox extends MessageBox {
	private static Images images = GWT.create(Images.class);
	private static InfoBox instance=new InfoBox();

	/*---------------------------------------------------------------------------------------------*/
	private InfoBox() {
		super("Message");
		setPredefinedButtons(PredefinedButton.CLOSE);
		setIcon(images.info());
		setHeight("auto");
		setWidth("auto");
		setHideOnButtonClick(true);
		setModal(true);
		setResizable(true);
	}
	/*---------------------------------------------------------------------------------------------*/
	public static void show(String msg) {
		instance.setMessage(msg);
		instance.setVisible(true);
	}

	/*---------------------------------------------------------------------------------------------*/
}
