package fr.pimouss.land.client.view.util;

import fr.pimouss.land.shared.exception.WarningException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ExceptionBox {

	/*---------------------------------------------------------------------------------------------*/
	public static void show(Throwable t) {
		if(t instanceof WarningException){
			WarningBox.show(t.getMessage());
		}else{
			ErrorBox.show(t.getMessage());
		}
	}

	/*---------------------------------------------------------------------------------------------*/
}
