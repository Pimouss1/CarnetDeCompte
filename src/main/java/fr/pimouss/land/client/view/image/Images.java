package fr.pimouss.land.client.view.image;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public interface Images extends ClientBundle {

	@Source("607.png")
	ImageResource discussion();

	@Source("080.png")
	ImageResource discussionPrivee();

	@Source("069.png")
	ImageResource ask();

	@Source("439.png")
	ImageResource user();

	@Source("Erreur.png")
	ImageResource error();

	@Source("027.png")
	ImageResource yes();

	@Source("028.png")
	ImageResource no();

	@Source("Warning.png")
	ImageResource warning();

	@Source("Valide.png")
	ImageResource info();

	@Source("049.png")
	ImageResource plus();

	@Source("043.png")
	ImageResource dupliquer();

	@Source("050.png")
	ImageResource moins();

	@Source("551.png")
	ImageResource folderClose();

	@Source("552.png")
	ImageResource folderOpen();

	@Source("166.png")
	ImageResource rotateLeft();

	@Source("167.png")
	ImageResource rotateRight();

	@Source("002.png")
	ImageResource prec();

	@Source("003.png")
	ImageResource suiv();

	@Source("004.png")
	ImageResource reduire();

	@Source("173.png")
	ImageResource delete();

	@Source("537.png")
	ImageResource album();

	@Source("538.png")
	ImageResource albumExpanded();

	@Source("265.png")
	ImageResource video();

	@Source("439.png")
	ImageResource contact();

	@Source("360.png")
	ImageResource histo();

	@Source("532.png")
	ImageResource download();

	@Source("076.png")
	ImageResource previsionnel();

	@Source("trash.png")
	ImageResource trash();

	@Source("google-maps.png")
	ImageResource googleMaps();

	@Source("plus.png")
	ImageResource nouveau();

	@Source("doc.png")
	ImageResource justif();
}
