package fr.pimouss.land.client.view.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.form.Field;
import com.sencha.gxt.widget.core.client.form.ValueBaseField;

public class UtilForm {
	private static final String STYLE_LABEL="fieldLabel";

	/*---------------------------------------------------------------------------------------------*/
	public static Widget getFieldLabel(final Field<?> widget, String texte) {
		HTML result = new HTML();
		result.addStyleName(STYLE_LABEL);
		result.setHTML(texte+" : ");
		result.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		return result;
	}
	public static Widget getFieldLabel(final ValueBaseField<?> widget, String texte) {
		HTML result = new HTML();
		result.addStyleName(STYLE_LABEL);
		if(widget!=null && !widget.isAllowBlank()){
			result.setHTML(texte+" <span style=\"color:red;\">*</span> : ");
		}else{
			result.setHTML(texte+" : ");
		}
		if(widget!=null){
			result.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent arg0) {
					widget.focus();
				}
			});
		}
		result.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
}
