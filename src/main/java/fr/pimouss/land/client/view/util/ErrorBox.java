package fr.pimouss.land.client.view.util;

import com.google.gwt.core.client.GWT;
import com.sencha.gxt.widget.core.client.box.MessageBox;

import fr.pimouss.land.client.view.image.Images;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ErrorBox extends MessageBox {
	private static Images images = GWT.create(Images.class);
	private static ErrorBox instance=new ErrorBox();

	/*---------------------------------------------------------------------------------------------*/
	private ErrorBox() {
		super("Error");
		setPredefinedButtons(PredefinedButton.CLOSE);
		setIcon(images.error());
		setHeight("auto");
		setWidth("auto");
		setHideOnButtonClick(true);
		setModal(true);
	}

	/*---------------------------------------------------------------------------------------------*/
	public static void show(String msg) {
		if(ContextClient.isLogClient()){
			instance.setMessage(msg);
		}else{
			instance.setMessage("Une erreur technique est survenue.<br/>Merci d'appuyer sur les touches \"ctrl + F5\" de votre clavier.<br/>Si le problème persiste merci de contacter le webmaster de Pimouss Land.<br/><br/>Contact : pimouss.land@gmail.com");
		}
		instance.setVisible(true);
	}

	/*---------------------------------------------------------------------------------------------*/
}
