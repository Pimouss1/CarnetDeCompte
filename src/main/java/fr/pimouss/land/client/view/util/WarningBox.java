package fr.pimouss.land.client.view.util;

import com.google.gwt.core.client.GWT;
import com.sencha.gxt.widget.core.client.box.MessageBox;

import fr.pimouss.land.client.view.image.Images;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class WarningBox extends MessageBox {
	private static Images images = GWT.create(Images.class);
	private static WarningBox instance=new WarningBox();

	/*---------------------------------------------------------------------------------------------*/
	private WarningBox() {
		super("Warning");
		setPredefinedButtons(PredefinedButton.CLOSE);
		setIcon(images.warning());
		setHeight("auto");
		setWidth("auto");
		setHideOnButtonClick(true);
		setModal(true);
	}
	/*---------------------------------------------------------------------------------------------*/
	public static void show(String msg) {
		instance.setMessage(msg);
		instance.setVisible(true);
	}

	/*---------------------------------------------------------------------------------------------*/
}
