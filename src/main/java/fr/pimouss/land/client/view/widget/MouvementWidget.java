package fr.pimouss.land.client.view.widget;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.BlurEvent;
import com.sencha.gxt.widget.core.client.event.BlurEvent.BlurHandler;
import com.sencha.gxt.widget.core.client.event.RowClickEvent;
import com.sencha.gxt.widget.core.client.event.RowClickEvent.RowClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.BigDecimalField;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.IntegerField;
import com.sencha.gxt.widget.core.client.form.TextArea;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;
import com.sencha.gxt.widget.core.client.treegrid.TreeGrid;

import fr.pimouss.land.client.controller.MouvementController.IFace;
import fr.pimouss.land.client.controller.MouvementController.IFaceAsync;
import fr.pimouss.land.client.model.GenericComboBox;
import fr.pimouss.land.client.model.GenericComboModel;
import fr.pimouss.land.client.model.GenericComboStore;
import fr.pimouss.land.client.model.GenericTreeModel;
import fr.pimouss.land.client.model.GenericTreeStore;
import fr.pimouss.land.client.model.MouvementListStore;
import fr.pimouss.land.client.view.image.Images;
import fr.pimouss.land.client.view.util.ConfirmBox;
import fr.pimouss.land.client.view.util.ConfirmBox.BooleanCallback;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ExceptionBox;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.UtilForm;
import fr.pimouss.land.client.view.util.WaitBox;
import fr.pimouss.land.client.view.util.WarningBox;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.dao.VentilationDo;
import fr.pimouss.land.shared.exception.WarningException;
import fr.pimouss.land.shared.service.MouvementBo;
import gwtupload.client.IFileInput.FileInputType;
import gwtupload.client.IUploader;
import gwtupload.client.IUploader.OnFinishUploaderHandler;
import gwtupload.client.SingleUploader;

public class MouvementWidget extends Composite implements ITabPanelItem {
	private static final String PREFIX_KEY_MODE_PAIEMENT="m";
	private static final String PREFIX_KEY_VENTILATION="v";
	private static final String PREFIX_KEY_COMPTE="c";
	private static final DateTimeFormat DATE_FORMAT=DateTimeFormat.getFormat("dd/MM/yyyy");
	private static final long UNE_JOURNEE=24l*60l*60l*1000l;
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"mouvementService";
	private String downloadFileModuleRelativeURL=GWT.getModuleBaseURL()+"downloadJustificatifFileService";
	private String uploadFileModuleRelativeURL=GWT.getModuleBaseURL()+"uploadJustificatifFileService";
	private FlowPanel rootPanel=new FlowPanel();
	private final Images images = GWT.create(Images.class);

	private GenericComboBox periodeComboBox;
	private GenericComboStore periodeComboStore=new GenericComboStore();
	private GenericComboBox vueComboBox;
	private GenericComboStore vueComboStore=new GenericComboStore();
	private GenericComboBox compteComboBox;
	private GenericComboStore compteComboStore=new GenericComboStore();
	private List<PeriodeDo> listePeriode;
	private String typeMouvement;
	private TreeGrid<GenericTreeModel> ventilationTreeGrid;
	private GenericTreeStore ventilationTreeStore=new GenericTreeStore();
	private Grid<MouvementBo> mouvementGrid;
	private MouvementListStore mouvementGridStore=new MouvementListStore();
	private MouvementBo mouvementPrecedent;
	private MouvementBo mouvementEnCours;
	private boolean mouvementEnCoursModified=false;
	private DateField dateFacturationField=new DateField();
	private Date dateFacturationValueTmp=new Date();
	private DateField datePaiementField=new DateField();
	private TextArea libelleField=new TextArea();
	private TextArea objetField=new TextArea();
	private GenericComboStore modePaiementComboStore=new GenericComboStore();
	private GenericComboBox modePaiementComboBox=new GenericComboBox(modePaiementComboStore);
	private IntegerField numChequeField=new IntegerField();
	private BigDecimalField montantField=new BigDecimalField();
	private IntegerField numPieceField=new IntegerField();
	private CheckBox previsionnelCheckBox=new CheckBox();
	private CheckBox horsBilanCheckBox=new CheckBox();

	private VerticalPanel ventilationPanel=new VerticalPanel();
	private List<TypeVentilationDo> listeTypeVentilation;
	private List<GenericComboBox> listeVentilationComboBox=new ArrayList<>();
	private List<BigDecimalField> listeMontantVentilationField=new ArrayList<>();
	private GenericComboStore ventilationComboStore=new GenericComboStore();

	private VerticalPanel justificatifPanel=new VerticalPanel();
	private SingleUploader uploadField;

	/*---------------------------------------------------------------------------------------------*/
	public MouvementWidget(String typeMouvement) {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);
		this.typeMouvement=typeMouvement;

		HorizontalPanel hp=new HorizontalPanel();
		rootPanel.add(hp);

		hp.add(initGrid());

		VerticalPanel vp=new VerticalPanel();
		hp.add(vp);
		vp.add(initEtatFieldSet());
		vp.add(initVentilationFieldSet());
		vp.add(initJustificatifFieldSet());
		vp.add(initButtonPanel());

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				mouvementGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT-22);
				ventilationTreeGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT-22);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initGrid(){
		VerticalPanel result=new VerticalPanel();

		HorizontalPanel hp=new HorizontalPanel();
		result.add(hp);

		compteComboBox=new GenericComboBox(compteComboStore);
		hp.add(compteComboBox);
		service.getAllCompte(new DefaultAsyncCallback<List<CompteDo>>() {
			@Override
			public void onSuccess(List<CompteDo> result) {
				for(CompteDo elt:result){
					compteComboStore.add(new GenericComboModel(PREFIX_KEY_COMPTE+elt.getRowid(),elt.getCompte()));
				}
				ContextClient.setKeyCompteEnCours("c1");
				compteComboBox.setValue("c1");
			}
		});
		compteComboBox.setWidth(200);
		compteComboBox.setTriggerAction(TriggerAction.ALL);
		compteComboBox.setEditable(false);
		compteComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				ContextClient.setKeyCompteEnCours(event.getSelectedItem().getKey());
				load("");
			}
		});

		periodeComboBox=new GenericComboBox(periodeComboStore);
		hp.add(periodeComboBox);
		service.getAllPeriode(new DefaultAsyncCallback<List<PeriodeDo>>() {
			@Override
			public void onSuccess(List<PeriodeDo> result) {
				listePeriode=result;
				for(PeriodeDo elt:result){
					periodeComboStore.add(new GenericComboModel("a"+elt.getRowid(),elt.getNom()));
				}
				service.getPeriodeByDate(System.currentTimeMillis(), new DefaultAsyncCallback<PeriodeDo>() {
					@Override
					public void onSuccess(PeriodeDo result) {
						ContextClient.setKeyPeriodeEnCours("a"+result.getRowid());
						periodeComboBox.setValue("a"+result.getRowid());
					}
				});
			}
		});
		periodeComboBox.setTriggerAction(TriggerAction.ALL);
		periodeComboBox.setEditable(false);
		periodeComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				ContextClient.setKeyPeriodeEnCours(event.getSelectedItem().getKey());
				load("");
			}
		});

		vueComboBox=new GenericComboBox(vueComboStore);
		hp.add(vueComboBox);
		vueComboStore.add(new GenericComboModel("v1","Vue liste mouvement"));
		vueComboStore.add(new GenericComboModel("v2","Vue ventilations groupées"));
		vueComboBox.setValue("v1");
		vueComboBox.setWidth(200);
		vueComboBox.setTriggerAction(TriggerAction.ALL);
		vueComboBox.setEditable(false);
		vueComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				vueComboBox.setValue(event.getSelectedItem().getKey());
				if("v1".equals(vueComboBox.getValue().getKey())){
					mouvementGrid.setVisible(true);
					ventilationTreeGrid.setVisible(false);
				}else {
					mouvementGrid.setVisible(false);
					ventilationTreeGrid.setVisible(true);
				}
				load("");
			}
		});

		result.add(initMouvementGrid());
		result.add(initVentilationTreeGrid());

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initMouvementGrid() {
		List<ColumnConfig<MouvementBo, ?>> config=new ArrayList<>();

		ValueProvider<MouvementBo, ImageResource> imageProvider=new ValueProvider<MouvementBo, ImageResource>() {
			@Override
			public void setValue(MouvementBo object, ImageResource value) {/*nothing to do*/}
			@Override
			public ImageResource getValue(MouvementBo object) {
				if(object.getListeVentilation().isEmpty()){
					return images.nouveau();
				}
				BigDecimal sumVentil=new BigDecimal(0);
				for(VentilationDo ventil:object.getListeVentilation()){
					sumVentil=sumVentil.add(ventil.getMontant());
				}
				if(!ContextClient.DECIMAL_FORMAT.format(sumVentil).equals(ContextClient.DECIMAL_FORMAT.format(object.getMouvementDo().getMontant()))){
					return images.error();
				}
				if(object.getMouvementDo().isPrevisionnel()){
					return images.previsionnel();
				}
				if(object.getMouvementDo().getIdModePaiement()==ModePaiementDo.CHEQUE && object.getMouvementDo().getNumCheque()==0){
					return images.warning();
				}
				if(object.getMouvementDo().isRapprochementBancaire()){
					return images.info();
				}
				if(object.getListeJustificatif().isEmpty()){
					return images.justif();
				}
				return null;
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<MouvementBo, ImageResource> cf1=new ColumnConfig<>(imageProvider,25,"");
		cf1.setCell(new ImageResourceCell());
		config.add(cf1);

		ValueProvider<MouvementBo, Long> dateProvider=new ValueProvider<MouvementBo, Long>() {
			@Override
			public void setValue(MouvementBo object, Long value) {/*nothing to do*/}
			@Override
			public Long getValue(MouvementBo object) {
				return object.getMouvementDo().getDateFacturation();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<MouvementBo, Long> cf2=new ColumnConfig<>(dateProvider,80,"Date");
		cf2.setCell(new AbstractCell<Long>() {
			@Override
			public void render(Context context, Long value, SafeHtmlBuilder sb) {
				sb.appendHtmlConstant(DATE_FORMAT.format(new Date(value)));
			}
		});
		config.add(cf2);

		ValueProvider<MouvementBo, String> numPieceProvider=new ValueProvider<MouvementBo, String>() {
			@Override
			public void setValue(MouvementBo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(MouvementBo object) {
				return String.valueOf(object.getMouvementDo().getNumPiece());
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<MouvementBo, String> cf3=new ColumnConfig<>(numPieceProvider,80,"N° Pièce");
		config.add(cf3);

		ValueProvider<MouvementBo, String> libelleProvider=new ValueProvider<MouvementBo, String>() {
			@Override
			public void setValue(MouvementBo object, String value) {/*nothing to do*/}
			@Override
			public String getValue(MouvementBo object) {
				return object.getMouvementDo().getLibelle();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<MouvementBo, String> cf4=new ColumnConfig<>(libelleProvider,ContextClient.GRID_WIDTH-300,"Libellé");
		config.add(cf4);

		ValueProvider<MouvementBo, BigDecimal> montantProvider=new ValueProvider<MouvementBo, BigDecimal>() {
			@Override
			public void setValue(MouvementBo object, BigDecimal value) {/*nothing to do*/}
			@Override
			public BigDecimal getValue(MouvementBo object) {
				return object.getMouvementDo().getMontant();
			}
			@Override
			public String getPath() {return null;}
		};
		ColumnConfig<MouvementBo, BigDecimal> cf5=new ColumnConfig<>(montantProvider,80,"Montant");
		cf5.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		cf5.setCell(new AbstractCell<BigDecimal>() {
			@Override
			public void render(com.google.gwt.cell.client.Cell.Context context, BigDecimal value, SafeHtmlBuilder sb) {
				if(value!=null){
					sb.appendHtmlConstant(ContextClient.DECIMAL_FORMAT.format(value)+" €");
				}
			}
		});
		config.add(cf5);

		MenuItem dupliquerMenuItem=new MenuItem("Dupliquer");
		dupliquerMenuItem.setIcon(images.dupliquer());
		dupliquerMenuItem.addSelectionHandler(new SelectionHandler<Item>() {
			@Override
			public void onSelection(SelectionEvent<Item> event) {
				MouvementBo mvtADupliquer=mouvementGrid.getSelectionModel().getSelectedItem();	
				MouvementBo newMvt=new MouvementBo(new MouvementDo());
				newMvt.getMouvementDo().setDateFacturation(System.currentTimeMillis());
				newMvt.getMouvementDo().setIdModePaiement(mvtADupliquer.getMouvementDo().getIdModePaiement());
				newMvt.getMouvementDo().setLibelle(mvtADupliquer.getMouvementDo().getLibelle());
				newMvt.getMouvementDo().setMontant(mvtADupliquer.getMouvementDo().getMontant());
				newMvt.getMouvementDo().setObjet(mvtADupliquer.getMouvementDo().getObjet());
				newMvt.getMouvementDo().setRapprochementBancaire(false);
				newMvt.getMouvementDo().setSolde(false);
				newMvt.getMouvementDo().setTypeMouvement(mvtADupliquer.getMouvementDo().getTypeMouvement());
				newMvt.setModePaiementDo(mvtADupliquer.getModePaiementDo());
				for(VentilationDo ventilADupliquer:mvtADupliquer.getListeVentilation()){
					VentilationDo newVentil=new VentilationDo();
					newVentil.setIdTypeVentilation(ventilADupliquer.getIdTypeVentilation());
					newVentil.setMontant(ventilADupliquer.getMontant());
					newMvt.getListeVentilation().add(newVentil);
				}
				mouvementEnCours=newMvt;
				load("");
			}
		});
		Menu contextMenu=new Menu();
		contextMenu.add(dupliquerMenuItem);

		mouvementGrid=new Grid<>(mouvementGridStore, new ColumnModel<>(config));
		mouvementGrid.setContextMenu(contextMenu);
		mouvementGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT-22);
		mouvementGrid.setWidth(ContextClient.GRID_WIDTH);
		mouvementGrid.setVisible(true);
		mouvementGrid.addRowClickHandler(new RowClickHandler() {
			@Override
			public void onRowClick(RowClickEvent event) {
				mouvementPrecedent=mouvementEnCours;
				mouvementEnCours=mouvementGrid.getStore().get(event.getRowIndex());
				load(false,true);
			}
		});

		return mouvementGrid;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initVentilationTreeGrid(){
		List<ColumnConfig<GenericTreeModel, ?>> config=new ArrayList<>();

		ColumnConfig<GenericTreeModel, String> cf1=new ColumnConfig<>(new ValueProvider<GenericTreeModel, String>() {
			@Override public void setValue(GenericTreeModel object, String value) {/*Nothing to do*/}
			@Override public String getPath() {return null;}
			@Override
			public String getValue(GenericTreeModel object) {
				if(object.getNiveau()==1){
					TypeVentilationDo source=(TypeVentilationDo)object.getSource();
					return source.getTypeVentilation();
				}
				MouvementBo source=(MouvementBo)object.getSource();
				return source.getMouvementDo().getLibelle();
			}
		}, ContextClient.GRID_WIDTH-380, "Libellé");
		config.add(cf1);

		ColumnConfig<GenericTreeModel, String> cf2=new ColumnConfig<>(new ValueProvider<GenericTreeModel, String>() {
			@Override public void setValue(GenericTreeModel object, String value) {/*Nothing to do*/}
			@Override public String getPath() {return null;}
			@Override
			public String getValue(GenericTreeModel object) {
				if(object.getNiveau()==1){
					return "";
				}
				MouvementBo source=(MouvementBo)object.getSource();
				return DATE_FORMAT.format(new Date(source.getMouvementDo().getDateFacturation()));
			}
		}, 80, "Date");
		config.add(cf2);

		ColumnConfig<GenericTreeModel, String> cf3=new ColumnConfig<>(new ValueProvider<GenericTreeModel, String>() {
			@Override public void setValue(GenericTreeModel object, String value) {/*Nothing to do*/}
			@Override public String getPath() {return null;}
			@Override
			public String getValue(GenericTreeModel object) {
				if(object.getNiveau()==1){
					return "";
				}
				MouvementBo source=(MouvementBo)object.getSource();
				return String.valueOf(source.getMouvementDo().getNumPiece());
			}
		}, 80, "N° Pièce");
		config.add(cf3);

		ColumnConfig<GenericTreeModel, String> cf4=new ColumnConfig<>(new ValueProvider<GenericTreeModel, String>() {
			@Override public void setValue(GenericTreeModel object, String value) {/*Nothing to do*/}
			@Override public String getPath() {return null;}
			@Override
			public String getValue(GenericTreeModel object) {
				if(object.getNiveau()==1){
					return "";
				}
				long keyTypeVentil=Long.parseLong(object.getKey().substring(1,object.getKey().indexOf("m")));
				MouvementBo source=(MouvementBo)object.getSource();
				for(VentilationDo elt:source.getListeVentilation()) {
					if(keyTypeVentil==elt.getIdTypeVentilation()) {
						if(elt.getMontant().compareTo(source.getMouvementDo().getMontant())<0) {
							return "w"+elt.getMontant()+" € / "+source.getMouvementDo().getMontant()+" €";
						}
						if(elt.getMontant().compareTo(source.getMouvementDo().getMontant())>0) {
							return "e"+elt.getMontant()+" € / "+source.getMouvementDo().getMontant()+" €";
						}
						return "i"+elt.getMontant()+" € / "+source.getMouvementDo().getMontant()+" €";
					}
				}
				return "";
			}
		}, 160, "Montant");
		cf4.setCell(new AbstractCell<String>() {
			@Override
			public void render(Context context, String value, SafeHtmlBuilder sb) {
				String color="000000";
				if(value.startsWith("w")) {
					color="FF9F5C";
				}
				if(value.startsWith("e")) {
					color="FF0000";
				}
				sb.appendHtmlConstant("<span style=\"color:#"+color+"\">"+value.substring(1)+"</span>");
			}
		});
		config.add(cf4);

		ventilationTreeGrid=new TreeGrid<>(ventilationTreeStore, new ColumnModel<GenericTreeModel>(config), cf1);
		ventilationTreeGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		ventilationTreeGrid.getView().setAutoExpandColumn(cf1);
		ventilationTreeGrid.setHeight(Window.getClientHeight()-ContextClient.MARGE_HEIGHT-22);
		ventilationTreeGrid.setWidth(ContextClient.GRID_WIDTH);
		ventilationTreeGrid.setVisible(false);
		ventilationTreeGrid.addRowClickHandler(new RowClickHandler() {
			@Override
			public void onRowClick(RowClickEvent event) {
				GenericTreeModel selectedItem=(GenericTreeModel)ventilationTreeGrid.getStore().get(event.getRowIndex());
				if(selectedItem.getNiveau()==2){
					final MouvementBo source=(MouvementBo)selectedItem.getSource();
					mouvementEnCours=source;
					load(false,true);
				}
			}
		});

		return ventilationTreeGrid;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initEtatFieldSet(){
		FieldSet result=new FieldSet();
		if(MouvementDo.DEPENSE.equals(typeMouvement)){
			result.setHeadingText("Etat de dépense");
		}else{
			result.setHeadingText("Etat de recette");
		}
		FlexTable table=new FlexTable();
		result.add(table);
		int row=0;

		dateFacturationField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(dateFacturationField, "Date de facturation"));
		table.setWidget(row, 1, dateFacturationField);
		dateFacturationField.setEnabled(false);
		dateFacturationField.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				boolean dateAutorisee=false;
				long d=((DateField)event.getSource()).getValue().getTime();
				for(PeriodeDo periode:listePeriode){
					if(periode.getDateDebut()<=d && d<(periode.getDateFin()+UNE_JOURNEE) && !periode.isBilanFige()){
						dateAutorisee=true;
					}
				}
				if(!dateAutorisee){
					WarningBox.show("Date de facturation incorrect.<br/>La date se trouve dans une période dont le bilan a été effectué.");
					if(dateFacturationValueTmp!=null){
						dateFacturationField.setValue(dateFacturationValueTmp);
					}else{
						dateFacturationField.clear();
					}
				}else{
					dateFacturationValueTmp=((DateField)event.getSource()).getValue();
				}
			}
		});
		dateFacturationField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(datePaiementField, "Date de paiement"));
		table.setWidget(row, 1, datePaiementField);
		datePaiementField.setEnabled(false);
		datePaiementField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		row++;

		libelleField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(libelleField, "Libellé"));
		libelleField.setWidth(380);
		libelleField.setHeight(50);
		libelleField.setEnabled(false);
		libelleField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		table.setWidget(row, 1, libelleField);
		table.getFlexCellFormatter().setColSpan(row,1,3);
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(objetField, "Objet"));
		objetField.setWidth(380);
		objetField.setHeight(50);
		objetField.setEnabled(false);
		objetField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		table.setWidget(row, 1, objetField);
		table.getFlexCellFormatter().setColSpan(row,1,3);
		row++;

		modePaiementComboBox.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(modePaiementComboBox, "Mode de paiement"));
		table.setWidget(row, 1, modePaiementComboBox);
		modePaiementComboBox.setTriggerAction(TriggerAction.ALL);
		modePaiementComboBox.setEditable(false);
		modePaiementComboBox.setEnabled(false);
		modePaiementComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> arg0) {
				numChequeField.setEnabled(arg0.getSelectedItem().getKey()==PREFIX_KEY_MODE_PAIEMENT+"1");
			}
		});
		modePaiementComboBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		table.setWidget(row, 2, UtilForm.getFieldLabel(numChequeField, "N° chèque"));
		table.setWidget(row, 3, numChequeField);
		numChequeField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		numChequeField.setEnabled(false);
		row++;

		montantField.setAllowBlank(false);
		table.setWidget(row, 0, UtilForm.getFieldLabel(montantField, "Montant"));
		table.setWidget(row, 1, montantField);
		montantField.setEnabled(false);
		montantField.setFormat(ContextClient.DECIMAL_FORMAT);
		montantField.addValueChangeHandler(new ValueChangeHandler<BigDecimal>() {
			@Override
			public void onValueChange(ValueChangeEvent<BigDecimal> event) {
				if(listeVentilationComboBox.size()==1){
					listeMontantVentilationField.get(0).setValue(event.getValue());
				}
			}
		});
		montantField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		table.setWidget(row, 2, UtilForm.getFieldLabel(previsionnelCheckBox, "Prévisionnel"));
		table.setWidget(row, 3, previsionnelCheckBox);
		previsionnelCheckBox.setBoxLabel("");
		previsionnelCheckBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		previsionnelCheckBox.setEnabled(false);
		row++;

		table.setWidget(row, 0, UtilForm.getFieldLabel(numPieceField, "N° Pièce"));
		table.setWidget(row, 1, numPieceField);
		numPieceField.setEnabled(false);
		numPieceField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		table.setWidget(row, 2, UtilForm.getFieldLabel(horsBilanCheckBox,"Hors bilan"));
		table.setWidget(row, 3, horsBilanCheckBox);
		horsBilanCheckBox.setBoxLabel("");
		horsBilanCheckBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		horsBilanCheckBox.setEnabled(false);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initVentilationFieldSet(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Ventilations");
		result.add(ventilationPanel);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget createJustificatifLine(final JustificatifDo justificatif){
		HorizontalPanel result=new HorizontalPanel();
		Label l=new Label(justificatif.getFilename());
		l.setWidth("500px");
		result.add(l);

		TextButton downloadButton=new TextButton();
		downloadButton.setIcon(images.download());
		result.add(downloadButton);
		downloadButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Window.open(downloadFileModuleRelativeURL+"?idJustificatif="+justificatif.getRowid(),"","");
			}
		});

		TextButton deleteButton=new TextButton();
		deleteButton.setIcon(images.trash());
		result.add(deleteButton);
		deleteButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				new ConfirmBox("Etes-vous sûr de vouloir supprimer ce justificatif ?", new BooleanCallback() {
					@Override
					public void execute(boolean answer) {
						if(answer){
							service.deleteJustificatif(ContextClient.getIdUserEnCours(), justificatif.getRowid(), new DefaultAsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {
									load("");
								}
							});
						}
					}
				}).show();
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget createVentilationLine(final VentilationDo ventilation){
		if(ventilation==null){
			for(int i=0;i<listeVentilationComboBox.size();i++){
				if(listeVentilationComboBox.get(i).getValue()==null
						|| "0".equals(listeVentilationComboBox.get(i).getValue().getKey().substring(1))){
					return null;
				}
			}
		}
		HorizontalPanel result=new HorizontalPanel();
		final GenericComboBox combo=new GenericComboBox(ventilationComboStore);
		combo.setWidth(400);
		combo.setTriggerAction(TriggerAction.ALL);
		combo.setEditable(false);
		if(ventilation!=null){
			combo.setValue(PREFIX_KEY_VENTILATION+ventilation.getIdTypeVentilation());
		}
		combo.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				if(ventilation==null
						&& event.getSelectedItem().getKey()!=null
						&& !"0".equals(event.getSelectedItem().getKey().substring(1))){
					combo.setValue(event.getSelectedItem());
					ventilationPanel.add(createVentilationLine(null));
				}
			}
		});
		combo.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		listeVentilationComboBox.add(combo);
		result.add(combo);
		result.add(UtilForm.getFieldLabel(null, "Montant"));
		BigDecimalField montantVentilationField=new BigDecimalField();
		montantVentilationField.setEnabled(false);
		montantVentilationField.setFormat(ContextClient.DECIMAL_FORMAT);
		if(ventilation!=null){
			montantVentilationField.setValue(ventilation.getMontant());
		}
		montantVentilationField.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				mouvementEnCoursModified=true;
			}
		});
		listeMontantVentilationField.add(montantVentilationField);
		result.add(montantVentilationField);
		
		if(ventilation==null){
			if(listeVentilationComboBox.size()==1){
				listeMontantVentilationField.get(0).setValue(mouvementEnCours.getMouvementDo().getMontant());
			}else{
				for(BigDecimalField mntField:listeMontantVentilationField){
					mntField.setEnabled(true);
				}
			}
		}

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initJustificatifFieldSet(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Justificatifs");
		VerticalPanel vp=new VerticalPanel();
		result.add(vp);

		final TextButton archiveButton=new TextButton();
		uploadField=new SingleUploader(FileInputType.BROWSER_INPUT, null, archiveButton);
		archiveButton.setText("Envoyer");
		uploadField.setAutoSubmit(false);
		uploadField.setAvoidRepeatFiles(false);
		vp.add(uploadField);
		archiveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if(mouvementEnCours==null){
					if(MouvementDo.DEPENSE.equals(typeMouvement)){
						WarningBox.show("Vous devez sélectionner une dépense.");
					}else{
						WarningBox.show("Vous devez sélectionner une recette.");
					}
				}else if(uploadField.getFileNames()==null || uploadField.getFileNames().isEmpty()){
					WarningBox.show("Vous devez sélectionner un fichier.");
				}else{
					boolean justifExist=false;
					for(String filename:uploadField.getFileNames()){
						for(JustificatifDo justificatif:mouvementEnCours.getListeJustificatif()){
							if(justificatif.getFilename().endsWith(filename)){
								justifExist=true;
							}
						}
					}
					if(justifExist){
						WarningBox.show("Le justificatif existe déjà.");
					}else{
						save(true);
					}
				}
			}
		});
		uploadField.addOnFinishUploadHandler(new OnFinishUploaderHandler() {
			@Override
			public void onFinish(IUploader uploader) {
				load("");
			}
		});

		vp.add(justificatifPanel);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		load(true,false);
	}

	/*---------------------------------------------------------------------------------------------*/
	private void loadVentilGrid(List<MouvementBo> listeMvt) {
		for(TypeVentilationDo elt1:listeTypeVentilation) {
			GenericTreeModel node1=new GenericTreeModel();
			node1.setSource(elt1);
			node1.setNiveau(1);
			node1.setKey("v"+elt1.getRowid());
			ventilationTreeStore.add(node1);
			for(MouvementBo elt2:listeMvt) {
				for(VentilationDo elt3:elt2.getListeVentilation()) {
					if(elt1.getRowid()==elt3.getIdTypeVentilation()) {
						GenericTreeModel node2=new GenericTreeModel();
						node2.setSource(elt2);
						node2.setNiveau(2);
						node2.setKey("v"+elt1.getRowid()+"m"+elt2.getMouvementDo().getRowid()+"_"+elt3.getRowid());
						ventilationTreeStore.add(node1,node2);
					}
				}
			}
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void loadMouvement(boolean reloadListe){
		mouvementEnCoursModified=false;
		if(reloadListe){
			mouvementGridStore.clear();
			ventilationTreeStore.clear();
			if(MouvementDo.DEPENSE.equals(typeMouvement)){
				service.getDepenseByPeriode(ContextClient.getIdUserEnCours(),
						Long.parseLong(periodeComboBox.getValue().getKey().substring(1)),
						Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
						new DefaultAsyncCallback<List<MouvementBo>>() {
					@Override
					public void onSuccess(List<MouvementBo> arg0) {
						if("v1".equals(vueComboBox.getValue().getKey())){
							mouvementGridStore.add(new MouvementBo(new MouvementDo()));
							mouvementGridStore.addAll(arg0);
							if(mouvementEnCours!=null){
								boolean trouve=false;
								for(int i=0;i<mouvementGrid.getStore().size();i++) {
									if(mouvementEnCours.getMouvementDo().getRowid()==mouvementGrid.getStore().get(i).getMouvementDo().getRowid()) {
										trouve=true;
										mouvementGrid.getSelectionModel().select(i,false);
										mouvementGrid.getView().focusRow(i);
									}
								}
								if(!trouve) {
									clearMouvementEnCours();
								}
							}
						}else {
							loadVentilGrid(arg0);
						}
					}
				});
			}else{
				service.getRecetteByPeriode(ContextClient.getIdUserEnCours(),
						Long.parseLong(periodeComboBox.getValue().getKey().substring(1)),
						Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
						new DefaultAsyncCallback<List<MouvementBo>>() {
					@Override
					public void onSuccess(List<MouvementBo> arg0) {
						if("v1".equals(vueComboBox.getValue().getKey())){
							mouvementGridStore.add(new MouvementBo(new MouvementDo()));
							mouvementGridStore.addAll(arg0);
							if(mouvementEnCours!=null){
								boolean trouve=false;
								for(int i=0;i<mouvementGrid.getStore().size();i++) {
									if(mouvementEnCours.getMouvementDo().getRowid()==mouvementGrid.getStore().get(i).getMouvementDo().getRowid()) {
										trouve=true;
										mouvementGrid.getSelectionModel().select(i,false);
										mouvementGrid.getView().focusRow(i);
									}
								}
								if(!trouve) {
									clearMouvementEnCours();
								}
							}
						}else {
							loadVentilGrid(arg0);
						}
					}
				});
			}
		}

		clearFields();
		if(mouvementEnCours!=null){
			dateFacturationField.setValue(new Date(mouvementEnCours.getMouvementDo().getDateFacturation()));
			dateFacturationField.setEnabled(true);
			if(mouvementEnCours.getMouvementDo().getDatePaiement()!=0){
				datePaiementField.setValue(new Date(mouvementEnCours.getMouvementDo().getDatePaiement()));
			}
			libelleField.setValue(mouvementEnCours.getMouvementDo().getLibelle());
			libelleField.setEnabled(true);
			objetField.setValue(mouvementEnCours.getMouvementDo().getObjet());
			objetField.setEnabled(true);
			modePaiementComboBox.clear();
			if(mouvementEnCours.getMouvementDo().getIdModePaiement()!=0){
				modePaiementComboBox.setValue(PREFIX_KEY_MODE_PAIEMENT+mouvementEnCours.getMouvementDo().getIdModePaiement());
			}
			modePaiementComboBox.setEnabled(true);
			numChequeField.setEnabled(mouvementEnCours.getMouvementDo().getIdModePaiement()==ModePaiementDo.CHEQUE);
			montantField.setValue(mouvementEnCours.getMouvementDo().getMontant());
			montantField.setEnabled(!mouvementEnCours.getMouvementDo().isRapprochementBancaire());
			numPieceField.setValue(mouvementEnCours.getMouvementDo().getNumPiece());
			numChequeField.setValue(mouvementEnCours.getMouvementDo().getNumCheque());
			previsionnelCheckBox.setValue(mouvementEnCours.getMouvementDo().isPrevisionnel());
			previsionnelCheckBox.setEnabled(!mouvementEnCours.getMouvementDo().isRapprochementBancaire());
			horsBilanCheckBox.setValue(mouvementEnCours.getMouvementDo().isHorsBilan());
			horsBilanCheckBox.setEnabled(true);
			
			ventilationPanel.clear();
			listeVentilationComboBox.clear();
			listeMontantVentilationField.clear();
			for(VentilationDo ventilation:mouvementEnCours.getListeVentilation()){
				ventilationPanel.add(createVentilationLine(ventilation));
			}
			ventilationPanel.add(createVentilationLine(null));
			
			service.loadJustificatif(ContextClient.getIdUserEnCours(), mouvementEnCours, new DefaultAsyncCallback<MouvementBo>() {
				@Override
				public void onSuccess(MouvementBo result) {
					mouvementEnCours=result;
					justificatifPanel.clear();
					for(JustificatifDo justificatif:mouvementEnCours.getListeJustificatif()){
						justificatifPanel.add(createJustificatifLine(justificatif));
					}
				}
			});
		}else{
			disableFields();
		}
		WaitBox.hideBox();
	}

	/*---------------------------------------------------------------------------------------------*/
	private void load(final boolean reloadListe, final boolean byClickGrid) {
		compteComboBox.setValue(ContextClient.getKeyCompteEnCours());
		periodeComboBox.setValue(ContextClient.getKeyPeriodeEnCours());

		if(periodeComboBox.getValue()!=null && !"".equals(periodeComboBox.getValue().getKey())){
			if(mouvementEnCoursModified){
				new ConfirmBox("Votre mouvement a été modifié.<br/>Etes-vous sûr de vouloir perdre les modifications ?",new BooleanCallback() {
					@Override
					public void execute(boolean answer) {
						if(answer){
							loadMouvement(reloadListe);
						}else{
							if(byClickGrid){
								mouvementEnCours=mouvementPrecedent;
							}
						}
					}
				}).show();
			}else{
				loadMouvement(reloadListe);
			}
		}else{
			service.getPeriodeByDate(System.currentTimeMillis(), new DefaultAsyncCallback<PeriodeDo>() {
				@Override
				public void onSuccess(PeriodeDo result) {
					periodeComboBox.setValue("a"+result.getRowid());
					loadMouvement(reloadListe);
				}
			});
		}

		if(modePaiementComboStore.size()==0){
			if(MouvementDo.DEPENSE.equals(typeMouvement)){
				service.getAllModePaiementDepense(new DefaultAsyncCallback<List<ModePaiementDo>>() {
					@Override
					public void onSuccess(List<ModePaiementDo> arg0) {
						modePaiementComboStore.clear();
						for(ModePaiementDo elt:arg0){
							modePaiementComboStore.add(new GenericComboModel(PREFIX_KEY_MODE_PAIEMENT+elt.getRowid(), elt.getModePaiement()));
						}
					}
				});
			}else{
				service.getAllModePaiementRecette(new DefaultAsyncCallback<List<ModePaiementDo>>() {
					@Override
					public void onSuccess(List<ModePaiementDo> arg0) {
						modePaiementComboStore.clear();
						for(ModePaiementDo elt:arg0){
							modePaiementComboStore.add(new GenericComboModel(PREFIX_KEY_MODE_PAIEMENT+elt.getRowid(), elt.getModePaiement()));
						}
					}
				});
			}
		}
		if(MouvementDo.DEPENSE.equals(typeMouvement)){
			service.getAllTypeVentilationDepense(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
				@Override
				public void onSuccess(List<TypeVentilationDo> arg0) {
					listeTypeVentilation=arg0;
					ventilationComboStore.clear();
					ventilationComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION+"0","_"));
					for(TypeVentilationDo elt:arg0){
						ventilationComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION+elt.getRowid(), elt.getTypeVentilation()));
					}
				}
			});
		}else{
			service.getAllTypeVentilationRecette(new DefaultAsyncCallback<List<TypeVentilationDo>>() {
				@Override
				public void onSuccess(List<TypeVentilationDo> arg0) {
					listeTypeVentilation=arg0;
					ventilationComboStore.clear();
					ventilationComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION+"0","_"));
					for(TypeVentilationDo elt:arg0){
						ventilationComboStore.add(new GenericComboModel(PREFIX_KEY_VENTILATION+elt.getRowid(), elt.getTypeVentilation()));
					}
				}
			});
		}
	}

	/*---------------------------------------------------------------------------------------------*/
	private void save(final boolean upload){
		if(mouvementEnCours!=null && checkField()){
			WaitBox.showBox();
			mouvementEnCours.getMouvementDo().setIdCompte(Long.parseLong(compteComboBox.getValue().getKey().substring(1)));
			mouvementEnCours.getMouvementDo().setDateFacturation(dateFacturationField.getValue().getTime());
			if(datePaiementField.getValue()!=null){
				mouvementEnCours.getMouvementDo().setDatePaiement(datePaiementField.getValue().getTime());
			}
			mouvementEnCours.getMouvementDo().setLibelle(libelleField.getValue());
			mouvementEnCours.getMouvementDo().setObjet(objetField.getValue());
			if(montantField.getValue()!=null){
				mouvementEnCours.getMouvementDo().setMontant(montantField.getValue());
			}else {
				mouvementEnCours.getMouvementDo().setMontant(new BigDecimal(0));
			}
			if(numChequeField.getValue()!=null){
				mouvementEnCours.getMouvementDo().setNumCheque(numChequeField.getValue());
			}
			if(modePaiementComboBox.getValue()!=null){
				mouvementEnCours.getMouvementDo().setIdModePaiement(Long.parseLong(modePaiementComboBox.getValue().getKey().substring(1)));
				if(mouvementEnCours.getMouvementDo().getIdModePaiement()!=ModePaiementDo.CHEQUE){
					mouvementEnCours.getMouvementDo().setNumCheque(0);
				}
			}
			if(numPieceField.getValue()!=null){
				mouvementEnCours.getMouvementDo().setNumPiece(numPieceField.getValue());
			}
			mouvementEnCours.getMouvementDo().setTypeMouvement(typeMouvement);
			mouvementEnCours.getMouvementDo().setPrevisionnel(previsionnelCheckBox.getValue()!=null && previsionnelCheckBox.getValue());
			mouvementEnCours.getMouvementDo().setHorsBilan(horsBilanCheckBox.getValue()!=null && horsBilanCheckBox.getValue());

			mouvementEnCours.getListeVentilation().clear();
			BigDecimal sumVentil=new BigDecimal(0);
			for(int i=0;i<listeVentilationComboBox.size();i++){
				if(listeVentilationComboBox.get(i).getValue()!=null
						&& !"0".equals(listeVentilationComboBox.get(i).getValue().getKey().substring(1))
						&& listeMontantVentilationField.get(i).getValue()!=null
						&& listeMontantVentilationField.get(i).getValue().compareTo(new BigDecimal(0))!=0){
					VentilationDo ventil=new VentilationDo();
					ventil.setIdTypeVentilation(Long.parseLong(listeVentilationComboBox.get(i).getValue().getKey().substring(1)));
					ventil.setMontant(listeMontantVentilationField.get(i).getValue());
					mouvementEnCours.getListeVentilation().add(ventil);
					sumVentil=sumVentil.add(ventil.getMontant());
				}
			}

			if(!ContextClient.DECIMAL_FORMAT.format(sumVentil).equals(ContextClient.DECIMAL_FORMAT.format(mouvementEnCours.getMouvementDo().getMontant()))){
				WarningBox.show("La somme des ventilations ("+ContextClient.DECIMAL_FORMAT.format(sumVentil)+")"
						+ " n'est pas égale au montant ("+ContextClient.DECIMAL_FORMAT.format(mouvementEnCours.getMouvementDo().getMontant())+").");
			}else{
				service.saveMvt(ContextClient.getIdUserEnCours(), mouvementEnCours, new DefaultAsyncCallback<MouvementBo>() {
					@Override
					public void onSuccess(MouvementBo arg0) {
						mouvementEnCours=arg0;
						mouvementEnCoursModified=false;
						if(upload){
							uploadField.setServletPath(uploadFileModuleRelativeURL+"?idMouvement="+mouvementEnCours.getMouvementDo().getRowid());
							uploadField.submit();
						}else{
							load("");
						}
					}
				});
			}
		}
		WaitBox.hideBox();
	}

	/*---------------------------------------------------------------------------------------------*/
	private boolean checkField(){
		dateFacturationField.focus();
		dateFacturationField.fireEvent(new BlurEvent());
		libelleField.focus();
		libelleField.fireEvent(new BlurEvent());
		objetField.focus();
		objetField.fireEvent(new BlurEvent());
		modePaiementComboBox.focus();
		modePaiementComboBox.fireEvent(new BlurEvent());
		montantField.focus();
		montantField.fireEvent(new BlurEvent());
		return !(!dateFacturationField.isAllowBlank() && dateFacturationField.getValue()==null
				|| !libelleField.isAllowBlank() && (libelleField.getValue()==null || "".equals(libelleField.getValue()))
				|| !objetField.isAllowBlank() && (objetField.getValue()==null || "".equals(objetField.getValue()))
				|| !modePaiementComboBox.isAllowBlank() && modePaiementComboBox.getValue()==null
				|| !montantField.isAllowBlank() && (montantField.getValue()==null || montantField.getValue().doubleValue()==0));
	}

	/*---------------------------------------------------------------------------------------------*/
	private void clearMouvementEnCours() {
		mouvementEnCours=null;
		disableFields();
		clearFields();
	}
	private void clearFields() {
		dateFacturationField.clear();
		datePaiementField.clear();
		libelleField.clear();
		objetField.clear();
		modePaiementComboBox.clear();
		numChequeField.clear();
		montantField.clear();
		numPieceField.clear();
		previsionnelCheckBox.clear();
		horsBilanCheckBox.clear();
		ventilationPanel.clear();
		listeVentilationComboBox.clear();
		listeMontantVentilationField.clear();
		justificatifPanel.clear();
	}
	private void disableFields(){
		dateFacturationField.setEnabled(false);
		datePaiementField.setEnabled(false);
		libelleField.setEnabled(false);
		objetField.setEnabled(false);
		modePaiementComboBox.setEnabled(false);
		numChequeField.setEnabled(false);
		montantField.setEnabled(false);
		numPieceField.setEnabled(false);
		previsionnelCheckBox.setEnabled(false);
		horsBilanCheckBox.setEnabled(false);
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initButtonPanel(){
		HorizontalPanel result=new HorizontalPanel();

		// enregistrer
		final TextButton saveButton=new TextButton("Enregistrer");
		result.add(saveButton);
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				save(false);
			}
		});

		// annuler
		final TextButton cancelButton=new TextButton("Annuler");
		result.add(cancelButton);
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				mouvementEnCoursModified=false;
				load("");
			}
		});

		// supprimer
		result.add(new SeparatorToolItem());
		final TextButton deleteButton=new TextButton("Supprimer");
		result.add(deleteButton);
		deleteButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if(mouvementEnCours!=null){
					new ConfirmBox("Etes-vous sûr de vouloir supprimer ce mouvement ?",new BooleanCallback() {
						@Override
						public void execute(boolean answer) {
							if(answer){
								WaitBox.showBox();
								service.deleteMvt(ContextClient.getIdUserEnCours(), mouvementEnCours.getMouvementDo().getRowid(), false, new AsyncCallback<Void>() {
									@Override
									public void onSuccess(Void result) {
										mouvementEnCours=null;
										load("");
									}
									@Override
									public void onFailure(Throwable caught) {
										WaitBox.hideBox();
										if(caught instanceof WarningException && ContextClient.getUserIsAdministrateur()){
											new ConfirmBox("Le rapprochement bancaire de ce mouvement a été fait.\nEtes-vous sûr de vouloir supprimer ce mouvement ?", new BooleanCallback() {
												@Override
												public void execute(boolean answer) {
													service.deleteMvt(ContextClient.getIdUserEnCours(), mouvementEnCours.getMouvementDo().getRowid(), true, new DefaultAsyncCallback<Void>() {
														@Override
														public void onSuccess(Void result) {
															mouvementEnCours=null;
															load("");
														}
													});
												}
											}).show();
										}else{
											ExceptionBox.show(caught);
										}
									}
								});
							}
						}
					}).show();
				}
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
