package fr.pimouss.land.client.view.widget;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.widget.core.client.form.BigDecimalField;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.IntegerField;
import com.sencha.gxt.widget.core.client.form.TextField;

import fr.pimouss.land.client.controller.SoldeController.IFace;
import fr.pimouss.land.client.controller.SoldeController.IFaceAsync;
import fr.pimouss.land.client.model.GenericComboBox;
import fr.pimouss.land.client.model.GenericComboModel;
import fr.pimouss.land.client.model.GenericComboStore;
import fr.pimouss.land.client.view.util.ContextClient;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.land.client.view.util.ITabPanelItem;
import fr.pimouss.land.client.view.util.WarningBox;
import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.MouvementDo;

public class SoldeWidget extends Composite implements ITabPanelItem {
	public static final String NOIR="#000000";
	public static final String ORANGE="#E88C0F";
	public static final String ROUGE="#E80F0F";
	private static final String SOLDE_THEORIQUE="Solde théorique = ";
	private static final String SOLDE_ANTICIPE="Solde anticipé = ";
	private static final String SOLDE_BANCAIRE="Solde bancaire = ";
	private IFaceAsync service=GWT.create(IFace.class);
	private String moduleRelativeURL=GWT.getModuleBaseURL()+"soldeService";
	private FlowPanel rootPanel=new FlowPanel();

	private VerticalPanel rapprochementPanel=new VerticalPanel();

	private GenericComboBox compteComboBox;
	private GenericComboStore compteComboStore=new GenericComboStore();

	private Label soldeTheoriqueLabel=new Label(SOLDE_THEORIQUE);
	private Label soldeAnticipeLabel=new Label(SOLDE_ANTICIPE);
	private Label soldeBancaireLabel=new Label(SOLDE_BANCAIRE);

	/*---------------------------------------------------------------------------------------------*/
	public SoldeWidget() {
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		initWidget(rootPanel);

		VerticalPanel vp=new VerticalPanel();
		rootPanel.add(vp);

		HorizontalPanel hp=new HorizontalPanel();
		vp.add(hp);
		hp.add(initCompteFieldSet());
		hp.add(initSoldeLabel());

		vp.add(initRapprochementGrid());
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initCompteFieldSet(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Compte");
		VerticalPanel vp=new VerticalPanel();
		result.add(vp);

		compteComboBox=new GenericComboBox(compteComboStore);
		vp.add(compteComboBox);
		service.getAllCompte(new DefaultAsyncCallback<List<CompteDo>>() {
			@Override
			public void onSuccess(List<CompteDo> result) {
				for(CompteDo elt:result){
					compteComboStore.add(new GenericComboModel("c"+elt.getRowid(),elt.getCompte()));
				}
				ContextClient.setKeyCompteEnCours("c1");
				compteComboBox.setValue("c1");
			}
		});
		compteComboBox.setWidth(200);
		compteComboBox.setTriggerAction(TriggerAction.ALL);
		compteComboBox.setEditable(false);
		compteComboBox.addSelectionHandler(new SelectionHandler<GenericComboModel>() {
			@Override
			public void onSelection(SelectionEvent<GenericComboModel> event) {
				ContextClient.setKeyCompteEnCours(event.getSelectedItem().getKey());
				load("");
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initSoldeLabel(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Soldes");
		VerticalPanel vp=new VerticalPanel();
		result.add(vp);
		vp.add(soldeTheoriqueLabel);
		vp.add(soldeAnticipeLabel);
		vp.add(soldeBancaireLabel);
		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget initRapprochementGrid(){
		FieldSet result=new FieldSet();
		result.setHeadingText("Rapprochement bancaire");
		final ScrollPanel sc=new ScrollPanel();
		result.add(sc);
		sc.setWidth((Window.getClientWidth()-25)+"px");
		sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-112)+"px");

		sc.add(rapprochementPanel);

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				sc.setWidth((Window.getClientWidth()-25)+"px");
				sc.setHeight((Window.getClientHeight()-ContextClient.MARGE_HEIGHT-112)+"px");
			}
		});

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	private Widget createRapprochementLine(final MouvementDo mvt){
		HorizontalPanel result=new HorizontalPanel();

		DateField dateFacturationField=new DateField();
		dateFacturationField.setWidth(90);
		dateFacturationField.setEnabled(false);
		dateFacturationField.setValue(new Date(mvt.getDateFacturation()));
		result.add(dateFacturationField);

		TextField libelleField=new TextField();
		libelleField.setWidth(300);
		libelleField.setEnabled(false);
		libelleField.setValue(mvt.getLibelle());
		result.add(libelleField);

		TextField objetField=new TextField();
		objetField.setWidth(300);
		objetField.setEnabled(false);
		objetField.setValue(mvt.getObjet());
		result.add(objetField);

		IntegerField numPieceField=new IntegerField();
		numPieceField.setWidth(100);
		numPieceField.setEnabled(false);
		numPieceField.setValue(mvt.getNumPiece());
		result.add(numPieceField);

		IntegerField numChequeField=new IntegerField();
		numChequeField.setWidth(100);
		numChequeField.setEnabled(false);
		if(mvt.getNumCheque()!=0){
			numChequeField.setValue(mvt.getNumCheque());
		}
		result.add(numChequeField);

		BigDecimalField depenseField=new BigDecimalField();
		depenseField.setWidth(100);
		depenseField.setDirection(Direction.RTL);
		depenseField.setEnabled(false);
		depenseField.setFormat(ContextClient.DECIMAL_FORMAT);
		if(mvt.getTypeMouvement()==MouvementDo.DEPENSE){
			depenseField.setValue(mvt.getMontant());
		}
		result.add(depenseField);

		BigDecimalField recetteField=new BigDecimalField();
		recetteField.setWidth(100);
		recetteField.setDirection(Direction.RTL);
		recetteField.setEnabled(false);
		recetteField.setFormat(ContextClient.DECIMAL_FORMAT);
		if(mvt.getTypeMouvement()==MouvementDo.RECETTE){
			recetteField.setValue(mvt.getMontant());
		}
		result.add(recetteField);

		final DateField datePaiementField=new DateField();
		datePaiementField.setWidth(90);
		datePaiementField.setEnabled(ContextClient.getUserIsTresorier());
		if(mvt.getDatePaiement()!=0){
			datePaiementField.setValue(new Date(mvt.getDatePaiement()));
		}
		result.add(datePaiementField);

		CheckBox checkBox=new CheckBox();
		checkBox.setEnabled(ContextClient.getUserIsTresorier());
		checkBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if(datePaiementField.getValue()==null){
					WarningBox.show("Vous devez entrer une date de paiement");
					((CheckBox)event.getSource()).setValue(false);
					datePaiementField.setValue(new Date(0));
				}
				final boolean checked=((CheckBox)event.getSource()).getValue();
				service.rapprochMvt(ContextClient.getIdUserEnCours(), mvt.getRowid(), datePaiementField.getValue().getTime(),
						checked,
						Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
						new DefaultAsyncCallback<BigDecimal>() {
					@Override
					public void onSuccess(BigDecimal result) {
						datePaiementField.setEnabled(!checked);
						soldeBancaireLabel.setText(SOLDE_BANCAIRE+ContextClient.DECIMAL_FORMAT.format(result)+" €");
						soldeBancaireLabel.getElement().getStyle().setColor(NOIR);
						if(result.compareTo(new BigDecimal(1000))<0){
							soldeBancaireLabel.getElement().getStyle().setColor(ORANGE);
						}
						if(result.compareTo(new BigDecimal(0))<0){
							soldeBancaireLabel.getElement().getStyle().setColor(ROUGE);
						}
						if(datePaiementField.getValue().getTime()==0){
							datePaiementField.clear();
						}
					}
				});
			}
		});
		result.add(checkBox);

		return result;
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public void load(String ancre) {
		compteComboBox.setValue(ContextClient.getKeyCompteEnCours());

		service.getSoldeTheorique(ContextClient.getIdUserEnCours(),
				Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
				new DefaultAsyncCallback<BigDecimal>() {
			@Override
			public void onSuccess(BigDecimal arg0) {
				soldeTheoriqueLabel.setText(SOLDE_THEORIQUE+ContextClient.DECIMAL_FORMAT.format(arg0)+" €");
				soldeTheoriqueLabel.getElement().getStyle().setColor(NOIR);
				if(arg0.compareTo(new BigDecimal(1000))<0){
					soldeTheoriqueLabel.getElement().getStyle().setColor(ORANGE);
				}
				if(arg0.compareTo(new BigDecimal(0))<0){
					soldeTheoriqueLabel.getElement().getStyle().setColor(ROUGE);
				}
			}
		});
		service.getSoldeAnticipe(ContextClient.getIdUserEnCours(),
				Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
				new DefaultAsyncCallback<BigDecimal>() {
			@Override
			public void onSuccess(BigDecimal arg0) {
				soldeAnticipeLabel.setText(SOLDE_ANTICIPE+ContextClient.DECIMAL_FORMAT.format(arg0)+" €");
				soldeAnticipeLabel.getElement().getStyle().setColor(NOIR);
				if(arg0.compareTo(new BigDecimal(1000))<0){
					soldeAnticipeLabel.getElement().getStyle().setColor(ORANGE);
				}
				if(arg0.compareTo(new BigDecimal(0))<0){
					soldeAnticipeLabel.getElement().getStyle().setColor(ROUGE);
				}
			}
		});
		service.getSoldeBancaire(ContextClient.getIdUserEnCours(),
				Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
				new DefaultAsyncCallback<BigDecimal>() {
			@Override
			public void onSuccess(BigDecimal arg0) {
				soldeBancaireLabel.setText(SOLDE_BANCAIRE+ContextClient.DECIMAL_FORMAT.format(arg0)+" €");
				soldeBancaireLabel.getElement().getStyle().setColor(NOIR);
				if(arg0.compareTo(new BigDecimal(1000))<0){
					soldeBancaireLabel.getElement().getStyle().setColor(ORANGE);
				}
				if(arg0.compareTo(new BigDecimal(0))<0){
					soldeBancaireLabel.getElement().getStyle().setColor(ROUGE);
				}
			}
		});
		service.getMvtNotRapproch(ContextClient.getIdUserEnCours(),
				Long.parseLong(compteComboBox.getValue().getKey().substring(1)),
				new DefaultAsyncCallback<List<MouvementDo>>() {
			public void onSuccess(List<MouvementDo> result) {
				rapprochementPanel.clear();
				for(MouvementDo mvt:result){
					rapprochementPanel.add(createRapprochementLine(mvt));
				}
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public boolean isLoaded() {
		return false;
	}

	/*---------------------------------------------------------------------------------------------*/
}
