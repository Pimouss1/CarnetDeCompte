package fr.pimouss.land.client.controller;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.service.TabBudgetBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		TabBudgetBo[] getBudgetDepense(long idUser) throws ApplicationException;
		TabBudgetBo[] getBudgetRecette(long idUser) throws ApplicationException;
		void saveBudget(long idUser, List<BudgetDo> listeBudget) throws ApplicationException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getBudgetDepense(long idUser, AsyncCallback<TabBudgetBo[]> callback);
		void getBudgetRecette(long idUser, AsyncCallback<TabBudgetBo[]> callback);
		void saveBudget(long idUser, List<BudgetDo> listeBudget, AsyncCallback<Void> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
