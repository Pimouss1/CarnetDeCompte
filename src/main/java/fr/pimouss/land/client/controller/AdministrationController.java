package fr.pimouss.land.client.controller;

import java.io.IOException;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.SurveillanceBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class AdministrationController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		boolean isLogClient() throws IOException;
		UtilisateurDo connect(String login, String password) throws ApplicationException, IOException;
		UtilisateurDo connectByToken(String token) throws ApplicationException, IOException;
		void motDePassePerdu(String mail) throws ApplicationException, IOException;
		void changePassword(Long idUser, String password, String newPassword) throws ApplicationException;
		String[] getNumVersion() throws ApplicationException;
		boolean isUserAdministrateur(long idUser) throws DaoException;
		boolean isUserTresorier(long idUser) throws DaoException;
		boolean isUserContributeur(long idUser) throws DaoException;
		List<PeriodeDo> getAllPeriode() throws DaoException;
		void forceSaveDatabase(long idUser) throws ApplicationException;
		void forceRAZSolde(long idUser) throws ApplicationException;
		List<SurveillanceBo> getLastAction(long idUser) throws ApplicationException;
		void forceVerifSolde(long idUser) throws ApplicationException;
		void forceVerifVentil(long idUser) throws ApplicationException;
		void forceVerifNumPiece(long idUser) throws ApplicationException;
		void verifJustif(long idUser) throws ApplicationException, IOException;
		PeriodeDo createPeriode(long idUser) throws ApplicationException;
		List<PeriodeDo> saveListePeriode(long idUser, List<PeriodeDo> listePeriode) throws ApplicationException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void isLogClient(AsyncCallback<Boolean> callback);
		void connect(String login, String password, AsyncCallback<UtilisateurDo> callback);
		void connectByToken(String token, AsyncCallback<UtilisateurDo> callback);
		void motDePassePerdu(String mail, AsyncCallback<Void> callback);
		void changePassword(Long idUser, String password, String newPassword, AsyncCallback<Void> callback);
		void getNumVersion(AsyncCallback<String[]> callback);
		void isUserAdministrateur(long idUser, AsyncCallback<Boolean> callback);
		void isUserTresorier(long idUser, AsyncCallback<Boolean> callback);
		void isUserContributeur(long idUser, AsyncCallback<Boolean> callback);
		void forceSaveDatabase(long idUser, AsyncCallback<Void> callback);
		void forceRAZSolde(long idUser, AsyncCallback<Void> callback);
		void forceVerifSolde(long idUser, AsyncCallback<Void> callback);
		void forceVerifVentil(long idUser, AsyncCallback<Void> callback);
		void forceVerifNumPiece(long idUser, AsyncCallback<Void> callback);
		void verifJustif(long idUser, AsyncCallback<Void> callback);
		void getLastAction(long idUser, AsyncCallback<List<SurveillanceBo>> callback);
		void getAllPeriode(AsyncCallback<List<PeriodeDo>> callback);
		void createPeriode(long idUser, AsyncCallback<PeriodeDo> callback);
		void saveListePeriode(long idUser, List<PeriodeDo> listePeriode, AsyncCallback<List<PeriodeDo>> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
