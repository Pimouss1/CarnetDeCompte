package fr.pimouss.land.client.controller;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.RegleDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		List<RegleDo> getAllRegle() throws DaoException;
		RegleDo saveRegle(long idUser, RegleDo regle) throws ApplicationException;
		void deleteRegle(long idUser, long idRegle) throws ApplicationException;
		List<ModePaiementDo> getAllModePaiementDepense() throws DaoException;
		List<ModePaiementDo> getAllModePaiementRecette() throws DaoException;
		List<TypeVentilationDo> getAllTypeVentilationDepense() throws DaoException;
		List<TypeVentilationDo> getAllTypeVentilationRecette() throws DaoException;
		List<CompteDo> getAllCompte() throws DaoException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getAllRegle(AsyncCallback<List<RegleDo>> callback);
		void saveRegle(long idUser, RegleDo regle, AsyncCallback<RegleDo> callback);
		void deleteRegle(long idUser, long idRegle, AsyncCallback<Void> callback);
		void getAllModePaiementDepense(AsyncCallback<List<ModePaiementDo>> callback);
		void getAllModePaiementRecette(AsyncCallback<List<ModePaiementDo>> callback);
		void getAllTypeVentilationDepense(AsyncCallback<List<TypeVentilationDo>> callback);
		void getAllTypeVentilationRecette(AsyncCallback<List<TypeVentilationDo>> callback);
		void getAllCompte(AsyncCallback<List<CompteDo>> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
