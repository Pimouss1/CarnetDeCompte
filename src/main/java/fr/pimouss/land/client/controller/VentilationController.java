package fr.pimouss.land.client.controller;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		List<TypeVentilationDo> getAllTypeVentilation() throws DaoException;
		void deleteVentil(long idUser, long idVentil) throws ApplicationException;
		TypeVentilationDo saveVentil(long idUser, TypeVentilationDo ventil) throws ApplicationException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getAllTypeVentilation(AsyncCallback<List<TypeVentilationDo>> callback);
		void saveVentil(long idUser, TypeVentilationDo ventil, AsyncCallback<TypeVentilationDo> callback);
		void deleteVentil(long idUser, long idVentil, AsyncCallback<Void> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
