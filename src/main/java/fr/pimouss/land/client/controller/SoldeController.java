package fr.pimouss.land.client.controller;

import java.math.BigDecimal;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		BigDecimal rapprochMvt(long idUser, long idMvt, long datePaiement, boolean rapproch, long idCompte) throws ApplicationException;
		List<MouvementDo> getMvtNotRapproch(long idUser, long idCompte) throws ApplicationException;
		BigDecimal getSoldeTheorique(long idUser, long idCompte) throws ApplicationException;
		BigDecimal getSoldeAnticipe(long idUser, long idCompte) throws ApplicationException;
		BigDecimal getSoldeBancaire(long idUser, long idCompte) throws ApplicationException;
		List<CompteDo> getAllCompte() throws DaoException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getSoldeTheorique(long idUser, long idCompte, AsyncCallback<BigDecimal> callback);
		void getSoldeAnticipe(long idUser, long idCompte, AsyncCallback<BigDecimal> callback);
		void getSoldeBancaire(long idUser, long idCompte, AsyncCallback<BigDecimal> callback);
		void getMvtNotRapproch(long idUser, long idCompte, AsyncCallback<List<MouvementDo>> callback);
		void rapprochMvt(long idUser, long idMvt, long datePaiement, boolean rapproch, long idCompte, AsyncCallback<BigDecimal> callback);
		void getAllCompte(AsyncCallback<List<CompteDo>> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
