package fr.pimouss.land.client.controller;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.BilanBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BilanController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		List<BilanBo> getBilanDepenseByPeriode(long idPeriode) throws DaoException;
		List<BilanBo> getBilanRecetteByPeriode(long idPeriode) throws DaoException;
		List<BilanBo> getBilanGroupeDepenseByPeriode(long idPeriode) throws ApplicationException;
		List<BilanBo> getBilanGroupeRecetteByPeriode(long idPeriode) throws ApplicationException;
		List<PeriodeDo> getAllPeriode() throws DaoException;
		PeriodeDo getPeriodeByDate(long date) throws DaoException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getBilanDepenseByPeriode(long idPeriode, AsyncCallback<List<BilanBo>> callback);
		void getBilanRecetteByPeriode(long idPeriode, AsyncCallback<List<BilanBo>> callback);
		void getBilanGroupeDepenseByPeriode(long idPeriode, AsyncCallback<List<BilanBo>> callback);
		void getBilanGroupeRecetteByPeriode(long idPeriode, AsyncCallback<List<BilanBo>> callback);
		void getAllPeriode(AsyncCallback<List<PeriodeDo>> callback);
		void getPeriodeByDate(long date, AsyncCallback<PeriodeDo> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
