package fr.pimouss.land.client.controller;

import java.io.IOException;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import fr.pimouss.land.shared.dao.CompteDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;
import fr.pimouss.land.shared.exception.ApplicationException;
import fr.pimouss.land.shared.exception.DaoException;
import fr.pimouss.land.shared.service.MouvementBo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementController {

	/*---------------------------------------------------------------------------------------------*/
	public interface IFace extends RemoteService {
		List<ModePaiementDo> getAllModePaiementDepense() throws DaoException;
		List<ModePaiementDo> getAllModePaiementRecette() throws DaoException;
		List<TypeVentilationDo> getAllTypeVentilationDepense() throws DaoException;
		List<TypeVentilationDo> getAllTypeVentilationRecette() throws DaoException;
		PeriodeDo getPeriodeByDate(long date) throws DaoException;
		List<PeriodeDo> getAllPeriode() throws DaoException;
		List<CompteDo> getAllCompte() throws DaoException;
		List<MouvementBo> getRecetteByPeriode(long idUser, long idPeriode, long idCompte) throws ApplicationException;
		List<MouvementBo> getDepenseByPeriode(long idUser, long idPeriode, long idCompte) throws ApplicationException;
		MouvementBo loadJustificatif(long idUser, MouvementBo mvt) throws ApplicationException;
		void deleteJustificatif(long idUser, long idJustificatif) throws ApplicationException, IOException;
		void deleteMvt(long idUser, long idMouvement, boolean force) throws ApplicationException;
		MouvementBo saveMvt(long idUser, MouvementBo mvt) throws ApplicationException;
	}

	/*---------------------------------------------------------------------------------------------*/
	public interface IFaceAsync {
		void getDepenseByPeriode(long idUser, long idPeriode, long idCompte, AsyncCallback<List<MouvementBo>> callback);
		void getRecetteByPeriode(long idUser, long idPeriode, long idCompte, AsyncCallback<List<MouvementBo>> callback);
		void getAllModePaiementDepense(AsyncCallback<List<ModePaiementDo>> callback);
		void getAllModePaiementRecette(AsyncCallback<List<ModePaiementDo>> callback);
		void getAllTypeVentilationDepense(AsyncCallback<List<TypeVentilationDo>> callback);
		void getAllTypeVentilationRecette(AsyncCallback<List<TypeVentilationDo>> callback);
		void getAllPeriode(AsyncCallback<List<PeriodeDo>> callback);
		void getAllCompte(AsyncCallback<List<CompteDo>> callback);
		void saveMvt(long idUser, MouvementBo mvt, AsyncCallback<MouvementBo> callback);
		void deleteMvt(long idUser, long idMouvement, boolean force, AsyncCallback<Void> callback);
		void deleteJustificatif(long idUser, long idJustificatif, AsyncCallback<Void> callback);
		void loadJustificatif(long idUser, MouvementBo mvt, AsyncCallback<MouvementBo> callback);
		void getPeriodeByDate(long date, AsyncCallback<PeriodeDo> callback);
	}

	/*---------------------------------------------------------------------------------------------*/
}
