package fr.pimouss.land.shared.service;

import java.io.Serializable;

import fr.pimouss.land.shared.dao.SurveillanceDo;
import fr.pimouss.land.shared.dao.UtilisateurDo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SurveillanceBo implements Serializable, Comparable<SurveillanceBo> {
	private static final long serialVersionUID = 776036108833880189L;
	private SurveillanceDo surveillanceDo;
	private UtilisateurDo utilisateurDo;

	public SurveillanceBo() {/*nothing to do*/}
	public SurveillanceBo(SurveillanceDo surveillanceDo) {
		this.surveillanceDo=surveillanceDo;
	}

	public UtilisateurDo getUtilisateurDo() {
		return utilisateurDo;
	}
	public void setUtilisateurDo(UtilisateurDo utilisateurDo) {
		this.utilisateurDo = utilisateurDo;
	}
	public SurveillanceDo getSurveillanceDo() {
		return surveillanceDo;
	}
	public void setSurveillanceDo(SurveillanceDo surveillanceDo) {
		this.surveillanceDo = surveillanceDo;
	}

	@Override
	public int compareTo(SurveillanceBo o) {
		return getSurveillanceDo().compareTo(o.getSurveillanceDo());
	}
}
