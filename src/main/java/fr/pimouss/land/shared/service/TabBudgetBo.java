package fr.pimouss.land.shared.service;

import java.io.Serializable;

public class TabBudgetBo implements Serializable {
	private static final long serialVersionUID = 3022574486025461685L;
	private BudgetBo[] tabBudgetBo;

	public TabBudgetBo() {/*nothing to do*/}

	public BudgetBo[] getTabBudgetBo() {
		return tabBudgetBo;
	}
	public void setTabBudgetBo(BudgetBo[] tabBudgetBo) {
		this.tabBudgetBo = tabBudgetBo;
	}
}
