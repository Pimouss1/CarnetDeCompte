package fr.pimouss.land.shared.service;

import java.io.Serializable;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BilanBo implements Serializable, Comparable<BilanBo> {
	private static final long serialVersionUID = 3163947435602297130L;
	private long idTypeVentilation;
	private String typeVentilation;
	private double budget;
	private double montant;
	private double montantEncaisse;

	public long getIdTypeVentilation() {
		return idTypeVentilation;
	}
	public void setIdTypeVentilation(long idTypeVentilation) {
		this.idTypeVentilation = idTypeVentilation;
	}
	public String getTypeVentilation() {
		return typeVentilation;
	}
	public void setTypeVentilation(String typeVentilation) {
		this.typeVentilation = typeVentilation;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public double getBudget() {
		return budget;
	}
	public void setBudget(double budget) {
		this.budget = budget;
	}
	public double getMontantEncaisse() {
		return montantEncaisse;
	}
	public void setMontantEncaisse(double montantEncaisse) {
		this.montantEncaisse = montantEncaisse;
	}

	@Override
	public int compareTo(BilanBo o) {
		return typeVentilation.compareTo(o.typeVentilation);
	}
}
