package fr.pimouss.land.shared.service;

import java.io.Serializable;
import java.math.BigDecimal;

import fr.pimouss.land.shared.dao.BudgetDo;
import fr.pimouss.land.shared.dao.PeriodeDo;
import fr.pimouss.land.shared.dao.TypeVentilationDo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetBo implements Serializable {
	private static final long serialVersionUID = -6668558405764703883L;
	private BudgetDo budgetDo;
	private PeriodeDo periodeDo;
	private BigDecimal totalPeriode;
	private TypeVentilationDo typeVentilationDo;

	public BudgetBo() {/*nothing to do*/}
	public BudgetBo(BudgetDo budgetDo) {
		this.budgetDo=budgetDo;
	}

	public BudgetDo getBudgetDo() {
		return budgetDo;
	}
	public void setBudgetDo(BudgetDo budgetDo) {
		this.budgetDo = budgetDo;
	}
	public PeriodeDo getPeriodeDo() {
		return periodeDo;
	}
	public void setPeriodeDo(PeriodeDo periodeDo) {
		this.periodeDo = periodeDo;
	}
	public TypeVentilationDo getTypeVentilationDo() {
		return typeVentilationDo;
	}
	public void setTypeVentilationDo(TypeVentilationDo typeVentilationDo) {
		this.typeVentilationDo = typeVentilationDo;
	}
	public BigDecimal getTotalPeriode() {
		return totalPeriode;
	}
	public void setTotalPeriode(BigDecimal totalPeriode) {
		this.totalPeriode = totalPeriode;
	}
}
