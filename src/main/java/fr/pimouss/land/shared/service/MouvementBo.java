package fr.pimouss.land.shared.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.pimouss.land.shared.dao.JustificatifDo;
import fr.pimouss.land.shared.dao.ModePaiementDo;
import fr.pimouss.land.shared.dao.MouvementDo;
import fr.pimouss.land.shared.dao.VentilationDo;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementBo implements Serializable,Comparable<MouvementBo> {
	private static final long serialVersionUID = 4770180454351891482L;
	private MouvementDo mouvementDo;
	private ModePaiementDo modePaiementDo;
	private List<VentilationDo> listeVentilation;
	private List<JustificatifDo> listeJustificatif;

	public MouvementBo() {/*nothing to do*/}
	public MouvementBo(MouvementDo mouvementDo){
		this.mouvementDo=mouvementDo;
	}

	public MouvementDo getMouvementDo() {
		return mouvementDo;
	}
	public void setMouvementDo(MouvementDo mouvementDo) {
		this.mouvementDo = mouvementDo;
	}
	public ModePaiementDo getModePaiementDo() {
		return modePaiementDo;
	}
	public void setModePaiementDo(ModePaiementDo modePaiementDo) {
		this.modePaiementDo = modePaiementDo;
	}
	public List<JustificatifDo> getListeJustificatif() {
		if(listeJustificatif==null){
			listeJustificatif=new ArrayList<>();
		}
		return listeJustificatif;
	}
	public void setListeJustificatif(List<JustificatifDo> listeJustificatif) {
		this.listeJustificatif = listeJustificatif;
	}
	public List<VentilationDo> getListeVentilation() {
		if(listeVentilation==null){
			listeVentilation=new ArrayList<>();
		}
		return listeVentilation;
	}
	public void setListeVentilation(List<VentilationDo> listeVentilation) {
		this.listeVentilation = listeVentilation;
	}

	@Override
	public String toString() {
		return mouvementDo.toString();
	}
	@Override
	public int compareTo(MouvementBo o) {
		return getMouvementDo().compareTo(o.getMouvementDo());
	}
}
