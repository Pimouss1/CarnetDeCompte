package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ServiceException extends ApplicationException {
	private static final long serialVersionUID = -248927276839980529L;

	/*---------------------------------------------------------------------------------------------*/
	public ServiceException() {}
	public ServiceException(String message, Exception exception) {
		super(message, exception);
	}

	/*---------------------------------------------------------------------------------------------*/
}
