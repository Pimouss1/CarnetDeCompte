package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilException extends ApplicationException {
	private static final long serialVersionUID = 4215328247929089491L;

	/*---------------------------------------------------------------------------------------------*/
	public UtilException() {}
	public UtilException(String message, Exception exception) {
		super(message, exception);
	}

	/*---------------------------------------------------------------------------------------------*/
}
