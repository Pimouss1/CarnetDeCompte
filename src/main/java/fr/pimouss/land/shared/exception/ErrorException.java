package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ErrorException extends ApplicationException {
	static private final long serialVersionUID = 455186421176789247L;

	/*---------------------------------------------------------------------------------------------*/
	public ErrorException() {}
	public ErrorException(String message, Exception exception) {
		super(message, exception);
	}

	/*---------------------------------------------------------------------------------------------*/
}
