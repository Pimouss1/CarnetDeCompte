package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class WarningException extends ApplicationException {
	private static final long serialVersionUID = -7650906691731847647L;

	/*---------------------------------------------------------------------------------------------*/
	public WarningException() {}
	public WarningException(String message, Exception exception) {
		super(message, exception);
	}

	/*---------------------------------------------------------------------------------------------*/
}
