package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class DaoException extends ApplicationException {
	private static final long serialVersionUID = -6826459613759732380L;

	/*---------------------------------------------------------------------------------------------*/
	public DaoException() {
		// Use by introspection
	}
	public DaoException(String message, Exception exception) {
		super(message, exception);
	}

	/*---------------------------------------------------------------------------------------------*/
}
