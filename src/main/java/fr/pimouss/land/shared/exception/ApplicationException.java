package fr.pimouss.land.shared.exception;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ApplicationException extends Exception {
	static private final long serialVersionUID = 8446216914996805738L;

	private String message;
	private Exception exception;

	/*---------------------------------------------------------------------------------------------*/
	public ApplicationException() {}
	public ApplicationException(String message, Exception exception) {
		super();
		this.message=message;
		this.exception=exception;
	}

	/*---------------------------------------------------------------------------------------------*/
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}

	/*---------------------------------------------------------------------------------------------*/
}
