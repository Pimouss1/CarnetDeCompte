package fr.pimouss.land.shared.dao;

import java.math.BigDecimal;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class RegleDo extends AbstractDo {
	private static final long serialVersionUID = -1030848989206328242L;
	public static final String FREQUENCE_MENSUELLE="M";
	public static final String FREQUENCE_ANNUELLE="A";
	private String typeMouvementSrc;
	private long idTypeVentilationSrc;
	private String frequence;
	private int jourDuMois;
	private int mois;
	private int anticipe;
	private long idTypeVentilationTarget;
	private long idCompte;
	private String typeMouvementTarget;
	private String libelle;
	private String objet;
	private long idModePaiement;
	private BigDecimal montant;
	private boolean horsBilan;

	public RegleDo() {}
	public RegleDo(long rowid, String typeMouvementSrc, long idTypeVentilationSrc, String frequence, int jourDuMois, int mois, int anticipe, long idTypeVentilationTarget, long idCompte,
			String typeMouvementTarget, String libelle, String objet, long idModePaiement, BigDecimal montant, boolean horsBilan) {
		setRowid(rowid);
		setTypeMouvementSrc(typeMouvementSrc);
		setIdTypeVentilationSrc(idTypeVentilationSrc);
		setFrequence(frequence);
		setJourDuMois(jourDuMois);
		setMois(mois);
		setAnticipe(anticipe);
		setIdTypeVentilationTarget(idTypeVentilationTarget);
		setIdCompte(idCompte);
		setTypeMouvementTarget(typeMouvementTarget);
		setLibelle(libelle);
		setObjet(objet);
		setIdModePaiement(idModePaiement);
		setMontant(montant);
		setHorsBilan(horsBilan);
	}

	public long getIdModePaiement() {
		return idModePaiement;
	}
	public void setIdModePaiement(long idModePaiement) {
		this.idModePaiement = idModePaiement;
	}
	public void setHorsBilan(boolean horsBilan) {
		this.horsBilan = horsBilan;
	}
	public boolean isHorsBilan() {
		return horsBilan;
	}
	public String getTypeMouvementSrc() {
		return typeMouvementSrc;
	}
	public void setTypeMouvementSrc(String typeMouvementSrc) {
		this.typeMouvementSrc = typeMouvementSrc;
	}
	public String getTypeMouvementTarget() {
		return typeMouvementTarget;
	}
	public void setTypeMouvementTarget(String typeMouvementTarget) {
		this.typeMouvementTarget = typeMouvementTarget;
	}
	public long getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(long idCompte) {
		this.idCompte = idCompte;
	}
	public long getIdTypeVentilationSrc() {
		return idTypeVentilationSrc;
	}
	public void setIdTypeVentilationSrc(long idTypeVentilationSrc) {
		this.idTypeVentilationSrc = idTypeVentilationSrc;
	}
	public String getFrequence() {
		return frequence;
	}
	public void setFrequence(String frequence) {
		this.frequence = frequence;
	}
	public int getJourDuMois() {
		return jourDuMois;
	}
	public void setJourDuMois(int jourDuMois) {
		this.jourDuMois = jourDuMois;
	}
	public int getMois() {
		return mois;
	}
	public void setMois(int mois) {
		this.mois = mois;
	}
	public int getAnticipe() {
		return anticipe;
	}
	public void setAnticipe(int anticipe) {
		this.anticipe = anticipe;
	}
	public long getIdTypeVentilationTarget() {
		return idTypeVentilationTarget;
	}
	public void setIdTypeVentilationTarget(long idTypeVentilationTarget) {
		this.idTypeVentilationTarget = idTypeVentilationTarget;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return (int)(((RegleDo)o).getRowid()-getRowid());
	}
}
