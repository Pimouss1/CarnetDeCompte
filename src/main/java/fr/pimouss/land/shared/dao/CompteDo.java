package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class CompteDo extends AbstractDo {
	private static final long serialVersionUID = -4872721464541976131L;
	private String compte;

	public CompteDo() {}
	public CompteDo(long rowid, String compte) {
		setRowid(rowid);
		setCompte(compte);
	}

	public String getCompte() {
		return compte;
	}
	public void setCompte(String compte) {
		this.compte = compte;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return compte.compareTo(((CompteDo)o).compte);
	}
}
