package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class GroupeUtilisateurDo extends AbstractDo {
	private static final long serialVersionUID = 1146598766255689623L;
	public static final long ADMINISTRATEUR=1;
	public static final long TRESORIER=2;
	public static final long CONTRIBUTEUR=3;

	private String groupe;

	public GroupeUtilisateurDo() {}
	public GroupeUtilisateurDo(long rowid, String groupe) {
		setRowid(rowid);
		setGroupe(groupe);
	}

	public String getGroupe() {
		return groupe;
	}
	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return groupe.compareTo(((GroupeUtilisateurDo)o).groupe);
	}
}
