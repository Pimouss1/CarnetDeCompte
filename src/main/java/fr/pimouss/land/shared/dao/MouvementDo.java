package fr.pimouss.land.shared.dao;

import java.math.BigDecimal;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class MouvementDo extends AbstractDo {
	private static final long serialVersionUID = -1657618716911449703L;
	public static final String DEPENSE="D";
	public static final String RECETTE="R";
	private long idCompte;
	private String typeMouvement;
	private long datePaiement;
	private long dateFacturation;
	private String libelle;
	private String objet;
	private long idModePaiement;
	private int numCheque;
	private BigDecimal montant;
	private int numPiece;
	private boolean rapprochementBancaire;
	private boolean solde;
	private boolean regleAppliquee;
	private boolean previsionnel;
	private boolean horsBilan;

	public MouvementDo() {
		dateFacturation=System.currentTimeMillis();
	}
	public MouvementDo(long rowid, long idCompte, String typeMouvement, long datePaiement, long dateFacturation, String libelle, String objet,
			long idModePaiement, int numCheque, BigDecimal montant, int numPiece, boolean rapprochementBancaire, boolean solde, boolean regleAppliquee,
			boolean previsionnel, boolean horsBilan) {
		setRowid(rowid);
		setIdCompte(idCompte);
		setTypeMouvement(typeMouvement);
		setDatePaiement(datePaiement);
		setDateFacturation(dateFacturation);
		setLibelle(libelle);
		setObjet(objet);
		setIdModePaiement(idModePaiement);
		setNumCheque(numCheque);
		setMontant(montant);
		setNumPiece(numPiece);
		setRapprochementBancaire(rapprochementBancaire);
		setSolde(solde);
		setRegleAppliquee(regleAppliquee);
		setPrevisionnel(previsionnel);
		setHorsBilan(horsBilan);
	}

	public String getTypeMouvement() {
		return typeMouvement;
	}
	public void setTypeMouvement(String typeMouvement) {
		this.typeMouvement = typeMouvement;
	}
	public long getDateFacturation() {
		return dateFacturation;
	}
	public void setDateFacturation(long dateFacturation) {
		this.dateFacturation = dateFacturation;
	}
	public long getDatePaiement() {
		return datePaiement;
	}
	public void setDatePaiement(long datePaiement) {
		this.datePaiement = datePaiement;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public long getIdModePaiement() {
		return idModePaiement;
	}
	public void setIdModePaiement(long idModePaiement) {
		this.idModePaiement = idModePaiement;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public int getNumCheque() {
		return numCheque;
	}
	public void setNumCheque(int numCheque) {
		this.numCheque = numCheque;
	}
	public int getNumPiece() {
		return numPiece;
	}
	public void setNumPiece(int numPiece) {
		this.numPiece = numPiece;
	}
	public boolean isRapprochementBancaire() {
		return rapprochementBancaire;
	}
	public void setRapprochementBancaire(boolean rapprochementBancaire) {
		this.rapprochementBancaire = rapprochementBancaire;
	}
	public boolean isSolde() {
		return solde;
	}
	public void setSolde(boolean solde) {
		this.solde = solde;
	}
	public long getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(long idCompte) {
		this.idCompte = idCompte;
	}
	public boolean isRegleAppliquee() {
		return regleAppliquee;
	}
	public void setRegleAppliquee(boolean regleAppliquee) {
		this.regleAppliquee = regleAppliquee;
	}
	public boolean isPrevisionnel() {
		return previsionnel;
	}
	public void setPrevisionnel(boolean previsionnel) {
		this.previsionnel = previsionnel;
	}
	public boolean isHorsBilan() {
		return horsBilan;
	}
	public void setHorsBilan(boolean horsBilan) {
		this.horsBilan = horsBilan;
	}

	@Override
	public String toString() {
		return libelle;
	}
	@Override
	public int compareTo(AbstractDo o) {
		return (int)(((MouvementDo)o).dateFacturation/1000-dateFacturation/1000);
	}
}
