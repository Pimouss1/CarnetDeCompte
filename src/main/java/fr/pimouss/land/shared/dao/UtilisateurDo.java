package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class UtilisateurDo extends AbstractDo {
	private static final long serialVersionUID = -6363324735078819017L;
	private String nom;
	private String prenom;
	private String login;
	private String password;
	private String mail;
	private boolean actif;
	private String token;

	public UtilisateurDo() {}
	public UtilisateurDo(long rowid, String nom, String prenom, String login, String password, String mail, boolean actif, String token) {
		setRowid(rowid);
		setNom(nom);
		setPrenom(prenom);
		setLogin(login);
		setPassword(password);
		setMail(mail);
		setActif(actif);
		setToken(token);
	}

	public String getNom() {
		return nom;
	}
	public boolean isActif() {
		return actif;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setActif(boolean actif) {
		this.actif = actif;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public int compareTo(AbstractDo o) {
		if(this.getNom().equals(((UtilisateurDo)o).getNom())){
			return this.getPrenom().compareTo(((UtilisateurDo)o).getPrenom());
		}
		return this.getNom().compareTo(((UtilisateurDo)o).getNom());
	}
}
