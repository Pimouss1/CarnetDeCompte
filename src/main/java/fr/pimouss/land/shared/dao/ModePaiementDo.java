package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class ModePaiementDo extends AbstractDo {
	private static final long serialVersionUID = -1822515457674220167L;
	public static final long CHEQUE=1;
	public static final long CARTE_BANCAIRE=2;
	public static final long VIREMENT_D=3;
	public static final long PRELEVEMENT=4;
	public static final long REMISE_CHEQUE=5;
	public static final long DEPOT_ESPECES=6;
	public static final long VIREMENT_R=7;
	private String typeMouvement;
	private String modePaiement;

	public ModePaiementDo() {}
	public ModePaiementDo(long rowid, String typeMouvement, String modePaiement) {
		setRowid(rowid);
		setTypeMouvement(typeMouvement);
		setModePaiement(modePaiement);
	}

	public String getTypeMouvement() {
		return typeMouvement;
	}
	public void setTypeMouvement(String typeMouvement) {
		this.typeMouvement = typeMouvement;
	}
	public String getModePaiement() {
		return modePaiement;
	}
	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return modePaiement.compareTo(((ModePaiementDo)o).modePaiement);
	}
}
