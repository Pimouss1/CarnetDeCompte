package fr.pimouss.land.shared.dao;

import java.math.BigDecimal;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VentilationDo extends AbstractDo {
	private static final long serialVersionUID = 3907024096588450354L;
	private long idMouvement;
	private long idTypeVentilation;
	private BigDecimal montant;

	public VentilationDo() {}
	public VentilationDo(long rowid, long idMouvement, long idTypeVentilation, BigDecimal montant) {
		setRowid(rowid);
		setIdMouvement(idMouvement);
		setIdTypeVentilation(idTypeVentilation);
		setMontant(montant);
	}

	public long getIdTypeVentilation() {
		return idTypeVentilation;
	}
	public void setIdTypeVentilation(long idTypeVentilation) {
		this.idTypeVentilation = idTypeVentilation;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public long getIdMouvement() {
		return idMouvement;
	}
	public void setIdMouvement(long idMouvement) {
		this.idMouvement = idMouvement;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return (int)(idTypeVentilation-(((VentilationDo)o).idTypeVentilation));
	}
}
