package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SurveillanceDo extends AbstractDo {
	private static final long serialVersionUID = 547650604270659288L;
	private long date;
	private long idUtilisateur;
	private String classe;
	private String methode;
	private String ip;

	public SurveillanceDo() {}
	public SurveillanceDo(long rowid, long date, long idUtilisateur, String classe, String methode, String ip) {
		setRowid(rowid);
		setDate(date);
		setIdUtilisateur(idUtilisateur);
		setClasse(classe);
		setMethode(methode);
		setIp(ip);
	}

	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public long getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getMethode() {
		return methode;
	}
	public void setMethode(String methode) {
		this.methode = methode;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return (int)(((SurveillanceDo)o).date/1000-(date/1000));
	}
}
