package fr.pimouss.land.shared.dao;

import java.io.Serializable;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public abstract class AbstractDo implements Serializable, Comparable<AbstractDo> {
	private static final long serialVersionUID = 7262388634332675314L;
	private long rowid;

	/*-------------------------------------------------------------------------*/
	public long getRowid() {
		return rowid;
	}
	public void setRowid(long rowid) {
		this.rowid = rowid;
	}

	/*-------------------------------------------------------------------------*/
}
