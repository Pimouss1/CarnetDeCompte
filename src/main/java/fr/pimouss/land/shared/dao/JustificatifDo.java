package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class JustificatifDo extends AbstractDo {
	private static final long serialVersionUID = -7805053458861919275L;
	private long idMouvement;
	private String filename;

	public JustificatifDo() {}
	public JustificatifDo(long rowid, long idMouvement, String filename) {
		setRowid(rowid);
		setIdMouvement(idMouvement);
		setFilename(filename);
	}

	public long getIdMouvement() {
		return idMouvement;
	}
	public void setIdMouvement(long idMouvement) {
		this.idMouvement = idMouvement;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return filename.compareTo(((JustificatifDo)o).filename);
	}
}
