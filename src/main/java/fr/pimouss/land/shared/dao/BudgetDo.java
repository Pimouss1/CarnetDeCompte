package fr.pimouss.land.shared.dao;

import java.math.BigDecimal;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class BudgetDo extends AbstractDo {
	private static final long serialVersionUID = 8284357577943563850L;
	private long idTypeVentilation;
	private long idPeriode;
	private BigDecimal montant;

	public BudgetDo() {}
	public BudgetDo(long rowid, long idTypeVentilation, long idPeriode, BigDecimal montant) {
		setRowid(rowid);
		setIdTypeVentilation(idTypeVentilation);
		setIdPeriode(idPeriode);
		setMontant(montant);
	}

	public long getIdPeriode() {
		return idPeriode;
	}
	public void setIdPeriode(long idPeriode) {
		this.idPeriode = idPeriode;
	}
	public long getIdTypeVentilation() {
		return idTypeVentilation;
	}
	public void setIdTypeVentilation(long idTypeVentilation) {
		this.idTypeVentilation = idTypeVentilation;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return 0;
	}
}
