package fr.pimouss.land.shared.dao;

import java.math.BigDecimal;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class SoldeDo extends AbstractDo {
	private static final long serialVersionUID = 554696924742186198L;
	private long idCompte;
	private long date;
	private BigDecimal montant;

	public SoldeDo() {}
	public SoldeDo(long rowid, long idCompte, long date, BigDecimal montant) {
		setRowid(rowid);
		setIdCompte(idCompte);
		setDate(date);
		setMontant(montant);
	}

	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public long getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(long idCompte) {
		this.idCompte = idCompte;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return (int)(date/1000-(((SoldeDo)o).date/1000));
	}
}
