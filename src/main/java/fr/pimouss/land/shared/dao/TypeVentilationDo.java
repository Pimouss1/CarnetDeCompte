package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class TypeVentilationDo extends AbstractDo {
	private static final long serialVersionUID = 5837771188177365260L;
	private String typeMouvement;
	private String typeVentilation;
	private boolean actif;

	public TypeVentilationDo() {}
	public TypeVentilationDo(long rowid, String typeMouvement, String typeVentilation, boolean actif) {
		setRowid(rowid);
		setTypeMouvement(typeMouvement);
		setTypeVentilation(typeVentilation);
		setActif(actif);
	}

	public String getTypeMouvement() {
		return typeMouvement;
	}
	public void setTypeMouvement(String typeMouvement) {
		this.typeMouvement = typeMouvement;
	}
	public String getTypeVentilation() {
		return typeVentilation;
	}
	public void setTypeVentilation(String typeVentilation) {
		this.typeVentilation = typeVentilation;
	}
	public void setActif(boolean actif) {
		this.actif = actif;
	}
	public boolean isActif() {
		return actif;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return typeVentilation.compareTo(((TypeVentilationDo)o).typeVentilation);
	}
}
