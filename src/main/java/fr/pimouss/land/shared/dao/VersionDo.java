package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class VersionDo extends AbstractDo {
	private static final long serialVersionUID = -3027295355166831259L;
	private long date;
	private String numVersion;

	public VersionDo() {}
	public VersionDo(long rowid, long date, String numVersion) {
		setRowid(rowid);
		setDate(date);
		setNumVersion(numVersion);
	}

	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getNumVersion() {
		return numVersion;
	}
	public void setNumVersion(String numVersion) {
		this.numVersion = numVersion;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return numVersion.compareTo(((VersionDo)o).numVersion);
	}
}
