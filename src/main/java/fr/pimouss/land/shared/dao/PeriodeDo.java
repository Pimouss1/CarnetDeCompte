package fr.pimouss.land.shared.dao;

/**
 * @author Pimouss - http://www.pimouss-land.fr
 */
public class PeriodeDo extends AbstractDo {
	private static final long serialVersionUID = 7542327461376845616L;
	private long dateDebut;
	private long dateFin;
	private String nom;
	private boolean bilanFige;

	public PeriodeDo() {}
	public PeriodeDo(long rowid, long dateDebut, long dateFin, String nom, boolean bilanFige) {
		setRowid(rowid);
		setDateDebut(dateDebut);
		setDateFin(dateFin);
		setNom(nom);
		setBilanFige(bilanFige);
	}

	public long getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(long dateDebut) {
		this.dateDebut = dateDebut;
	}
	public long getDateFin() {
		return dateFin;
	}
	public void setDateFin(long dateFin) {
		this.dateFin = dateFin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public boolean isBilanFige() {
		return bilanFige;
	}
	public void setBilanFige(boolean bilanFige) {
		this.bilanFige = bilanFige;
	}

	@Override
	public int compareTo(AbstractDo o) {
		return (int)(dateDebut/1000-(((PeriodeDo)o).dateDebut/1000));
	}
}
