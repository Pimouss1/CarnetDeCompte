package fr.pimouss.land.server;

import java.io.File;
import java.io.IOException;

import com.google.gwt.junit.client.GWTTestCase;

import fr.pimouss.util.cst.ConfEnv;

public class TestLoggeur extends GWTTestCase {

	/*---------------------------------------------------------------------------------------------*/
	public TestLoggeur() throws IOException {
		ConfEnv.setPropAppInClassPath("carnetcompte_test.properties");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public String getModuleName() {
		return "fr.pimouss.land.PimoussLandJUnit";
	}

	/*---------------------------------------------------------------------------------------------*/
	public void testRepLog() throws IOException{
		assertTrue((new File(ConfEnv.getAppRepLog())).isDirectory());
	}

	/*---------------------------------------------------------------------------------------------*/
}
