package fr.pimouss.land.server;

import java.util.Collections;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import junit.framework.TestFailure;
import junit.framework.TestResult;
import junit.framework.TestSuite;

@RunWith(Suite.class)
//@SuiteClasses({TestAdmin.class, TestLoggeur.class})
public class SuiteServer extends TestSuite {
	public static void main(String[] args) {
		System.out.println("-----BEGIN-----");
		SuiteServer suite=new SuiteServer();
		TestResult result=new TestResult();
		suite.run(result);

		for(TestFailure elt:Collections.list(result.failures())){
			System.out.println(elt.failedTest().toString());
		}
		for(TestFailure elt:Collections.list(result.errors())){
			System.out.println(elt.failedTest().toString());
		}

		System.out.println("-----END-----");
	}
}
