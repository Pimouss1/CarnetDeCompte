package fr.pimouss.land.server;

import java.io.IOException;

import org.junit.Test;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import fr.pimouss.land.client.controller.AdministrationController.IFace;
import fr.pimouss.land.client.controller.AdministrationController.IFaceAsync;
import fr.pimouss.land.client.view.util.DefaultAsyncCallback;
import fr.pimouss.util.cst.ConfApp;
import fr.pimouss.util.cst.ConfEnv;

public class TestAdmin extends GWTTestCase {
	private IFaceAsync service;
	private String moduleRelativeURL;

	/*---------------------------------------------------------------------------------------------*/
	public TestAdmin() throws IOException {
		service=GWT.create(IFace.class);
		moduleRelativeURL=GWT.getModuleBaseURL()+"administrationService";
		((ServiceDefTarget)service).setServiceEntryPoint(moduleRelativeURL);
		ConfEnv.setPropAppInClassPath("carnetcompte_test.properties");
	}

	/*---------------------------------------------------------------------------------------------*/
	@Override
	public String getModuleName() {
		return "fr.pimouss.land.PimoussLandJUnit";
	}

	/*---------------------------------------------------------------------------------------------*/
	@Test
	public void testNumVersion() throws IOException {
		assertEquals("%1", ConfApp.getVersion());

		service.getNumVersion(new DefaultAsyncCallback<String[]>() {
			@Override
			public void onSuccess(String[] result) {
				assertEquals("%1", result[0]);
			}
		});
	}

	/*---------------------------------------------------------------------------------------------*/
}
