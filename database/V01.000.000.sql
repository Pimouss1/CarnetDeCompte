DROP DATABASE IF EXISTS CarnetCompte%AppName%DB;

CREATE DATABASE CarnetCompte%AppName%DB;

USE CarnetCompte%AppName%DB;

CREATE TABLE `Version_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` BIGINT UNSIGNED NOT NULL,
  `numVersion` VARCHAR(10),
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

INSERT INTO `Version_G` (`date`,`numVersion`) VALUES 
 (UNIX_TIMESTAMP(NOW()),'01.000.000');

CREATE TABLE `Utilisateur_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `actif` BOOLEAN NOT NULL,
  `token` VARCHAR(8),
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `GroupeUtilisateur_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `groupe` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

INSERT INTO `GroupeUtilisateur_G` (`rowid`,`groupe`) VALUES 
 (1,'Administrateur'),
 (2,'Trésorier'),
 (3,'Contributeur');

CREATE TABLE `UtilisateurGroupeUtilisateur_A` (
  `idUtilisateur` BIGINT UNSIGNED NOT NULL,
  `idGroupe` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`idUtilisateur`,`idGroupe`)
)
ENGINE = MYISAM;

CREATE TABLE `Mouvement_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idCompte` BIGINT UNSIGNED NOT NULL,
  `typeMouvement` VARCHAR(1) NOT NULL,
  `dateFacturation` BIGINT UNSIGNED NOT NULL,
  `datePaiement` BIGINT UNSIGNED NULL,
  `libelle` VARCHAR(2048) NOT NULL,
  `objet` VARCHAR(2048) NOT NULL,
  `idModePaiement` BIGINT UNSIGNED NOT NULL,
  `numCheque` INTEGER NULL,
  `montant` DECIMAL(7,2) NOT NULL,
  `numPiece` INTEGER NOT NULL,
  `solde` BOOLEAN NOT NULL,
  `rapprochementBancaire` BOOLEAN NOT NULL,
  `regleAppliquee` BOOLEAN NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `ModePaiement_R` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `typeMouvement` VARCHAR(1) NOT NULL,
  `modePaiement` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

INSERT INTO `ModePaiement_R` (`rowid`,`typeMouvement`,`modePaiement`) VALUES 
 (1,'D','Chèque'),
 (2,'D','Carte Bancaire'),
 (3,'D','Virement'),
 (4,'D','Prélèvement'),
 (5,'R','Remise de chèque'),
 (6,'R','Dépôt espèces'),
 (7,'R','Virement');

CREATE TABLE `Justificatif_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idMouvement` BIGINT UNSIGNED NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Ventilation_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idMouvement` BIGINT UNSIGNED NOT NULL,
  `idTypeVentilation` BIGINT UNSIGNED NOT NULL,
  `montant` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `TypeVentilation_R` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `typeMouvement` VARCHAR(1) NOT NULL,
  `typeVentilation` VARCHAR(255) NOT NULL,
  `actif` BOOLEAN NOT NULL DEFAULT true,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Solde_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idCompte` BIGINT UNSIGNED NOT NULL,
  `date` BIGINT UNSIGNED NOT NULL,
  `montant` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Budget_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idCompte` BIGINT UNSIGNED NOT NULL,
  `idPeriode` BIGINT UNSIGNED NOT NULL,
  `idTypeVentilation` BIGINT UNSIGNED NOT NULL,
  `montant` DECIMAL(9,2) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Periode_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `dateDebut` BIGINT UNSIGNED NOT NULL,
  `dateFin` BIGINT UNSIGNED NOT NULL,
  `nom` VARCHAR(45) NOT NULL,
  `bilanFige` BOOLEAN NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Compte_R` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `compte` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;

CREATE TABLE `Regle_G` (
  `rowid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `typeMouvementSrc` VARCHAR(1) NULL,
  `idTypeVentilationSrc` BIGINT UNSIGNED NULL,
  `frequence` VARCHAR(1) NULL,
  `jourDuMois` INTEGER NULL,
  `mois` INTEGER NULL,
  `anticipe` INTEGER NULL,
  `idTypeVentilationTarget` BIGINT UNSIGNED NOT NULL,
  `idCompte` BIGINT UNSIGNED NOT NULL,
  `typeMouvementTarget` VARCHAR(1) NOT NULL,
  `libelle` VARCHAR(2048) NOT NULL,
  `objet` VARCHAR(2048) NOT NULL,
  `idModePaiement` BIGINT UNSIGNED NOT NULL,
  `montant` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`rowid`)
)
ENGINE = MYISAM;
