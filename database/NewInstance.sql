USE CarnetCompte%AppName%DB;

DELETE FROM Budget_G;
DELETE FROM Justificatif_G;
DELETE FROM Mouvement_G;
DELETE FROM Solde_G;
DELETE FROM Utilisateur_G WHERE rowid<>881089670;
DELETE FROM UtilisateurGroupeUtilisateur_A WHERE idUtilisateur<>881089670;
DELETE FROM Ventilation_G;
DELETE FROM Periode_G WHERE bilanFige=true;
