USE CarnetCompte%AppName%DB;

INSERT INTO `Version_G` (`date`,`numVersion`) VALUES 
 (UNIX_TIMESTAMP(NOW()),'01.002.000');

ALTER TABLE `Mouvement_G` 
ADD COLUMN `previsionnel` BOOLEAN NOT NULL AFTER `regleAppliquee`,
ADD COLUMN `horsBilan` BOOLEAN NOT NULL AFTER `previsionnel`;
