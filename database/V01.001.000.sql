USE CarnetCompte%AppName%DB;

INSERT INTO `Version_G` (`date`,`numVersion`) VALUES 
 (UNIX_TIMESTAMP(NOW()),'01.001.000');

ALTER TABLE `Mouvement_G` CHANGE COLUMN `montant` `montant` DECIMAL(9,2) NOT NULL;
ALTER TABLE `Ventilation_G` CHANGE COLUMN `montant` `montant` DECIMAL(9,2) NOT NULL;
ALTER TABLE `Solde_G` CHANGE COLUMN `montant` `montant` DECIMAL(9,2) NOT NULL;
