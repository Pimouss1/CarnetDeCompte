USE CarnetCompte%AppName%DB;

INSERT INTO `Version_G` (`date`,`numVersion`) VALUES 
 (UNIX_TIMESTAMP(NOW()),'01.003.000');

ALTER TABLE `Mouvement_G`
ADD INDEX `Mouvement_I1` (`idCompte`,`dateFacturation`);

ALTER TABLE `Mouvement_G`
ADD UNIQUE `Mouvement_U1` (`numPiece`);

ALTER TABLE `Ventilation_G`
ADD INDEX `Ventilation_I1` (`idMouvement`);

ALTER TABLE `Justificatif_G`
ADD INDEX `Justificatif_I1` (`idMouvement`);

ALTER TABLE `Budget_G`
DROP COLUMN `idCompte`;
