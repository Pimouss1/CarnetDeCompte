USE CarnetCompteRodolpheDB;

INSERT INTO `Utilisateur_G` (`rowid`,`nom`,`prenom`,`mail`,`login`,`password`,`actif`) VALUES 
 (881089670,'BARBARIT','Rodolphe','pimouss79@gmail.com','rodolphe','d8e54af7a9ba9d995b09beb81a434b03',true);

INSERT INTO `UtilisateurGroupeUtilisateur_A` (`idUtilisateur`,`idGroupe`) VALUES 
 (881089670,1),
 (881089670,2),
 (881089670,3);

INSERT INTO `TypeVentilation_R` (`typeMouvement`,`typeVentilation`) VALUES
 ('R','Travail - Paie Capgemini'),
 ('D','Travail - Frais Capgemini'),
 ('R','Travail - Remb. frais Capgemini'),
 ('D','Travail-CE - Séjour'),
 ('D','Travail-CE - Billeterie'),
 ('R','Travail-CE - Subvention'),
 ('R','Médical - Remboursement CPAM'),
 ('R','Médical - Remboursement Mutuelle'),
 ('D','Médical - Médecin généraliste'),
 ('D','Médical - Spécialiste'),
 ('D','Médical - Kiné'),
 ('D','Médical - Dentiste'),
 ('R','Financier - Intérêts Livret bleu'),
 ('R','Financier - Intérêts CEL'),
 ('R','Financier - Mise de côté'),
 ('D','Financier - Mise de côté'),
 ('R','Financier - Autre'),
 ('D','Financier - Autre'),
 ('R','Cadeau - Noël'),
 ('R','Cadeau - Anniversaire'),
 ('R','Cadeau - Autre'),
 ('R','Appart - Loyer + prov. charges 21'),
 ('D','Appart - Charge courante trimestrielle'),
 ('D','Appart - Charge exceptionnelle'),
 ('D','Appart - Travaux par syndic'),
 ('D','Appart - Travaux perso'),
 ('D','Appart - Assurance'),
 ('D','Appart - Electricité / Gaz'),
 ('D','Appart - Electroménager'),
 ('D','Vie courante - Courses alimentaire'),
 ('D','Vie courante - Vêtements'),
 ('D','Vie courante - Shopping Internet'),
 ('D','Vie courante - Retrait espèces'),
 ('D','Commun - Virement mensuel'),
 ('R','Commun - Vente tickets resto'),
 ('D','Transport - Abonnement TAN'),
 ('D','Transport - Carburant voiture'),
 ('D','Transport - Entretien voiture'),
 ('D','Emprunt - Prêt immo 14'),
 ('D','Emprunt - Prêt immo employeur'),
 ('D','Emprunt - Prêt immo 15'),
 ('D','Impôts - Impôts sur le revenu'),
 ('D','Impôts - Taxe foncière'),
 ('D','Impôts - Taxe d''habitation'),
 ('D','Vacances - Sport d''hiver'),
 ('D','Vacances - Autre');

INSERT INTO `Periode_G` (`dateDebut`,`dateFin`,`nom`,`bilanFige`) VALUES 
 (1514761200000,1546297200000,'2018',false),
 (1546297200000,1577833200000,'2019',false);

INSERT INTO `Compte_R` (`rowid`,`compte`) VALUES
 (1,'01-Compte Courant'),
 (2,'16-Compte Immo Thonon'),
 (3,'10-Livret bleu'),
 (4,'12-CEL');

INSERT INTO `Solde_G` (`idCompte`,`date`,`montant`) VALUES 
 (1,1514761200000,1477.07),
 (2,1514761200000,2710.20),
 (3,1514761200000,22000),
 (4,1514761200000,5600);
