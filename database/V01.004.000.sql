USE CarnetCompte%AppName%DB;

INSERT INTO `Version_G` (`date`,`numVersion`) VALUES 
 (UNIX_TIMESTAMP(NOW()),'01.004.000');

ALTER TABLE `Regle_G` 
ADD COLUMN `horsBilan` BOOLEAN NOT NULL AFTER `montant`;
